package khameleon.core;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
public enum ApplicationMode {

  DEVELOPMENT("dev", 0),
  INTEGRATION("integration", 1),
  TEST("test", 2),
  RELEASE("release", 3),
  LIVE("www", 4),
  ALPHA("alpha", 5),
  BETA("beta", 6),
  LOCAL("local", 7);

  private final String domain;

  private final int order;

  private ApplicationMode(final String domain, final int order) {
    this.domain = domain;
    this.order = order;
  }

  public int getOrder() {
    return order;
  }

  @Nonnull
  public String getDomain() {
    return domain;
  }

  @Nonnull
  public static ApplicationMode getApplicationMode(@Nonnull final String domain) {
    checkNotNull(domain, "The domain must not be null");

    for (final ApplicationMode mode : values()) {
      if (mode.domain.equalsIgnoreCase(domain)
              || mode.name().equalsIgnoreCase(domain)) {
        return mode;
      }
    }

    throw new IllegalArgumentException("There is no application mode defined for domain: " + domain);
  }

}
