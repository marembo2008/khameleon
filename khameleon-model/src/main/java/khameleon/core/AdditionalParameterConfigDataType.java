package khameleon.core;

/**
 *
 * @author mochieng
 */
public enum AdditionalParameterConfigDataType {

    PARAMETER_REQUIRED,
    PARAMETER_PATTERN,
    PARAMETER_CALENDAR,
    PARAMETER_ENUM,
    PARAMETER_NAMESPACE_PARAM,
    PARAMETER_BOOLEAN,
    PARAMETER_REGEX,
    PARAMETER_CONFIG_PROVIDED_OPTION;
}
