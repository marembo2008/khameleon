package khameleon.core;

import java.util.Objects;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 14, 2015, 1:14:25 AM
 */
public final class NamespaceDataUnique {

    private final NamespaceData namespaceData;

    public NamespaceDataUnique(@Nonnull final NamespaceData namespaceData) {
        this.namespaceData = requireNonNull(namespaceData, "The namespaceData must not be null");
    }

    @Nonnull
    public NamespaceData getNamespaceData() {
        return namespaceData;
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespaceData.getDataType());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final NamespaceDataUnique other = (NamespaceDataUnique) obj;
        return Objects.equals(this.namespaceData.getDataType(), other.namespaceData.getDataType());
    }

}
