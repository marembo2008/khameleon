package khameleon.core;

import java.io.Serializable;
import java.util.Objects;
import lombok.Data;

/**
 *
 * @author marembo
 */
@Data
public class EnumValue implements Serializable, Comparable<EnumValue> {

  private static final long serialVersionUID = 3434839434931L;
  
  //from Enum.name()
  private String enumName;
  
  //from Enum.toString()
  private String displayName;

  public EnumValue() {
  }

  public EnumValue(EnumValue enumValue) {
    this.enumName = enumValue.enumName;
    this.displayName = enumValue.displayName;
  }

  public EnumValue(String enumName, String displayName) {
    this.enumName = enumName;
    this.displayName = displayName;
  }

  @Override
  public String toString() {
    return displayName;
  }

  @Override
  public int compareTo(EnumValue o) {
    return this.enumName.compareTo(o.enumName);
  }

}
