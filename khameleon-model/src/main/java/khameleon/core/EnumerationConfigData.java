package khameleon.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import anosym.common.StringTransient;
import jakarta.persistence.Lob;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Transient;

import static java.util.Objects.requireNonNull;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;

import lombok.Data;

/**
 *
 * @author marembo
 */
@Data
@MappedSuperclass
public abstract class EnumerationConfigData implements Serializable {

  private static final long serialVersionUID = 1343430490L;

  /**
   * Enumeration List to use for selection.
   */
  @Lob
  private ArrayList<EnumValue> enumList;

  @Transient
  @JsonIgnore
  @StringTransient
  private final Map<String, EnumValue> enumListCache = Maps.newHashMap();

  @PostLoad
  void initEnumList() {
    if (enumList != null) {
      enumList.stream()
              .forEach((ev) -> {
                enumListCache.put(ev.getEnumName(), ev);
              });
    }
  }

  @Nonnull
  public Optional<EnumValue> getEnumValue(@Nonnull final String name) {
    requireNonNull(name, "The name must not be null");

    if (enumListCache.isEmpty()) {
      initEnumList();
    }

    return Optional.fromNullable(enumListCache.get(name));
  }

  public void setEnumList(@Nonnull final List<EnumValue> enumList) {
    requireNonNull(enumList, "The enumList must not be null");

    this.enumList = newArrayList(transform(enumList, (input) -> new EnumValue(input)));
  }

  public void setEnumList(@Nonnull final Class<? extends Enum> enumClazz) {
    requireNonNull(enumClazz, "The enumClazz must not be null");

    final List<? extends Enum> enumConstants = ImmutableList.copyOf(enumClazz.getEnumConstants());
    this.enumList = newArrayList(transform(enumConstants, (input) -> new EnumValue(input.name(), input.toString())));
  }

  @Nonnull
  public List<EnumValue> getEnumList() {
    return enumList == null ? (enumList = new ArrayList<>()) : enumList;
  }

  public void addEnumValue(@Nonnull final EnumValue enumValue) {
    requireNonNull(enumValue, "The enumValue must not be null");

    getEnumList().add(enumValue);
  }

}
