package khameleon.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static jakarta.persistence.CascadeType.ALL;
import static khameleon.core.ConfigurationUUID.uuid;
import static org.hibernate.annotations.LazyCollectionOption.FALSE;

/**
 *
 * The namespace of the configuration is determined from the interface declaring it.
 *
 * @author marembo
 */
@Data
@Entity
@NamedQueries({
  @NamedQuery(name = "Configuration.findConfigurationByCanonicalNamespaceAndConfigName",
              query = "SELECT c FROM Configuration c WHERE c.configName = :configName AND c.namespace.canonicalName = :canonicalName"),
  @NamedQuery(name = "Configuration.findConfigurationByNamespace",
              query = "SELECT c FROM Configuration c WHERE c.namespace.namespaceId = :namespaceId ORDER BY c.configName ASC"),
  @NamedQuery(name = "Configuration.searchConfigurationByNamespace",
              query = "SELECT c FROM Configuration c WHERE c.configName LIKE :configName AND c.namespace.namespaceId = :namespaceId ORDER BY c.configName ASC"),
  @NamedQuery(name = "Configuration.findConfigurationsNotRegisteredForContext",
              query = "SELECT c FROM Configuration c WHERE c.namespace.namespaceId = :namespaceId AND NOT EXISTS (SELECT cv.configuration FROM ConfigurationValue cv WHERE cv.context.contextId = :contextId)")
})
@Table(indexes = {
  @Index(name = "configuration_configName", columnList = "configName"),
  @Index(name = "configuration_namespaceIdAndconfigName", columnList = "configName,namespace_namespaceId"),
  @Index(name = "configuration_configNameAndNamespaceId", columnList = "namespace_namespaceId,configName")
})
public class Configuration implements Serializable {

  private static final long serialVersionUID = 143948933L;

  @Id
  @JsonIgnore
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String configId = uuid();

  /**
   * The configuration namespace determines the interface that declares it.
   */
  @ManyToOne
  private Namespace namespace;

  /**
   * The configuration name, as declared in the interface (Generally, this is the property name derived from the method
   * declaration).
   *
   * Within a namespace, this name must be unique.
   */
  private String configName;

  /**
   * This is the fully qualified class name.
   *
   * Currently only primitive types and their wrappers, String.class, BigDecimal.class, Calendar.class and Enums are
   * expected.
   *
   */
  private String typeClass;

  /**
   * The default value for all configurations. Used to create new configuration values.
   */
  @Column(columnDefinition = "TEXT")
  private String defaultValue;

  /**
   * Additional parameters that may be supplied.
   *
   */
  @LazyCollection(FALSE)
  @OneToMany(cascade = ALL, orphanRemoval = true)
  private List<AdditionalParameter> additionalParameters;

  @LazyCollection(FALSE)
  @OneToMany(cascade = ALL, orphanRemoval = true)
  private List<ConfigurationData> configurationDatas;

  /**
   * This will be displayed as additional information next to the configuration value within the config front-end.
   */
  @LazyCollection(FALSE)
  @OneToMany(cascade = ALL, orphanRemoval = true)
  private List<AdditionalInformation> additionalInformations;

  protected Configuration() {
  }

  public Configuration(@Nonnull final Namespace namespace, @Nonnull final String configName,
                       @Nonnull final String typeClass) {
    this.namespace = checkNotNull(namespace, "The namespace must not be null");
    this.configName = checkNotNull(configName, "The configName must not be null");
    this.typeClass = checkNotNull(typeClass, "The typeClass must not be null");
  }

  public void addAdditionalParameter(@Nonnull final AdditionalParameter additionalParameter) {
    checkNotNull(additionalParameter, "The additionalParameter must not be null");

    if (additionalParameters == null) {
      additionalParameters = Lists.newArrayList();
    }

    final boolean notExists = additionalParameters
            .stream()
            .noneMatch((ap) -> Objects.equals(ap.getParameterKey(), additionalParameter.getParameterKey()));
    checkArgument(notExists, "Additional Parameter %s allready added", additionalParameter);

    additionalParameters.add(additionalParameter);
  }

  public void addAdditionalParameters(@Nonnull final List<AdditionalParameter> additionalParameters) {
    checkNotNull(additionalParameters, "The additionalParameters must not be null");

    for (final AdditionalParameter additionalParameter : additionalParameters) {
      addAdditionalParameter(additionalParameter);
    }
  }

  public void addAdditionalParameter(@Nonnull final String key, @Nullable String description) {
    checkNotNull(key, "The parameter key must not be null");

    addAdditionalParameter(new AdditionalParameter(key, description));
  }

  @Nonnull
  public ConfigurationData addConfigurationData(@Nonnull final ConfigurationDataOption dataOption,
                                                @Nullable final Object value) {
    checkNotNull(dataOption, "The dataOption must not be null");

    final ConfigurationData configurationData = new ConfigurationData(dataOption, Objects.toString(value));
    addConfigurationData(configurationData);

    return configurationData;
  }

  public void addConfigurationData(@Nonnull final ConfigurationData confData) {
    checkNotNull(confData, "The configuration data must not be null");

    final boolean notExists = getConfigurationDatas()
            .stream()
            .noneMatch((cData) -> cData.getDataOption() == confData.getDataOption());
    checkArgument(notExists, "Configuration: %s already added", confData);

    getConfigurationDatas().add(confData);
  }

  @Nonnull
  public Optional<ConfigurationData> getConfigurationData(@Nonnull final ConfigurationDataOption dataOption) {
    checkNotNull(dataOption, "The dataOption must not be null");

    return getConfigurationDatas()
            .stream()
            .filter((cd) -> cd.getDataOption() == dataOption)
            .findFirst()
            .map(Optional::of)
            .orElse(Optional.absent());
  }

  @Nonnull
  public Optional<ConfigurationData> removeConfigurationData(@Nonnull final ConfigurationDataOption dataOption) {
    checkNotNull(dataOption, "The dataoption must not be null");

    final Optional<ConfigurationData> dataOptional = getConfigurationData(dataOption);
    if (dataOptional.isPresent()) {
      configurationDatas.remove(dataOptional.get());
    }

    return dataOptional;
  }

  @Nonnull
  public Optional<AdditionalInformation> getAdditionalInformation(@Nonnull final String infoName) {
    checkNotNull(infoName, "The info name must not be null");

    return getAdditionalInformations()
            .stream()
            .filter((cd) -> Objects.equals(cd.getName(), infoName))
            .findFirst()
            .map(Optional::of)
            .orElse(Optional.absent());
  }

  @Nonnull
  public Optional<AdditionalParameter> getAdditionalParameter(@Nonnull final String paramKey) {
    checkNotNull(paramKey, "The parameterkey must not be null");

    return getAdditionalParameters()
            .stream()
            .filter((cd) -> Objects.equals(cd.getParameterKey(), paramKey))
            .findFirst()
            .map(Optional::of)
            .orElse(Optional.absent());
  }

  @Nonnull
  public List<AdditionalParameter> getAdditionalParameters() {
    return additionalParameters == null ? (additionalParameters = new ArrayList<>()) : additionalParameters;
  }

  @Nonnull
  public List<ConfigurationData> getConfigurationDatas() {
    return configurationDatas == null ? (configurationDatas = Lists.newArrayList()) : configurationDatas;
  }

  @Nonnull
  public List<AdditionalInformation> getAdditionalInformations() {
    return additionalInformations == null ? (additionalInformations = Lists.newArrayList()) : additionalInformations;
  }

  public void addAdditionalInformation(@Nonnull final String name, @Nonnull final String value) {
    checkNotNull(name, "The information name must be specified");
    checkNotNull(value, "The information value must be specified");

    getAdditionalInformations().add(new AdditionalInformation(name, value));
  }

  public void replaceAdditionalParameters(@Nonnull final List<AdditionalParameter> additionalParameters) {
    checkNotNull(additionalParameters, "The additionalParameters must not be null");

    getAdditionalParameters().clear();
    getAdditionalParameters().addAll(additionalParameters);
  }

  public void replaceAdditionalInformations(@Nonnull final List<AdditionalInformation> additionalInformations) {
    checkNotNull(additionalInformations, "The additionalInformations must not be null");

    getAdditionalInformations().clear();
    getAdditionalInformations().addAll(additionalInformations);
  }

  public void replaceConfigurationConfigData(@Nonnull final List<ConfigurationData> configurationDatas) {
    checkNotNull(configurationDatas, "The configurationDatas must not be null");

    getConfigurationDatas().clear();
    getConfigurationDatas().addAll(configurationDatas);
  }

  @Nonnull
  public Map<String, String> getAdditionalParametersMap() {
    final Map<String, String> additionalParemeterKeyMap = Maps.newHashMap();
    for (AdditionalParameter param : getAdditionalParameters()) {
      additionalParemeterKeyMap.put(param.getParameterKey(), null);
    }

    return additionalParemeterKeyMap;
  }

}
