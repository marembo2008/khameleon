package khameleon.core;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import anosym.common.StringTransient;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static khameleon.core.ConfigurationUUID.uuid;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * The namespace of the configuration.
 *
 * A namespace can have a parent, when it is declared as a return type of a parent instance.
 *
 * @author marembo
 */
@Entity
@NamedQueries({
  @NamedQuery(name = "Namespace.findNamespaceByCanonicalName",
              query = "SELECT n FROM Namespace n WHERE n.canonicalName = :canonicalName"),
  @NamedQuery(name = "Namespace.searchNamespaces",
              query = "SELECT n FROM Namespace n WHERE n.canonicalName LIKE :searchQuery ORDER BY n.canonicalName"),
  @NamedQuery(name = "Namespace.findChildNamespaces",
              query = "SELECT n FROM Namespace n WHERE n.parent.namespaceId = :parentNamespaceId  ORDER BY n.canonicalName"),
  @NamedQuery(name = "Namespace.findRootNamespaces",
              query = "SELECT n FROM Namespace n WHERE n.parent IS NULL ORDER BY n.canonicalName")
})
@Table(indexes = {
  @Index(name = "namespace_canonicalName", columnList = "canonicalName")
})
public class Namespace implements Serializable, Comparable<Namespace> {

  private static final long serialVersionUID = 1478343L;

  @Id
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String namespaceId = uuid();

  /**
   * The canonical name of a namespace is a full qualified name, preceded by its parents canonical name.
   *
   * e.g. if the parent namespace is anosym.khameleon.DefaultConfiguration and the current namespace is
   * anosym.khameleon.SimpleConfig, then the canonical name shall be
   * <code>anosym.khameleon.DefaultConfiguration:anosym.khameleon.SimpleConfig</code>
   */
  @Column(length = 1024)
  private String canonicalName;

  @NotNull(message = "The namespace simple name is required")
  private String simpleName;

  @ManyToOne
  private Namespace parent;

  /**
   * Name that can be displayed to the user.
   */
  private String displayName;

  @Column(columnDefinition = "TEXT")
  private String description;

  /**
   * Configuration data for this namespace.
   */
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
  private Set<NamespaceData> namespaceData;

  @Transient
  @JsonIgnore
  @StringTransient
  private Map<NamespaceDataType, NamespaceData> namespaceDataCache;

  protected Namespace() {
  }

  private Namespace(@Nonnull final Builder builder) {
    this.simpleName = checkNotNull(builder.simpleName, "The builder.simpleName must not be null");
    this.displayName = builder.displayeName;
    this.parent = builder.parent;
    this.description = builder.description;
  }

  @PostLoad
  void initializeNamespaceDataCache() {
    //There is a bug that has been adding namespacedata twice
    //So this method will always fails
    this.namespaceDataCache = getNamespaceData()
            .stream()
            .map(NamespaceDataUnique::new)
            .distinct()
            .map(NamespaceDataUnique::getNamespaceData)
            .collect(Collectors.toMap(NamespaceData::getDataType, Function.identity()));
  }

  @PrePersist
  void onSave() {
    this.canonicalName = getCanonicalName();
  }

  @Nonnull
  public String getNamespaceId() {
    return namespaceId;
  }

  @Nonnull
  public String getSimpleName() {
    return simpleName;
  }

  public void setSimpleName(@Nonnull final String simpleName) {
    this.simpleName = checkNotNull(simpleName, "The simpleName must not be null");
  }

  @Nonnull
  public String getCanonicalName() {
    checkState(simpleName != null, "The simplename should not be null");

    if (parent != null) {
      final String parentCanonicalName
              = checkNotNull(parent.getCanonicalName(), "The parent.getCanonicalName() must not be null");
      return format("%s:%s", parentCanonicalName, simpleName);
    }
    return simpleName;
  }

  public void setDisplayName(@Nonnull final String displayName) {
    this.displayName = checkNotNull(displayName, "The displayName must not be null");
  }

  @Nullable
  public String getDisplayName() {
    if (isNullOrEmpty(displayName)) {
      return simpleName;
    }
    return displayName;
  }

  public void setDescription(@Nonnull final String description) {
    this.description = checkNotNull(description, "The description must not be null");
  }

  @Nullable
  public String getDescription() {
    return description;
  }

  @Nullable
  public Namespace getParent() {
    return parent;
  }

  public void setParent(@Nonnull final Namespace parent) {
    this.parent = checkNotNull(parent, "The parent must not be null");
  }

  /**
   * Will call String.valueOf(value)
   *
   * @param na
   * @param value
   */
  public void addOrUpdateNamespaceData(@Nonnull final NamespaceData namespaceData) {
    requireNonNull(namespaceData, "The namespaceData must not be null");

    addOrUpdateNamespaceData(namespaceData.getDataType(), namespaceData.getDataValue());
  }

  /**
   * Will call String.valueOf(value)
   *
   * @param na
   * @param value
   */
  public void addOrUpdateNamespaceData(@Nonnull final NamespaceDataType namespaceDataType, @Nonnull final Object value) {
    checkNotNull(namespaceDataType, "The namespaceDataType must not be null");
    checkNotNull(value, "The value must not be null");

    final Optional<NamespaceData> registeredNamespaceData = getNamespaceData(namespaceDataType);
    if (registeredNamespaceData.isPresent()) {
      registeredNamespaceData.get().setDataValue(String.valueOf(value));
    } else {
      getNamespaceData().add(new NamespaceData(namespaceDataType, String.valueOf(value)));
    }

    initializeNamespaceDataCache();
  }

  @Nonnull
  public Optional<NamespaceData> getNamespaceData(@Nonnull final NamespaceDataType namespaceDataType) {
    checkNotNull(namespaceDataType, "The namespaceDataType must not be null");

    if (namespaceDataCache == null || namespaceDataCache.isEmpty()) {
      initializeNamespaceDataCache();
    }

    return Optional.fromNullable(namespaceDataCache.get(namespaceDataType));
  }

  public void removeNamespaceData(@Nonnull final NamespaceDataType namespaceDataType) {
    checkNotNull(namespaceDataType, "The namespaceDataType must not be null");

    final List<NamespaceData> newNamespaceData = ImmutableList
            .copyOf(getNamespaceData())
            .stream()
            .filter((namespaceData_) -> namespaceData_.getDataType() != namespaceDataType)
            .collect(Collectors.toList());

    getNamespaceData().clear();
    getNamespaceData().addAll(newNamespaceData);

    //reinitializee cache if need be.
    initializeNamespaceDataCache();
  }

  @Nonnull
  public Set<NamespaceData> getNamespaceData() {
    return namespaceData == null ? (namespaceData = Sets.newHashSet()) : namespaceData;
  }

  public void setNamespaceData(@Nonnull final Set<NamespaceData> namespaceData) {
    requireNonNull(namespaceData, "The namespaceData must not be null");

    getNamespaceData().clear();
    getNamespaceData().addAll(namespaceData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.canonicalName, this.parent);
  }

  @Override
  public boolean equals(@Nullable final Object obj) {
    if (obj == null || !(obj instanceof Namespace)) {
      return false;
    }
    final Namespace other = (Namespace) obj;
    return Objects.equals(this.canonicalName, other.canonicalName)
            && Objects.equals(this.parent, other.parent);
  }

  @Override
  public String toString() {
    return getCanonicalName();
  }

  @Override
  public int compareTo(@Nonnull final Namespace namespace) {
    checkNotNull(namespace, "The namespace must not be null");
    final String thisCanonicalName = checkNotNull(getCanonicalName(), "The this.getCanonicalName() must not be null");
    final String otherCanonicalName = checkNotNull(namespace.getCanonicalName(),
                                                   "The namespace.getCanonicalName() must not be null");
    return thisCanonicalName.compareTo(otherCanonicalName);
  }

  @Nonnull
  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private String simpleName;

    private String displayeName;

    private Namespace parent;

    private String description;

    private Builder() {
    }

    @Nonnull
    public Builder withSimpleName(@Nonnull final String simpleName) {
      this.simpleName = checkNotNull(simpleName, "The simpleName must not be null");
      return this;
    }

    @Nonnull
    public Builder withDisplayName(@Nonnull final String displayName) {
      this.displayeName = checkNotNull(displayName, "The displayName must not be null");
      return this;
    }

    @Nonnull
    public Builder withParent(@Nonnull final Namespace parentNamespace) {
      this.parent = checkNotNull(parentNamespace, "The parentNamespace must not be null");
      return this;
    }

    @Nonnull
    public Builder withDescription(@Nonnull final String description) {
      this.description = checkNotNull(description, "The description must not be null");
      return this;
    }

    @Nonnull
    public Namespace build() {
      return new Namespace(this);
    }

  }

}
