package khameleon.core;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationUUID.uuid;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author mochieng
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@NamedQuery(name = "AdditionalParameterConfigData.findAdditionalParameterConfigDataByDataType",
            query = "SELECT cd FROM AdditionalParameterConfigData cd WHERE cd.configDataType = :configDataType")
public class AdditionalParameterConfigData extends EnumerationConfigData implements Serializable {

  private static final long serialVersionUID = 338479438935387l;

  @Id
  @JsonIgnore
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String uuid = uuid();

  private AdditionalParameterConfigDataType configDataType;

  private String configData;

  private boolean booleanData;

  public AdditionalParameterConfigData() {
  }

  public AdditionalParameterConfigData(@Nonnull final AdditionalParameterConfigDataType configDataType,
                                       @Nullable final Object configData) {
    checkNotNull(configDataType, "The AdditionalParameterConfigDataType (configDataType) must be specified");
    this.configDataType = configDataType;
    this.configData = String.valueOf(configData);
    this.booleanData = configData != null
            && (configData.getClass() == boolean.class || configData.getClass() == Boolean.class);
  }

  public void updateAdditionalParameterConfigData(@Nonnull final AdditionalParameterConfigData configData) {
    checkNotNull(configData, "The AdditionalParameterConfigData (configData) must not be null");
    this.configData = configData.configData;
    this.booleanData = configData.booleanData;
  }

  public boolean getBooleanValue() {
    return Boolean.valueOf(configData);
  }

  public int getIntegerValue() {
    return Integer.valueOf(configData);
  }

}
