package khameleon.core;

/**
 *
 * @author marembo
 */
public enum NamespaceDataType {

    NAMESPACE_APPLICATION_CONTEXT,
    NAMESPACE_CONFIG_PROJECT,
    NAMESPACE_CONFIG_DELETE,
    NAMESPACE_CONFIG_RENAME,
    NAMESPACE_INSTANCE_ID;

}
