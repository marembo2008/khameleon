package khameleon.core;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.MoreObjects;

import anosym.common.Amount;
import anosym.common.Country;
import anosym.common.Currency;
import anosym.common.Language;
import anosym.common.TLD;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.ToString;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

import static com.google.common.base.Preconditions.checkState;

/**
 * if any of the values for the application context is missing for any configuration, it will be assumed to have been
 * defined for all contexts.
 *
 * The following properties of application contexts are ordered. if high order values are not defined, then they are
 * assumed to be all defined.
 *
 * e.g. if application context defines all the following properties:
 *
 * - application mode
 *
 * - top level domain
 *
 * - language
 *
 * - country.
 *
 * Calling the current applications, {@link ApplicationContext#getParentContextId() } will return contextId considering
 * country as a none-specified entity.
 *
 * In cases where the low-order properties are not defined, but high order entities are defined, the same scenario
 * applies.
 *
 * Considering for example that application mode, tld and country not defined, but language is defined, the parent
 * context will be the default context, similarly to when only one of the properties is defined.
 *
 * @author marembo
 */
@Entity
@ToString
@NamedQueries({
  @NamedQuery(name = "ApplicationContext.findApplicationContextByMode",
              query = "SELECT ac FROM ApplicationContext ac WHERE ac.applicationMode = :applicationMode ORDER BY ac.contextId ASC"),
  @NamedQuery(name = "ApplicationContext.findRootApplicationContextByMode",
              query = "SELECT ac FROM ApplicationContext ac WHERE ac.applicationMode = :applicationMode AND ac.countryCode IS NULL AND ac.languageCode IS NULL AND ac.topLevelDomain IS NULL ORDER BY ac.contextId ASC"),
  //To avoid returning the root application context which will always set itself as parent!
  @NamedQuery(name = "ApplicationContext.findChildrenApplicationContext",
              query = "SELECT ap FROM ApplicationContext ap WHERE ap.parentContextId = :contextId AND ap.parentContextId != ap.contextId")
})
@Table(indexes = {
  @Index(name = "application_context_parentContextId", columnList = "parentContextId"),
  @Index(name = "application_context_applicationMode", columnList = "applicationMode"),
  @Index(name = "application_context_topLevelDomain", columnList = "topLevelDomain"),
  @Index(name = "application_context_languageCode", columnList = "languageCode"),
  @Index(name = "application_context_countryCode", columnList = "countryCode")
})
public class ApplicationContext implements Serializable, Comparable<ApplicationContext> {

  private static final long serialVersionUID = 134384839834L;

  /**
   * This default context will always be created in the configuration database.
   *
   * A default context is simply an instance with none of its properties set.
   */
  public static final ApplicationContext DEFAULT_APPLICATION_CONTEXT = new ApplicationContext();

  /**
   * Application mode context.
   */
  public static final ApplicationContext DEVELOPMENT_APPLICATION_CONTEXT = new ApplicationContext(
          ApplicationMode.DEVELOPMENT, null, null, null);

  /**
   * The unique context id, determined from the state of the current context.
   *
   * The generation of the contextid ensures that a context knows its parent.
   *
   * When a configuration is requested based a context, the contextId is evaluated and used to lookup the configuration.
   */
  @Id
  private int contextId;

  /**
   * Each applicationcontext evaluates its own parent id.
   */
  private int parentContextId;

  /**
   * Application mode, e.g. dev.mydomain.com within a web application, resolves to {@link ApplicationMode#DEVELOPMENT}
   */
  private ApplicationMode applicationMode;

  /**
   * The application domains. If running within a web application, this can be determined from the host e.g. mydomain.de
   * and mydomain,co.uk returns a topLevelDomain of de and co.uk respectively.
   *
   * Generally, this is the tld of the domain. In most cases (save for .com, .org and the like) it can be used to
   * determine the language code and country code of the current application context.
   */
  private TLD topLevelDomain;

  /**
   * The two letter iso code for the languageCode.
   */
  private Language languageCode;

  /**
   * The two letter iso code for the countrycode.
   */
  private Country countryCode;

  @PrePersist
  void onSave() {
    this.contextId = generateContextId();
    updateParentContextId();
  }

  public void updateParentContextId() {
    this.parentContextId = getParentContextId();
  }

  public ApplicationContext() {
    this(null, null, null, null);
  }

  public ApplicationContext(ApplicationContext appContext) {
    this(appContext.applicationMode, appContext.topLevelDomain, appContext.languageCode, appContext.countryCode);
  }

  public ApplicationContext(ApplicationMode applicationMode, TLD topLevelDomain, Language languageCode,
                            Country countryCode) {
    this.applicationMode = applicationMode;
    this.topLevelDomain = topLevelDomain;
    this.languageCode = languageCode;
    this.countryCode = countryCode;
  }

  public ApplicationMode getApplicationMode() {
    return applicationMode;
  }

  public void setApplicationMode(ApplicationMode applicationMode) {
    this.applicationMode = applicationMode;
  }

  public ApplicationContext withApplicationMode(ApplicationMode applicationMode) {
    this.applicationMode = applicationMode;
    return this;
  }

  public TLD getTopLevelDomain() {
    return topLevelDomain;
  }

  public void setTopLevelDomain(TLD topLevelDomain) {
    this.topLevelDomain = topLevelDomain;
  }

  public ApplicationContext withTopLevelDomain(TLD topLevelDomain) {
    this.topLevelDomain = topLevelDomain;
    return this;
  }

  public Language getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(Language languageCode) {
    this.languageCode = languageCode;
  }

  public ApplicationContext withLanguageCode(Language languageCode) {
    this.languageCode = languageCode;
    return this;
  }

  public Country getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(Country countryCode) {
    this.countryCode = countryCode;
  }

  public ApplicationContext withCountryCode(Country countryCode) {
    this.countryCode = countryCode;
    return this;
  }

  /**
   * This method returns the current context id based on its current state.
   *
   * If state changes, the contextid changes.
   *
   * @return
   */
  public int getContextId() {
    return generateContextId();
  }

  private int generateContextId() {
    int hash = 3;
    hash = 79 * hash + generateHash(this.applicationMode);
    hash = 79 * hash + generateHash(this.topLevelDomain);
    hash = 79 * hash + generateHash(this.languageCode);
    hash = 79 * hash + generateHash(this.countryCode);
    return hash;
  }

  private int generateHash(Object obj) {
    //simply return the integer for the characters.
    int hash = 0;
    if (obj != null) {
      for (char c : obj.toString().toCharArray()) {
        hash += c;
      }
    }
    return hash;
  }

  //Special handling as the name of the language changed semantics.
  private int generateHash(@Nullable final Language language) {
    //simply return the integer for the characters.
    int hash = 0;
    if (language != null) {
      final String nameForHash = language.name() + " (" + language.getIsoCode() + ")";
      for (char c : nameForHash.toCharArray()) {
        hash += c;
      }
    }
    return hash;
  }

  /**
   * If this is the default context, it returns its own context id.
   *
   * @return
   */
  public int getParentContextId() {
    return getParentApplicationContext().generateContextId();
  }

  private Object[] orderedProperties() {
    // Changing the order of this may result to applications breaking up their configurations.
    // Lookup into the heavens before you dare change this order!

    return new Object[]{applicationMode, topLevelDomain, languageCode, countryCode};
  }

  /**
   * Based on the current status of this context, it can create its parent.
   *
   * This method creates the parent on the fly, and it has no reference to existing parent besides its own properties.
   *
   * @return
   */
  @Nonnull
  public ApplicationContext getParentApplicationContext() {
    final Object[] properties = orderedProperties();
    for (int i = properties.length - 1; i >= 0; i--) {
      if (properties[i] != null) {
        properties[i] = null;
        break;
      }
    }
    return new ApplicationContext((ApplicationMode) properties[0],
                                  (TLD) properties[1],
                                  (Language) properties[2],
                                  (Country) properties[3]);
  }

  @Nonnull
  public ApplicationContext getApplicationModeApplicationContext() {
    checkState(!isDefaultContext(), "Cannot get applicationmode applicationcontext for the default context");

    return new ApplicationContext(applicationMode, null, null, null);
  }

  @Nonnull
  public ApplicationContext copy() {
    return new ApplicationContext(applicationMode, topLevelDomain, languageCode, countryCode);
  }

  /**
   * Returns the locale for the current application context.
   *
   * Returns the default locale, if the following application context does not define language or country.
   *
   * @return
   */
  public Locale getLocale() {
    if (this.languageCode != null) {
      if (this.countryCode != null) {
        return new Locale(this.languageCode.getIsoCode(), this.countryCode.getIsoCode());
      }
      return new Locale(this.languageCode.getIsoCode());
    }
    return Locale.getDefault();
  }

  @Nonnull
  public Amount getZero() {
    final Locale locale = getLocale();
    final String countryIsoCode = locale.getCountry();
    final Country country = Country.fromIsoCode(countryIsoCode);
    final Currency currency = Currency.countryCurrencies(country)
            .stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException("Could not create currency from locale"));
    return new Amount(currency, 0);
  }

  @Nonnull
  public Amount getZero(@Nonnull final Country country) {
    requireNonNull(country, "The country must not be null");

    final Currency currency = Currency.countryCurrencies(country)
            .stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException("Could not create currency from locale"));
    return new Amount(currency, 0);
  }

  public String getDisplayName() {
    /**
     * For default context, we have no name generated fromm its properties.
     */
    if (getContextId() == DEFAULT_APPLICATION_CONTEXT.getContextId()) {
      return "DEFAULT CONTEXT";
    }

    String name = applicationMode.name();
    if (topLevelDomain != null) {
      name += " >> " + topLevelDomain;
    }
    if (languageCode != null) {
      name += " >> " + languageCode;
    }
    if (countryCode != null) {
      name += " >> " + countryCode;
    }

    return name;
  }

  public boolean isDefaultContext() {
    return getContextId() == DEFAULT_APPLICATION_CONTEXT.getContextId();
  }

  public boolean isPossibleParent(@Nonnull final ApplicationContext applicationContext) {
    if (applicationContext.isDefaultContext()) {
      return true;
    }
    if (applicationContext.getContextId() == getContextId()) {
      return true;
    }
    if (isDefaultContext()) {
      return false;
    }
    final ApplicationContext parentContext = getParentApplicationContext();
    return parentContext.isPossibleParent(applicationContext);
  }

  @Override
  public int compareTo(ApplicationContext o) {
    return Integer.valueOf(getContextId()).compareTo(o.getContextId());
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 17 * hash + this.getContextId();
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ApplicationContext other = (ApplicationContext) obj;
    return getContextId() == other.getContextId();
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
            .add("contextId", getContextId())
            .add("applicationMode", applicationMode)
            .add("topLevelDomain", topLevelDomain)
            .add("languageCode", languageCode)
            .add("countryCode", countryCode)
            .toString();
  }

  public String toLocalizedString() {
    return format("mode=%s,tld=%s,lang=%s,country=%s,id=%s",
                  applicationMode, topLevelDomain, languageCode, countryCode, getContextId());
  }

}
