package khameleon.core;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import lombok.Data;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationUUID.uuid;

/**
 * With the exception of Calendar, BigDecimal, and Enums, these are meant to be simple types (primitives).
 *
 * @author marembo
 */
@Data
@Entity
@NamedQueries({
  @NamedQuery(name = "AdditionalParameter.findAdditionalParameterByKey",
              query = "SELECT ap FROM AdditionalParameter ap WHERE ap.parameterKey = :parameterKey"),
  @NamedQuery(name = "AdditionalParameter.findDynamicAdditionalParameters",
              query = "SELECT ap FROM AdditionalParameter ap WHERE ap.dynamicParameter = :dynamicParameter")
})
public class AdditionalParameter implements Serializable, Cloneable {

  private static final long serialVersionUID = 1343843940330L;

  @Id
  @JsonIgnore
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String paramId = uuid();

  private String parameterKey;

  /**
   * The default value used for the additional parameters.
   */
  @Column(columnDefinition = "TEXT")
  private String defaultValue;

  private String parameterDescription;

  /**
   * Additional configuration data for this parameter.
   */
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private List<AdditionalParameterConfigData> configData;

  /**
   * If added through the config-interface.
   */
  private boolean dynamicParameter;

  protected AdditionalParameter() {
  }

  public AdditionalParameter(@Nonnull final String key) {
    this(key, null);
  }

  public AdditionalParameter(@Nonnull final String key, @Nullable String description) {
    this.parameterKey = checkNotNull(key, "The parameter key must not be null");
    this.parameterDescription = description;
  }

  public AdditionalParameter(@Nonnull final String key, @Nullable String description, @Nullable String defaultValue) {
    this.parameterKey = checkNotNull(key, "The parameter key must not be null");
    this.parameterDescription = description;
    this.defaultValue = defaultValue;
  }

  @Nonnull
  public List<AdditionalParameterConfigData> getConfigData() {
    return configData == null ? (configData = Lists.newArrayList()) : configData;
  }

  public void addConfigData(@Nonnull final AdditionalParameterConfigData configData) {
    checkNotNull(configData, "The configData must not be null");

    final boolean notExists = getConfigData()
            .stream()
            .noneMatch((cd) -> cd.getConfigDataType() == configData.getConfigDataType());
    checkArgument(notExists, "The AddtionalParameterConfigData %s is already added", configData);

    getConfigData().add(configData);
  }

  public void addOrUpdateConfigData(@Nonnull final AdditionalParameterConfigData configData) {
    checkNotNull(configData, "The configData must not be null");

    final Optional<AdditionalParameterConfigData> configDataOptional = getConfigData(configData.getConfigDataType());
    if (!configDataOptional.isPresent()) {
      addConfigData(configData);
    } else {
      final AdditionalParameterConfigData registeredConfigData = configDataOptional.get();
      registeredConfigData.setEnumList(configData.getEnumList());
      registeredConfigData.setBooleanData(configData.isBooleanData());
    }
  }

  /**
   * Will call String.valueOf(value)
   *
   * @param configDataType
   * @param value
   */
  public void addConfigData(@Nonnull final AdditionalParameterConfigDataType configDataType,
                            @Nonnull final Object value) {
    checkNotNull(configDataType, "The configDataType must not be null");
    checkNotNull(value, "The value must not be null");

    addConfigData(new AdditionalParameterConfigData(configDataType, String.valueOf(value)));
  }

  @Nonnull
  public Optional<AdditionalParameterConfigData> getConfigData(
          @Nonnull final AdditionalParameterConfigDataType configDataType) {
    checkNotNull(configDataType, "The configDataType must not be null");

    return getConfigData()
            .stream()
            .filter((cd) -> cd.getConfigDataType() == configDataType)
            .findFirst()
            .map(Optional::of)
            .orElse(Optional.absent());
  }

  public void replaceAdditionalParameterConfigData(@Nonnull final List<AdditionalParameterConfigData> configData) {
    checkNotNull(configData, "The configData must not be null");

    getConfigData().clear();
    getConfigData().addAll(configData);
  }

}
