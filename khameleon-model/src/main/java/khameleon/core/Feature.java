package khameleon.core;

/**
 *
 * @author marembo
 */
public enum Feature {

    APPLICATION_MODE("Application mode. Default has no effect. Always enabled", 0),
    LANGUAGE("Language.", 1),
    COUNTRY("Country", 2),
    TLD("Top Level Domains. Can be used to differentiate languages for the same country", 3);

    private final String description;
    private final int index;

    private Feature(final String description, final int index) {
        this.description = description;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
