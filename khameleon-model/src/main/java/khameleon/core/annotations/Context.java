package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Internal use only. Designates the method as returning the current configuration context.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Context {
}
