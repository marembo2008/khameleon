package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The regex to enforce on the config value. The annotated must be of type String.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target({METHOD, PARAMETER})
public @interface ConfigRegex {

    /**
     * The regex
     */
    String value();

}
