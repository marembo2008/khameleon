package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 *
 * @author marembo
 */
@Documented
@Target({METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Default {

    /**
     * The default value for the configuration.
     *
     * @return
     */
    String value();

    /**
     * Return this value if the configuration returns no results.
     *
     * Default is true.
     *
     * @return
     */
    boolean defaultWhenNullOrEmpty() default true;

}
