package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * marks the config interface or configuration for deletion. the namespace and/or configurations including all
 * configuration values will be deleted.
 *
 * @author marembo
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target({METHOD, TYPE})
public @interface ConfigDelete {
}
