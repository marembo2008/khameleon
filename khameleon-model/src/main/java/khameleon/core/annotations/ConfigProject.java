package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * When found on a config value, it indicates that the configuration requires additional information as relates to the
 * type of project it is valid.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target({TYPE, METHOD, PACKAGE})
@IndexAnnotated
public @interface ConfigProject {

    String PROJECT_KEY = "Project";

    /**
     * The projects for which the particular namespace applies. The project names must be the same as the ones defined
     * in the application contexts.
     *
     * @return
     */
    String[] value();

}
