package khameleon.core.annotations.localcache;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Implicitly implies that no permanent file cache should be done
 *
 * @author marembo
 */
@Inherited
@Documented
@Target(METHOD)
@Retention(RUNTIME)
public @interface InMemoryCache {

    /**
     * How long to maintain the in-memory cache
     */
    long period() default 0;

    /**
     * The units for the period
     */
    TimeUnit unit() default TimeUnit.SECONDS;

}
