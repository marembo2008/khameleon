/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Used to provide parameters to a parametrized string value.
 *
 * If this annotation appears on a method parameter, only index of the config option is considered.
 *
 * A method parameter cannot both be a config-option and a config-param.
 *
 * @author marembo
 */
@Documented
@Target({METHOD, PARAMETER})
@Retention(RUNTIME)
public @interface ConfigOption {

    /**
     * The configuration value to be used as template parameter value.
     */
    @Retention(RUNTIME)
    @interface ConfigName {

        /**
         * The value to use for the parameter replacement.
         *
         * @return
         */
        String value();

        /**
         * If not specified, assumes current namespace.
         *
         * Current restriction is that, the namespace must be a fully qualified configuration name.
         *
         * If the namespace is not a root namespace, it must be specified as: parentNamespace:thisNamespace
         *
         * @return
         */
        String namespace() default "";
    }

    /**
     * The index within the string this option will replace.
     *
     * It is imperative that no two config options, whether declared as parameter or method annotation, have the same
     * index.
     *
     * @return
     */
    int[] index();

    /**
     * Default value to use for replacement.
     *
     * @return
     */
    String value() default "";

    /**
     * if value is not specified, the value to use to retrieve the value.
     *
     * The value must be part of the current configuration class.
     *
     * The configuration must not accept any parameter. It is allowed to define configoptions at the method level.
     *
     * The value here is an array in order to give a default value. However, the config service will only use the value
     * specified at index 0.
     *
     * @return
     */
    ConfigName[] configName() default {};

    /**
     * Parameters to be used in when performing this replacement.
     *
     * This can be date format, etc. Depending on how the configuration is replaced and the semantics attached to it.
     *
     * @return
     */
    String parameter() default "";

    /**
     * Instead of allowing the MessageFormatter to do locale specific formatting, simply do an in replacement for the
     * template values.
     *
     * @return
     */
    boolean templateReplace() default false;
}
