package khameleon.core.annotations.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * @author marembo
 */
@Inherited
@Documented
@Retention(RUNTIME)
@Target(TYPE)
@IndexAnnotated
public @interface ConfigValidator {
}
