package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Used to invoke an implementation dependent default configuration values.
 *
 * These default values, saved in a particular directory, can be uploaded to the config-service frontend.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target(TYPE)
@Documented
@IndexAnnotated
public @interface ConfigUpload {
}
