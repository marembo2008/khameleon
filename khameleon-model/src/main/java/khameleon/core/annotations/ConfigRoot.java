package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The roots of the configurations.
 *
 * The interface must still be annotated with {@link Config} annotation.
 *
 * @author marembo
 */
@Target(TYPE)
@Retention(RUNTIME)
@IndexAnnotated
public @interface ConfigRoot {
}
