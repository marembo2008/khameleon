package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CalendarBounded {

    String CALENDAR_BOUNDED_FROM_PARAM_KEY = "fromDate";

    String CALENDAR_BOUNDED_FROM_PARAM_DESC = "The Beginning Date for the calendar bounded configuration";

    String CALENDAR_BOUNDED_TO_PARAM_KEY = "toDate";

    String CALENDAR_BOUNDED_TO_PARAM_DESC = "The End Date for the calendar bounded configuration";

}
