package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(METHOD)
public @interface ConfigGlobal {

    /**
     * The state for which this configuration is assumed to enable the
     *
     * @ConfigManaged configurations.
     *
     * i.e. If the annotated configuration is set to this state, then the
     * @ConfigManaged configurations shall return their default configured values, if any.
     *
     */
    boolean enabledState() default false;

    /**
     * The unique value for the global configuration which will be used to uniquely group configurations.
     */
    String value() default "";

}
