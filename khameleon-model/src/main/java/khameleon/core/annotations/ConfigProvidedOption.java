package khameleon.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * That the configuration provides an enumerated string option for the parameters or config-values.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 25, 2015, 4:22:43 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.PARAMETER})
public @interface ConfigProvidedOption {

    ConfigOptionType value();

    static enum ConfigOptionType {

        PROJECT_ID,
        APPLICATION_MODES;

    }

}
