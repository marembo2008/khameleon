package khameleon.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 11:20:58 PM
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
public @interface ConfigUpdateListener {

  /**
   * The namespace supported by this listener.
   */
  String namespace();

}
