package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Indicates that the configuration is deprecated. A message will be added to config frontend to indicate to users of
 * such.
 * <p>
 * @author marembo
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface ConfigDeprecated {

    /**
     * If true, will be removed at the specified date from the config front-end.
     * <p>
     * @return
     */
    boolean autoRemove() default false;

    /**
     * The date to remove the configuration in the following format: yyyy-MM-dd
     * <p>
     * @return
     */
    String when() default "";

    /**
     * Reason for deprecation.
     * <p>
     * @return
     */
    String message();

}
