package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

/**
 *
 * @author marembo
 */
@Inherited
@Documented
@Target({FIELD, METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Config {

    /**
     * The namespace of the configuration set. If not specified, it is derived from the fully qualified class name.
     *
     * <pre>If you intend to use {@link ConfigOption#configName() } with namespace:</pre>
     *
     * <pre>1. Do not specify this value</pre>
     * <pre>2. Do not use inner classes</pre>
     * <pre>3. The namespace would be assigned from the {@link Class#getCanonicalName()}</pre>
     *
     * @return
     */
    String namespace() default "";

    /**
     * Names for this config namespace, for which the user can add a meaningful display name.
     *
     * @return
     */
    String name() default "";

    /**
     * Explanation about the current namespace. You can use html for better display on the frontend.
     *
     * @return
     */
    String description() default "";

}
