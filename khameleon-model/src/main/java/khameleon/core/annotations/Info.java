package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Info {

    /**
     * The name of this information.
     *
     * @return
     */
    String name() default "";

    /**
     * The specified value of the information.
     *
     * @return
     */
    String value();

}
