package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The configuration value requires that the specified configuration, within the current context has the specified
 * value.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target({METHOD, TYPE, PACKAGE})
public @interface Requires {

    /**
     * The value of the configuration for which this configuration update will be successful.
     *
     * @return
     */
    String value();

    /**
     * The namespace of the configuration. Defaults to current namespace.
     *
     * @return
     */
    String namespace() default "";

    /**
     * The configName to check for the specified value.
     *
     * @return
     */
    String configName();

}
