package khameleon.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 20, 2015, 12:36:29 PM
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredThrows {

    /**
     * Exception must have a single parameter constructor that accepts a string message.
     */
    Class<? extends RuntimeException> value();

}
