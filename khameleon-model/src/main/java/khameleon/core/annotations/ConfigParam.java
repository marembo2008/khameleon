package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface ConfigParam {

    /**
     * The name of the parameter.
     *
     * A unique name for the param. It must follow java naming convention.
     *
     * @return
     */
    String name();

    /**
     * When displayed on the config front-end, this information will give the user a more explanation of how the
     * parameter should be used.
     *
     * @return
     */
    String description() default "";

    /**
     * The default value for the parameter.
     *
     * Currently, we do not so much of consider the type of the value, but future versions may.
     *
     * @return
     */
    String value() default "";

    /**
     * If this parameter is mandatory before user can save the configuration.
     *
     * @return
     */
    boolean required() default false;

}
