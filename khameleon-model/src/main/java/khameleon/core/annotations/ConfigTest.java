package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * That the configuration is a config test and should not be sent to the service.
 *
 * @author marembo
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target({METHOD, TYPE})
public @interface ConfigTest {
}
