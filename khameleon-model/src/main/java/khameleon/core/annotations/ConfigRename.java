package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks a configuration or namespace, that it has been renamed, and specifies the old name.
 *
 * @author marembo
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target({METHOD, TYPE})
public @interface ConfigRename {

    /*
     * The old name for the configfuration/namespace! For a namespace, this needs to be the fully qualified simple name
     * (Excluding the parent canonicals)
     */
    String value();

}
