package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Specifies that a string field is to be treated as rich text.
 *
 * @author marembo
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface RichText {
}
