package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * That the selection of values comes from registered namespace data.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target({PARAMETER, METHOD})
public @interface NamespaceParam {
}
