package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * If a config is marked as globally managed, then a valid value will only be returned if a global configuration is
 * enabled.
 *
 * The global configuration must be annotated @ConfigGlobal, and must be a boolean.
 *
 * This annotation must annotate only boolean configs.
 *
 * This annotation must be defined together with @Default, as when the
 *
 * @ConfigGlobal configuration is disabled, the default value shall be returned.
 *
 * @author marembo
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target(METHOD)
public @interface ConfigManaged {

    /**
     * The config global id, for which this managed config is assigned.
     */
    String value() default "";

}
