package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import khameleon.core.DefaultConfigurator;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The default is only calculated during configuration call.
 *
 * @author marembo
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target(METHOD)
public @interface DynamicDefault {

    Class<? extends DefaultConfigurator> value();

}
