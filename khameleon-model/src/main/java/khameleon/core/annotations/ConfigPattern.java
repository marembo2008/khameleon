package khameleon.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * A regular expression to apply to the input and configuration value, for which both return true.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface ConfigPattern {

    /**
     * The name of the additional parameter.
     *
     * @return
     */
    String name();

    /**
     * The pattern to apply.
     *
     * @return
     */
    String value();

    /**
     * Description of the pattern.
     *
     * @return
     */
    String description() default "";

}
