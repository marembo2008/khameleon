package khameleon.core.annotations.processor;

import static khameleon.core.annotations.processor.ConfigProcessor.ConfigProcessorOrder.ORDER_ANY_ORDER;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.atteo.classindex.IndexAnnotated;

/**
 *
 * @author mochieng
 */
@Inherited
@Documented
@IndexAnnotated
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigProcessor {

    enum ConfigProcessorOrder {

        //Maintain the order of declarations of these enum values.
        ORDER_DEFAULT_VALUE,
        ORDER_CONFIG_PARAM,
        ORDER_CONFIG_REGEX,
        ORDER_ANY_ORDER,
        ORDER_NAMESPACE_PARAM,
        ORDER_CONFIG_GLOBAL,
        ORDER_CONFIG_MANAGED,
        ORDER_CONFIG_PROVIDED_OPTION;
    }

    /**
     * The order of processing the processors. Lower orders are processed first.
     *
     * @return
     */
    ConfigProcessorOrder order() default ORDER_ANY_ORDER;
}
