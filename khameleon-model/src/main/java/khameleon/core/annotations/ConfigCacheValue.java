package khameleon.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The if local cache should cache the configurationvalue, if the configuration actual value is null or empty.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 19, 2015, 6:44:44 PM
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigCacheValue {

    CacheValutOption value();

    enum CacheValutOption {

        //By passes the cache value only if the value is null.
        IGNORE_WHEN_NULL,
        //By passes the cached value if the value is null or empty (empty for array too)
        IGNORE_WHEN_EMPTY;

    }

}
