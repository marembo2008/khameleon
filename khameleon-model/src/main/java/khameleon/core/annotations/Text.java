package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Signifies that the property is long text, and hence we should use textarea for example, to display the content.
 *
 * Also note that as per current version of Khameleon, text cannot be longer than 4096 characters.
 *
 * @author marembo
 */
@Retention(RUNTIME)
@Target(METHOD)
@Documented
public @interface Text {
}
