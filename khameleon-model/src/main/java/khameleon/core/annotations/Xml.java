package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Implies that the property/config value is served as xml content.
 *
 * This annotation should only be used on property that returns a String value.
 *
 * @author marembo
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Xml {
}
