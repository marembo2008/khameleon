package khameleon.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The annotation equivalence for {@link ApplicationContext}.
 *
 * This is used to explicitly declare a namespace to belong to a particular context.
 *
 * All config-services defined within a context are invoked using the current context only if the current context is one
 * of the defined contexts, otherwise the first context in the list is used.
 *
 * @author marembo
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface AppContext {

    ApplicationMode[] applicationModes() default {};

    TLD[] topLevelDomains() default {};

    Language[] languages() default {};

    Country[] countries() default {};

}
