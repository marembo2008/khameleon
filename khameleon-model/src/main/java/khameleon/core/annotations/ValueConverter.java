package khameleon.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Nonnull;

import com.google.common.reflect.TypeToken;
import lombok.NonNull;
import org.atteo.classindex.IndexSubclasses;

/**
 * On a config method, converts a value to a type returned by the configuration method. For arrays, applies to the array
 * components.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 12:42:39 PM
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValueConverter {

    Class<? extends Converter> value();

    @IndexSubclasses
    interface Converter<T> {

        T apply(@Nonnull final String value);

        default boolean isCompatible(@NonNull final Class<?> classToCheck) {
            final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
            };

            return typeToken.getRawType().isAssignableFrom(classToCheck);
        }

        @NonNull
        default Class<? super T> getDestinationType() {
            final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
            };

            return typeToken.getRawType();
        }

    }

}
