package khameleon.core;

import java.util.UUID;
import javax.annotation.Nonnull;

import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 16, 2016, 4:41:58 PM
 */
public final class ConfigurationUUID {

    @Nonnull
    public static String uuid() {
        return format("%s-%s", System.currentTimeMillis(), UUID.randomUUID().toString());
    }
}
