package khameleon.core;

import java.io.Serializable;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationUUID.uuid;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This is configuration which manipulates how the user sees the configuration value on the interface.
 *
 * They are not meant to be manipulated by the user, and in most cases, this data is static.
 *
 * @author marembo
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class ConfigurationData extends EnumerationConfigData implements Serializable {

  private static final long serialVersionUID = 1348394839390L;

  @Id
  @JsonIgnore
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String dataUUID = uuid();

  private ConfigurationDataOption dataOption;

  private String dataValue;

  private boolean booleanData;

  //JPA
  protected ConfigurationData() {
  }

  public ConfigurationData(@Nonnull final ConfigurationDataOption dataOption, @Nonnull final Object dataValue) {
    this.dataOption = checkNotNull(dataOption, "The dataOption must not be null");
    this.dataValue = String.valueOf(checkNotNull(dataValue, "The dataValue must not be null"));
    this.booleanData = dataValue.getClass() == boolean.class || dataValue.getClass() == Boolean.class;
  }

  public void updateConfigurationData(@Nonnull final ConfigurationData updateData) {
    checkNotNull(updateData, "The ConfigurationData (updateData) must not be null");

    this.dataValue = updateData.dataValue;
    this.booleanData = updateData.booleanData;
  }

  public boolean getBooleanValue() {
    return Boolean.valueOf(dataValue);
  }

  public int getIntValue() {
    return dataValue != null ? Integer.parseInt(dataValue) : 0;
  }

}
