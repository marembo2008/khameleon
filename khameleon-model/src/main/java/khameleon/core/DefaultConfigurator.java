package khameleon.core;

import java.lang.reflect.Method;
import javax.annotation.Nonnull;

/**
 * For determining dynamic defaults of a configuration. Must not return null. Runtime check will throw
 * NullPointerException if the dynamically calculated value is null;
 *
 * @author marembo
 */
public interface DefaultConfigurator {

    @Nonnull
    String getDefault(@Nonnull final Method configMethod, @Nonnull final Object[] args);
}
