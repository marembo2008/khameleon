package khameleon.core;

import static khameleon.core.ConfigurationUUID.uuid;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;

/**
 * More information describing the configurations.
 *
 * @author marembo
 */
@Entity
public class AdditionalInformation implements Serializable {

  private static final long serialVersionUID = 13439439430L;

  @Id
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String infoId = uuid();

  @NotNull
  private String name;

  @Column(columnDefinition = "TEXT")
  private String infoValue;

  public AdditionalInformation() {
  }

  public AdditionalInformation(String name, String value) {
    this.name = name;
    this.infoValue = value;
  }

  public String getInfoId() {
    return infoId;
  }

  public void setInfoId(String infoId) {
    this.infoId = infoId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInfoValue() {
    return infoValue;
  }

  public void setInfoValue(String infoValue) {
    this.infoValue = infoValue;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 79 * hash + Objects.hashCode(this.name);
    hash = 79 * hash + Objects.hashCode(this.infoValue);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final AdditionalInformation other = (AdditionalInformation) obj;
    if (!Objects.equals(this.name, other.name)) {
      return false;
    }
    return Objects.equals(this.infoValue, other.infoValue);
  }

  @Override
  public String toString() {
    return "AdditionalInformation{name=" + name + ", value=" + infoValue + '}';
  }

}
