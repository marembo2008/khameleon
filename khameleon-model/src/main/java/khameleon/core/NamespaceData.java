package khameleon.core;

import static khameleon.core.ConfigurationUUID.uuid;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import anosym.common.StringTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * @author marembo
 */
@Data
@Entity
public class NamespaceData implements Serializable {

  private static final long serialVersionUID = 13438893439L;

  @Id
  @JsonIgnore
  @StringTransient
  @Column(length = 100)
  @SuppressWarnings("FieldMayBeFinal")
  private String dataUUID = uuid();

  private NamespaceDataType dataType;

  @Column(columnDefinition = "TEXT")
  private String dataValue;

  public NamespaceData(NamespaceDataType dataType, String dataValue) {
    this.dataType = dataType;
    this.dataValue = dataValue;
  }

  public NamespaceData(NamespaceData confData) {
    this.dataType = confData.dataType;
    this.dataValue = confData.dataValue;
  }

  public NamespaceData() {
  }

  public Boolean getBooleanValue() {
    return Boolean.valueOf(dataValue);
  }

  public Integer getIntegerValue() {
    return Integer.valueOf(dataValue);
  }

  public Long getLongValue() {
    return Long.valueOf(dataValue);
  }

  public BigDecimal getBigDecimalValue() {
    return new BigDecimal(dataValue);
  }

}
