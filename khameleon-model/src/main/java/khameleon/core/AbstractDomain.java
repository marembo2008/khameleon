package khameleon.core;

import java.util.Calendar;

import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 21, 2016, 9:47:40 PM
 */
@Data
@MappedSuperclass
public class AbstractDomain {

  private static final long serialVersionUID = -9134310271082837480L;

  @Temporal(TemporalType.TIMESTAMP)
  private Calendar createdDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Calendar updatedDate;

  protected AbstractDomain() {
    this.createdDate = this.updatedDate = Calendar.getInstance();
  }

  @PreUpdate
  void onUpdate() {
    this.updatedDate = Calendar.getInstance();
  }

}
