package khameleon.core.spi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marembo
 */
public class Option {

    private String id;
    private List<String> values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

}
