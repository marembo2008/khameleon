package khameleon.core.spi.interactive;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigValue {

  private String value;

  private List<ConfigParameter> configParameters;

  public ConfigValue(String value) {
    this.value = value;
  }

}
