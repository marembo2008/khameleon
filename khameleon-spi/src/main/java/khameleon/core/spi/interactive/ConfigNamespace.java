package khameleon.core.spi.interactive;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo
 */
@Data
@NoArgsConstructor
public class ConfigNamespace {

  /**
   * The canonical namespace name.
   */
  private String name;

  private List<Config> configs;

  public ConfigNamespace(String name) {
    this.name = name;
  }

}
