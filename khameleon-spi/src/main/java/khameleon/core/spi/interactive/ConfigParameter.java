package khameleon.core.spi.interactive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigParameter {

  private String key;

  private String value;

}
