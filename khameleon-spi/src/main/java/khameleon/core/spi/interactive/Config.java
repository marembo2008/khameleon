package khameleon.core.spi.interactive;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo
 */
@Data
@NoArgsConstructor
public class Config {

  private String name;

  private List<ConfigValue> values;

  public Config(String name) {
    this.name = name;
  }

}
