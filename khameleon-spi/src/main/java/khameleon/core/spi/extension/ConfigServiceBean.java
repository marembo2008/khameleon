package khameleon.core.spi.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Stereotype;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.inject.spi.PassivationCapable;
import jakarta.inject.Named;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.applicationcontext.ConfigServiceContext;
import khameleon.core.applicationcontext.DefaultContext;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static khameleon.core.manager.ConfigurationManager.getInstance;

@Slf4j
@ToString
class ConfigServiceBean implements Bean, PassivationCapable {

	private final Class<?> configInterface;

	private final String id;

	public ConfigServiceBean(@Nonnull final Class<?> configInterface) {
		requireNonNull(configInterface, "The configInterface must not be null");

		this.configInterface = configInterface;
		this.id = format("%s-%s", configInterface.getCanonicalName(), UUID.randomUUID());
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Class getBeanClass() {
		return configInterface;
	}

	@Override
	public Set getInjectionPoints() {
		return ImmutableSet.of();
	}

	@Override
	public boolean isNullable() {
		return false;
	}

	@Override
	public Object create(final CreationalContext creationalContext) {
		final ApplicationContextService applicationContextService = getApplicationContextService();
		return getInstance(applicationContextService, configInterface);
	}

	@Override
	public void destroy(final Object instance, final CreationalContext creationalContext) {
		creationalContext.release();
	}

	@Override
	public Set<Type> getTypes() {
		return ImmutableSet.<Type>builder().addAll(getBeanClasses(configInterface)).add(Object.class).build();
	}

	@Override
	public Set<Annotation> getQualifiers() {
		return ImmutableSet.of(Default.Literal.INSTANCE, Any.Literal.INSTANCE, new NamedLiteral(getName()));
	}

	@Override
	public Class<? extends Annotation> getScope() {
		return ApplicationScoped.class;
	}

	@Override
	public String getName() {
		final String simpleName = configInterface.getSimpleName();
		return simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
	}

	@Override
	public Set<Class<? extends Annotation>> getStereotypes() {
		final Set<Class<? extends Annotation>> definedStereoTypes = Arrays.stream(configInterface.getAnnotations())
				.map((annot) -> annot.annotationType())
				.filter((annotType) -> annotType.isAnnotationPresent(Stereotype.class)).collect(toSet());

		final Set<Class<? extends Annotation>> stereotypes = ImmutableSet.<Class<? extends Annotation>>builder()
				.addAll(definedStereoTypes).add(ConfigService.class).build();

		log.info("Stereotypes for beans <{}>: {}", id, stereotypes);

		return stereotypes;
	}

	@Override
	public boolean isAlternative() {
		return false;
	}

	@Nonnull
	private ApplicationContextService getApplicationContextService() {
		final ConfigServiceContext configServiceContext = new ConfigServiceContextLiteral();
		final DefaultContext defaultContext = new DefaultContextLiteral();
		final Instance<ApplicationContextService> applicationContextServices = CDI.current()
				.select(ApplicationContextService.class);
		return ImmutableList.copyOf(applicationContextServices.select(configServiceContext)).stream().findFirst()
				.orElseGet(() -> applicationContextServices.select(defaultContext).get());
	}

	@Nonnull
	private List<Class<?>> getBeanClasses(@Nonnull final Class<?> configInterface) {
		final ImmutableList.Builder<Class<?>> builder = ImmutableList.builder();
		builder.add(configInterface);

		final Class<?>[] interfaces = configInterface.getInterfaces();
		if (interfaces != null && interfaces.length > 0) {
			builder.addAll(getBeanClasses(interfaces[0]));
		}

		return builder.build();
	}

}
