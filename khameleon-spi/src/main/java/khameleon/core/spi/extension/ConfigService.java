package khameleon.core.spi.extension;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Stereotype;
import jakarta.inject.Named;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 2, 2017, 10:00:24 PM
 */
@Named
@Stereotype
@ApplicationScoped
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface ConfigService {
}
