package khameleon.core.spi.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.inject.spi.PassivationCapable;
import khameleon.core.ApplicationContext;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.applicationcontext.ConfigServiceContext;
import khameleon.core.applicationcontext.DefaultContext;

import static java.lang.String.format;

class ApplicationContextBean implements Bean, PassivationCapable {

  private final String id = format("%s-%s", ApplicationContext.class.getCanonicalName(), UUID.randomUUID());

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Class getBeanClass() {
    return ApplicationContext.class;
  }

  @Override
  public Set getInjectionPoints() {
    return ImmutableSet.of();
  }

  @Override
  public boolean isNullable() {
    return false;
  }

  @Override
  public Object create(final CreationalContext creationalContext) {
    final ApplicationContextService applicationContextService = getApplicationContextService();
    return applicationContextService.getApplicationContext();
  }

  @Override
  public void destroy(final Object instance, final CreationalContext creationalContext) {
    creationalContext.release();
  }

  @Override
  public Set<Type> getTypes() {
    return ImmutableSet.of(ApplicationContext.class, Object.class);
  }

  @Override
  public Set<Annotation> getQualifiers() {
    return ImmutableSet.of(Default.Literal.INSTANCE, Any.Literal.INSTANCE);
  }

  @Override
  public Class<? extends Annotation> getScope() {
    return Dependent.class;
  }

  @Override
  public String getName() {
    return "applicationContext";
  }

  @Override
  public Set<Class<? extends Annotation>> getStereotypes() {
    return ImmutableSet.of();
  }

  @Override
  public boolean isAlternative() {
    return false;
  }

  @Nonnull
  private ApplicationContextService getApplicationContextService() {
    final ConfigServiceContext configServiceContext = new ConfigServiceContextLiteral();
    final DefaultContext defaultContext = new DefaultContextLiteral();
    final Instance<ApplicationContextService> applicationContextServices = CDI.current()
            .select(ApplicationContextService.class);
    return ImmutableList
            .copyOf(applicationContextServices.select(configServiceContext))
            .stream()
            .findFirst()
            .orElseGet(() -> applicationContextServices.select(defaultContext).get());
  }

}
