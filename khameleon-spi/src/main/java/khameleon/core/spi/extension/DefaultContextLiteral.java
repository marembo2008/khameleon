package khameleon.core.spi.extension;

import jakarta.enterprise.util.AnnotationLiteral;
import khameleon.core.applicationcontext.DefaultContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 6, 2017, 9:48:34 PM
 */
@SuppressWarnings(value = "AnnotationAsSuperInterface")
final class DefaultContextLiteral extends AnnotationLiteral<DefaultContext> implements DefaultContext {

  private static final long serialVersionUID = -1485969418517333998L;

}
