package khameleon.core.spi.extension;

import javax.annotation.Nonnull;

import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Named;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 6, 2017, 9:48:34 PM
 */
@RequiredArgsConstructor
@SuppressWarnings(value = "AnnotationAsSuperInterface")
final class NamedLiteral extends AnnotationLiteral<Named> implements Named {

	private static final long serialVersionUID = -1485969418517333998L;

	@Nonnull
	private final String name;

	@Nonnull
	public String value() {
		return name;
	}
}
