package khameleon.core.spi.extension;

import jakarta.enterprise.util.AnnotationLiteral;
import khameleon.core.applicationcontext.ConfigServiceContext;

@SuppressWarnings(value = "AnnotationAsSuperInterface")
final class ConfigServiceContextLiteral extends AnnotationLiteral<ConfigServiceContext> implements ConfigServiceContext {

  private static final long serialVersionUID = -1485969418517333998L;

}
