package khameleon.core.spi.extension;

import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.collect.Sets;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.AfterBeanDiscovery;
import jakarta.enterprise.inject.spi.BeforeBeanDiscovery;
import jakarta.enterprise.inject.spi.Extension;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.ConfigTest;
import lombok.extern.slf4j.Slf4j;
import org.atteo.classindex.ClassIndex;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 2, 2017, 9:51:05 PM
 */
@Slf4j
public class ConfigServiceExtension implements Extension {

  private static final Set<ConfigServiceBean> configBeans;

  static {
    final Set<Class<?>> configInterfaces = getConfigInterfaces();

    log.info("\n................Initializing Config Service beans ..................{}",
             configInterfaces.stream().map(Class::getCanonicalName).collect(joining("\n---", "\n", "\n")));

    configBeans = configInterfaces
            .stream()
            .map(ConfigServiceBean::new)
            .collect(toSet());
  }

  void beforeBeanDiscovery(@Observes final BeforeBeanDiscovery beforeBeanDiscovery) {
    // We register all the stereotypes before bean discovery, so that they can be discovered
    configBeans.stream()
            .flatMap((bean) -> bean.getStereotypes().stream())
            .forEach(beforeBeanDiscovery::addStereotype);
  }

  void afterBeanDiscovery(@Observes final AfterBeanDiscovery afterBeanDiscovery) {
    configBeans.forEach(afterBeanDiscovery::addBean);

    //Register ApplicationContext
    afterBeanDiscovery.addBean(new ApplicationContextBean());
  }

  @Nonnull
  private static Set<Class<?>> getConfigInterfaces() {
    return Sets
            .newHashSet(ClassIndex.getAnnotated(ConfigRoot.class))
            .stream()
            .filter((cls) -> !cls.isAnnotationPresent(ConfigTest.class))
            .sorted((o1, o2) -> o1.getCanonicalName().compareTo(o2.getCanonicalName()))
            .collect(Collectors.toSet());
  }

}
