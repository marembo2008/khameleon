package khameleon.core.spi.async;

import static anosym.common.util.ObjectsUtil.firstNonNull;

import javax.annotation.Nonnull;

import jakarta.annotation.Priority;
import jakarta.decorator.Decorator;
import jakarta.decorator.Delegate;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.interceptor.Interceptor;
import khameleon.core.ApplicationContext;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.applicationcontext.ConfigServiceContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 1:00:51 PM
 */
@Decorator
@Dependent
@Priority(Interceptor.Priority.APPLICATION)
public abstract class AsynchronousSupportedApplicationContextService implements ApplicationContextService {

	private static final long serialVersionUID = 5972979855624261326L;

	@Inject
	private AsynchronousApplicationContextProvider asynchronousApplicationContextProvider;

	@Inject
	@Delegate
	@ConfigServiceContext
	private ApplicationContextService applicationContextService;

	@Nonnull
	@Override
	public ApplicationContext getApplicationContext() {
		return firstNonNull(asynchronousApplicationContextProvider.getThreadApplicationContext(),
				() -> applicationContextService.getApplicationContext());
	}

}
