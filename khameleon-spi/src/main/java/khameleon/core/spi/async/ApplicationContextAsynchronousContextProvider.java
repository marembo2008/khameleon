package khameleon.core.spi.async;

import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextConsumer;
import anosym.async.context.AsynchronousContextRemover;
import anosym.async.context.AsynchronousContextSupplier;
import anosym.async.context.ForAsynchronousContext;
import anosym.async.typesafe.TypeSafeKey;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import lombok.extern.slf4j.Slf4j;

import static anosym.async.typesafe.TypeSafeKeys.key;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 11:20:36 AM
 */
@Slf4j
@ApplicationScoped
@ForAsynchronousContext(ApplicationContext.class)
class ApplicationContextAsynchronousContextProvider implements AsynchronousContextSupplier<ApplicationContext>,
                                                               AsynchronousContextConsumer<ApplicationContext>,
                                                               AsynchronousContextRemover<ApplicationContext> {

  private static final TypeSafeKey<ApplicationContext> APPLICATION_CONTEXT_CONTEXT
          = key("propagated-context.applicationContext", ApplicationContext.class);

  @Inject
  private Instance<ApplicationContext> applicationContexts;

  @Inject
  private AsynchronousApplicationContextProvider asynchronousApplicationContextProvider;

  @Override
  public Optional<AsynchronousContext<ApplicationContext>> provide() {
    final ApplicationContext applicationContext = applicationContexts.get();

    log.debug("Propagating applicationContext in async context: {}", applicationContext);

    if (applicationContext == null) {
      return Optional.empty();
    }

    return AsynchronousContext
            .<ApplicationContext>builder()
            .context(applicationContext)
            .contextKey(APPLICATION_CONTEXT_CONTEXT)
            .build()
            .toOptional();
  }

  @Override
  public void consume(@Nonnull final AsynchronousContext<ApplicationContext> applicationContextAsynchronousContext) {
    checkNotNull(applicationContextAsynchronousContext, "The applicationContextAsynchronousContext must not be null");

    final ApplicationContext applicationContext = applicationContextAsynchronousContext.getContext();
    asynchronousApplicationContextProvider.onThreadContextChanged(applicationContext);
  }

  @Override
  public void remove(@Nonnull final AsynchronousContext<ApplicationContext> applicationContextAsynchronousContext) {
    checkNotNull(applicationContextAsynchronousContext, "The applicationContextAsynchronousContext must not be null");

    asynchronousApplicationContextProvider.onThreadContextComplete();
  }

}
