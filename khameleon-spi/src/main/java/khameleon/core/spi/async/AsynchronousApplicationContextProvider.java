package khameleon.core.spi.async;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.ApplicationContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 1:03:24 PM
 */
@ApplicationScoped
class AsynchronousApplicationContextProvider {

	private static final ThreadLocal<ApplicationContext> APPLICATION_CONTEXT_CONTEXT = new ThreadLocal<>();

	void onThreadContextChanged(@Nonnull final ApplicationContext applicationContext) {
		checkNotNull(applicationContext, "The applicationContext must not be null");

		APPLICATION_CONTEXT_CONTEXT.set(applicationContext);
	}

	void onThreadContextComplete() {
		APPLICATION_CONTEXT_CONTEXT.remove();
	}

	@Nullable
	ApplicationContext getThreadApplicationContext() {
		return APPLICATION_CONTEXT_CONTEXT.get();
	}

}
