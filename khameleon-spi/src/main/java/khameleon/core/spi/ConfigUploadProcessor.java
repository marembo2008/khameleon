package khameleon.core.spi;

import java.util.List;

/**
 * Any class annotated with @ConfigUpload must implement this interface.
 *
 * The class must have a no arg constructor.
 *
 * @author marembo
 */
public interface ConfigUploadProcessor {

    void doProcess(List<Option> options);
}
