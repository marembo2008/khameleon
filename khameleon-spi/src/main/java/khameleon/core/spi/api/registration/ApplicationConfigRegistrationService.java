package khameleon.core.spi.api.registration;

import static khameleon.core.internal.util.KhameleonParameters.CONFIG_SOURCE_KEY;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.DependsOn;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.core.configsource.Source;
import khameleon.core.internal.service.registration.RegistrationService;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 20, 2018, 11:21:38 PM
 */
@Slf4j
@Startup
@Singleton
public class ApplicationConfigRegistrationService extends RegistrationService {

  // Because an event has to have a payload.
  private static final Object EVENT_PAYLOAD = new Object();

  @Inject
  @OnRegistrationCompleted
  private Event<Object> onRegistrationCompletedEvent;

  @Inject
  @OnRegistrationStarted
  private Event<Object> onRegistrationStartedEvent;
  
  @PostConstruct
  void onStartUp() {
    final Source source = Source.valueOf(System.getProperty(CONFIG_SOURCE_KEY, "CONFIG_SERVICE").toUpperCase());
    if (source == Source.LOCAL_SERVICE) {
      log.info("Skipping config registration for local config source");
      return;
    }

    log.info("Notifying on registration started.......");
    
    onRegistrationStartedEvent.fire(EVENT_PAYLOAD);
    
    log.info("Running configuration registration.........");

    register();
    
    log.info("Notifying on registration completed....");
    
    onRegistrationCompletedEvent.fire(EVENT_PAYLOAD);

    log.info("Completed configuration registration................");
  }

}
