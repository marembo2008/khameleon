package khameleon.core.spi.api;

import static java.util.stream.Collectors.toMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.atteo.classindex.ClassIndex;

import com.google.common.collect.ImmutableList;

import anosym.common.Pair;
import anosym.feign.Feign;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Inject;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.NamespaceDataType;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.api.ConfigRegistrationApi;
import khameleon.core.internal.api.ConfigRequestApi;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 22, 2018, 7:24:36 PM
 */
@ApplicationScoped
public class ApiSelector {

	private static final ConfigurationInitializer CONFIG_INITIALIZER = new DefaultConfigurationInitializor();

	private static final Map<String, String> NAMESPACE_TO_INSTANCE_ID;

	static {
		NAMESPACE_TO_INSTANCE_ID = ImmutableList.copyOf(ClassIndex.getAnnotated(ConfigRoot.class)).stream()
				.map(CONFIG_INITIALIZER::getNamespaces).flatMap(List::stream)
				.map((ns) -> Pair.of(ns.getCanonicalName(), getInstanceId(ns)))
				.filter((nsPair) -> nsPair.getSecond() != null).collect(toMap(Pair::getFirst, Pair::getSecond));
	}

	@Inject
	private Instance<ConfigRegistrationApi> configRegistrationApis;

	@Inject
	private Instance<ConfigRequestApi> configRequestApis;

	@NonNull
	public ConfigRegistrationApi getConfigRegistrationApi(@NonNull final String namespaceCanonicalName) {
		return getInstanceId(namespaceCanonicalName).map(Feign.FeignInstance::new).map(configRegistrationApis::select)
				.orElse(configRegistrationApis).get();
	}

	@NonNull
	public ConfigRequestApi getConfigRequestApi(@NonNull final String namespaceCanonicalName) {
		return getInstanceId(namespaceCanonicalName).map(Feign.FeignInstance::new).map(configRequestApis::select)
				.orElse(configRequestApis).get();
	}

	private static String getInstanceId(@NonNull final Namespace namespace) {
		return namespace.getNamespaceData(NamespaceDataType.NAMESPACE_INSTANCE_ID)
				.transform(NamespaceData::getDataValue).orNull();
	}

	@NonNull
	private Optional<String> getInstanceId(@NonNull final String namespaceCanonicalName) {
		return Optional.ofNullable(NAMESPACE_TO_INSTANCE_ID.get(namespaceCanonicalName));
	}

}
