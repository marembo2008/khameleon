package khameleon.core.spi.api;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.internal.util.KhameleonParameters.CONFIG_SOURCE_KEY;
import static khameleon.core.internal.util.KhameleonParameters.KHAMELEON_FEIGN_CLIENT;
import static khameleon.core.spi.api.ConfigServiceProperties.getDefaultEndPoint;
import static khameleon.core.spi.api.ConfigServiceProperties.getInstanceIdEndPoint;
import static khameleon.core.spi.api.ConfigServiceProperties.getInstanceIds;
import static khameleon.core.spi.api.ConfigServiceProperties.getPropertiesString;

import java.util.List;

import com.google.common.base.Splitter;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import khameleon.core.configsource.Source;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 22, 2018, 11:40:15 PM
 */
@Slf4j
@FeignConfig(KHAMELEON_FEIGN_CLIENT)
public class ConfigServiceFeignConfigProvider implements FeignConfigProvider {

	private static final Splitter HASH_SPLITTER = Splitter.on('#');

	@Override
	public String getEndPoint(@NonNull final String feignClientId) {
		log.debug("Loading endpoint for khameleon config-service: {}", feignClientId);

		final String endPoint = getInstanceIdEndPoint(feignClientId).orElseGet(() -> getDefaultEndPoint().orElse(null));
		return checkNotNull(endPoint,
				"Config-service configurations are missing for <%s>. Please define relevant configurations: \n%s\n",
				feignClientId, getPropertiesString());
	}

	@Override
	public String[] getFeignClientInstanceIds(@NonNull final String feignClientId) {
		final String feignClientGroupId = HASH_SPLITTER.splitToList(feignClientId).get(0);
		checkArgument(KHAMELEON_FEIGN_CLIENT.equalsIgnoreCase(feignClientGroupId),
				"Unsupported feign-client-id <%s>. Can only provide client instance ids for group: %s", feignClientId,
				KHAMELEON_FEIGN_CLIENT);

		final List<String> instanceIds = getInstanceIds();
		if (instanceIds.isEmpty()) {
			return new String[] { feignClientId };
		}

		return instanceIds.toArray(new String[0]);
	}

	@Override
	public boolean active() {
		final Source source = Source.valueOf(System.getProperty(CONFIG_SOURCE_KEY, "CONFIG_SERVICE").toUpperCase());
		return source == Source.CONFIG_SERVICE;
	}

}
