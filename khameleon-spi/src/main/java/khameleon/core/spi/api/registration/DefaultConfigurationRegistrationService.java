package khameleon.core.spi.api.registration;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.internal.service.ConfigurationRegistrationService;
import khameleon.core.spi.api.ApiSelector;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class DefaultConfigurationRegistrationService implements ConfigurationRegistrationService {

  @Inject
  private ApiSelector apiSelector;

  @Override
  public void registerNamespace(@NonNull final Namespace namespace) {
    log.info("Registering namespace: {}", namespace);

    apiSelector.getConfigRegistrationApi(namespace.getCanonicalName()).registerNamespace(namespace);
  }

  @Override
  public void registerConfiguration(@NonNull final Configuration configuration) {
    log.info("Registering configuration: {}", configuration);

    final Namespace namespace = configuration.getNamespace();
    apiSelector.getConfigRegistrationApi(namespace.getCanonicalName()).registerConfiguration(configuration);
  }

}
