package khameleon.core.spi.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Optional;

import javax.annotation.Nullable;

import khameleon.core.Namespace;
import khameleon.core.annotations.namespace.NamespaceProcessor;
import khameleon.core.processor.namespace.ConfigNamespaceProcessor;
import lombok.NonNull;
import org.atteo.classindex.IndexAnnotated;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static khameleon.core.NamespaceDataType.NAMESPACE_INSTANCE_ID;
import static khameleon.core.spi.api.ConfigServiceProperties.getDefaultInstanceId;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 22, 2018, 1:43:43 PM
 */
@IndexAnnotated
@Retention(RUNTIME)
@Target(ElementType.PACKAGE)
public @interface InstanceProvider {

    /**
     * The id that provides the instance of khameleon configuration. e.g. config-server or translation-server. Must
     * match one of the values returned through the environment variable:
     * "anosym.khameleon.config-service.instanceIds"
     */
    String value();

    @NamespaceProcessor
    class InstanceProviderNamespaceProcessor implements ConfigNamespaceProcessor {

        @Override
        public void process(@NonNull final Namespace namespace, @NonNull final Class<?> configInterface) {
            final Package classPackage = configInterface.getPackage();
            final String instanceId = getInstanceId(classPackage)
                    .orElseGet(() -> getDefaultInstanceId().orElse(null));
            if (instanceId != null) {
                namespace.addOrUpdateNamespaceData(NAMESPACE_INSTANCE_ID, instanceId);
            }
        }

        @NonNull
        private Optional<String> getInstanceId(@NonNull final Package classPackage) {
            if (classPackage.isAnnotationPresent(InstanceProvider.class)) {
                return Optional.of(classPackage.getAnnotation(InstanceProvider.class).value());
            }

            final String packageName = classPackage.getName();
            final Package parentPackage = getParentPackage(packageName);
            if (parentPackage == null) {
                return Optional.empty();
            }

            return getInstanceId(parentPackage);
        }

        @Nullable
        private Package getParentPackage(@NonNull final String packageName) {
            final int parentIx = packageName.lastIndexOf(".");
            if (parentIx < 0) {
                return null;
            }

            final String parentPackageName = packageName.substring(0, parentIx);
            final Package parentPackage = Package.getPackage(parentPackageName);
            if (parentPackage == null) {
                return getParentPackage(parentPackageName);
            }

            return parentPackage;
        }

    }

}
