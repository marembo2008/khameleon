package khameleon.core.spi.api.registration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 26, 2015, 11:32:48 PM
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface OnRegistrationCompleted {
}
