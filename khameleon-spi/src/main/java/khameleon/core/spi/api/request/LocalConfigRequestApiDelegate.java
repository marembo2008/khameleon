package khameleon.core.spi.api.request;

import static com.google.common.collect.Iterables.concat;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.atteo.classindex.ClassIndex;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import anosym.common.Language;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.Configuration;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.configsource.ConfigSource;
import khameleon.core.configsource.Source;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.internal.service.ConfigurationServiceException;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 3, 2017, 2:30:22 PM
 */
@Slf4j
@ApplicationScoped
@ConfigSource(Source.LOCAL_SERVICE)
class LocalConfigRequestApiDelegate implements ConfigurationRequestService {

	private static final Logger LOG = Logger.getLogger(LocalConfigRequestApiDelegate.class.getName());

	private static final Splitter HYPHEN_SPLITTER = Splitter.on("-").omitEmptyStrings().trimResults();

	private static final Splitter HASH_SPLITTER = Splitter.on("#").omitEmptyStrings().trimResults();

	private static final Splitter COMMA_SPLITTER = Splitter.on(",").omitEmptyStrings().trimResults();

	private static final String LOCAL_CONFIG_PATH_KEY = "configurationPath";

	private static final Pattern CONFIG_PARAMETERS = Pattern.compile(".*\\{(.+)\\}.*");

	private static final ConfigurationInitializer CONFIGURATION_INITIALIZER = new DefaultConfigurationInitializor();

	private final Map<ApplicationContext, Map<String, ConfigurationValue>> APPLICATION_CONTEXT_CONFIGURATIONS = new HashMap<>();

	private final Map<String, Configuration> CONFIGURATION_KEY_MAP = new ConcurrentHashMap<>();

	@PostConstruct
	void initConfigs() {
		final List<Class<?>> configInterfaces = ImmutableList.copyOf(ClassIndex.getAnnotated(ConfigRoot.class));
		final Map<String, Configuration> configurationKeyMap = configInterfaces.stream()
				.map(CONFIGURATION_INITIALIZER::getConfigurations).flatMap(List::stream)
				.collect(toMap((config) -> createConfigKey(config), Function.identity()));
		CONFIGURATION_KEY_MAP.putAll(configurationKeyMap);

		final String configPath = System.getProperty(LOCAL_CONFIG_PATH_KEY, System.getenv(LOCAL_CONFIG_PATH_KEY));

		log.info("Initializing local config from path: {}", configPath);

		if (configPath != null) {
			final File configFolder = new File(configPath);
			Arrays.stream(configFolder.listFiles())
					.filter((file) -> file.getName().startsWith("application") && file.getName().endsWith(".conf"))
					.forEach((file) -> createApplicationContextConfigValues(file));
		}

	}

	@Override
	public ConfigurationValue findConfigurationValue(@Nonnull final ApplicationContext applicationContext,
			@Nonnull final String namespaceCanonicalName, @Nonnull final String configName,
			@Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) throws ConfigurationServiceException {
		requireNonNull(applicationContext, "The applicationContext must not be null");
		requireNonNull(namespaceCanonicalName, "The namespaceCanonicalName must not be null");
		requireNonNull(configName, "The configName must not be null");
		requireNonNull(parameterKeyValues, "The parameterKeyValues must not be null");

		LOG.log(Level.FINE, "Application-Context: {0}", applicationContext);

		ApplicationContext currentApplicationContext = applicationContext;
		final AtomicReference<Map<String, ConfigurationValue>> configurationValuesRef = new AtomicReference<>();
		while (configurationValuesRef.get() == null) {
			configurationValuesRef.set(APPLICATION_CONTEXT_CONFIGURATIONS.get(currentApplicationContext));
			if (configurationValuesRef.get() != null || currentApplicationContext.equals(DEFAULT_APPLICATION_CONTEXT)) {
				break;
			}

			currentApplicationContext = currentApplicationContext.getParentApplicationContext();
		}

		final Map<String, ConfigurationValue> configurationValues = configurationValuesRef.get();
		if (configurationValues == null) {
			throw new IllegalStateException("No default configuration properties defined.");
		}

		final String configValueKey = createConfigValueKey(namespaceCanonicalName, configName, parameterKeyValues);
		if (configurationValues.containsKey(configValueKey)) {
			return configurationValues.get(configValueKey);
		}

		final List<AdditionalParameterKeyValue> nonDynamicParameterKeyValues = parameterKeyValues.stream()
				.filter((kv) -> !kv.isDynamic()).collect(toList());
		if (nonDynamicParameterKeyValues.size() == parameterKeyValues.size()) {
			return null;
		}

		final List<AdditionalParameterKeyValue> dynamicParameterKeyValues = parameterKeyValues.stream()
				.filter((kv) -> kv.isDynamic()).collect(toList());

		// We have dynamic keys, create a permutation of all possible keys.
		return getCombinedKeyValues(dynamicParameterKeyValues, nonDynamicParameterKeyValues).stream().map((kvs) -> {
			final String calConfigValueKey = createConfigValueKey(namespaceCanonicalName, configName, kvs);
			return configurationValues.get(calConfigValueKey);
		}).filter(Objects::nonNull).findFirst().orElse(null);
	}

	private void createApplicationContextConfigValues(@Nonnull final File applicationContextFile) {
		final ApplicationContext applicationContext = createApplicationContext(applicationContextFile);
		final ImmutableMap.Builder<String, ConfigurationValue> builder = ImmutableMap.builder();
		try (final BufferedReader reader = new BufferedReader(new FileReader(applicationContextFile))) {
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.trim().isEmpty() || line.trim().startsWith("#")) {
					continue;
				}

				final String[] configValueKeyAndValue = line.split("=");
				final String configValueKey = configValueKeyAndValue[0];
				final Configuration configuration = getConfiguration(configValueKey);
				if (configuration == null) {
					log.warn("No Configuration matched for configValueKey <{}>: {}",
							applicationContext.toLocalizedString(), configValueKey);
					continue;
				}

				final String configValue = configValueKeyAndValue.length == 2 ? configValueKeyAndValue[1] : null;
				builder.put(configValueKey, new ConfigurationValue(applicationContext, configuration, configValue));
			}
		} catch (final Exception ex) {
			throw new RuntimeException(ex);
		}

		APPLICATION_CONTEXT_CONFIGURATIONS.put(applicationContext, builder.build());
	}

	private ApplicationContext createApplicationContext(@Nonnull final File applicationContextFile) {
		final String name = applicationContextFile.getName();
		final String appContextParts = name.replaceAll("application", "").replaceAll("\\.conf", "");
		final List<String> appContextProperties = HYPHEN_SPLITTER.splitToList(appContextParts).stream()
				.filter((str) -> !str.trim().isEmpty()).collect(toList());
		if (appContextProperties.isEmpty()) {
			return ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
		}

		final ApplicationContext applicationContext = new ApplicationContext();
		final String applicationModeName = appContextProperties.get(0).toUpperCase();
		final ApplicationMode applicationMode = ApplicationMode.valueOf(applicationModeName);
		applicationContext.setApplicationMode(applicationMode);

		// We only consider application mode and language.
		if (appContextProperties.size() > 1) {
			final String languageCode = appContextProperties.get(1);
			applicationContext.setLanguageCode(Language.fromIsoCode(languageCode));
		}

		return applicationContext;
	}

	@Nonnull
	private String createConfigValueKey(@Nonnull final String namespaceCanonicalName, @Nonnull final String configName,
			@Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) {
		final String parameters = parameterKeyValues.stream()
				.map((keyValue) -> format("%s#%s", keyValue.getKey(), keyValue.getValue())).sorted()
				.collect(joining(",", "{", "}"));
		return format("%s.%s.%s", namespaceCanonicalName, configName, parameters);
	}

	@Nonnull
	private String createConfigKey(@Nonnull final Configuration configuration) {
		final String namespaceCanonicalName = configuration.getNamespace().getCanonicalName();
		final String configName = configuration.getConfigName();
		final String parameters = configuration.getAdditionalParameters().stream()
				.map((configParam) -> format("%s#", configParam.getParameterKey())).sorted()
				.collect(joining(",", "{", "}"));
		final String configKey = format("%s.%s.%s", namespaceCanonicalName, configName, parameters);

		log.info("Resolved Config Key: {}", configKey);

		return configKey;
	}

	@Nonnull
	private String createConfigKey(@Nonnull final String configValueKey) {
		// Value keys may have parameter values
		final Matcher matcher = CONFIG_PARAMETERS.matcher(configValueKey);
		if (!matcher.find()) {
			return configValueKey;
		}

		final String configParams = matcher.group(1);
		final String paramValues = COMMA_SPLITTER.splitToList(configParams).stream()
				.map((configParam) -> HASH_SPLITTER.splitToList(configParam).stream().skip(1).findFirst().orElse(null))
				.filter(Objects::nonNull).collect(joining("|"));

		final String configKey = configValueKey.replaceAll(paramValues, "");

		log.info("Mapped ConfigKey from ConfigValueKey: <{}>={}", configValueKey, configKey);

		return configKey;
	}

	@Nullable
	private Configuration getConfiguration(@Nonnull final String configValueKey) {
		final String configKey = createConfigKey(configValueKey);
		return CONFIGURATION_KEY_MAP.get(configKey);
	}

	@Nonnull
	@VisibleForTesting
	List<List<AdditionalParameterKeyValue>> getCombinedKeyValues(
			@Nonnull final List<AdditionalParameterKeyValue> dynamicKeyValues,
			@Nonnull final List<AdditionalParameterKeyValue> nonDynamicKeyValues) {
		return getCombinationsStream(dynamicKeyValues)
				.map((list) -> ImmutableList.copyOf(concat(list, nonDynamicKeyValues)))
				.sorted((list1, list2) -> Integer.valueOf(list2.size()).compareTo(list1.size()))
				.collect(Collectors.toList());
	}

	@Nonnull
	@VisibleForTesting
	<T> Stream<List<T>> getCombinationsStream(@Nonnull final List<T> paramaters) {
		// there are 2 ^ list.size() possible combinations
		// stream through them and map the number of the combination to the combination
		return LongStream.range(1, 1 << paramaters.size()).mapToObj(l -> bitMapToList(l, paramaters));
	}

	@Nonnull
	private <T> List<T> bitMapToList(final long bitmap, @Nonnull final List<T> parameters) {
		// use the number of the combination (bitmap) as a bitmap to filter the input
		// list
		return IntStream.range(0, parameters.size()).filter(i -> 0 != ((1 << i) & bitmap)).mapToObj(parameters::get)
				.collect(Collectors.toList());
	}

}
