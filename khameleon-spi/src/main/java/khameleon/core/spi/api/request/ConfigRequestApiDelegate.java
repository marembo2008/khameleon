package khameleon.core.spi.api.request;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import khameleon.core.configsource.ConfigSource;
import khameleon.core.configsource.Source;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.spi.api.ApiSelector;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.core.values.ConfigurationValueRequest;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo
 */
@Slf4j
@ApplicationScoped
@ConfigSource(Source.CONFIG_SERVICE)
public class ConfigRequestApiDelegate implements ConfigurationRequestService {

  @Inject
  private ApiSelector apiSelector;

  @Override
  public ConfigurationValue findConfigurationValue(@NonNull final ApplicationContext applicationContext,
                                                   @NonNull final String namespaceCanonicalName,
                                                   @NonNull final String configName,
                                                   @NonNull final List<AdditionalParameterKeyValue> parameterKeyValues) {
    final ConfigurationValueRequest configurationValueRequest = ConfigurationValueRequest.builder()
            .applicationContext(applicationContext)
            .namespaceCanonicalName(namespaceCanonicalName)
            .configName(configName)
            .parameterKeyValues(parameterKeyValues)
            .build();

    log.debug("Requesting ConfigurationValue from Webservice: {}", configurationValueRequest);

    return apiSelector.getConfigRequestApi(namespaceCanonicalName).getConfigurationValue(configurationValueRequest);
  }

}
