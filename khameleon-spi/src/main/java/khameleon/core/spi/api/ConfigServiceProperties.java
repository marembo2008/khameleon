package khameleon.core.spi.api;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.Optional;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 22, 2018, 6:33:26 PM
 */
public final class ConfigServiceProperties {

	public static final Splitter INSTANCE_SPLITTER = Splitter.on(",").trimResults();

	public static final String DEFAULT_ENDPOINT = "khameleon.config-service.endPoint";

	public static final String INSTANCE_IDS = "khameleon.config-service.instanceIds";

	public static final String DEFAULT_INSTANCE_ID = "khameleon.config-service.instanceIds.default";

	public static final String INSTANCE_ID_ENDPOINT = "khameleon.config-service.endPoint.%s";

	@NonNull
	public static Optional<String> getDefaultEndPoint() {
		return Optional.ofNullable(getProperty(DEFAULT_ENDPOINT));
	}

	@NonNull
	public static List<String> getInstanceIds() {
		return Optional.ofNullable(getProperty(INSTANCE_IDS)).map(INSTANCE_SPLITTER::splitToList)
				.orElse(ImmutableList.of());
	}

	@NonNull
	public static Optional<String> getDefaultInstanceId() {
		return Optional.ofNullable(getProperty(DEFAULT_INSTANCE_ID));
	}

	@NonNull
	public static Optional<String> getInstanceIdEndPoint(@NonNull final String instanceId) {
		return Optional.ofNullable(getProperty(format(INSTANCE_ID_ENDPOINT, instanceId)));
	}

	@NonNull
	public static String getPropertiesString() {
		return ImmutableList.of(DEFAULT_ENDPOINT, INSTANCE_IDS, DEFAULT_INSTANCE_ID, INSTANCE_ID_ENDPOINT).stream()
				.collect(joining("\n", "\n", "\n"));
	}

}
