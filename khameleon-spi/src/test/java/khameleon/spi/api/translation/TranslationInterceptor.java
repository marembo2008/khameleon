package khameleon.spi.api.translation;

import jakarta.enterprise.context.Dependent;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.InvocationContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2019, 6:50:04 AM
 */
@Dependent
@Translation
public class TranslationInterceptor {

  @AroundInvoke
  public Object intercept(final InvocationContext ic) throws Exception {
    return ic.proceed();
  }

}
