package khameleon.spi.api.translation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.inject.Stereotype;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2019, 6:47:15 AM
 */
@Stereotype
@Translation
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TranslationContext {
}
