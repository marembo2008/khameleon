package khameleon.spi.api.configuration;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 23, 2018, 12:14:49 AM
 */
@Config
@ConfigRoot
public interface ConfigurationService {

    String getConfig();

}
