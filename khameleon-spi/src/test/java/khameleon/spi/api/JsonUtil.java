package khameleon.spi.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import lombok.NonNull;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 23, 2018, 12:35:50 AM
 */
public class JsonUtil {

  @NonNull
  public static String getJson(@NonNull final String path) {
    final InputStream inn = JsonUtil.class.getResourceAsStream(path);
    try {
      return IOUtils.toString(new InputStreamReader(inn));
    } catch (final IOException ex) {
      throw new RuntimeException(ex);
    }
  }

}
