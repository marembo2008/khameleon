
function registerConfigurationNamesTabChangeEvent() {
  $('#configNamesPanel.ui-accordion .ui-accordion-header.ui-state-default').click(function () {
    var style = $(this).attr('style');
    var aria_expanded = $(this).attr('aria-expanded');
    if (aria_expanded === 'true') {
      if (style) {
        if (style.indexOf('conf_') === 0) {
          //send the current tab index.
          $('input#active_config_id').val(style);
          onConfigSelected();
        }
      }
    }
  });
}

function setEditorContent(valueComponentName) {
  var richtexteditor = $('span.khameleon-richtexteditor[data-name="' + valueComponentName + '"]');
  if (richtexteditor && richtexteditor !== 'undefined') {
    var textarea = $(richtexteditor).find('textarea')[0];
    if (textarea && textarea !== 'undefined') {
      var content = $(textarea).val();
      console.log('Content: ' + content);
      $('input[name="' + valueComponentName + '"]').val(content);
    }
  }
}

function updateConfigurationOnSuccess(args) {
  if (!args.validationFailed) {
    updateConfigurations();
  }
}