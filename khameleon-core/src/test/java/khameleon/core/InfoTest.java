package khameleon.core;

import khameleon.core.AdditionalInformation;
import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.Info;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

/**
 *
 * @author marembo
 */
public class InfoTest {

  @Config
  @ConfigTest
  public static interface InfoConfigProcessorTest {

    @Info(name = "information", value = "This is informational")
    boolean isInformation();

  }

  @Test
  public void testOnlyOneInfoParameter() {
    DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    List<Configuration> configurations = initializor.getConfigurations(InfoConfigProcessorTest.class);
    assertThat(configurations, hasSize(1));
    Configuration configuration = configurations.get(0);
    List<AdditionalInformation> additionalInformations = configuration.getAdditionalInformations();
    assertThat(additionalInformations, hasItem(new AdditionalInformation("information", "This is informational")));
  }

}
