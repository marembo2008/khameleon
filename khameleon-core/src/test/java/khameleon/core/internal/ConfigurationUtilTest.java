package khameleon.core.internal;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import khameleon.core.internal.PrimitiveType;

import static khameleon.core.internal.ConfigurationUtil.appendValueToArrayStringIfNotMember;
import static khameleon.core.internal.ConfigurationUtil.capitalize;
import static khameleon.core.internal.ConfigurationUtil.fromArrayString;
import static khameleon.core.internal.ConfigurationUtil.getPrimitiveType;
import static khameleon.core.internal.PrimitiveType.BOOLEAN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class ConfigurationUtilTest {

  @Test
  public void testCapitalize() {
    final String value = "VALUE HANDLE";
    final String expected = "Value Handle";
    final String actual = capitalize(value);
    assertThat(actual, is(expected));
  }

  @Test
  public void testFromArrayString() {
    String[] expected = {"one", "two", "three"};
    String array = Arrays.toString(expected);
    String[] actual = fromArrayString(array);
    assertThat(actual, is(expected));
  }

  @Test
  public void testFromArrayStringWithSpaces() {
    String[] expected = {"PAYAPI", "ADMERIS", "HALLO_THERE"};
    String array = "[ PAYAPI,  ADMERIS,    HALLO_THERE]";
    String[] actual = fromArrayString(array);
    assertThat(actual, is(expected));
  }

  @Test
  public void testAppendValueToArrayStringIfNotMember_Absent() {
    String expected = "[one, two, three]";
    String array = "[one, two]";
    String actual = appendValueToArrayStringIfNotMember(array, "three");
    assertThat(actual, is(expected));
  }

  @Test
  public void testAppendValueToArrayStringIfNotMember_Present() {
    String expected = "[one, two, three]";
    String actual = appendValueToArrayStringIfNotMember(expected, "three");
    assertThat(actual, is(expected));
  }

  @Test
  public void testPrimitiveBooleanTypeMapping() {
    final PrimitiveType expected = BOOLEAN;
    final PrimitiveType actual = getPrimitiveType(boolean.class);
    assertThat(actual, is(expected));
  }

  @Test
  public void testWrapperBooleanTypeMapping() {
    final PrimitiveType expected = BOOLEAN;
    final PrimitiveType actual = getPrimitiveType(Boolean.class);
    assertThat(actual, is(expected));
  }

}
