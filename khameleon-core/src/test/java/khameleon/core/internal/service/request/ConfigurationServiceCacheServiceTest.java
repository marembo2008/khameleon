package khameleon.core.internal.service.request;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.Callable;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;

import anosym.common.Country;
import jakarta.ejb.AsyncResult;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.inject.Instance;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.Config;
import khameleon.core.updatelistener.ConfigurationUpdateListener;
import khameleon.core.updatelistener.ConfigurationUpdaterContext;
import khameleon.core.values.AdditionalParameterKeyValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 13, 2015, 6:49:26 PM
 */
@ExtendWith(MockitoExtension.class)
public class ConfigurationServiceCacheServiceTest {

	private static final String CANONICAL_NAMESPACE = "canonical.namespace";

	private static final String CONFIG_NAME = "configName";

	private static final Class<?> CONFIG_INTERFACE = TestConfigurationService.class;

	private static final Method CONFIG_METHOD;

	static {
		try {
			CONFIG_METHOD = CONFIG_INTERFACE.getDeclaredMethod("getValue");
		} catch (final NoSuchMethodException | SecurityException ex) {
			throw Throwables.propagate(ex);
		}
	}

	private static final List<AdditionalParameterKeyValue> ADDITIONAL_PARAMETER_KEY_VALUES = ImmutableList.of();

	@Mock
	private Instance<ConfigurationUpdateListener> configurationUpdateListeners;

	@Mock
	private ManagedExecutorService managedExecutorService;

	@Mock
	private ConfigurationService configurationService;

	@Spy
	@InjectMocks
	private ConfigurationServiceCacheService configurationServiceCacheService;

	@BeforeEach
	public void setUp() {
		configurationServiceCacheService.init();
	}

	@Test
	public void confirmOriginalConfigurationRequestContextCached() {
		final ApplicationContext originalApplicationContext = new ApplicationContext(ApplicationMode.INTEGRATION, null,
				null, Country.KENYA);
		final ConfigValueCacheKeyContext requestCacheKeyContext = new ConfigValueCacheKeyContext(
				originalApplicationContext, CONFIG_INTERFACE, CONFIG_METHOD, CANONICAL_NAMESPACE,
				ADDITIONAL_PARAMETER_KEY_VALUES, new Object[0], configurationService);
		final ApplicationContext definedApplicationContext = new ApplicationContext(ApplicationMode.INTEGRATION, null,
				null, null);
		final ConfigValueCacheKeyContext definedCacheKeyContext = new ConfigValueCacheKeyContext(
				definedApplicationContext, CONFIG_INTERFACE, CONFIG_METHOD, CANONICAL_NAMESPACE,
				ADDITIONAL_PARAMETER_KEY_VALUES, new Object[0], configurationService);
		final Object configValue = "config-value";
		doReturn(configValue).when(configurationServiceCacheService).getValue(definedCacheKeyContext);

		final Object requestConfigValue = configurationServiceCacheService.getCachedValue(requestCacheKeyContext);
		assertThat(requestConfigValue, is(configValue));
		verify(configurationServiceCacheService, times(1)).getValue(requestCacheKeyContext);
		verify(configurationServiceCacheService, times(1)).getValue(definedCacheKeyContext);

		// If we do another request, the cache should not call getValue, but should
		// return the cached value
		// the number of getValue calls should not change
		final Object requestCachedConfigValue = configurationServiceCacheService.getCachedValue(requestCacheKeyContext);
		assertThat(requestCachedConfigValue, is(configValue));
		verify(configurationServiceCacheService, times(1)).getValue(requestCacheKeyContext);
		verify(configurationServiceCacheService, times(1)).getValue(definedCacheKeyContext);
	}

	@Test
	@Disabled
	public void verifySchedulerForcesRefresh() {
		final ConfigurationUpdateListener configurationUpdateListener = mock(ConfigurationUpdateListener.class);
		when(configurationUpdateListeners.iterator())
				.thenReturn(ImmutableList.<ConfigurationUpdateListener>of(configurationUpdateListener).iterator());
		when(managedExecutorService.submit(any(Callable.class))).thenAnswer((InvocationOnMock invocation) -> {
			final Callable call = invocation.getArgument(0, Callable.class);
			return new AsyncResult<>(call.call());
		});

		final ApplicationContext originalApplicationContext = new ApplicationContext(ApplicationMode.INTEGRATION, null,
				null, Country.KENYA);
		final ConfigValueCacheKeyContext requestCacheKeyContext = new ConfigValueCacheKeyContext(
				originalApplicationContext, CONFIG_INTERFACE, CONFIG_METHOD, CANONICAL_NAMESPACE,
				ADDITIONAL_PARAMETER_KEY_VALUES, new Object[0], configurationService);
		doReturn("new.value").when(configurationServiceCacheService).getValue(requestCacheKeyContext);
		configurationServiceCacheService.getCachedValue(requestCacheKeyContext);
		configurationServiceCacheService.refresh();

		verify(configurationUpdateListener, times(1)).onConfigurationUpdated(any(ConfigurationUpdaterContext.class));
	}

	@Config
	static interface TestConfigurationService {

		String getValue();

	}

}
