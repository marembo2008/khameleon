package khameleon.core.internal.service.request;

import static khameleon.core.ApplicationMode.ALPHA;
import static khameleon.core.ApplicationMode.BETA;
import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static khameleon.core.ApplicationMode.INTEGRATION;
import static khameleon.core.ApplicationMode.LIVE;
import static khameleon.core.ApplicationMode.LOCAL;
import static khameleon.core.ApplicationMode.RELEASE;
import static khameleon.core.ApplicationMode.TEST;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import anosym.common.Country;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.applicationcontext.ApplicationContextService;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 21, 2017, 10:31:18 AM
 */
public class ApplicationContextProviderTest {

	private ApplicationContextService applicationContextService;

	private ApplicationContextProvider applicationContextProvider;

	@BeforeEach
	public void setUp() {
		applicationContextService = mock(ApplicationContextService.class);
		this.applicationContextProvider = new ApplicationContextProvider(applicationContextService);
	}

	@Test
	public void verifyDefinedApplicationContextMatches() {
		final ApplicationContext applicationContext = new ApplicationContext();
		applicationContext.setApplicationMode(ApplicationMode.LIVE);
		when(applicationContextService.getApplicationContext()).thenReturn(applicationContext);

		final ApplicationContext configApplicationContext = applicationContextProvider
				.getApplicationContext(AppContextConfigurationService.class);
		assertThat(configApplicationContext, is(applicationContext));
	}

	@Test
	public void verifyDefinedApplicationContextIsParentMatches() {
		final ApplicationContext applicationContext = new ApplicationContext();
		applicationContext.setApplicationMode(LIVE);
		applicationContext.setCountryCode(Country.KENYA);
		when(applicationContextService.getApplicationContext()).thenReturn(applicationContext);

		final ApplicationContext configApplicationContext = applicationContextProvider
				.getApplicationContext(AppContextConfigurationService.class);
		final ApplicationContext expectedApplicationContext = new ApplicationContext();
		expectedApplicationContext.setApplicationMode(LIVE);
		assertThat(configApplicationContext, is(expectedApplicationContext));
	}

	@Test
	public void verifyNoApplicationContextDefined() {
		final ApplicationContext applicationContext = new ApplicationContext();
		applicationContext.setApplicationMode(ApplicationMode.LIVE);
		when(applicationContextService.getApplicationContext()).thenReturn(applicationContext);

		final ApplicationContext configApplicationContext = applicationContextProvider
				.getApplicationContext(NoAppContextConfigurationService.class);
		assertThat(configApplicationContext, is(applicationContext));
	}

	@Test
	public void verifyInvalidApplicationContextDefined() {
		final ApplicationContext applicationContext = new ApplicationContext();
		applicationContext.setApplicationMode(ApplicationMode.LIVE);
		when(applicationContextService.getApplicationContext()).thenReturn(applicationContext);

		assertThrows(IllegalStateException.class,
				() -> applicationContextProvider.getApplicationContext(CountryAppContextConfigurationService.class));
	}

	@Config
	@AppContext(applicationModes = { LOCAL, DEVELOPMENT, LIVE, TEST, ALPHA, BETA, INTEGRATION, RELEASE })
	private static interface AppContextConfigurationService {
	}

	@Config
	@AppContext(applicationModes = { LOCAL, DEVELOPMENT, LIVE, TEST, ALPHA, BETA, INTEGRATION,
			RELEASE }, countries = Country.KENYA)
	private static interface CountryAppContextConfigurationService {
	}

	@Config
	private static interface NoAppContextConfigurationService {
	}

}
