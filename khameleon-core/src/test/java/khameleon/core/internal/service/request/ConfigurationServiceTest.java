package khameleon.core.internal.service.request;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.inject.Instance;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.DefaultConfigurator;
import khameleon.core.Namespace;
import khameleon.core.annotations.CalendarFormat;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.DynamicDefault;
import khameleon.core.annotations.RequiredThrows;
import khameleon.core.annotations.ConfigOption.ConfigName;
import khameleon.core.applicationcontext.DefaultApplicationContextService;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.localcache.AbstractLocalCacheSupportTest;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.internal.service.request.ConfigurationServiceCacheServiceLookup;
import khameleon.core.internal.service.request.ConfigurationServiceHelperImpl;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.util.Collections.emptyList;
import static khameleon.core.internal.service.request.ConfigurationServiceCacheServiceLookup.cleanConfigurationServiceCacheService;
import static khameleon.core.manager.ConfigurationManager.getConfigInstance;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class ConfigurationServiceTest extends AbstractLocalCacheSupportTest {

  private static final String SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED = "anosym.khameleon.localcache.localCacheDisabled";

  private final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();

  @Mock
  private ConfigurationRequestService configurationRequestService;

  @Spy
  @InjectMocks
  private ConfigurationServiceHelperImpl configurationServiceHelper;

  @Mock
  private ApplicationContextEnabledConfig applicationContextConfigurationService;

  @Mock
  private Instance<ApplicationContextEnabledConfig> applicationContextConfigurationServices;

  @InjectMocks
  private DefaultApplicationContextService applicationContextService;

  @BeforeEach
  public void setUp() {
    when(applicationContextConfigurationServices.isUnsatisfied()).thenReturn(false);
    when(applicationContextConfigurationServices.get()).thenReturn(applicationContextConfigurationService);
    when(applicationContextConfigurationService.isApplicationContextEnabled(anyInt())).thenReturn(true);

    final ManagedExecutorService managedExecutorService = mock(ManagedExecutorService.class);
    ConfigurationServiceCacheServiceLookup.setConfigurationServiceCacheService(managedExecutorService);

    lenient().doReturn(localCacheService).when(configurationServiceHelper).getLocalCacheService();
    lenient().doReturn(configurationRequestService).when(configurationServiceHelper).getConfigurationRequestService();
  }

  @AfterEach
  public void tearDownDefault() {
    cleanConfigurationServiceCacheService();
  }

  @BeforeAll
  public static void setupLocalCache() {
    System.setProperty(SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED, "true");
  }

  @AfterAll
  public static void tearDownLocalCache() {
    final Properties properties = System.getProperties();
    properties.remove(SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED);
    System.setProperties(properties);
  }

  @Test
  public void testDefaultCalendatConfiguration() {
    setupDefaultCalendarConfiguration();
    final DefaultCalendarConfiguration defaultCalendarConfiguration
            = getConfigInstance(applicationContextService, DefaultCalendarConfiguration.class,
                                configurationServiceHelper);
    final Calendar value = defaultCalendarConfiguration.getValue();
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    expected.set(2014, 9, 2, 14, 0, 0);
    assertThat(value, is(expected));
  }

  @Test
  public void testCalendatFormatConfiguration() {
    setupCalendarFormatConfiguration();
    final CalendarFormatConfiguration calendarFormatConfiguration
            = getConfigInstance(applicationContextService, CalendarFormatConfiguration.class,
                                configurationServiceHelper);
    final Calendar value = calendarFormatConfiguration.getValue();
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    expected.set(2014, 01, 23, 0, 0, 0); //2014-23-02
    assertThat(value, is(expected));
  }

  @Test
  public void testDefaultBigDecimalConfiguration() {
    setupDefaultBigDecimalConfiguration();
    final DefaultBigDecimalConfiguration defaultCalendarConfiguration
            = getConfigInstance(applicationContextService, DefaultBigDecimalConfiguration.class,
                                configurationServiceHelper);
    final BigDecimal value = defaultCalendarConfiguration.getValue();
    assertThat(value, is(new BigDecimal("456.89")));
  }

  @Test
  public void testDefaultPrimitiveConfiguration() {
    setupDefaultPrimitiveConfiguration();
    final DefaultPrimitiveConfiguration defaultPrimitiveConfiguration
            = getConfigInstance(applicationContextService, DefaultPrimitiveConfiguration.class,
                                configurationServiceHelper);
    final int value = defaultPrimitiveConfiguration.getValue();
    assertThat(value, is(9090909));
  }

  @Test
  public void testArrayConfiguration() {
    setupArrayConfiguration();
    final ArrayConfiguration arrayConfiguration
            = getConfigInstance(applicationContextService, ArrayConfiguration.class, configurationServiceHelper);
    final String[] value = arrayConfiguration.getValue();
    assertThat(value, arrayContainingInAnyOrder("hundle", "janki", "makilop"));
  }

  @Test
  public void testNonnullArrayConfiguration() {
    setupNonnullArrayConfiguration();
    final NonnullArrayConfiguration arrayConfiguration
            = getConfigInstance(applicationContextService, NonnullArrayConfiguration.class,
                                configurationServiceHelper);
    final String[] value = arrayConfiguration.getValue();
    assertThat(value, is(notNullValue()));
    assertThat(value.length, is(0));
  }

  @Test
  public void testDefaultConfiguration() {
    setupDefaultConfiguration();
    final DefaultConfiguration defaultConfiguration
            = getConfigInstance(applicationContextService, DefaultConfiguration.class, configurationServiceHelper);
    final String value = defaultConfiguration.getValue();
    assertThat(value, is("default-value-9292929"));
  }

  @Test
  public void testConfigOptionConfiguration() {
    setupConfigOptionConfiguration();
    final ConfigOptionConfiguration configOptionConfiguration
            = getConfigInstance(applicationContextService, ConfigOptionConfiguration.class,
                                configurationServiceHelper);
    final String value = configOptionConfiguration.getValue("909090-909090");
    assertThat(value, is("909090-909090 this is an option"));
  }

  @Test
  public void testConfigOptionFromConfigConfiguration() {
    setupConfigOptionFromConfigConfiguration();
    final ConfigOptionFromConfigConfiguration configOptionFromConfigConfiguration
            = getConfigInstance(applicationContextService, ConfigOptionFromConfigConfiguration.class,
                                configurationServiceHelper);
    final String value = configOptionFromConfigConfiguration.getValue();
    assertThat(value, is("67 this is an option with config-param as an option"));
  }

  @Test
  public void testConfigParamConfiguration() {
    setupConfigParamConfiguration();
    final ConfigParamConfiguration configParamConfiguration
            = getConfigInstance(applicationContextService, ConfigParamConfiguration.class,
                                configurationServiceHelper);
    final String value = configParamConfiguration.getValue("this-is-a-config-param");
    assertThat(value, is("this-is-a-config-param"));
  }

  @Test
  public void testEnumConfigParamConfiguration() {
    setupEnumConfigParamConfiguration();
    final EnumConfigParamConfiguration configParamConfiguration
            = getConfigInstance(applicationContextService, EnumConfigParamConfiguration.class,
                                configurationServiceHelper);
    final String value = configParamConfiguration.getValue(Value.ONE);
    assertThat(value, is("this-is-an-enum-config-param"));
    final String valuetwo = configParamConfiguration.getValue(Value.TWO);
    assertThat(valuetwo, is(nullValue()));
  }

  @Test
  public void testDefaultEnumConfigParamConfiguration() {
    setupDefaultEnumConfigParamConfiguration();
    final DefaultEnumConfigParamConfiguration configParamConfiguration
            = getConfigInstance(applicationContextService, DefaultEnumConfigParamConfiguration.class,
                                configurationServiceHelper);
    final String value = configParamConfiguration.getValue(Value.ONE);
    assertThat(value, is("this-is-an-enum-config-param"));
    final String valuetwo = configParamConfiguration.getValue(Value.TWO);
    assertThat(valuetwo, is("default-value-when-not-found"));
  }

  @Test
  public void testDefaultEnumConfigParamConfigurationCached() throws Exception {
    final ConfigurationValue cValue = setupDefaultEnumConfigParamConfiguration();
    final DefaultEnumConfigParamConfiguration configParamConfiguration
            = getConfigInstance(applicationContextService, DefaultEnumConfigParamConfiguration.class,
                                configurationServiceHelper);
    final ApplicationContext applicationContext = cValue.getContext();
    final Namespace namespace = cValue.getConfiguration().getNamespace();
    final List<AdditionalParameterKeyValue> keyValues
            = ImmutableList.of(new AdditionalParameterKeyValue("option", "TWO"));
    final String configName = cValue.getConfiguration().getConfigName();

    final String valuetwo = configParamConfiguration.getValue(Value.TWO);
    assertThat(valuetwo, is("default-value-when-not-found"));
    final String valuetwo1 = configParamConfiguration.getValue(Value.TWO);
    assertThat(valuetwo1, is("default-value-when-not-found"));

    // The previously parsed value is cached. whether it is default or not.
    // Unless this test takes more than ten minutes to execute!
    verify(configurationRequestService, times(1))
            .findConfigurationValue(applicationContext, namespace.getCanonicalName(), configName, keyValues);
  }

  @Test
  public void testDefaultEnumConfiguration() {
    setupDefaultEnumConfiguration();
    final DefaultEnumConfiguration defaultEnumConfiguration
            = getConfigInstance(applicationContextService, DefaultEnumConfiguration.class,
                                configurationServiceHelper);
    final Value value = defaultEnumConfiguration.getValue();
    assertThat(value, is(Value.TWO));
  }

  @Test
  public void testArrayEnumConfiguration() {
    setupArrayEnumConfiguration();
    final ArrayEnumConfiguration arrayEnumConfiguration
            = getConfigInstance(applicationContextService, ArrayEnumConfiguration.class, configurationServiceHelper);
    final Value[] value = arrayEnumConfiguration.getValue();
    assertThat(value, arrayContainingInAnyOrder(Value.THREE, Value.TWO));
  }

  @Test
  public void testDynamicDefaultConfiguration() {
    setupDynamicDefaultConfiguration();
    final DynamicDefaultConfiguration dynamicDefaultConfiguration
            = getConfigInstance(applicationContextService, DynamicDefaultConfiguration.class,
                                configurationServiceHelper);
    final String defaultType1 = dynamicDefaultConfiguration.getDynamicDefault(DefaultType.TYPE_1);
    assertThat(defaultType1, is(DefaultType.TYPE_1.toString()));
    final String defaultType2 = dynamicDefaultConfiguration.getDynamicDefault(DefaultType.TYPE_2);
    assertThat(defaultType2, is(DefaultType.TYPE_2.toString()));
  }

  @Test
  public void testInvalidDynamicDefaultConfiguration() {
    setupDynamicDefaultConfiguration();
    final DynamicDefaultConfiguration dynamicDefaultConfiguration
            = getConfigInstance(applicationContextService, DynamicDefaultConfiguration.class,
                                configurationServiceHelper);
    assertThrows(NullPointerException.class,
                 () -> dynamicDefaultConfiguration.getInvalidDynamicDefault(DefaultType.TYPE_1));
  }

  @Test
  public void testMainConfigDefaultName() {
    setupMainConfig();
    final MainConfig mainConfig = getConfigInstance(applicationContextService, MainConfig.class,
                                                    configurationServiceHelper);
    final ChildConfig childConfig = mainConfig.getChildConfig();
    final String name = childConfig.getName();
    assertThat(name, is("default-name"));
  }

  @Test
  public void testMainConfigSetName() {
    setupMainConfigSetName();
    final MainConfig mainConfig = getConfigInstance(applicationContextService, MainConfig.class,
                                                    configurationServiceHelper);
    final ChildConfig childConfig = mainConfig.getChildConfig();
    final String name = childConfig.getName();
    assertThat(name, is("set-name"));
  }

  @Test
  public void testRequiredThrows() {
    final RequiredThrowsConfig requiredThrowsConfig
            = getConfigInstance(applicationContextService, RequiredThrowsConfig.class, configurationServiceHelper);
    assertThrows(RequiredThrowsConfig.RequiredException.class,
                 () -> requiredThrowsConfig.getRequired());
  }

  @Test
  public void testRequiredThrowsRequired() {
    setupRequiredThrows();
    final RequiredThrowsConfig requiredThrowsConfig = getConfigInstance(applicationContextService,
                                                                        RequiredThrowsConfig.class,
                                                                        configurationServiceHelper);
    final String required = requiredThrowsConfig.getRequired();
    assertThat(required, is("required"));
  }

  private void setupDefaultPrimitiveConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DefaultPrimitiveConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration, "9090909");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface DefaultPrimitiveConfiguration {

    int getValue();

  }

  private void setupDefaultCalendarConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DefaultCalendarConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "2014-10-02 14:00:00");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface DefaultCalendarConfiguration {

    Calendar getValue();

  }

  private void setupCalendarFormatConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(CalendarFormatConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration, "2014-23-02");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface CalendarFormatConfiguration {

    @CalendarFormat("yyyy-dd-MM")
    Calendar getValue();

  }

  private void setupDefaultBigDecimalConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DefaultBigDecimalConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration, "456.89");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface DefaultBigDecimalConfiguration {

    BigDecimal getValue();

  }

  private void setupDefaultConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DefaultConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "default-value-9292929");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface DefaultConfiguration {

    String getValue();

  }

  private void setupConfigOptionConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigOptionConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "{0} this is an option");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface ConfigOptionConfiguration {

    String getValue(@ConfigOption(index = 0) final String option);

  }

  private void setupConfigOptionFromConfigConfiguration() {
    final List<Configuration> configurations
            = initializer.getConfigurations(ConfigOptionFromConfigConfiguration.class);
    //there are two configurations
    final String vConfigName = "value";
    final String iConfigName = "index";
    final Configuration valueConf = getConfiguration(configurations, vConfigName);
    final Configuration indexConf = getConfiguration(configurations, iConfigName);
    final Namespace valueNamespace = valueConf.getNamespace();
    final ConfigurationValue value = new ConfigurationValue(applicationContextService.getApplicationContext(),
                                                            valueConf,
                                                            "{0} this is an option with config-param as an option");
    final ConfigurationValue index = new ConfigurationValue(applicationContextService.getApplicationContext(),
                                                            indexConf, "67");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = valueNamespace.getCanonicalName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, vConfigName, emptyList()))
            .thenReturn(value);
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, iConfigName, emptyList()))
            .thenReturn(index);
  }

  private Configuration getConfiguration(@Nonnull final List<Configuration> confs, @Nonnull final String configName) {
    for (Configuration conf : confs) {
      if (conf.getConfigName().equals(configName)) {
        return conf;
      }
    }
    throw new IllegalStateException("Configuration not found: " + configName);
  }

  @Config
  public static interface ConfigOptionFromConfigConfiguration {

    @ConfigOption(index = 0, configName = @ConfigName("index"))
    String getValue();

    int getIndex();

  }

  private void setupConfigParamConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigParamConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "this-is-a-config-param");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    final AdditionalParameterKeyValue keyValue = new AdditionalParameterKeyValue("option", "this-is-a-config-param");
    configurationValue.updateAdditionalParameters(ImmutableList.of(keyValue));
    final List<AdditionalParameterKeyValue> keyValues = ImmutableList.of(keyValue);
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, keyValues))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface ConfigParamConfiguration {

    String getValue(@ConfigParam(name = "option") final String option);

  }

  private void setupEnumConfigParamConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(EnumConfigParamConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "this-is-an-enum-config-param");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    final AdditionalParameterKeyValue keyValue = new AdditionalParameterKeyValue("option", "ONE");
    configurationValue.updateAdditionalParameters(ImmutableList.of(keyValue));
    final List<AdditionalParameterKeyValue> keyValues = ImmutableList.of(keyValue);
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, keyValues))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface EnumConfigParamConfiguration {

    String getValue(@ConfigParam(name = "option") final Value option);

  }

  private ConfigurationValue setupDefaultEnumConfigParamConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(
            DefaultEnumConfigParamConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue = getDefaultEnumConfigParamConfiguration();
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    final AdditionalParameterKeyValue keyValue = new AdditionalParameterKeyValue("option", "ONE");
    configurationValue.updateAdditionalParameters(ImmutableList.of(keyValue));
    final List<AdditionalParameterKeyValue> keyValues = ImmutableList.of(keyValue);
    lenient().when(configurationRequestService
            .findConfigurationValue(contextId, canonicalNamespace, configName, keyValues)).
            thenReturn(configurationValue);
    return configurationValue;
  }

  private ConfigurationValue getDefaultEnumConfigParamConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(
            DefaultEnumConfigParamConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "this-is-an-enum-config-param");
    return configurationValue;
  }

  @Config
  public static interface DefaultEnumConfigParamConfiguration {

    @Default("default-value-when-not-found")
    String getValue(@ConfigParam(name = "option") final Value option);

  }

  private void setupArrayConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ArrayConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "[hundle, janki, makilop]");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface ArrayConfiguration {

    String[] getValue();

  }

  private void setupNonnullArrayConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(NonnullArrayConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue = new ConfigurationValue(applicationContextService.
            getApplicationContext(), configuration);
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  public static interface NonnullArrayConfiguration {

    @Default("[]")
    String[] getValue();

  }

  private void setupDefaultEnumConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DefaultEnumConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration, "TWO");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  private void setupArrayEnumConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ArrayEnumConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "[THREE, TWO]");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  public static enum Value {

    ONE, TWO, THREE;

  }

  @Config
  public static interface DefaultEnumConfiguration {

    Value getValue();

  }

  @Config
  public static interface ArrayEnumConfiguration {

    Value[] getValue();

  }

  private void setupDynamicDefaultConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(DynamicDefaultConfiguration.class);
    //there will only be one value.
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = configuration.getNamespace();
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    final AdditionalParameterKeyValue keyValue = new AdditionalParameterKeyValue("optidefaultTypeon", "TYPE_1");
    final List<AdditionalParameterKeyValue> keyValues = ImmutableList.of(keyValue);
    lenient().when(configurationRequestService
            .findConfigurationValue(contextId, canonicalNamespace, configName, keyValues))
            .thenReturn(null);
  }

  @ConfigTest
  @Config
  public static interface DynamicDefaultConfiguration {

    @DynamicDefault(DefaultConfiguratorTestImpl.class)
    String getDynamicDefault(@ConfigParam(name = "defaultType") final DefaultType defaultType);

    @DynamicDefault(InvalidDefaultConfiguratorTestImpl.class)
    String getInvalidDynamicDefault(@ConfigParam(name = "defaultType") final DefaultType defaultType);

  }

  public static enum DefaultType {

    TYPE_1,
    TYPE_2;

  }

  public static class DefaultConfiguratorTestImpl implements DefaultConfigurator {

    @Override
    public String getDefault(Method configMethod, Object[] args) {
      final Object defaultType = args[0];
      return String.valueOf(defaultType);
    }

  }

  public static class InvalidDefaultConfiguratorTestImpl implements DefaultConfigurator {

    @Override
    public String getDefault(Method configMethod, Object[] args) {
      return null;
    }

  }

  private void setupMainConfig() {
    final List<Configuration> configurations = initializer.getConfigurations(MainConfig.class);
    //there will only be one value.
    assertThat(configurations, hasSize(1));
    final Configuration configuration = configurations.get(0);
    final Namespace namespace = checkNotNull(configuration.getNamespace(),
                                             "The configuration.getNamespace() must not be null");
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = checkNotNull(configuration.getConfigName(),
                                           "The configuration.getConfigName() must not be null");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(null);
  }

  private void setupMainConfigSetName() {
    final List<Configuration> configurations = initializer.getConfigurations(MainConfig.class);
    //there will only be one value.
    assertThat(configurations, hasSize(1));
    final Configuration configuration = configurations.get(0);
    final ConfigurationValue value = new ConfigurationValue(applicationContextService.getApplicationContext(),
                                                            configuration, "set-name");
    final Namespace namespace = configuration.getNamespace();
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(value);
  }

  private void setupRequiredThrows() {
    final List<Configuration> configurations = initializer.getConfigurations(RequiredThrowsConfig.class);
    //there will only be one value.
    assertThat(configurations, hasSize(1));
    final Configuration configuration = configurations.get(0);
    final ConfigurationValue value = new ConfigurationValue(applicationContextService.getApplicationContext(),
                                                            configuration, "required");
    final Namespace namespace = configuration.getNamespace();
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(value);
  }

  @Config
  @ConfigTest
  public static interface MainConfig {

    ChildConfig getChildConfig();

  }

  @Config
  @ConfigTest
  public static interface ChildConfig {

    @Default("default-name")
    String getName();

  }

  @Config
  @ConfigTest
  public static interface RequiredThrowsConfig {

    @Nonnull
    @RequiredThrows(RequiredException.class)
    String getRequired();

    static class RequiredException extends RuntimeException {

      private static final long serialVersionUID = 1642592505655854622L;

      public RequiredException(String message) {
        super(message);
      }

    }

  }

}
