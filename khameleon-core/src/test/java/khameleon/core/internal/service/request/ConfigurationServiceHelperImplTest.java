package khameleon.core.internal.service.request;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.internal.service.request.ConfigurationServiceHelperImpl;
import khameleon.core.values.ConfigurationValue;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_LOCAL_CACHE_VALUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 19, 2015, 7:06:05 PM
 */
public class ConfigurationServiceHelperImplTest {

  private final ConfigurationServiceHelperImpl configurationServiceHelper = new ConfigurationServiceHelperImpl();

  @Test
  public void testConfigurationValueNotAcceptedForCacheIfNull() {
    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(null);
    assertThat(acceptCachedValue, is(false));
  }

  @Test
  public void testConfigurationValueAcceptedForCacheIfNoConfigValueCacheIsDefined() {
    final Configuration configuration = mock(Configuration.class);
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.
            <ConfigurationData>absent());

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(true));
  }

  @Test
  public void testConfigurationValueAcceptedForCacheIfConfigValueCacheIsDefinedAndConfigValueIsNotNullNorEmpty() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_NULL");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfigValue()).thenReturn("non-null-value");
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(true));
  }

  @Test
  public void testConfigurationValueNotAcceptedForCacheIfConfigValueCacheIsIgnoreWhenNullAndConfigValueIsNull() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_NULL");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(false));
  }

  @Test
  public void testConfigurationValueAcceptedForCacheIfConfigValueCacheIsIgnoreWhenNullAndConfigValueIsEmpty() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_NULL");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfigValue()).thenReturn("");
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(true));
  }

  @Test
  public void testConfigurationValueNotAcceptedForCacheIfConfigValueCacheIsIgnoreWhenEmptyAndConfigValueIsNull() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_EMPTY");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(false));
  }

  @Test
  public void testConfigurationValueNotAcceptedForCacheIfConfigValueCacheIsIgnoreWhenEmptyAndConfigValueIsEmpty() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_EMPTY");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfigValue()).thenReturn("");
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(false));
  }

  @Test
  public void testConfigurationValueNotAcceptedForCacheIfConfigValueCacheIsIgnoreWhenEmptyAndConfigValueIsEmptyArray() {
    final Configuration configuration = mock(Configuration.class);
    final ConfigurationData configurationData = mock(ConfigurationData.class);
    when(configurationData.getDataValue()).thenReturn("IGNORE_WHEN_EMPTY");
    when(configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE)).thenReturn(Optional.of(
            configurationData));

    final ConfigurationValue configurationValue = mock(ConfigurationValue.class);
    when(configurationValue.getConfigValue()).thenReturn("[]");
    when(configurationValue.getConfiguration()).thenReturn(configuration);

    final boolean acceptCachedValue = configurationServiceHelper.acceptCachedValue(configurationValue);
    assertThat(acceptCachedValue, is(false));
  }

}
