package khameleon.core.internal.localcache;

import com.google.common.collect.ImmutableList;

import khameleon.core.AdditionalParameter;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;

import static khameleon.core.internal.localcache.LocalCacheServiceImpl.removeVersions;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 *
 * @author mochieng
 */
public class LocalCacheServiceTest extends AbstractLocalCacheSupportTest {

  private AdditionalParameter additionalParameter(final String key, final String value) {
    return new AdditionalParameter(key, value);
  }

  private List<AdditionalParameter> additionalParameters() {
    return ImmutableList.of(additionalParameter("my-key", "my-value"),
                            additionalParameter("my-key1", "my-value1"),
                            additionalParameter("my-key2", "my-value2"));
  }

  private ConfigurationValue configurationValueWithoutAdditionalParameters() {
    final ApplicationContext appCtx = new ApplicationContext();
    final Namespace namespace = Namespace.builder().withSimpleName("anosym.khameleon.test").build();
    final String configName = "configNameTest-9939393939";
    final String configType = "java.lang.String";
    final Configuration conf = new Configuration(namespace, configName, configType);
    final ConfigurationValue confValue = new ConfigurationValue(appCtx, conf);
    return confValue;
  }

  private ConfigurationValue configurationValueWithAdditionalParameters() {
    final ApplicationContext appCtx = new ApplicationContext();
    final Namespace namespace = Namespace.builder().withSimpleName("anosym.khameleon.test").build();
    final String configName = "configNameTest-9939393939";
    final String typeClass = "java.lang.String";
    final Configuration conf = new Configuration(namespace, configName, typeClass);
    conf.addAdditionalParameters(additionalParameters());
    final ConfigurationValue confValue = new ConfigurationValue(appCtx, conf);
    return confValue;
  }

  private ConfigurationValue localCacheConfigurationValueWithoutAdditionalParameters() throws Exception {
    final ConfigurationValue confValue0 = configurationValueWithoutAdditionalParameters();
    final ConfigurationValue confValue = localCacheService.findConfiguration(
            confValue0.getContext().getContextId(),
            confValue0.getConfiguration().getNamespace().getCanonicalName(),
            confValue0.getConfiguration().getConfigName(),
            ImmutableList.<AdditionalParameterKeyValue>of());
    return confValue;
  }

  private ConfigurationValue localCacheConfigurationValueWithAdditionalParameters() throws Exception {
    final ConfigurationValue confValue0 = configurationValueWithAdditionalParameters();
    final ConfigurationValue confValue = localCacheService.findConfiguration(
            confValue0.getContext().getContextId(),
            confValue0.getConfiguration().getNamespace().getCanonicalName(),
            confValue0.getConfiguration().getConfigName(),
            confValue0.getAdditionalParameterKeyValues());
    return confValue;
  }

  private File configFileWithoutAdditionalParameters() throws Exception {
    final ConfigurationValue confValue = configurationValueWithoutAdditionalParameters();
    final Configuration conf = confValue.getConfiguration();
    final int contextId = confValue.getContext().getContextId();
    final String canonicalName = conf.getNamespace().getCanonicalName();
    return localCacheService.getConfigFile(contextId, canonicalName, confValue);
  }

  private File configFileWithAdditionalParameters() throws Exception {
    final ConfigurationValue confValue = configurationValueWithAdditionalParameters();
    final Configuration conf = confValue.getConfiguration();
    final int contextId = confValue.getContext().getContextId();
    final String canonicalName = conf.getNamespace().getCanonicalName();
    return localCacheService.getConfigFile(contextId, canonicalName, confValue);
  }

  private ConfigurationValue saveConfigurationValueWithoutAdditionalParameters() throws Exception {
    final ConfigurationValue conf = configurationValueWithoutAdditionalParameters();
    localCacheService.saveConfiguration(conf.getContext().getContextId(),
                                        conf.getConfiguration().getNamespace().getCanonicalName(),
                                        conf);
    return conf;
  }

  private ConfigurationValue saveConfigurationValueWithAdditionalParameters() throws Exception {
    final ConfigurationValue confValue = configurationValueWithAdditionalParameters();
    localCacheService.saveConfiguration(confValue.getContext().getContextId(),
                                        confValue.getConfiguration().getNamespace().getCanonicalName(),
                                        confValue);
    return confValue;
  }

  @Test
  public void testConfigurationValueWithoutAdditionalParametersDoesNotExists() throws Exception {
    final ConfigurationValue nullValue = localCacheConfigurationValueWithoutAdditionalParameters();
    final File notExistFile = configFileWithoutAdditionalParameters();

    assertThat(nullValue, is(nullValue()));
    assertThat(notExistFile.exists(), is(false));
  }

  @Test
  public void testConfigurationValueWithAdditionalParametersDoesNotExists() throws Exception {
    final ConfigurationValue nullValue = localCacheConfigurationValueWithAdditionalParameters();
    final File notExistFile = configFileWithAdditionalParameters();

    assertThat(nullValue, is(nullValue()));
    assertThat(notExistFile.exists(), is(false));
  }

  @Test
  public void testConfigurationValueWithoutAdditionalParametersExists() throws Exception {
    final ConfigurationValue savedValue = saveConfigurationValueWithoutAdditionalParameters();
    final ConfigurationValue exisitingValue = localCacheConfigurationValueWithoutAdditionalParameters();
    final File existingFile = configFileWithoutAdditionalParameters();

    assertThat(exisitingValue, is(notNullValue()));
    assertThat(existingFile.exists(), is(true));
    assertThat(savedValue, is(exisitingValue));
  }

  @Test
  public void testConfigurationValueWithAdditionalParametersExists() throws Exception {
    final ConfigurationValue savedValue = saveConfigurationValueWithAdditionalParameters();
    final ConfigurationValue exisitingValue = localCacheConfigurationValueWithAdditionalParameters();
    final File existingFile = configFileWithAdditionalParameters();

    assertThat(exisitingValue, is(notNullValue()));
    assertThat(existingFile.exists(), is(true));
    assertThat(savedValue, is(exisitingValue));
  }

  @Test
  public void testRemoveVersionsIfExists() {
    final String moduleName = "khameleon-2.0.1-SNAPSHOT";
    final String correctName = removeVersions(moduleName);
    assertThat(correctName, is("khameleon"));
  }

  @Test
  public void testRemove2DigitVersionsIfExists() {
    final String moduleName = "afrocoin-main-1.1-SNAPSHOT.khameleon";
    final String correctName = removeVersions(moduleName);
    assertThat(correctName, is("afrocoin-main"));
  }

}
