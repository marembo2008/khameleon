package khameleon.core.internal.localcache;

import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.localcache.InMemoryCache;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.localcache.ConfigContextKey;
import khameleon.core.internal.localcache.InMemoryCacheService;
import khameleon.core.internal.localcache.LocalCacheServiceHelper;
import khameleon.core.values.ConfigurationValue;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.internal.localcache.InMemoryCacheService.createInMemoryCacheService;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:53:08 AM
 */
public class InMemoryCacheServiceTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  private InMemoryCacheService inMemoryCacheService;

  @BeforeEach
  public void initialize() {
    inMemoryCacheService = createInMemoryCacheService();
    //wait for 1 second to start the scheduler.
    waitForSchedulerToRun();
  }

  @Test
  public void testExpiresImmediately() {
    final Configuration configuration = configurationInitializer.getConfigurations(
            ImmediatelyExpiresInMemoryCacheConfiguration.class).get(0);
    final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT, configuration);
    final ConfigContextKey configContextKey = LocalCacheServiceHelper.createConfigContextKey(configurationValue);
    inMemoryCacheService.setCachedValue(configContextKey, configurationValue);
    waitForSchedulerToRun();

    final ConfigurationValue cachedConfigurationValue = inMemoryCacheService.getCachedValue(configContextKey);
    assertThat(cachedConfigurationValue, is(nullValue()));
  }

  @Test
  public void testExpiresAfterSomeTimes() {
    final Configuration configuration = configurationInitializer.getConfigurations(
            InMemoryCacheConfiguration.class).get(0);
    final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT, configuration);
    final ConfigContextKey configContextKey = LocalCacheServiceHelper.createConfigContextKey(configurationValue);
    inMemoryCacheService.setCachedValue(configContextKey, configurationValue);
    waitForSchedulerToRun();

    final ConfigurationValue cachedConfigurationValue = inMemoryCacheService.getCachedValue(configContextKey);
    assertThat(cachedConfigurationValue, is(configurationValue));
  }

  private void waitForSchedulerToRun() {
    synchronized (this) {
      try {
        this.wait(1000);
      } catch (final InterruptedException ex) {
        Logger.getLogger(InMemoryCacheServiceTest.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  @Config
  @ConfigTest
  protected static interface ImmediatelyExpiresInMemoryCacheConfiguration {

    @InMemoryCache
    public String getValue();

  }

  @Config
  @ConfigTest
  protected static interface InMemoryCacheConfiguration {

    @InMemoryCache(period = 10)
    public String getValue();

  }

}
