package khameleon.core.internal.localcache;

import java.io.File;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import khameleon.core.internal.localcache.InMemoryCacheService;
import khameleon.core.internal.localcache.LocalCacheService;
import khameleon.core.internal.localcache.LocalCacheServiceImpl;

/**
 * Please override this if your tests would reach localcache service.
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public abstract class AbstractLocalCacheSupportTest {

  private static final String TEST_LOCAL_CACHE_DIR = ".test_khameleon";

  @Mock
  private InMemoryCacheService inMemoryCacheService;

  @Spy
  @InjectMocks
  protected LocalCacheServiceImpl localCacheService;

  @BeforeEach
  public void doSetUp() {
    System.setProperty(LocalCacheService.LOCAL_CACHE_DIR, TEST_LOCAL_CACHE_DIR);
    localCacheService.initialize();
  }

  @AfterEach
  public void tearDown() {
    final File dir = new File(TEST_LOCAL_CACHE_DIR);
    deleteFile(dir);
    localCacheService.cleanUp();
  }

  private static void deleteFile(File file) {
    if (file.isDirectory()) {
      for (File f : file.listFiles()) {
        deleteFile(f);
      }
    }
    file.delete();
  }

}
