package khameleon.core.internal;

import org.junit.jupiter.api.Test;

import static khameleon.core.internal.ConfigConstants.METHOD_START_SYNTAX;
import static khameleon.core.internal.ConfigConstants.METHOD_SYNTAX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author marembo
 */
public class ConfigConstantsTest {

  @Test
  public void testBooleanPropertyNameConforms() throws Exception {
    String booleanTest = "isDisabledTest";
    assertTrue(METHOD_SYNTAX.matcher(booleanTest).find());
  }

  @Test
  public void testBooleanPropertyNameDoesNotConforms() throws Exception {
    String booleanTest = "DisabledTest";
    assertFalse(METHOD_SYNTAX.matcher(booleanTest).find());
  }

  @Test
  public void testNormalPropertyNameConforms() throws Exception {
    String booleanTest = "getDisabledTest";
    assertTrue(METHOD_SYNTAX.matcher(booleanTest).find());
  }

  @Test
  public void testNormalPropertyNameDoesNotConforms() throws Exception {
    String booleanTest = "gthDisabledTest";
    assertFalse(METHOD_SYNTAX.matcher(booleanTest).find());
  }

  @Test
  public void testGetBooleanPropertyName() throws Exception {
    String booleanTest = "isDisabledTest";
    String actualBooleanTest = METHOD_START_SYNTAX.matcher(booleanTest).replaceAll("");
    assertEquals("DisabledTest", actualBooleanTest);
  }

  @Test
  public void testGetNormalPropertyName() throws Exception {
    String booleanTest = "getNameCollission";
    String actualBooleanTest = METHOD_START_SYNTAX.matcher(booleanTest).replaceAll("");
    assertEquals("NameCollission", actualBooleanTest);
  }

  @Test
  public void testGetNormalPropertyNameDoubled() throws Exception {
    String booleanTest = "getGetNameCollission";
    String actualBooleanTest = METHOD_START_SYNTAX.matcher(booleanTest).replaceAll("");
    assertEquals("GetNameCollission", actualBooleanTest);
  }

}
