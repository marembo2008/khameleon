package khameleon.core.internal;

import org.junit.jupiter.api.Test;

import khameleon.core.internal.PrimitiveClass;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author marembo
 */
public class PrimitiveClassTest {

  @Test
  public void testEquality() {
    PrimitiveClass pc = new PrimitiveClass(boolean.class, Boolean.class);
    PrimitiveClass primitive2 = new PrimitiveClass(boolean.class, boolean.class);
    PrimitiveClass primitive3 = new PrimitiveClass(Boolean.class, Boolean.class);
    assertTrue(pc.equals(primitive2));
    assertTrue(pc.equals(primitive3));
  }

}
