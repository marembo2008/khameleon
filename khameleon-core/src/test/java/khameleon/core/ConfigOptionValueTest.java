package khameleon.core;

import java.lang.reflect.Method;

import jakarta.enterprise.inject.Instance;
import khameleon.core.annotations.ConfigOption;
import khameleon.core.applicationcontext.DefaultApplicationContextService;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.util.ConfigOptionValue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class ConfigOptionValueTest {

  @Mock
  private ApplicationContextEnabledConfig applicationContextConfigurationService;

  @Mock
  private Instance<ApplicationContextEnabledConfig> applicationContextConfigurationServices;

  @InjectMocks
  private DefaultApplicationContextService applicationContextService;

  @Test
  public void testDefaultValue() throws Throwable {
    ConfigOptionValue cov = new ConfigOptionValue(0, "default", null, null, applicationContextService);
    assertEquals(cov.getOptionValue(new TestObject()), "default");
  }

  @Test
  public void testObjectValue() throws Throwable {
    Class c = TestObject.class;
    Method m = c.getDeclaredMethod("getTestStringTestValue", new Class[0]);
    ConfigOption configOption = m.getAnnotation(ConfigOption.class);
    ConfigOptionValue cov = new ConfigOptionValue(0, "", configOption, null, applicationContextService);
    assertEquals(cov.getOptionValue(new TestObject()), "getter test");
  }

  @Test
  public void testObjectBooleanValue() throws Throwable {
    Class c = TestObject.class;
    Method m = c.getDeclaredMethod("isTestBooleanTestValue", new Class[0]);
    ConfigOption configOption = m.getAnnotation(ConfigOption.class);
    ConfigOptionValue cov = new ConfigOptionValue(0, "", configOption, null, applicationContextService);
    assertEquals(cov.getOptionValue(new TestObject()), true);
  }

  public static class TestObject {

    @ConfigOption(index = 0, configName = @ConfigOption.ConfigName(value = "testStringTestValue"))
    public String getTestStringTestValue() {
      return "getter test";
    }

    @ConfigOption(index = 0, configName = @ConfigOption.ConfigName(value = "testBooleanTestValue"))
    public boolean isTestBooleanTestValue() {
      return true;
    }

  }

}
