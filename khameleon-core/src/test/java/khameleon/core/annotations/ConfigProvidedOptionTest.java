package khameleon.core.annotations;

import java.util.List;

import com.google.common.base.Optional;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigProvidedOption;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_CONFIG_PROVIDED_OPTION;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_PROVIDED_OPTION;
import static khameleon.core.annotations.ConfigProvidedOption.ConfigOptionType.PROJECT_ID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 25, 2015, 4:40:40 PM
 */
public class ConfigProvidedOptionTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testConfigValueNoOption() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(ConfigProvidedOptionNoValueService.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    final Optional<ConfigurationData> dataOptional
            = configuration.getConfigurationData(CONFIGURATION_CONFIG_PROVIDED_OPTION);
    assertThat(dataOptional.isPresent(), is(false));
  }

  @Test
  public void testConfigValueOption() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(ConfigProvidedOptionValueService.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    final Optional<ConfigurationData> dataOptional
            = configuration.getConfigurationData(CONFIGURATION_CONFIG_PROVIDED_OPTION);
    assertThat(dataOptional.isPresent(), is(true));

    final ConfigurationData configurationData = dataOptional.get();
    assertThat(configurationData.getDataValue(), is(PROJECT_ID.name()));
  }

  @Test
  public void testConfigParameterOption() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(ConfigProvidedOptionParameterService.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    final Optional<AdditionalParameter> additionalParameterOptional
            = configuration.getAdditionalParameter("param");
    assertThat(additionalParameterOptional.isPresent(), is(true));

    final AdditionalParameter additionalParameter = additionalParameterOptional.get();
    final Optional<AdditionalParameterConfigData> additionalParameterConfigDataOptional
            = additionalParameter.getConfigData(PARAMETER_CONFIG_PROVIDED_OPTION);
    assertThat(additionalParameterConfigDataOptional.isPresent(), is(true));

    final AdditionalParameterConfigData additionalParameterConfigData = additionalParameterConfigDataOptional.get();
    assertThat(additionalParameterConfigData.getConfigData(), is(PROJECT_ID.name()));
  }

  @Test
  public void testConfigParameterNoOption() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(ConfigProvidedOptionNoParameterService.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    final Optional<AdditionalParameter> additionalParameterOptional
            = configuration.getAdditionalParameter("param");
    assertThat(additionalParameterOptional.isPresent(), is(true));

    final AdditionalParameter additionalParameter = additionalParameterOptional.get();
    final Optional<AdditionalParameterConfigData> additionalParameterConfigDataOptional
            = additionalParameter.getConfigData(PARAMETER_CONFIG_PROVIDED_OPTION);
    assertThat(additionalParameterConfigDataOptional.isPresent(), is(false));
  }

  @Test
  public void testConfigParameterInvalidOption() {
    assertThrows(ConfigurationInitializationException.class,
                 () -> configurationInitializer.getConfigurations(ConfigProvidedOptionInvalidParameterService.class));
  }

  @Config
  @ConfigTest
  static interface ConfigProvidedOptionNoValueService {

    String getValueOption();

  }

  @Config
  @ConfigTest
  static interface ConfigProvidedOptionValueService {

    @ConfigProvidedOption(PROJECT_ID)
    String getValueOption();

  }

  @Config
  @ConfigTest
  static interface ConfigProvidedOptionParameterService {

    String getValueOption(@ConfigParam(name = "param") @ConfigProvidedOption(PROJECT_ID) final String parameter);

  }

  @Config
  @ConfigTest
  static interface ConfigProvidedOptionNoParameterService {

    String getValueOption(@ConfigParam(name = "param") final String parameter);

  }

  @Config
  @ConfigTest
  static interface ConfigProvidedOptionInvalidParameterService {

    String getValueOption(@ConfigOption(index = 0) @ConfigProvidedOption(PROJECT_ID) final String parameter);

  }

}
