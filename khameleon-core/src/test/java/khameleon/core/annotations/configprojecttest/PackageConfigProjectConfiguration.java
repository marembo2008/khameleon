package khameleon.core.annotations.configprojecttest;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;

/**
 *
 * @author marembo
 */
@Config
@ConfigTest
public interface PackageConfigProjectConfiguration {
}
