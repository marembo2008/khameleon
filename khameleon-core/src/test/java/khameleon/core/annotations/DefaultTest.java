package khameleon.core.annotations;

import java.util.List;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.Default;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE_WHEN_NULL_OR_EMPTY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 23, 2015, 10:50:42 AM
 */
@ExtendWith(MockitoExtension.class)
public class DefaultTest {

  @InjectMocks
  private DefaultConfigurationInitializor configurationInitializor;

  @Test
  public void testDefault() {
    final List<Configuration> configurations = configurationInitializor.getConfigurations(DefaultConfig.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getDefaultValue(), is("def-value"));

    final Optional<ConfigurationData> defaultValuePresent
            = configuration.getConfigurationData(CONFIGURATION_DEFAULT_VALUE);
    assertThat(defaultValuePresent.isPresent(), is(true));
    assertThat(defaultValuePresent.get().getBooleanValue(), is(true));

    final Optional<ConfigurationData> defaultValuePresentWhenNullOrEmpty
            = configuration.getConfigurationData(CONFIGURATION_DEFAULT_VALUE_WHEN_NULL_OR_EMPTY);
    assertThat(defaultValuePresentWhenNullOrEmpty.isPresent(), is(true));
    assertThat(defaultValuePresentWhenNullOrEmpty.get().getBooleanValue(), is(true));
  }

  @Test
  public void testPrimitiveDefaults() {
    final List<Configuration> configurations = configurationInitializor.getConfigurations(
            DefaultPrimitiveConfig.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getDefaultValue(), is("0"));
  }

  @Test
  public void testFloatingDefaults() {
    final List<Configuration> configurations = configurationInitializor.getConfigurations(
            DefaultFloatingPointConfig.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getDefaultValue(), is("0.0"));
  }

  @Test
  public void testCharacterDefaults() {
    final List<Configuration> configurations = configurationInitializor.getConfigurations(
            DefaultCharPointConfig.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getDefaultValue(), is(" "));
  }

  @Config
  @ConfigTest
  private static interface DefaultConfig {

    @Default("def-value")
    String getDefaultValue();

  }

  @Config
  @ConfigTest
  private static interface DefaultPrimitiveConfig {

    int getIntegerValue();

  }

  @Config
  @ConfigTest
  private static interface DefaultFloatingPointConfig {

    float getFloatValue();

  }

  @Config
  @ConfigTest
  private static interface DefaultCharPointConfig {

    char getCharValue();

  }

}
