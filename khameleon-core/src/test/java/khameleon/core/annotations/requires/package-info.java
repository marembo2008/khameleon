@Requires(value = "val",
          configName = "value",
          namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency")

package khameleon.core.annotations.requires;

import khameleon.core.annotations.Requires;
