package khameleon.core.annotations.requires;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;

/**
 *
 * @author marembo
 */
@Config
@ConfigTest
public interface ConfigInterfaceRequiresPackage {

    boolean isTest();
}
