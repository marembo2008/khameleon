package khameleon.core.annotations;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigProject;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.configprojecttest.PackageConfigProjectConfiguration;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_PROJECT;
import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_PROJECT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class ConfigProjectTest {

  private final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();

  @Test
  public void testConfigProjectConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigProjectConfiguration.class);

    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> configData = conf.getConfigurationData(CONFIGURATION_CONFIG_PROJECT);
    assertThat(configData.isPresent(), is(true));
    assertThat(configData.get().getBooleanValue(), is(true));
  }

  @Test
  public void testConfigProjectNamespaceConfiguration() {
    final List<Namespace> namespaces = initializer.getNamespaces(ConfigProjectNamespaceConfiguration.class);

    assertThat(namespaces, hasSize(1));
    final Namespace namespace = namespaces.get(0);
    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(NAMESPACE_CONFIG_PROJECT);
    assertThat(namespaceData.isPresent(), is(true));
    assertThat(namespaceData.get().getDataValue(), is(equalTo("[BACKEND, MAIN, MOBILE, ADMIN]")));
  }

  @Test
  public void testPackageConfigProjectNamespaceConfiguration() {
    final List<Namespace> namespaces = initializer.getNamespaces(PackageConfigProjectConfiguration.class);

    assertThat(namespaces, hasSize(1));
    final Namespace namespace = namespaces.get(0);
    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(NAMESPACE_CONFIG_PROJECT);
    assertThat(namespaceData.isPresent(), is(true));
    assertThat(namespaceData.get().getDataValue(), is(equalTo("[PROJECT1, PACKAGE]")));
  }

  @Config
  @ConfigTest
  public static interface ConfigProjectConfiguration {

    @ConfigProject({})
    String getData();

  }

  @Config
  @ConfigTest
  @ConfigProject({"BACKEND", "MAIN", "MOBILE", "ADMIN"})
  public static interface ConfigProjectNamespaceConfiguration {
  }

}
