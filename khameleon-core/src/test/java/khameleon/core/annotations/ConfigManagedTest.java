package khameleon.core.annotations;

import java.util.List;

import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigManaged;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_MANAGED;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_MANAGED_ID;
import static khameleon.core.processor.configuration.ConfigGlobalUtil.generateConfigGlobalId;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class ConfigManagedTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testDefaultConfiGManaged() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(DefaultConfigManagedConfiguration.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED_ID).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED_ID).get().getDataValue(),
               is(generateConfigGlobalId()));
  }

  @Test
  public void testDefinedIdConfigManaged() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(DefinedIdConfigManagedConfiguration.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED_ID).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_MANAGED_ID).get().getDataValue(),
               is("DefinedIdConfigGlobalConfiguration"));
  }

  @Test
  public void testInvalidConfigManaged() {
    assertThrows(IllegalArgumentException.class,
                 () -> configurationInitializer.getConfigurations(InvalidDefaultConfigManagedConfiguration.class));
  }

  @Config
  public static interface DefaultConfigManagedConfiguration {

    @ConfigManaged
    public boolean isConfigManaged();

  }

  @Config
  public static interface DefinedIdConfigManagedConfiguration {

    @ConfigManaged("DefinedIdConfigGlobalConfiguration")
    public boolean isConfigManaged();

  }

  @Config
  public static interface InvalidDefaultConfigManagedConfiguration {

    @ConfigManaged
    public String getInvalidConfigManaged();

  }

}
