package khameleon.core.annotations.processor;

import com.google.common.base.Optional;

import khameleon.core.AdditionalInformation;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Config;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 *
 * @author marembo
 */
public class DeprecatedCheckProcessorTest {

  @Test
  public void testDeprecated() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> confs = initializor.getConfigurations(DeprecatedConfiguration.class);
    assertThat(confs, hasSize(1));
    final Configuration conf = confs.get(0);
    final Optional<ConfigurationData> deprecated = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_DEPRECATED);
    assertThat(deprecated.isPresent(), is(true));
    final String aiName = "deprecated-deprecation";
    final Optional<AdditionalInformation> info = conf.getAdditionalInformation(aiName);
    assertThat(info.isPresent(), is(true));
    assertThat(info.get().getInfoValue(), is(notNullValue()));
  }

  @Config
  public static interface DeprecatedConfiguration {

    @Deprecated
    String getDeprecated();

  }

}
