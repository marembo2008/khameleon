package khameleon.core.annotations.processor;

import static anosym.common.Country.AFGHANISTAN;
import static khameleon.core.ApplicationMode.ALPHA;
import static khameleon.core.NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import javax.annotation.Nonnull;

import org.junit.jupiter.api.Test;

import khameleon.core.Namespace;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.exception.InvalidApplicationContextException;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

/**
 *
 * @author marembo
 */
public class NamespaceApplicationContextHelperTest {

	@Test
	public void testInvalidConfiguration() {
		final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();
		assertThrows(InvalidApplicationContextException.class,
				() -> initializer.getNamespaces(InvalidConfiguration.class));
	}

	@Test
	public void testValidConfigurationAtLeastApplicationMode() {
		final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();
		final List<Namespace> namespaces = initializer.getNamespaces(ValidConfigurationAtLeastApplicationMode.class);
		assertAppContext(namespaces);
	}

	@Test
	public void testValidConfigurationDefault() {
		final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();
		final List<Namespace> namespaces = initializer.getNamespaces(ValidConfigurationDefault.class);
		assertAppContext(namespaces);
	}

	@Test
	public void testValidConfigurationOnlyApplicationMode() {
		final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();
		final List<Namespace> namespaces = initializer.getNamespaces(ValidConfigurationOnlyApplicationMode.class);
		assertAppContext(namespaces);
	}

	private void assertAppContext(@Nonnull final List<Namespace> namespaces) {
		assertThat(namespaces, hasSize(1));

		final Namespace namespace = namespaces.get(0);
		final long appContextData = namespace.getNamespaceData().stream()
				.filter((namespaceData) -> namespaceData.getDataType() == NAMESPACE_APPLICATION_CONTEXT).count();
		assertThat(appContextData, is(1l));
	}

	@Config
	@ConfigTest
	@AppContext(countries = AFGHANISTAN)
	public static interface InvalidConfiguration {
	}

	@Config
	@ConfigTest
	@AppContext
	public static interface ValidConfigurationDefault {
	}

	@Config
	@ConfigTest
	@AppContext(applicationModes = ALPHA)
	public static interface ValidConfigurationOnlyApplicationMode {
	}

	@Config
	@ConfigTest
	@AppContext(applicationModes = ALPHA, countries = AFGHANISTAN)
	public static interface ValidConfigurationAtLeastApplicationMode {
	}

}
