package khameleon.core.annotations;

import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigDelete;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_DELETE;
import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_DELETE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class ConfigDeleteTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testNamespaceConfigDeleteConfiguration() {
    final List<Namespace> namespaces = configurationInitializer.getNamespaces(NamespaceConfigDeleteConfiguration.class);

    assertThat(namespaces, hasSize(1));
    final Namespace namespace = namespaces.get(0);
    assertThat(namespace.getNamespaceData(NAMESPACE_CONFIG_DELETE).isPresent(), is(true));
  }

  @Test
  public void testNamespaceConfigValueConfigDeleteConfiguration() {
    final List<Configuration> configurations = configurationInitializer.getConfigurations(
            NamespaceConfigDeleteConfiguration.class);

    assertThat(configurations, hasSize(1));
    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_DELETE).isPresent(), is(true));
  }

  @Test
  public void testConfigValueConfigDeleteConfiguration() {
    final List<Configuration> configurations = configurationInitializer.getConfigurations(
            ConfigValueConfigDeleteConfiguration.class);

    assertThat(configurations, hasSize(1));
    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_DELETE).isPresent(), is(true));
  }

  @Config
  @ConfigDelete
  @ConfigTest
  public static interface NamespaceConfigDeleteConfiguration {

    int getTestConfigValue();

  }

  @Config
  @ConfigTest
  public static interface ConfigValueConfigDeleteConfiguration {

    @ConfigDelete
    int getTestConfigValue();

  }

}
