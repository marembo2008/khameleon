package khameleon.core.annotations;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.EnumValue;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class ConfigParamTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testInvalidConfigParam() {
    assertThrows(IllegalArgumentException.class,
                 () -> configurations(InvalidConfigParamService.class));
  }

  @Test
  public void testNormalConfigParam() {
    final List<Configuration> configurations = configurations(ConfigParamService.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<AdditionalParameter> apOptional = conf.getAdditionalParameter("index");
    assertThat(apOptional.isPresent(), is(true));
  }

  @Test
  public void testBooleanConfigParam() {
    final List<Configuration> configurations = configurations(BooleanConfigParamService.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<AdditionalParameter> apOptional = conf.getAdditionalParameter("index");
    assertThat(apOptional.isPresent(), is(true));
    final AdditionalParameter ap = apOptional.get();
    final Optional<AdditionalParameterConfigData> apConfigData = ap.getConfigData(
            AdditionalParameterConfigDataType.PARAMETER_BOOLEAN);
    assertThat(apConfigData.isPresent(), is(true));
  }

  @Test
  public void testCalendarConfigParam() {
    final List<Configuration> configurations = configurations(CalendarConfigParamService.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<AdditionalParameter> apOptional = conf.getAdditionalParameter("index");
    assertThat(apOptional.isPresent(), is(true));
    final AdditionalParameter ap = apOptional.get();
    final Optional<AdditionalParameterConfigData> apConfigData = ap.getConfigData(
            AdditionalParameterConfigDataType.PARAMETER_CALENDAR);
    assertThat(apConfigData.isPresent(), is(true));
  }

  @Test
  public void testEnumConfigParam() {
    final List<Configuration> configurations = configurations(EnumConfigParamService.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<AdditionalParameter> apOptional = conf.getAdditionalParameter("index");
    assertThat(apOptional.isPresent(), is(true));
    final AdditionalParameter ap = apOptional.get();
    final Optional<AdditionalParameterConfigData> apConfigData = ap.getConfigData(
            AdditionalParameterConfigDataType.PARAMETER_ENUM);
    assertThat(apConfigData.isPresent(), is(true));
    final List<EnumValue> enumValues = apConfigData.get().getEnumList();
    assertThat(enumValues,
               contains(new EnumValue("ONE", "ONE"), new EnumValue("TWO", "TWO"), new EnumValue("THREE", "THREE")));
  }

  @Config
  public static interface InvalidConfigParamService {

    public static class InvalidConfigParam {
    }

    String getIntValue(@ConfigParam(name = "index") InvalidConfigParam index);

  }

  @Config
  public static interface ConfigParamService {

    String getIntValue(@ConfigParam(name = "index") int index);

  }

  @Config
  public static interface BooleanConfigParamService {

    String getIntValue(@ConfigParam(name = "index") boolean index);

  }

  @Config
  public static interface CalendarConfigParamService {

    String getIntValue(@ConfigParam(name = "index") Calendar index);

  }

  @Config
  public static interface EnumConfigParamService {

    public static enum MyEnum {

      ONE, TWO, THREE;

    }

    String getIntValue(@ConfigParam(name = "index") MyEnum index);

  }

  private List<Configuration> configurations(@Nonnull final Class<?> configInterface) {
    return configurationInitializer.getConfigurations(configInterface);
  }

}
