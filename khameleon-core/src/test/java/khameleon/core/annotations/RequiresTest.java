package khameleon.core.annotations;

import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.Requires;
import khameleon.core.annotations.requires.ConfigInterfaceRequiresPackage;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_CONFIGNAME;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_NAMESPACE;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_VALUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class RequiresTest {

  @Test
  public void testRequiresForCurrentNamespace() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor.getConfigurations(ConfigInterface.class);
    final Configuration conf = configurations.get(0);
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).get().getDataValue(), is("val"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).get().getDataValue(), is("value"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).isPresent(), is(true));
    final String namespace = conf.getNamespace().getCanonicalName();
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).get().getDataValue(), is(namespace));
  }

  @Test
  public void testRequiresForDefinedNamespace() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor.getConfigurations(ConfigInterfaceDependency.class);
    final Configuration conf = configurations.get(0);
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).get().getDataValue(), is("val"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).get().getDataValue(), is("value"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).isPresent(), is(true));
    final String namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency";
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).get().getDataValue(), is(namespace));
  }

  @Test
  public void testRequiresForEntireNamespaceForDefinedNamespace() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor.getConfigurations(ConfigInterfaceEntireNamespace.class);
    final Configuration conf = configurations.get(0);
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).get().getDataValue(), is("val"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).get().getDataValue(), is("value"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).isPresent(), is(true));
    final String namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency";
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).get().getDataValue(), is(namespace));
  }

  @Test
  public void testRequiresForEntirePackageForDefinedNamespace() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor.getConfigurations(ConfigInterfaceRequiresPackage.class);
    final Configuration conf = configurations.get(0);
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).get().getDataValue(), is("val"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).isPresent(), is(true));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).get().getDataValue(), is("value"));
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).isPresent(), is(true));
    final String namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency";
    assertThat(conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).get().getDataValue(), is(namespace));
  }

  @Config
  @ConfigTest
  public static interface ConfigInterface {

    @Requires(value = "val", configName = "value")
    String getName();

  }

  @Config
  @ConfigTest
  public static interface ConfigInterfaceDependency {

    @Requires(value = "val",
              configName = "value",
              namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency")
    String getValue();

  }

  @Requires(value = "val",
            configName = "value",
            namespace = "khameleon.core.annotations.RequiresTest.ConfigInterfaceDependency")
  @Config
  @ConfigTest
  public static interface ConfigInterfaceEntireNamespace {

    String getValue();

  }

}
