package khameleon.core.annotations;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.Encrypt;
import khameleon.core.annotations.Password;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 28, 2016, 5:47:01 AM
 */
public class EncryptTest {

  private final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();

  @Test
  public void verifyEncryptConfiguration() {
    final List<Configuration> configurations = initializor.getConfigurations(EncryptedConfigurationService.class);
    assertThat(configurations, hasSize(1));

    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_ENCRYPT);
    assertThat(confData.isPresent(), is(true));
    assertThat(confData.get().getBooleanValue(), is(true));
  }

  @Config
  private static interface EncryptedConfigurationService {

    @Encrypt
    @Password
    String getEncrypted();

  }

}
