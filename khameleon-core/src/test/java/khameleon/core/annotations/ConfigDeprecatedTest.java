package khameleon.core.annotations;

import java.util.List;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigDeprecated;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class ConfigDeprecatedTest {

  @Test
  public void testConfigDeprecated() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor.getConfigurations(DeprecatedConfiguration.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_DEPRECATED);
    assertThat(confData.isPresent(), is(true));
  }

  @Test
  public void testConfigDeprecatedAutoRemove() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    final List<Configuration> configurations = initializor
            .getConfigurations(DeprecatedConfigurationAutoRemove.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_DEPRECATED);
    assertThat(confData.isPresent(), is(true));
    final Optional<ConfigurationData> autoRemove = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_DEPRECATED_AUTOREMOVE);
    assertThat(autoRemove.isPresent(), is(true));
    assertThat(autoRemove.get().getBooleanValue(), is(true));
    final Optional<ConfigurationData> when = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_DEPRECATED_REMOVE_DATE);
    assertThat(when.isPresent(), is(true));
    assertThat(when.get().getDataValue(), is("2014-10-30"));
  }

  @Test
  public void testConfigDeprecatedAutoRemoveInvalid() {
    final DefaultConfigurationInitializor initializor = new DefaultConfigurationInitializor();
    assertThrows(IllegalArgumentException.class,
                 () -> initializor.getConfigurations(DeprecatedConfigurationAutoRemoveInvalid.class));
  }

  @Config
  public static interface DeprecatedConfiguration {

    @ConfigDeprecated(message = "This config has been deprecated")
    String getDeprecated();

  }

  @Config
  public static interface DeprecatedConfigurationAutoRemove {

    @ConfigDeprecated(message = "This config has been deprecated", autoRemove = true, when = "2014-10-30")
    String getDeprecated();

  }

  @Config
  public static interface DeprecatedConfigurationAutoRemoveInvalid {

    @ConfigDeprecated(message = "This config has been deprecated", autoRemove = true)
    String getDeprecated();

  }

}
