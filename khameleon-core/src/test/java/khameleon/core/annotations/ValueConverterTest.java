package khameleon.core.annotations;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import com.google.common.collect.ImmutableList;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.inject.Instance;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.ValueConverter;
import khameleon.core.applicationcontext.DefaultApplicationContextService;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.converter.InetAddressConverter;
import khameleon.core.internal.localcache.AbstractLocalCacheSupportTest;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.internal.service.request.ConfigurationServiceCacheServiceLookup;
import khameleon.core.internal.service.request.ConfigurationServiceHelperImpl;
import khameleon.core.values.ConfigurationValue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static java.util.Collections.emptyList;
import static khameleon.core.internal.service.request.ConfigurationServiceCacheServiceLookup.cleanConfigurationServiceCacheService;
import static khameleon.core.manager.ConfigurationManager.getConfigInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 1:23:50 PM
 */
public class ValueConverterTest extends AbstractLocalCacheSupportTest {

  private static final String SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED = "anosym.khameleon.localcache.localCacheDisabled";

  private final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();

  @Mock
  private ConfigurationRequestService configurationRequestService;

  @Spy
  @InjectMocks
  private ConfigurationServiceHelperImpl configurationServiceHelper;

  @Mock
  private ApplicationContextEnabledConfig applicationContextConfigurationService;

  @Mock
  private Instance<ApplicationContextEnabledConfig> applicationContextConfigurationServices;

  @InjectMocks
  private DefaultApplicationContextService applicationContextService;

  @BeforeAll
  public static void setupLocalCache() {
    System.setProperty(SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED, "true");
  }

  @AfterAll
  public static void tearDownLocalCache() {
    final Properties properties = System.getProperties();
    properties.remove(SYSTEM_PROPERTY_LOCAL_CACHE_DISABLED);
    System.setProperties(properties);
  }

  @BeforeEach
  public void setUp() {
    when(applicationContextConfigurationServices.isUnsatisfied()).thenReturn(false);
    when(applicationContextConfigurationServices.get()).thenReturn(applicationContextConfigurationService);
    when(applicationContextConfigurationService.isApplicationContextEnabled(anyInt())).thenReturn(true);

    final ManagedExecutorService managedExecutorService = mock(ManagedExecutorService.class);
    ConfigurationServiceCacheServiceLookup.setConfigurationServiceCacheService(managedExecutorService);

    doReturn(localCacheService).when(configurationServiceHelper).getLocalCacheService();
    doReturn(configurationRequestService).when(configurationServiceHelper).getConfigurationRequestService();
  }

  @AfterEach
  public void tearDownDefault() {
    cleanConfigurationServiceCacheService();
  }

  @Test
  public void testSingleValue() throws Exception {
    setupSingleValueConfiguration();

    final ConfigValueConverter configValueConverter
            = getConfigInstance(applicationContextService, ConfigValueConverter.class, configurationServiceHelper);
    final InetAddress inetAddress = configValueConverter.getAddress();
    final InetAddress expectedInetAddress = InetAddress.getByName("172.17.0.1");
    assertThat(inetAddress, equalTo(expectedInetAddress));
  }

  @Test
  public void testArrayValue() throws Exception {
    setupArrayValueConfiguration();

    final ConfigValueConverter configValueConverter
            = getConfigInstance(applicationContextService, ConfigValueConverter.class, configurationServiceHelper);
    final InetAddress[] inetAddresses = configValueConverter.getAddresses();
    final InetAddress[] expectedInetAddresses
            = new InetAddress[]{InetAddress.getByName("172.17.0.1"), InetAddress.getByName("172.20.0.1")};
    assertThat(ImmutableList.copyOf(inetAddresses), hasItems(expectedInetAddresses));
  }

  @Test
  public void testListValue() throws Exception {
    setupListValueConfiguration();

    final ConfigValueConverter configValueConverter
            = getConfigInstance(applicationContextService, ConfigValueConverter.class, configurationServiceHelper);
    final List<InetAddress> inetAddresses = configValueConverter.getAddressList();
    final InetAddress[] expectedInetAddresses
            = new InetAddress[]{InetAddress.getByName("172.17.0.1"), InetAddress.getByName("172.20.0.1")};
    assertThat(inetAddresses, hasItems(expectedInetAddresses));
  }

  @Test
  public void testUrlValue() throws Exception {
    setupUrlValueConfiguration();

    final ConfigValueConverter configValueConverter
            = getConfigInstance(applicationContextService, ConfigValueConverter.class, configurationServiceHelper);
    final URL url = configValueConverter.getUrl();
    final URL expectedUrl = new URL("https://www.babycenter.com/");
    assertThat(url, equalTo(expectedUrl));
  }

  @Test
  public void testInetSocketAddressValue() throws Exception {
    setupInetSocketAddressValueConfiguration();

    final ConfigValueConverter configValueConverter
            = getConfigInstance(applicationContextService, ConfigValueConverter.class, configurationServiceHelper);
    final InetSocketAddress socketAddress = configValueConverter.getInetSocketAddress();
    final InetSocketAddress expectedSocketAddress = new InetSocketAddress("www.babycenter.com", 443);
    assertThat(socketAddress, equalTo(expectedSocketAddress));
  }

  private void setupSingleValueConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigValueConverter.class);
    //there will only be one value.
    final Configuration configuration = configurations
            .stream()
            .filter((config) -> config.getConfigName().equals("address"))
            .findFirst()
            .get();
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration, "172.17.0.1");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  private void setupArrayValueConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigValueConverter.class);
    //there will only be one value.
    final Configuration configuration = configurations
            .stream()
            .filter((config) -> config.getConfigName().equals("addresses"))
            .findFirst()
            .get();
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "[172.17.0.1, 172.20.0.1]");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  private void setupListValueConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigValueConverter.class);
    //there will only be one value.
    final Configuration configuration = configurations
            .stream()
            .filter((config) -> config.getConfigName().equals("addressList"))
            .findFirst()
            .get();
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "[172.17.0.1, 172.20.0.1]");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  private void setupUrlValueConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigValueConverter.class);
    //there will only be one value.
    final Configuration configuration = configurations
            .stream()
            .filter((config) -> config.getConfigName().equals("url"))
            .findFirst()
            .get();
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "https://www.babycenter.com/");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  private void setupInetSocketAddressValueConfiguration() {
    final List<Configuration> configurations = initializer.getConfigurations(ConfigValueConverter.class);
    //there will only be one value.
    final Configuration configuration = configurations
            .stream()
            .filter((config) -> config.getConfigName().equals("inetSocketAddress"))
            .findFirst()
            .get();
    final Namespace namespace = configuration.getNamespace();
    final ConfigurationValue configurationValue
            = new ConfigurationValue(applicationContextService.getApplicationContext(), configuration,
                                     "www.babycenter.com:443");
    final ApplicationContext contextId = applicationContextService.getApplicationContext();
    final String canonicalNamespace = namespace.getCanonicalName();
    final String configName = configuration.getConfigName();
    when(configurationRequestService.findConfigurationValue(contextId, canonicalNamespace, configName, emptyList()))
            .thenReturn(configurationValue);
  }

  @Config
  @ConfigTest
  public static interface ConfigValueConverter {

    @ValueConverter(InetAddressConverter.class)
    InetAddress getAddress();

    @ValueConverter(InetAddressConverter.class)
    InetAddress[] getAddresses();

    @ValueConverter(InetAddressConverter.class)
    List<InetAddress> getAddressList();

    URL getUrl();

    InetSocketAddress getInetSocketAddress();

  }

}
