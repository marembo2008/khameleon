package khameleon.core.annotations;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRegex;
import khameleon.core.annotations.Default;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class ConfigRegexTest {

  private final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();

  @Test
  public void testValidRegexWithoutDefault() {
    initializer.getConfigurations(ConfigRegexConfiguration.class);
  }

  @Test
  public void testUnmatchedDefaultValue() {
    assertThrows(IllegalArgumentException.class,
                 () -> initializer.getConfigurations(UnmatchedDefaultValueConfigRegexConfiguration.class));
  }

  @Test
  public void testInvalidType() {
    assertThrows(IllegalArgumentException.class,
                 () -> initializer.getConfigurations(InvalidTypeConfigRegexConfiguration.class));
  }

  @Test
  public void testValidParameterConfigRegex() {
    initializer.getConfigurations(ConfigParamConfigRegexConfiguration.class);
  }

  @Test
  public void testMissingConfigParamConfigRegex() {
    assertThrows(IllegalArgumentException.class,
                 () -> initializer.getConfigurations(MissingConfigParamConfigRegexConfiguration.class));
  }

  @Test
  public void testInvalidParameterTypeConfigRegex() {
    assertThrows(IllegalArgumentException.class,
                 () -> initializer.getConfigurations(InvalidParameterTypeConfigRegexConfiguration.class));
  }

  @Config
  public interface ConfigRegexConfiguration {

    @ConfigRegex("(\\w+/\\d{3})+")
    @Default("smartp/345")
    String getValidValue();

  }

  @Config
  public interface UnmatchedDefaultValueConfigRegexConfiguration {

    @ConfigRegex("[\\w]+/[\\d]")
    @Default("3434/3434")
    String getValidValue();

  }

  @Config
  public interface InvalidTypeConfigRegexConfiguration {

    @ConfigRegex("regex")
    int getValidValue();

  }

  @Config
  public interface ConfigParamConfigRegexConfiguration {

    String getValue(@ConfigParam(name = "name") @ConfigRegex("\\w+") final String name);

  }

  @Config
  public interface MissingConfigParamConfigRegexConfiguration {

    String getValue(@ConfigRegex("\\w+") final String name);

  }

  @Config
  public interface InvalidParameterTypeConfigRegexConfiguration {

    String getValue(@ConfigParam(name = "value") @ConfigRegex("\\w+") final long value);

  }

}
