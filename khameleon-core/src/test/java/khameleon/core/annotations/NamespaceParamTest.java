package khameleon.core.annotations;

import java.util.List;

import com.google.common.base.Optional;

import khameleon.core.AdditionalParameter;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.annotations.NamespaceParam;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_NAMESPACE_PARAM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class NamespaceParamTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testNamespaceParamForInvalidConfiguration() {
    assertThrows(ConfigurationInitializationException.class,
                 () -> configurationInitializer.getConfigurations(NamespaceParamInvalidConfiguration.class));
  }

  @Test
  public void testNamespaceParamForConfiguration() {
    final List<Configuration> configurations = configurationInitializer
            .getConfigurations(NamespaceParamConfiguration.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_NAMESPACE_PARAM);
    assertThat(confData.isPresent(), is(true));
    final Optional<AdditionalParameter> ap = conf.getAdditionalParameter("ap");
    assertThat(ap.isPresent(), is(true));
    AdditionalParameter ap_ = ap.get();
    assertThat(ap_.getConfigData(PARAMETER_NAMESPACE_PARAM).isPresent(), is(false));
  }

  @Test
  public void testNamespaceParamForArrayConfiguration() {
    final List<Configuration> configurations = configurationInitializer
            .getConfigurations(NamespaceParamArrayConfiguration.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_NAMESPACE_PARAM);
    assertThat(confData.isPresent(), is(true));
    final Optional<AdditionalParameter> ap = conf.getAdditionalParameter("ap");
    assertThat(ap.isPresent(), is(true));
    AdditionalParameter ap_ = ap.get();
    assertThat(ap_.getConfigData(PARAMETER_NAMESPACE_PARAM).isPresent(), is(false));
  }

  @Test
  public void testNamespaceParamForAdditionalParameter() {
    final List<Configuration> configurations = configurationInitializer
            .getConfigurations(NamespaceParamAdditionalParameter.class);
    assertThat(configurations, hasSize(1));
    final Configuration conf = configurations.get(0);
    final Optional<ConfigurationData> confData = conf.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_NAMESPACE_PARAM);
    assertThat(confData.isPresent(), is(false));
    final Optional<AdditionalParameter> ap = conf.getAdditionalParameter("ap");
    assertThat(ap.isPresent(), is(true));
    AdditionalParameter ap_ = ap.get();
    assertThat(ap_.getConfigData(PARAMETER_NAMESPACE_PARAM).isPresent(), is(true));
  }

  @Test
  public void testNamespaceParamForInvalidAdditionalParameterType() {
    assertThrows(ConfigurationInitializationException.class,
                 () -> configurationInitializer.getConfigurations(NamespaceParamInvalidAdditionalParameterType.class));
  }

  @Test
  public void testNamespaceParamForInvalidAdditionalParameterNoConfigParam() {
    assertThrows(ConfigurationInitializationException.class,
                 () -> configurationInitializer.getConfigurations(
                         NamespaceParamInvalidAdditionalParameterNoConfigParam.class));
  }

  @Config
  @ConfigTest
  public static interface NamespaceParamInvalidConfiguration {

    @NamespaceParam
    int getParam(@ConfigParam(name = "ap") final String param);

  }

  @Config
  @ConfigTest
  public static interface NamespaceParamConfiguration {

    @NamespaceParam
    String getParam(@ConfigParam(name = "ap") final String param);

  }

  @Config
  @ConfigTest
  public static interface NamespaceParamArrayConfiguration {

    @NamespaceParam
    String[] getParamArray(@ConfigParam(name = "ap") final String param);

  }

  @Config
  @ConfigTest
  public static interface NamespaceParamAdditionalParameter {

    String getParam(@ConfigParam(name = "ap") @NamespaceParam final String param);

  }

  @Config
  @ConfigTest
  public static interface NamespaceParamInvalidAdditionalParameterType {

    String getParam(@ConfigParam(name = "ap") @NamespaceParam final Float param);

  }

  @Config
  @ConfigTest
  public static interface NamespaceParamInvalidAdditionalParameterNoConfigParam {

    String getParam(@NamespaceParam final Float param);

  }

}
