package khameleon.core.annotations;

import java.util.List;

import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigGlobal;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

import org.junit.jupiter.api.Test;

import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_GLOBAL;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_GLOBAL_ID;
import static khameleon.core.processor.configuration.ConfigGlobalUtil.generateConfigGlobalId;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class ConfigGlobalTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testDefaultConfigGlobal() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(DefaultConfigGlobalConfiguration.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL_ID).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL_ID).get().getDataValue(),
               is(generateConfigGlobalId()));
  }

  @Test
  public void testDefinedIdConfigGlobal() {
    final List<Configuration> configurations
            = configurationInitializer.getConfigurations(DefinedIdConfigGlobalConfiguration.class);
    assertThat(configurations, hasSize(1));

    final Configuration configuration = configurations.get(0);
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL_ID).isPresent(), is(true));
    assertThat(configuration.getConfigurationData(CONFIGURATION_CONFIG_GLOBAL_ID).get().getDataValue(),
               is("DefinedIdConfigGlobalConfiguration"));
  }

  @Test
  public void testInvalidConfigGlobal() {
    assertThrows(IllegalArgumentException.class,
                 () -> configurationInitializer.getConfigurations(InvalidDefaultConfigGlobalConfiguration.class));
  }

  @Config
  public static interface DefaultConfigGlobalConfiguration {

    @ConfigGlobal
    public boolean isConfigGlobal();

  }

  @Config
  public static interface DefinedIdConfigGlobalConfiguration {

    @ConfigGlobal("DefinedIdConfigGlobalConfiguration")
    public boolean isConfigGlobal();

  }

  @Config
  public static interface InvalidDefaultConfigGlobalConfiguration {

    @ConfigGlobal
    public String getInvalidConfigGlobal();

  }

}
