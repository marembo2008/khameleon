package khameleon.core;

import static anosym.common.Country.UNITED_KINGDOM;
import static anosym.common.Language.ENGLISH;
import static anosym.common.TLD.CO_UK;
import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

/**
 *
 * @author marembo
 */
public class ApplicationContextTest {

	@Test
	public void testDefaultContextId() {
		final int expected = 116850243;
		final ApplicationContext defaultContext = new ApplicationContext();
		final int actual = defaultContext.getContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testParentContextId() {
		final int expected = 116850243;
		final ApplicationContext defaultContext = new ApplicationContext(DEVELOPMENT, null, null, null);
		final int actual = defaultContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAsParentContextId() {
		final int expected = 528537808;
		final ApplicationContext tldContext = new ApplicationContext(DEVELOPMENT, CO_UK, null, null);
		final int actual = tldContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAndTldAsParentContextId() {
		final int expected = 531533488;
		final ApplicationContext langCodeContext = new ApplicationContext(DEVELOPMENT, CO_UK, ENGLISH, null);
		final int actual = langCodeContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAsLangCodeParentContextId() {
		final int expected = 528537808;
		final ApplicationContext langCodeContext = new ApplicationContext(DEVELOPMENT, null, ENGLISH, null);
		final int actual = langCodeContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAsCountryCodeParentContextId() {
		final int expected = 528537808;
		final ApplicationContext langCodeContext = new ApplicationContext(DEVELOPMENT, null, null, UNITED_KINGDOM);
		final int actual = langCodeContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAndTldAndLangCodeAsCountryParentContextId() {
		final int expected = 531533488;
		final ApplicationContext langCodeContext = new ApplicationContext(DEVELOPMENT, CO_UK, null, UNITED_KINGDOM);
		final int actual = langCodeContext.getParentContextId();
		assertThat(actual, is(expected));
	}

	@Test
	public void testModeAndTldAndLangCodeAsParentContextId() {
		final int expected = 531600322;
		final ApplicationContext langCodeContext = new ApplicationContext(DEVELOPMENT, CO_UK, ENGLISH, UNITED_KINGDOM);
		final int actual = langCodeContext.getParentContextId();
		assertThat(actual, is(expected));
	}

}
