package khameleon.core;

import static anosym.common.TLD.COM;
import static anosym.common.TLD.DE;
import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static khameleon.core.ApplicationMode.TEST;
import static khameleon.core.NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT;
import static khameleon.core.processor.configuration.NamespaceApplicationContextHelper.unmarshall;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import anosym.common.TLD;
import jakarta.xml.bind.JAXBException;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigTest;
import khameleon.core.initializer.DefaultConfigurationInitializor;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class NamespaceTest {

	private static final Logger LOG = Logger.getLogger(NamespaceTest.class.getName());

	@InjectMocks
	private DefaultConfigurationInitializor configurationInitializor;

	@Test
	public void testDefaultContextConfig() throws JAXBException {
		List<Namespace> namespaces = configurationInitializor.getNamespaces(TestDefaultContextConfig.class);
		assertThat(namespaces, hasSize(1));
		Namespace namespace = namespaces.get(0);
		namespace.initializeNamespaceDataCache();
		Optional<NamespaceData> namespaceData = namespace
				.getNamespaceData(NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
		assertThat(namespaceData.isPresent(), is(true));
		List<ApplicationContext> actualContexts = unmarshall(namespaceData.get().getDataValue());
		assertThat(actualContexts, hasSize(1));
		assertThat(actualContexts, hasItem(new ApplicationContext(null, null, null, null)));
	}

	@Test
	public void testApplicationModesContextConfig() throws JAXBException {
		List<Namespace> namespaces = configurationInitializor.getNamespaces(TestApplicationModesContextConfig.class);
		assertThat(namespaces, hasSize(1));
		Namespace namespace = namespaces.get(0);
		namespace.initializeNamespaceDataCache();
		Optional<NamespaceData> namespaceData = namespace
				.getNamespaceData(NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
		assertThat(namespaceData.isPresent(), is(true));
		List<ApplicationContext> actualContexts = unmarshall(namespaceData.get().getDataValue());
		assertThat(actualContexts, hasSize(2));
		List<ApplicationContext> applicationContexts = Lists.newArrayList(
				new ApplicationContext(DEVELOPMENT, null, null, null), new ApplicationContext(TEST, null, null, null));
		Collections.sort(actualContexts);
		Collections.sort(applicationContexts);
		assertEquals(applicationContexts, actualContexts);
	}

	@Test
	public void testApplicationModesAndTLDContextConfig() throws JAXBException {
		List<Namespace> namespaces = configurationInitializor
				.getNamespaces(TestApplicationModesAndTLDContextConfig.class);
		assertThat(namespaces, hasSize(1));
		Namespace namespace = namespaces.get(0);
		namespace.initializeNamespaceDataCache();
		Optional<NamespaceData> namespaceData = namespace
				.getNamespaceData(NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
		assertThat(namespaceData.isPresent(), is(true));
		List<ApplicationContext> actualContexts = unmarshall(namespaceData.get().getDataValue());
		assertThat(actualContexts, hasSize(4));
		List<ApplicationContext> applicationContexts = Lists.newArrayList(
				new ApplicationContext(DEVELOPMENT, COM, null, null),
				new ApplicationContext(DEVELOPMENT, DE, null, null), new ApplicationContext(TEST, COM, null, null),
				new ApplicationContext(TEST, DE, null, null));
		Collections.sort(actualContexts);
		Collections.sort(applicationContexts);
		assertEquals(applicationContexts, actualContexts);
	}

	@Test
	public void verifyCannotAddDuplicateNamespaceData() {
		final Namespace namespace = Namespace.builder().withSimpleName("simple-name").build();
		namespace.addOrUpdateNamespaceData(NAMESPACE_APPLICATION_CONTEXT, "data-1");
		namespace.addOrUpdateNamespaceData(NAMESPACE_APPLICATION_CONTEXT, "data-2");
		namespace.addOrUpdateNamespaceData(new NamespaceData(NAMESPACE_APPLICATION_CONTEXT, "data=14"));

		final long count = namespace.getNamespaceData().stream().count();

		assertThat(count, is(1l));
	}

	private void assertAppContext(@Nonnull final List<Namespace> namespaces) {
		assertThat(namespaces, hasSize(1));

		final Namespace namespace = namespaces.get(0);
		final long appContextData = namespace.getNamespaceData().stream()
				.filter((namespaceData) -> namespaceData.getDataType() == NAMESPACE_APPLICATION_CONTEXT).count();
		assertThat(appContextData, is(1l));
	}

	@Config
	@ConfigTest
	@AppContext
	public interface TestDefaultContextConfig {

	}

	@Config
	@ConfigTest
	@AppContext(applicationModes = { TEST, DEVELOPMENT })
	public interface TestApplicationModesContextConfig {

	}

	@Config
	@ConfigTest
	@AppContext(applicationModes = { TEST, DEVELOPMENT }, topLevelDomains = { TLD.COM, TLD.DE })
	public interface TestApplicationModesAndTLDContextConfig {

	}

}
