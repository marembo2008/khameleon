package khameleon.core.values;

import com.google.common.base.Optional;

import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;

import java.util.List;

import org.junit.jupiter.api.Test;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class ConfigurationValueTest {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  @Test
  public void testUpdateAdditionalParameters() {
    final List<Configuration> configurations = configurationInitializer.getConfigurations(MyParameterService.class);
    assertThat(configurations, hasSize(1));
    final ConfigurationValue configurationValue
            = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT, configurations.get(0));
    assertThat(configurationValue.getAdditionalParameterValues(), hasSize(2));
    final Optional<AdditionalParameterValue> first = configurationValue.getAdditionalParameterValue("first");
    assertThat(first.isPresent(), is(true));
    final Optional<AdditionalParameterValue> option = configurationValue.getAdditionalParameterValue("option");
    assertThat(option.isPresent(), is(true));
    assertThat(configurationValue.updateAdditionalParameterValue("first", "first"), is(true));
    assertThat(configurationValue.updateAdditionalParameterValue("option", "THREE"), is(true));
  }

  @Config
  public static interface MyParameterService {

    public static enum Option {

      ONE, TWO, THREE;

    }

    String getMyValue(@ConfigParam(name = "first") String first, @ConfigParam(name = "option") Option option);

  }

}
