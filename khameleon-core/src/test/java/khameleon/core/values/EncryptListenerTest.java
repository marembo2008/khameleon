package khameleon.core.values;

import static khameleon.core.values.EncyrptConstants.ENCRYPTION_KEY;
import static khameleon.core.values.EncryptListener.ENCRYPT_INDICATOR_PREFIX;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import java.util.List;

import org.junit.jupiter.api.Test;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Encrypt;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 28, 2016, 4:49:27 AM
 */
public class EncryptListenerTest {

	private final ConfigurationInitializer initializer = new DefaultConfigurationInitializor();

	private final EncryptListener encryptListener = new EncryptListener();

	@Test
	public void verifyEncryptDecrypt() throws Exception {
		System.setProperty(ENCRYPTION_KEY, "sdasdkdjsldsdk39402390432dksadjk32o84239ojwdmqwl4o823opwqmdldjk9234");

		final List<Configuration> configurations = initializer.getConfigurations(EncryptTestConfigurationService.class);
		final Configuration configuration = configurations.get(0);
		final ConfigurationValue configurationValue = new ConfigurationValue(
				ApplicationContext.DEFAULT_APPLICATION_CONTEXT, configuration, "config-value");
		encryptListener.encrypt(configurationValue);

		// We can only verify that the value has changed.
		assertThat(configurationValue.getConfigValue(), is(not(equalTo("config-value"))));

		// Verify the value starts with encryption prefix
		assertThat(configurationValue.getConfigValue(), startsWith(ENCRYPT_INDICATOR_PREFIX));

		// We then decrypt
		encryptListener.decrypt(configurationValue);

		// Affirm that the value is as expected
		assertThat(configurationValue.getConfigValue(), is(equalTo("config-value")));
	}

	@Config
	@ConfigRoot
	static interface EncryptTestConfigurationService {

		@Encrypt
		String getEncrypted();

	}

}
