package khameleon.core.values.helper;

import com.google.common.collect.Lists;

import khameleon.core.AdditionalParameter;
import khameleon.core.values.AdditionalParameterValue;

import java.util.List;

import javax.annotation.Nonnull;

import org.junit.jupiter.api.Test;

import static khameleon.core.values.helper.ConfigurationValueHelper.findUniqueKeyFromAdditionalParameterValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo
 */
public class ConfigurationValueHelperTest {

  private static final String[][] keyValues = {{"key0", "value0"}, {"key3", "value3"}, {"key1", "value1"}};

  @Test
  public void testCreateUniqueKey() {
    final String uniqueKey0 = findUniqueKeyFromAdditionalParameterValue(createAdditionalParameterValues(keyValues));
    final String uniqueKey1 = findUniqueKeyFromAdditionalParameterValue(createAdditionalParameterValues(keyValues));
    assertThat(uniqueKey0, is(uniqueKey1));
  }

  private List<AdditionalParameterValue> createAdditionalParameterValues(@Nonnull final String[][] keyValuePairs) {
    final List<AdditionalParameterValue> additionalParameterValues = Lists.newArrayList();
    for (final String[] keyValue : keyValuePairs) {
      additionalParameterValues.add(createAdditionalParameterValue(keyValue[0], keyValue[1]));
    }
    return additionalParameterValues;
  }

  private AdditionalParameter createAdditionalParameter(@Nonnull final String parameterKey) {
    return new AdditionalParameter(parameterKey);
  }

  private AdditionalParameterValue createAdditionalParameterValue(@Nonnull final String parameterKey,
                                                                  @Nonnull final String parameterValue) {
    final AdditionalParameterValue additionalParameterValue = new AdditionalParameterValue();
    additionalParameterValue.setAdditionalParameter(createAdditionalParameter(parameterKey));
    additionalParameterValue.setParameterValue(parameterValue);
    return additionalParameterValue;
  }

}
