package khameleon.core.validator.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.DynamicDefault;
import khameleon.core.annotations.validator.ConfigValidator;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:07:26 PM
 */
@ConfigValidator
public class DynamicDefaultConfigValidator implements ConfigurationValidator {

    @Override
    public void validate(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(DynamicDefault.class)) {
            if (configMethod.isAnnotationPresent(Default.class)) {
                throw new ConfigurationInitializationException("Cannot define both @Default and @DynamicDefault");
            }
        }
    }

}
