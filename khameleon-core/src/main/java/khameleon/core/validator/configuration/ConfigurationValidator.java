package khameleon.core.validator.configuration;

import khameleon.core.Configuration;
import khameleon.core.exception.ConfigurationInitializationException;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo
 */
public interface ConfigurationValidator {

    void validate(@Nonnull final Method configMethod,
                  @Nonnull final Configuration configuration) throws ConfigurationInitializationException;
}
