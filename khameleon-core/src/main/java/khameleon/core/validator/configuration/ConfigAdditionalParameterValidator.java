package khameleon.core.validator.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.annotations.ConfigOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigPattern;
import khameleon.core.annotations.validator.ConfigValidator;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo
 */
@ConfigValidator
public class ConfigAdditionalParameterValidator implements ConfigurationValidator {

    private static final String annotReq = "@ConfigOption, @ConfigPattern or @ConfigParam";

    private class CheckSingleSet {

        private Object set;

        CheckSingleSet check(final Object newSet) {
            if (set != null && newSet != null) {
                throw new ConfigurationInitializationException(
                        "Only one of " + annotReq + " may be specified: (" + set + " and " + newSet + ")");
            }
            return this;
        }

    }

    @Override
    public void validate(@Nonnull final Method configMethod,
                         @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        for (final Annotation[] paramAnnots : configMethod.getParameterAnnotations()) {
            if (paramAnnots.length == 0) {
                throw new ConfigurationInitializationException(
                        "Config has parameters but does not specify either " + annotReq + ": " + configMethod);
            }
            ConfigParam configParam = null;
            ConfigPattern configPattern = null;
            ConfigOption configOption = null;
            for (final Annotation annot : paramAnnots) {
                if (ConfigParam.class.isAssignableFrom(annot.annotationType())) {
                    configParam = (ConfigParam) annot;
                } else if (ConfigPattern.class.isAssignableFrom(annot.annotationType())) {
                    configPattern = (ConfigPattern) annot;
                } else if (ConfigOption.class.isAssignableFrom(annot.annotationType())) {
                    configOption = (ConfigOption) annot;
                }
            }
            if (configOption == null && configParam == null && configPattern == null) {
                throw new ConfigurationInitializationException(
                        "At least one of " + annotReq + " must be specified for config additional parameters" + configMethod);
            }
            new CheckSingleSet().check(configOption).check(configParam).check(configPattern);
        }

    }

}
