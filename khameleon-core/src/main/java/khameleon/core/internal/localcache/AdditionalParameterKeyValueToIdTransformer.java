package khameleon.core.internal.localcache;

import com.google.common.base.Function;

import khameleon.core.values.AdditionalParameterKeyValue;

import javax.annotation.Nullable;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:58:56 AM
 */
class AdditionalParameterKeyValueToIdTransformer implements Function<AdditionalParameterKeyValue, String> {

    @Override
    public String apply(@Nullable final AdditionalParameterKeyValue additionalParameterKeyValue) {
        if (additionalParameterKeyValue == null) {
            return "";
        }
        return String.valueOf(sumUp(additionalParameterKeyValue.keyValue()));
    }

    private static Long sumUp(final String str) {
        long l = 0;
        for (final char c : str.toCharArray()) {
            l += c;
        }
        return l;
    }

}
