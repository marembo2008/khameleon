package khameleon.core.internal.localcache;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.Local;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 9:15:09 PM
 */
@Local
public interface LocalCacheService {

  final String LOCAL_CACHE_DIR = "khameleon.localCacheDir";

  final String LOCAL_CACHE_DISABLED = "khameleon.localCache.disabled";

  void saveConfiguration(@Nonnull final Integer contextId,
                         @Nonnull final String namespace,
                         @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
                         @Nonnull final ConfigurationValue configurationValue) throws Exception;

  void saveConfiguration(@Nonnull final Integer contextId,
                         @Nonnull final String namespace,
                         @Nonnull final ConfigurationValue configurationValue) throws Exception;

  ConfigurationValue findConfiguration(@Nonnull final Integer contextId,
                                       @Nonnull final String namespace,
                                       @Nonnull final String configName,
                                       @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) throws Exception;

  void removeConfiguration(@Nonnull final Integer contextId,
                           @Nonnull final String namespace,
                           @Nonnull final ConfigurationValue configurationValue) throws Exception;

  void cleanUp();

}
