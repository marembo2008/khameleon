package khameleon.core.internal.localcache;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import khameleon.core.Configuration;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;

import static com.google.common.collect.Lists.newArrayList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:58:08 AM
 */
final class LocalCacheServiceHelper {

    private static final Joiner PARAMETER_ID_JOINER = Joiner.on('_').skipNulls();

    private static final Iterable<String> FILE_NAME_PARAMETER_ID_PREFIX = ImmutableList.of("config");

    private static final Function<AdditionalParameterValue, AdditionalParameterKeyValue> PARAMETER_KEY_VALUE_TRANSFORMER
            = new AdditionalParameterValueToKeyValueTransformer();

    private static final Function<AdditionalParameterKeyValue, String> PARAMETER_KEY_VALUE_TO_ID_TRANSFORMER
            = new AdditionalParameterKeyValueToIdTransformer();

    @Nonnull
    public static ConfigContextKey createConfigContextKey(@Nonnull final ConfigurationValue configurationValue) {
        final int applicationContextId = configurationValue.getContext().getContextId();
        final String namespaceCanonicalName = configurationValue.getConfiguration().getNamespace().getCanonicalName();
        return createConfigContextKey(applicationContextId, namespaceCanonicalName, configurationValue);
    }

    @Nonnull
    public static ConfigContextKey createConfigContextKey(final int applicationContextId,
                                                          @Nonnull final String namespaceCanonicalName,
                                                          @Nonnull final ConfigurationValue configurationValue) {

        final Configuration configuration = configurationValue.getConfiguration();
        final List<AdditionalParameterKeyValue> additionalParameterKeyValues
                = Lists.transform(configurationValue.getAdditionalParameterValues(), PARAMETER_KEY_VALUE_TRANSFORMER);
        return createConfigContextKey(applicationContextId, namespaceCanonicalName, configuration.getConfigName(),
                                      additionalParameterKeyValues);
    }

    @Nonnull
    public static ConfigContextKey createConfigContextKey(final int applicationContextId,
                                                          @Nonnull final String namespaceCanonicalName,
                                                          @Nonnull final String configName,
                                                          @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) {
        final Long namespaceId = getNamespaceId(namespaceCanonicalName);
        return ConfigContextKey.builder()
                .withApplicationContextId(applicationContextId)
                .withNamespaceCanonicalId(namespaceCanonicalName.hashCode())
                .withConfigName(configName)
                .withAdditionalParametersKey(parameterId(parameterKeyValues))
                .build();
    }

    @Nonnull
    public static String parameterId(@Nonnull final Iterable<AdditionalParameterKeyValue> apKeys) {
        final List<AdditionalParameterKeyValue> sortedKeyValues = newArrayList(apKeys);
        Collections.sort(sortedKeyValues);

        final Iterable<String> parameterKeys = Iterables.transform(sortedKeyValues,
                                                                   PARAMETER_KEY_VALUE_TO_ID_TRANSFORMER);
        final Iterable<String> mergedParameterKeys = Iterables.concat(FILE_NAME_PARAMETER_ID_PREFIX, parameterKeys);
        return PARAMETER_ID_JOINER.join(mergedParameterKeys);
    }

    private static long getNamespaceId(@Nonnull final String namespaceCanonicalName) {
        return sumUp(namespaceCanonicalName);
    }

    private static Long sumUp(final String str) {
        long l = 0;
        for (final char c : str.toCharArray()) {
            l += c;
        }
        return l;
    }

}
