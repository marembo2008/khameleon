package khameleon.core.internal.localcache;

import java.io.Serializable;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import lombok.Data;

/**
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:02:34 AM
 */
@Data
public final class ConfigContextKey implements Serializable {

  private final int applicationContextId;
  private final long namespaceCanonicalId;
  private final String configName;
  private final String additionalParametersKey;

  private ConfigContextKey(@Nonnull final int applicationContextId,
          @Nonnull final long namespaceCanonicalId,
          @Nonnull final String configName,
          @Nonnull final String additionalParametersKey) {
    this.applicationContextId = applicationContextId;
    this.namespaceCanonicalId = namespaceCanonicalId;
    this.configName = checkNotNull(configName, "The configName must not be null");
    this.additionalParametersKey = checkNotNull(additionalParametersKey,
            "The additionalParametersKey must not be null");
  }

  @Nonnull
  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private int applicationContextId;
    private long namespaceCanonicalId;
    private String configName;
    private String additionalParametersKey;

    private Builder() {
    }

    @Nonnull
    public Builder withApplicationContextId(final int applicationContextId) {
      this.applicationContextId = applicationContextId;
      return this;
    }

    @Nonnull
    public Builder withNamespaceCanonicalId(final long namespaceCanonicalId) {
      this.namespaceCanonicalId = namespaceCanonicalId;
      return this;
    }

    @Nonnull
    public Builder withAdditionalParametersKey(@Nonnull final String additionalParametersKey) {
      this.additionalParametersKey = checkNotNull(additionalParametersKey,
              "The additionalParametersKey must not be null");
      return this;
    }

    @Nonnull
    public Builder withConfigName(@Nonnull final String configName) {
      this.configName = checkNotNull(configName, "The configName must not be null");
      return this;
    }

    @Nonnull
    public ConfigContextKey build() {
      return new ConfigContextKey(applicationContextId, namespaceCanonicalId, configName, additionalParametersKey);
    }
  }
}
