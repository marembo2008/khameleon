package khameleon.core.internal.localcache;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.annotation.PreDestroy;
import jakarta.ejb.Lock;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.values.ConfigurationValue;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import static jakarta.ejb.LockType.READ;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_IN_MEMORY_CACHE_PERIOD;

/**
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:00:50 AM
 */
@Startup
@Singleton
@Lock(READ)
@ApplicationScoped
public class InMemoryCacheService {

  private static final Pattern PERIOD_UNIT = Pattern.compile("^(\\d+)(\\w+)");

  private final Map<ExpiribleConfigContextKey, ConfigurationValue> configurationValueCache = new ConcurrentHashMap<>();

  private ScheduledExecutorService executorService;

  @Nullable
  public ConfigurationValue getCachedValue(@Nonnull final ConfigContextKey configContextKey) {
    checkNotNull(configContextKey, "The configContextKey must not be null");

    //It realy does not matter the value of the period here
    final ExpiribleConfigContextKey expiribleConfigContextKey = new ExpiribleConfigContextKey(configContextKey, 0);
    return configurationValueCache.get(expiribleConfigContextKey);
  }

  public void setCachedValue(@Nonnull final ConfigContextKey configContextKey,
                             @Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configContextKey, "The configContextKey must not be null");
    checkNotNull(configurationValue, "The configurationValue must not be null");

    final Configuration configuration = configurationValue.getConfiguration();
    final Optional<ConfigurationData> inMemoryCache = configuration.getConfigurationData(
            CONFIGURATION_IN_MEMORY_CACHE_PERIOD);
    checkState(inMemoryCache.isPresent(), "Configuration must be annotated with @InMemoryCache: {%s}", configuration);

    final String periodValue = inMemoryCache.get().getDataValue();
    final Matcher matcher = PERIOD_UNIT.matcher(periodValue);
    checkState(matcher.find(), "The following periodValue for inmemorycache is invalid: {%s} ", periodValue);

    final String durationValue = matcher.group(1);
    final String unit = matcher.group(2);
    final long duration = Long.parseLong(durationValue);
    final TimeUnit timeUnit = TimeUnit.valueOf(unit);
    final long cacheDuration = TimeUnit.SECONDS.convert(duration, timeUnit);
    final ExpiribleConfigContextKey expiribleConfigContextKey
            = new ExpiribleConfigContextKey(configContextKey, cacheDuration);
    configurationValueCache.put(expiribleConfigContextKey, configurationValue);
  }

  @PreDestroy
  public void cleanUp() {
    if (executorService != null) {
      executorService.shutdownNow();
    }

    configurationValueCache.clear();
  }

  @Schedule(hour = "*", minute = "*", second = "*/1", persistent = false)
  private void checkConfiguration() {
    ImmutableList
            .copyOf(configurationValueCache.keySet())
            .stream()
            .filter((expiribleConfigContextKey) -> (expiribleConfigContextKey.isExpired()))
            .forEach((expiribleConfigContextKey) -> {
              configurationValueCache.remove(expiribleConfigContextKey);
            });
  }

  private void initializeManual() {
    executorService = Executors.newSingleThreadScheduledExecutor();
    executorService.scheduleAtFixedRate(this::checkConfiguration, 1, 1, TimeUnit.SECONDS);
  }

  @Nonnull
  static InMemoryCacheService createInMemoryCacheService() {
    final InMemoryCacheService inMemoryCacheService = new InMemoryCacheService();
    inMemoryCacheService.initializeManual();
    return inMemoryCacheService;
  }

  private static final class ExpiribleConfigContextKey implements Serializable {

    private final ConfigContextKey configContextKey;

    private final long expirationTime;

    private ExpiribleConfigContextKey(@Nonnull final ConfigContextKey configContextKey, final long durationInSeconds) {
      this.configContextKey = configContextKey;
      this.expirationTime = (Calendar.getInstance().getTimeInMillis() / 1000) + durationInSeconds;
    }

    public boolean isExpired() {
      final long now = Calendar.getInstance().getTimeInMillis() / 1000;
      return now > expirationTime;
    }

    @Override
    public int hashCode() {
      return configContextKey.hashCode();
    }

    @Override
    public boolean equals(@Nullable final Object obj) {
      if (obj instanceof ExpiribleConfigContextKey) {
        return Objects.equals(configContextKey, ((ExpiribleConfigContextKey) obj).configContextKey);
      }
      return false;
    }

  }

}
