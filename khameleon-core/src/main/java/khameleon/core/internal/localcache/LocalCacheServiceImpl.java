package khameleon.core.internal.localcache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Resource;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;

import static java.lang.String.format;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_IN_MEMORY_CACHE_PERIOD;
import static khameleon.core.internal.localcache.LocalCacheServiceHelper.createConfigContextKey;
import static khameleon.core.internal.localcache.LocalCacheServiceHelper.parameterId;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * If we are in a java-enabled server, it will be fired up automatically.
 *
 * @author mochieng
 */
@Singleton
@ApplicationScoped
@SuppressWarnings("ClassWithMultipleLoggers")
public class LocalCacheServiceImpl implements LocalCacheService {

  private static final XStream XSTREAM = new XStream();

  private static final Logger LOG = Logger.getLogger(LocalCacheService.class.getName());

  private static final Pattern STRIP_OFF_VERSIONS_PATTERN = Pattern.compile("\\-(\\d+(\\.)*)+\\-.*$");

  private static final String SYSTEM_PROPERTY_USE_MODULE_NAME = "anosym.khameleon.localcache.separateByModuleNames";

  private static final Function<AdditionalParameterValue, AdditionalParameterKeyValue> PARAMETER_KEY_VALUE_TRANSFORMER
          = new AdditionalParameterValueToKeyValueTransformer();

  private final LoadingCache<ConfigContextKey, ConfigurationValue> loadingConfigurationValueLocalCache = CacheBuilder
          .newBuilder()
          .concurrencyLevel(20)
          .expireAfterWrite(30, TimeUnit.DAYS) //If an update is done, we will be updated.
          .initialCapacity(10000)
          .build(new CacheLoader<ConfigContextKey, ConfigurationValue>() {

            @Override
            @Nullable
            public ConfigurationValue load(@Nonnull final ConfigContextKey key) throws Exception {
              return loadFromLocalCache(key);
            }

          });

  @Resource(lookup = "java:module/ModuleName")
  private String moduleName;

  @SuppressWarnings({"NonConstantLogger", "ClassWithMultipleLoggers"})
  private Logger logger;

  private File localCacheDir;

  @EJB
  private InMemoryCacheService inMemoryCacheService;

  /**
   * Will be called by EJB/CDI Container.
   */
  @PostConstruct
  synchronized void initialize() {
    //If called manually, initialize the inmemory cache.
    if (inMemoryCacheService == null) {
      inMemoryCacheService = InMemoryCacheService.createInMemoryCacheService();
    }

    this.logger = Logger.getLogger(format("%s.%s", LocalCacheServiceImpl.class.getName(), moduleName));
    logger.log(Level.INFO,
               "({0}) Localcache initialzed by CDIContainer. Local Cache Dir:({1})",
               new Object[]{moduleName, getLocalCacheDir()});
  }

  @PreDestroy
  @Override
  public synchronized void cleanUp() {
    inMemoryCacheService.cleanUp();
    clearMemCache();
  }

  @VisibleForTesting
  void clearMemCache() {
    loadingConfigurationValueLocalCache.invalidateAll();
  }

  @VisibleForTesting
  File getConfigFile(final Integer applicationContextId,
                     final String namespaceCanonicalName,
                     final ConfigurationValue confValue) {
    return getConfigFile(applicationContextId,
                         namespaceCanonicalName,
                         confValue.getConfiguration().getConfigName(),
                         parameterIdFromValue(confValue.getAdditionalParameterValues()));
  }

  @VisibleForTesting
  static String removeVersions(@Nonnull final String moduleName) {
    //We need to remove version numbers from the module name;
    //This ensures that subsequent module upgrade does not render the previous caches useless!
    if (!isNullOrEmpty(moduleName)) {
      final Matcher matcher = STRIP_OFF_VERSIONS_PATTERN.matcher(moduleName);
      if (matcher.find()) {
        return matcher.replaceAll("");
      }
    }
    return moduleName;
  }

  private static boolean separateByModules() {
    return Boolean.valueOf(System.getProperty(SYSTEM_PROPERTY_USE_MODULE_NAME, "false"));
  }

  @Override
  public void saveConfiguration(@Nonnull final Integer contextId,
                                @Nonnull final String namespace,
                                @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
                                @Nonnull final ConfigurationValue configurationValue) throws Exception {
    checkNotNull(configurationValue, "ConfigurationValue must not be null");
    checkNotNull(contextId, "ApplicationContextId must not be null");
    checkNotNull(namespace, "Namespace canonicalName must not be null");
    checkNotNull(additionalParameterKeyValues, "additionalParameterKeyValues must not be null");

    //We replace the configuration additional parameters, as these were the request parameters.
    configurationValue.updateAdditionalParameters(additionalParameterKeyValues);
    saveConfiguration(contextId, namespace, configurationValue);
  }

  @Override
  public void saveConfiguration(@Nonnull final Integer contextId,
                                @Nonnull final String namespace,
                                @Nonnull final ConfigurationValue configurationValue) throws Exception {
    checkNotNull(configurationValue, "Configuration must not be null");
    checkNotNull(contextId, "ApplicationContextId must not be null");
    checkNotNull(namespace, "Namespace canonicalName must not be null");

    final Configuration configuration = configurationValue.getConfiguration();

    logger.log(Level.FINE,
               "Saving Configuration Value '{'{0}={1}={2}={3}'}",
               new Object[]{contextId, namespace, configuration.getConfigName(), configurationValue.
                            getConfigValue()});

    final ConfigContextKey configContextKey = createConfigContextKey(contextId, namespace, configurationValue);

    final Optional<ConfigurationData> inMemoryCache = configuration.getConfigurationData(
            CONFIGURATION_IN_MEMORY_CACHE_PERIOD);
    if (!inMemoryCache.isPresent()) {
      final File configFile = getConfigFile(contextId, namespace, configurationValue);

      logger.log(Level.FINE,
                 "Saving Configuration to File:  {0}={1}",
                 new Object[]{configFile.getAbsolutePath(), configurationValue.getConfigValue()});

      saveConfiguration(configFile, configurationValue);

      //Save to cache
      loadingConfigurationValueLocalCache.put(configContextKey, configurationValue);
    } else {
      inMemoryCacheService.setCachedValue(configContextKey, configurationValue);
    }
  }

  @Override
  public void removeConfiguration(@Nonnull final Integer contextId,
                                  @Nonnull final String namespace,
                                  @Nonnull final ConfigurationValue configurationValue) throws Exception {
    checkNotNull(configurationValue, "Configuration must not be null");
    checkNotNull(contextId, "ApplicationContextId must not be null");
    checkNotNull(namespace, "Namespace canonicalName must not be null");

    final File configFile = getConfigFile(contextId, namespace, configurationValue);
    if (configFile.exists()) {
      configFile.delete();
    }

    //good, add to cache or replace.
    removeCacheConfiguration(contextId, namespace, configurationValue);
  }

  @Override
  @Nullable
  public ConfigurationValue findConfiguration(@Nonnull final Integer contextId,
                                              @Nonnull final String namespace,
                                              @Nonnull final String configName,
                                              @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) throws Exception {
    checkNotNull(configName, "configName must not be null");
    checkNotNull(contextId, "ApplicationContextId must not be null");
    checkNotNull(namespace, "Namespace canonicalName must not be null");
    checkNotNull(parameterKeyValues, "additionalParameterKeyValues must not be null");

    if (Boolean.valueOf(System.getProperty(LOCAL_CACHE_DISABLED))) {
      return null;
    }

    final Long namespaceId = getNamespaceId(namespace);
    logger.log(Level.FINE,
               "Finding configuration for: ContextId={0}, Namespace='{'{1}, id={2}'}', ConfigName={3}, ConfigParam={4}",
               new Object[]{contextId, namespace, namespaceId, configName, parameterKeyValues});
    final ConfigContextKey configContextKey = createConfigContextKey(contextId, namespace, configName,
                                                                     parameterKeyValues);
    final ConfigurationValue inMemoryCachedValue = inMemoryCacheService.getCachedValue(configContextKey);
    if (inMemoryCachedValue != null) {
      return inMemoryCachedValue;
    }

    try {
      return loadingConfigurationValueLocalCache.get(configContextKey);
    } catch (final InvalidCacheLoadException invalidCacheLoadException) {
      //We expect that there might not be a value for the key
      return null;
    }
  }

  private void saveConfiguration(final File configFile, final ConfigurationValue configurationValue) throws Exception {
    try (final FileOutputStream outputStream = new FileOutputStream(configFile)) {
      final String configurationValueContent = XSTREAM.toXML(configurationValue);
      outputStream.write(configurationValueContent.getBytes());
    }
  }

  @Nullable
  private ConfigurationValue findConfiguration0(@Nonnull final File configFile) throws Exception {
    try (final FileInputStream inputStream = new FileInputStream(configFile)) {
      return (ConfigurationValue) XSTREAM.fromXML(inputStream);
    } catch (final ConversionException uee) {

      //Serialization or deserialization may have changed, dont stop the entire configuration system!
      LOG.log(Level.WARNING, "Reading Configuration Error: " + configFile, uee);
      return null;
    }
  }

  private void removeCacheConfiguration(final Integer applicationContextId,
                                        final String namespaceCanonicalName,
                                        final ConfigurationValue configurationValue) {
    final ConfigContextKey anKey = createConfigContextKey(applicationContextId, namespaceCanonicalName,
                                                          configurationValue);
    loadingConfigurationValueLocalCache.invalidate(anKey);
  }

  @Nullable
  private ConfigurationValue loadFromLocalCache(final ConfigContextKey configContextKey) throws Exception {
    final File configFile = getConfigFile(configContextKey.getApplicationContextId(),
                                          configContextKey.getNamespaceCanonicalId(),
                                          configContextKey.getConfigName(),
                                          configContextKey.getAdditionalParametersKey());
    if (!configFile.exists() || configFile.length() < 1) {
      return null;
    }

    return findConfiguration0(configFile);
  }

  private static long getNamespaceId(@Nonnull final String namespaceCanonicalName) {
    return sumUp(namespaceCanonicalName);
  }

  private File getLocalCacheDir() {
    if (localCacheDir == null) {
      final File rootCacheDir = new File(System.getProperty(LOCAL_CACHE_DIR, ".khameleon"));
      localCacheDir = separateByModules() ? new File(rootCacheDir, moduleName) : rootCacheDir;
      if (!localCacheDir.exists()) {
        localCacheDir.mkdirs();
      }
    }
    return localCacheDir;
  }

  private File getAppContextDir(final Integer applicationContextId) {
    final File appContextDir = new File(getLocalCacheDir(), String.valueOf(applicationContextId));
    if (!appContextDir.exists()) {
      appContextDir.mkdirs();
    }
    return appContextDir;
  }

  private File getNamespaceDir(final File appContextDir, final Long namespaceCanonicalId) {
    final String namespaceDirName = String.valueOf(namespaceCanonicalId);
    final File namespaceDir = new File(appContextDir, namespaceDirName);
    if (!namespaceDir.exists()) {
      namespaceDir.mkdirs();
    }
    return namespaceDir;
  }

  private File getConfigNameDir(final File namespaceDir, final String configName) {
    final File configDir = new File(namespaceDir, configName);
    if (!configDir.exists()) {
      configDir.mkdirs();
    }
    return configDir;
  }

  private File getConfigFile(
          final Integer applicationContextId,
          final String namespaceCanonicalName,
          final String configName,
          final String parameterId) {
    return getConfigFile(applicationContextId, getNamespaceId(namespaceCanonicalName), configName, parameterId);
  }

  private File getConfigFile(final Integer applicationContextId,
                             final Long namespaceCanonicalId,
                             final String configName,
                             final String parameterId) {
    final File appContextDir = getAppContextDir(applicationContextId);
    final File namespaceDir = getNamespaceDir(appContextDir, namespaceCanonicalId);
    final File configDir = getConfigNameDir(namespaceDir, configName);
    final File configFile = new File(configDir, parameterId + ".xlcs");
    if (!configFile.getParentFile().exists()) {
      configFile.getParentFile().mkdirs();
    }

    return configFile;
  }

  private static Long sumUp(final String str) {
    long l = 0;
    for (final char c : str.toCharArray()) {
      l += c;
    }
    return l;
  }

  private static String parameterIdFromValue(final Iterable<AdditionalParameterValue> apValues) {
    return parameterId(Iterables.transform(apValues, PARAMETER_KEY_VALUE_TRANSFORMER));
  }

}
