package khameleon.core.internal.localcache;

import com.google.common.base.Function;

import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.AdditionalParameterValue;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 14, 2015, 10:58:56 AM
 */
class AdditionalParameterValueToKeyValueTransformer implements Function<AdditionalParameterValue, AdditionalParameterKeyValue> {

    @Override
    public AdditionalParameterKeyValue apply(@Nonnull final AdditionalParameterValue additionalParameterValue) {
        checkNotNull(additionalParameterValue, "The additionalParameterValue must not be null");

        final String paramKey = additionalParameterValue.getAdditionalParameter().getParameterKey();
        final String paramValue = additionalParameterValue.getParameterValue();
        return new AdditionalParameterKeyValue(paramKey, paramValue);
    }

}
