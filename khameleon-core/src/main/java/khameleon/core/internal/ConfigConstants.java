package khameleon.core.internal;

import java.util.regex.Pattern;

/**
 *
 * @author marembo
 */
public final class ConfigConstants {

    public static final Pattern METHOD_SYNTAX = Pattern.compile("^(is|get)\\w+");
    public static final Pattern METHOD_START_SYNTAX = Pattern.compile("^(is|get)");
    public static final String KHAMELEON_GENERATEDSOURCES_SOURCEPATH = "target/generated-sources/khameleon";
    public static final String KHAMELEON_CLASSES_CLASSPATH = "target/classes";
    public static final String KHAMELEON_TESTCLASSES_CLASSPATH = "target/test-classes";
    public static final String KHAMELEON_GENERATEDSOURCES_BUILDPATH = "anosym.khameleon.generatedsources.buildpath";
    public static final String KHAMELEON_CALENDAR_FORMAT_KEY = "Calendar Format";
    public static final String KHAMELEON_ENUM_VALUES = "Optional Values";
    public static final String KHAMELEON_CONFIGPARAM_PATTERN = "Parameter Expected Pattern";
    public static final String KHAMELEON_CONFIG_SYSTEM_CONSTANT = "@ConfigSystemConstant";

    private static final String KHAMELEON_LOCAL_CACHE_SERVICE_UPDATE_INTERVAL = "anosym.khamekeon.localCacheService.updateInterval";

    static {
        System.setProperty(KHAMELEON_GENERATEDSOURCES_BUILDPATH, KHAMELEON_CLASSES_CLASSPATH);
    }

    private ConfigConstants() {
    }

    public static int getLocalCacheServiceUpdateInterval() {
        return Integer.parseInt(System.getProperty(KHAMELEON_LOCAL_CACHE_SERVICE_UPDATE_INTERVAL, "600000"));
    }
}
