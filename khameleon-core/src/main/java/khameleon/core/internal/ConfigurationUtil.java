package khameleon.core.internal;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.text.DateFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.NonNull;

import static com.google.common.base.CharMatcher.is;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Splitter.on;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;

/**
 *
 * @author marembo
 */
public final class ConfigurationUtil {

    private ConfigurationUtil() {
    }

    private static final CharMatcher ARRAY_BOX = is('[').or(CharMatcher.is(']'));

    private static final Splitter COMPONENT_SPLITTER = on(",").trimResults().omitEmptyStrings();

    private static final Logger LOG = Logger.getLogger(ConfigurationUtil.class.getName());

    private static final Map<String, Class> PRIMITIVE_WRAPPER_MAPPING_NAMES = loadPrimitiveMappingByName();

    private static final Map<Class, Class> PRIMITIVE_WRAPPER_MAPPING = loadPrimitiveMapping();

    private static final Map<PrimitiveClass, PrimitiveType> PRIMITVE_TYPE_MAPPINGS = loadPrimitiveMappingTypes();

    public static final List<String> OBJECT_MEMBERS = loadObjectMembers();

    private static Map loadPrimitiveMappingByName() {
        Map<String, Class> maps = new HashMap<>();
        maps.put("boolean", boolean.class);
        maps.put("char", char.class);
        maps.put("byte", byte.class);
        maps.put("short", short.class);
        maps.put("int", int.class);
        maps.put("long", long.class);
        maps.put("float", float.class);
        maps.put("double", double.class);
        maps.put("void", void.class);
        return maps;
    }

    public static PrimitiveType getPrimitiveType(Class primitiveType) {
        for (PrimitiveClass primitiveClass : PRIMITVE_TYPE_MAPPINGS.keySet()) {
            if (primitiveClass.is(primitiveType)) {
                return PRIMITVE_TYPE_MAPPINGS.get(primitiveClass);
            }
        }
        throw new IllegalStateException("PrimitveType: " + primitiveType + ", is not mapped");
    }

    private static Map loadPrimitiveMapping() {
        Map<Class, Class> maps = new HashMap<>();
        maps.put(Boolean.class, boolean.class);
        maps.put(Character.class, char.class);
        maps.put(Byte.class, byte.class);
        maps.put(Short.class, short.class);
        maps.put(Integer.class, int.class);
        maps.put(Long.class, long.class);
        maps.put(Float.class, float.class);
        maps.put(Double.class, double.class);
        maps.put(Void.class, void.class);
        return maps;
    }

    public static boolean isPrimitiveOrPrimitveWrapper(Class cls) {
        return cls.isPrimitive() || PRIMITIVE_WRAPPER_MAPPING.containsKey(cls);
    }

    public static Class<?> initializeClass(@NonNull final String classType) {
        if (PRIMITIVE_WRAPPER_MAPPING_NAMES.containsKey(classType)) {
            return PRIMITIVE_WRAPPER_MAPPING_NAMES.get(classType);
        }

        try {
            return Class.forName(classType);
        } catch (final ClassNotFoundException ex) {
            throw new IllegalArgumentException("No class defined for:" + classType, ex);
        }
    }

    private static Map loadPrimitiveMappingTypes() {
        Map<PrimitiveClass, PrimitiveType> maps = new HashMap<>();
        maps.put(new PrimitiveClass(boolean.class, Boolean.class), PrimitiveType.BOOLEAN);
        maps.put(new PrimitiveClass(char.class, Character.class), PrimitiveType.CHAR);
        maps.put(new PrimitiveClass(byte.class, Byte.class), PrimitiveType.BYTE);
        maps.put(new PrimitiveClass(short.class, Short.class), PrimitiveType.SHORT);
        maps.put(new PrimitiveClass(int.class, Integer.class), PrimitiveType.INT);
        maps.put(new PrimitiveClass(long.class, Long.class), PrimitiveType.LONG);
        maps.put(new PrimitiveClass(float.class, Float.class), PrimitiveType.FLOAT);
        maps.put(new PrimitiveClass(double.class, Double.class), PrimitiveType.DOUBLE);
        maps.put(new PrimitiveClass(void.class, Void.class), PrimitiveType.VOID);
        return maps;
    }

    public static Calendar getCalendar(String value, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format);
            DateFormatter formatter = new DateFormatter(df);
            Date formattedValue = (Date) formatter.stringToValue(value);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.setTime(formattedValue);
            return cal;
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static List loadObjectMembers() {
        List<String> maps = Lists.newArrayList();
        for (Method m : Object.class.getMethods()) {
            maps.add(m.getName());
        }
        return maps;
    }

    public static String capitalize(String str) {
        if (Strings.isNullOrEmpty(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (String s : str.split(" ")) {
            s = s.trim().toLowerCase();
            if (!s.isEmpty()) {
                sb.append(Character.toUpperCase(s.charAt(0)));
                if (s.length() > 1) {
                    sb.append(s.substring(1));
                }
                sb.append(" ");
            }
        }
        return sb.toString().trim();
    }

    /**
     * Returns an empty array if the array string encloses whitespace characters.
     *
     * @param array
     * @return
     */
    @Nonnull
    public static String[] fromArrayString(@Nullable final String array) {
        final String nonNullArray = firstNonNull(array, "[]");
        final String arrayComponents = ARRAY_BOX.trimFrom(nonNullArray);

        //empty array!
        if (arrayComponents.trim().isEmpty()) {
            return new String[0];
        }

        return COMPONENT_SPLITTER.splitToList(arrayComponents).toArray(new String[0]);
    }

    @Nonnull
    public static String appendValueToArrayStringIfNotMember(@Nullable final String array, @Nullable final String value) {
        //The array could be null
        final String nonNullArray = firstNonNull(array, "[]");
        if (isNullOrEmpty(value)) {
            return nonNullArray;
        }

        final List<String> listArray = newArrayList(fromArrayString(nonNullArray));
        if (!listArray.contains(value)) {
            listArray.add(value);
        }

        return Arrays.toString(listArray.toArray(new String[0]));
    }

    @Nonnull
    public static Method getConfigMethod(@Nonnull final Class<?> configClassOrInterface,
                                         @Nonnull final String configName) {
        checkNotNull(configClassOrInterface, "ConfigInterface or class must not be null");
        checkArgument(!Strings.nullToEmpty(configName).trim().isEmpty(), "configName must not be null or empty");

        final String configNameProperty = configName.substring(0, 1).toUpperCase() + configName.substring(1);
        final String get = "get" + configNameProperty;
        final String is = "is" + configNameProperty;
        try {
            return configClassOrInterface.getDeclaredMethod(get, new Class[]{});
        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.log(Level.SEVERE, null, ex);
            try {
                return configClassOrInterface.getDeclaredMethod(is, new Class[]{});
            } catch (NoSuchMethodException | SecurityException ex1) {
                throw new IllegalStateException("configuration for config name: " + configName + " not found", ex1);
            }
        }
    }

}
