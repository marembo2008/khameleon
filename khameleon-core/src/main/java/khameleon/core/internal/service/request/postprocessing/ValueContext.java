package khameleon.core.internal.service.request.postprocessing;

import java.lang.reflect.Method;
import java.util.List;

import javax.annotation.Nonnull;

import khameleon.core.values.AdditionalParameterKeyValue;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 24, 2019, 8:10:35 PM
 */
@Data
@Builder(toBuilder = true)
public class ValueContext {

  @Nonnull
  private final Class<?> configInterface;

  @Nonnull
  private final Method configMethod;

  @Nonnull
  private final List<AdditionalParameterKeyValue> parameterKeyValues;

  @Nonnull
  private final Object[] args;

  @Nonnull
  private final Object value;

}
