package khameleon.core.internal.service.request;

import javax.annotation.Nonnull;

import com.google.common.annotations.VisibleForTesting;
import jakarta.enterprise.concurrent.ManagedExecutorService;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.internal.service.ConfigurationDependencyServiceHelper.lookup;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2019, 11:37:28 AM
 */
public class ConfigurationServiceCacheServiceLookup {

  private static final ThreadLocal<ConfigurationServiceCacheService> CONFIGURATIONSERVICECACHESERVICE_LOCAL
          = new ThreadLocal<ConfigurationServiceCacheService>() {

    @Nonnull
    @Override
    protected ConfigurationServiceCacheService initialValue() {
      return checkNotNull(lookup(ConfigurationServiceCacheService.class),
                          "The BeanManager.lookup(ConfigurationServiceCacheService.class) must not be null");
    }

  };

  @Nonnull
  public static ConfigurationServiceCacheService getConfigurationServiceCacheService() {
    return CONFIGURATIONSERVICECACHESERVICE_LOCAL.get();
  }

  @VisibleForTesting
  public static void setConfigurationServiceCacheService(@Nonnull final ManagedExecutorService managedExecutorService) {
    final ConfigurationServiceCacheService configurationServiceCacheService = new ConfigurationServiceCacheService();
    configurationServiceCacheService.initialize(managedExecutorService);

    CONFIGURATIONSERVICECACHESERVICE_LOCAL.set(configurationServiceCacheService);
  }

  @VisibleForTesting
  public static void cleanConfigurationServiceCacheService() {
    CONFIGURATIONSERVICECACHESERVICE_LOCAL.remove();
  }

}
