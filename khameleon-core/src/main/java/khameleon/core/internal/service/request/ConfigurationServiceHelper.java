package khameleon.core.internal.service.request;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import khameleon.core.ApplicationContext;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo
 */
public interface ConfigurationServiceHelper {

    @Nonnull
    ConfigurationRequestService getConfigurationRequestService();

    @Nullable
    <T> T callService(@Nonnull final ApplicationContext applicationContext,
                      @Nonnull final String namespaceCanonicalName,
                      @Nonnull final String configName,
                      @Nonnull final List<AdditionalParameterKeyValue> additionalParameters,
                      @Nullable final ConfigurationValue defaultValue);

}
