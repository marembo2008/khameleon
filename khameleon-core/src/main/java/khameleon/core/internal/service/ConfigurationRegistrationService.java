package khameleon.core.internal.service;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.Namespace;

/**
 *
 * @author marembo
 */
public interface ConfigurationRegistrationService {

    void registerNamespace(@Nonnull final Namespace namespace);

    void registerConfiguration(@Nonnull final Configuration configuration);

}
