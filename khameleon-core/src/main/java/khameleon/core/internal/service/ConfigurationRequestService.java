package khameleon.core.internal.service;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo
 */
public interface ConfigurationRequestService {

    @Nullable
    ConfigurationValue findConfigurationValue(@Nonnull final ApplicationContext applicationContext,
                                              @Nonnull final String namespaceCanonicalName,
                                              @Nonnull final String configName,
                                              @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues);

    default boolean registerConfigurationChangeListener(@Nonnull final ApplicationMode applicationMode,
                                                        @Nonnull final String changeListenerWsdlLocation,
                                                        @Nullable final String projectName) {
        return false;
    }

}
