package khameleon.core.internal.service.changelistener;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 20, 2018, 3:56:06 AM
 */
public enum ConfigurationChangeType {

    UPDATE,
    DELETE;

}
