package khameleon.core.internal.service.request;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ValueConverter;
import khameleon.core.annotations.ValueConverter.Converter;
import khameleon.core.internal.ConfigurationUtil;
import khameleon.core.values.ConfigurationValue;
import lombok.NonNull;
import org.atteo.classindex.ClassIndex;

import static java.util.stream.Collectors.toMap;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CALLENDAR_FORMAT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_COLLECTION_COMPONENT_TYPE;
import static khameleon.core.internal.ConfigurationUtil.fromArrayString;
import static khameleon.core.internal.ConfigurationUtil.getCalendar;
import static khameleon.core.internal.ConfigurationUtil.initializeClass;
import static khameleon.core.internal.ConfigurationUtil.isPrimitiveOrPrimitveWrapper;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 20, 2015, 12:47:33 AM
 */
final class ConfigurationValueConverter {

    private static final Logger LOG = Logger.getLogger(ConfigurationValueConverter.class.getName());

    private static final Map<Class, Converter> CONVERTER_MAP;

    static {
        CONVERTER_MAP = ImmutableList
                .copyOf(ClassIndex.getSubclasses(Converter.class))
                .stream()
                .map(ConfigurationValueConverter::newInstance)
                .collect(toMap(Converter::getDestinationType, Function.identity()));
    }

    public static <T> T convert(@Nonnull final ConfigurationValue configurationValue) {
        checkNotNull(configurationValue, "The configuration value must not be null");

        final String value = configurationValue.getConfigValue();
        final Configuration configuration = configurationValue.getConfiguration();
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }

        LOG.fine(configurationValue.toString());

        final String valueType = configuration.getTypeClass();

        final Class<?> typeClass = initializeClass(valueType);

        //Must be checked before array check..
        if (Collection.class.isAssignableFrom(typeClass)) {
            final Collection<T> collection
                    = checkNotNull(createCollection(typeClass), "Unsupported collection type: %s", typeClass);
            final String componentType = configuration
                    .getConfigurationData(CONFIGURATION_COLLECTION_COMPONENT_TYPE)
                    .toJavaUtil()
                    .map(ConfigurationData::getDataValue)
                    .orElseThrow(() -> new IllegalStateException("Collection type without component type"));
            final String values[] = fromArrayString(value);
            Arrays.stream(values).forEach((collValue) -> {
                addToCollection(collection, componentType, collValue, configuration);
            });

            return (T) collection;
        }

        final Optional<ConfigurationData> confData = configuration.getConfigurationData(
                ConfigurationDataOption.CONFIGURATION_ARRAY);
        if (confData.isPresent()) {
            //create an array.
            String componentTypeClass = confData.get().getDataValue();
            Class componentType = initializeClass(componentTypeClass);
            //we separate the array element.
            String values[] = fromArrayString(value);
            T array = (T) Array.newInstance(componentType, values.length);
            for (int i = 0; i < values.length; i++) {
                Object val = convert(componentTypeClass, values[i], configuration);
                Array.set(array, i, val);
            }

            return array;
        }

        return convert(valueType, value, configuration);
    }

    @Nullable
    private static <T> Collection<T> createCollection(@NonNull final Class<?> valueClass) {
        return List.class.isAssignableFrom(valueClass)
                ? new ArrayList<>() : Set.class.isAssignableFrom(valueClass)
                ? new HashSet<>() : null;
    }

    private static <T> void addToCollection(@NonNull final Collection<T> collection,
                                            @NonNull final String valueClassType,
                                            @NonNull final String value,
                                            @NonNull final Configuration configuration) {
        final T convertedValue = convert(valueClassType, value, configuration);
        collection.add(convertedValue);
    }

    @SuppressWarnings({"UnnecessaryBoxing"})
    private static <T> T convert(@NonNull final String valueClassType,
                                 @NonNull final String value,
                                 @NonNull final Configuration configuration) {
        final Class<?> cls = initializeClass(valueClassType);
        //check for primitives
        if (isPrimitiveOrPrimitveWrapper(cls)) {
            switch (ConfigurationUtil.getPrimitiveType(cls)) {
                case INT:
                    return (T) new Integer(value);
                case LONG:
                    return (T) new Long(value);
                case BYTE:
                    return (T) new Byte(value);
                case SHORT:
                    return (T) new Short(value);
                case CHAR:
                    return (T) Character.valueOf(value.charAt(0));
                case FLOAT:
                    return (T) new Float(value);
                case DOUBLE:
                    return (T) new Double(value);
                case BOOLEAN:
                    return (T) Boolean.valueOf(value);
                default:
                    throw new RuntimeException("Unknown primitive mapping: " + cls);
            }
        } else if (String.class.isAssignableFrom(cls)) {
            return (T) value;
        } else if (BigDecimal.class.isAssignableFrom(cls)) {
            return (T) new BigDecimal(value);
        } else if (BigInteger.class.isAssignableFrom(cls)) {
            return (T) new BigInteger(value);
        } else if (cls.isEnum()) {
            final Class<? extends Enum> enumclass = (Class<? extends Enum>) cls;
            return (T) Enum.valueOf(enumclass, value);
        } else if (Calendar.class.isAssignableFrom(cls)) {
            final String format;
            final Optional<ConfigurationData> calendarConfData
                    = configuration.getConfigurationData(CONFIGURATION_CALLENDAR_FORMAT);
            if (!calendarConfData.isPresent()) {
                format = "yyyy-MM-dd HH:mm:ss";
            } else {
                format = calendarConfData.get().getDataValue();
            }
            return (T) getCalendar(value, format);
        } else {
            return applyConverterOrThrow(valueClassType, value, configuration);
        }
    }

    private static <T> T applyConverterOrThrow(@NonNull final String expectedType,
                                               @NonNull final String value,
                                               @NonNull final Configuration configuration) {
        final Class<?> expectedClassType = initializeClass(expectedType);
        final ValueConverter.Converter<T> converter = configuration
                .getConfigurationData(ConfigurationDataOption.CONFIGURATION_VALUE_CONVERTER)
                .toJavaUtil()
                .map(ConfigurationData::getDataValue)
                .map(ConfigurationUtil::initializeClass)
                .map((cls) -> (Class<Converter<T>>) cls)
                .map(ConfigurationValueConverter::newInstance)
                .orElseGet(() -> {
                    final Converter<T> autoAppliedConverter = CONVERTER_MAP.get(expectedClassType);
                    if (autoAppliedConverter == null) {
                        throw new IllegalArgumentException("class type unsupported: " + expectedType);
                    }

                    return autoAppliedConverter;
                });
        checkState(converter.isCompatible(expectedClassType),
                   "Cannot convert <{}> using: {}",
                   expectedType,
                   converter);

        return converter.apply(value);
    }

    @NonNull
    private static <T> T newInstance(@NonNull final Class<T> typeClass) {
        try {
            return typeClass.newInstance();
        } catch (final InstantiationException | IllegalAccessException ex) {
            throw new IllegalArgumentException("No class definition for: " + typeClass, ex);
        }
    }

}
