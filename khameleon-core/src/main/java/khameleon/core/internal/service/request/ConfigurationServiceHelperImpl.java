package khameleon.core.internal.service.request;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CharMatcher;
import com.google.common.base.Optional;
import jakarta.enterprise.inject.spi.CDI;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.Namespace;
import khameleon.core.annotations.ConfigCacheValue;
import khameleon.core.configsource.ConfigSource;
import khameleon.core.configsource.Source;
import khameleon.core.internal.localcache.LocalCacheService;
import khameleon.core.internal.service.ConfigurationRequestService;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DYNAMIC_DEFAULT_VALUE;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_LOCAL_CACHE_VALUE;
import static khameleon.core.internal.service.ConfigurationDependencyServiceHelper.lookup;
import static khameleon.core.internal.service.request.ConfigurationValueConverter.convert;
import static khameleon.core.internal.util.KhameleonParameters.CONFIG_SOURCE_KEY;

/**
 *
 * @author marembo
 */
@Slf4j
public class ConfigurationServiceHelperImpl implements ConfigurationServiceHelper, Serializable {

  private static final long serialVersionUID = 34784893443463L;

  private static final CharMatcher EMPTY_ARRAY = CharMatcher.anyOf("[]");

  private static final Logger LOG = Logger.getLogger(ConfigurationServiceHelperImpl.class.getName());

  @Override
  public ConfigurationRequestService getConfigurationRequestService() {
    return createConfigurationRequestService();
  }

  @VisibleForTesting
  public LocalCacheService getLocalCacheService() {
    return lookup(LocalCacheService.class);
  }

  @Override
  public <T> T callService(@Nonnull final ApplicationContext applicationContext,
                           @Nonnull final String namespace,
                           @Nonnull final String configName,
                           @Nonnull final List<AdditionalParameterKeyValue> additionalParameterValues,
                           @Nullable final ConfigurationValue defaultValue) {
    try {
      final LocalCacheService localCacheService = getLocalCacheService();
      ConfigurationValue configurationValue
              = localCacheService.findConfiguration(applicationContext.getContextId(),
                                                    namespace,
                                                    configName,
                                                    additionalParameterValues);
      if (!acceptCachedValue(configurationValue)) {
        LOG.log(Level.FINE,
                "Failed to find cached configuration from LocalCacheService: ContextId={0}, Namespace={1}, ConfigName={2}, AdditionalParams={3}",
                new Object[]{applicationContext, namespace, configName, additionalParameterValues});
        final ConfigurationRequestService configService = getConfigurationRequestService();

        LOG.log(Level.FINE,
                "Requesting ConfigurationValue from ConfigurationRequestService: {0}",
                configService);

        configurationValue = configService.findConfigurationValue(
                applicationContext, namespace, configName, additionalParameterValues);

        configurationValue = configurationValue != null ? configurationValue : defaultValue;
        //still we may be null.
        if (configurationValue == null) {
          //nothing found.
          return null;
        }

        if (!Objects.equals(configurationValue, defaultValue)) {
          localCacheService.saveConfiguration(applicationContext.getContextId(),
                                              namespace,
                                              additionalParameterValues,
                                              configurationValue);
        }
      }

      final T value = convert(configurationValue);
      return checkAndReturn(configurationValue, value);
    } catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private <T> T checkAndReturn(@Nonnull final ConfigurationValue configurationValue, @Nullable final T value) {
    if (value == null) {
      final Configuration configuration = configurationValue.getConfiguration();
      final Optional<ConfigurationData> defaultValuePresent
              = configuration.getConfigurationData(CONFIGURATION_DEFAULT_VALUE);
      final Optional<ConfigurationData> dynamicDefaultValuePresent
              = configuration.getConfigurationData(CONFIGURATION_DYNAMIC_DEFAULT_VALUE);
      final boolean isDefaultDefined = defaultValuePresent.isPresent()
              && defaultValuePresent.get().getBooleanValue();
      final boolean isDynamicDefaultDefined = dynamicDefaultValuePresent.isPresent()
              && dynamicDefaultValuePresent.get().getBooleanValue();
      if (isDefaultDefined || isDynamicDefaultDefined) {
        final Namespace namespace = configuration.getNamespace();
        throw new NullPointerException(format("%s.%s must not be null",
                                              namespace.getCanonicalName(),
                                              configuration.getConfigName()));
      }
    }
    return value;
  }

  @VisibleForTesting
  boolean acceptCachedValue(@Nullable final ConfigurationValue configurationValue) {
    if (configurationValue == null) {
      return false;
    }

    final Configuration configuration = configurationValue.getConfiguration();
    final Optional<ConfigurationData> configurationDataOptional
            = configuration.getConfigurationData(CONFIGURATION_LOCAL_CACHE_VALUE);
    if (!configurationDataOptional.isPresent()) {
      return true;
    }

    final ConfigCacheValue.CacheValutOption cacheValutOption
            = ConfigCacheValue.CacheValutOption.valueOf(configurationDataOptional.get().getDataValue());
    final String configValue = configurationValue.getConfigValue();
    switch (cacheValutOption) {
      case IGNORE_WHEN_EMPTY:
        return configValue != null && !EMPTY_ARRAY.trimFrom(configValue).trim().isEmpty();
      case IGNORE_WHEN_NULL:
        return configValue != null;
    }

    return true;
  }

  @Nonnull
  private ConfigurationRequestService createConfigurationRequestService() {
    final Source source = Source.valueOf(System.getProperty(CONFIG_SOURCE_KEY, "CONFIG_SERVICE").toUpperCase());
    final ConfigSource configSource = new ConfigSource.ConfigSourceImpl(source);

    return CDI.current().select(ConfigurationRequestService.class, configSource).get();
  }

}
