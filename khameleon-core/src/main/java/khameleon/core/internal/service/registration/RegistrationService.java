package khameleon.core.internal.service.registration;

import java.util.Set;

import com.google.common.collect.Sets;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.service.ConfigurationRegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.atteo.classindex.ClassIndex;

import static java.util.stream.Collectors.joining;
import static khameleon.core.internal.util.KhameleonParameters.isSkipConfigurationInitialization;

/**
 *
 * @author marembo
 */
@Slf4j
public abstract class RegistrationService {

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  private Set<Class<?>> configInterfaces;

  @Inject
  private ConfigurationRegistrationService configurationRegistrationService;

  @PostConstruct
  void init() {
    configInterfaces = Sets.newHashSet(ClassIndex.getAnnotated(ConfigRoot.class));
    log.info("Found Config interfaces: {}",
             configInterfaces.stream().map(Class::getCanonicalName).collect(joining("\n---", "\n\n---", "\n\n")));
  }

  public void register() {
    if (isSkipConfigurationInitialization()) {
      log.info("Skipping configuration initialization....");
      return;
    }

    registerNamespaces();
    registerConfigurations();
  }

  private void registerNamespaces() {
    configInterfaces.stream()
            .flatMap((cls) -> configurationInitializer.getNamespaces(cls).stream())
            .forEachOrdered((namespace) -> {
              log.info("Registering namespace: \n{}", new Object[]{namespace});
              configurationRegistrationService.registerNamespace(namespace);
            });
  }

  private void registerConfigurations() {
    configInterfaces.stream()
            .flatMap((cls) -> configurationInitializer.getConfigurations(cls).stream())
            .forEachOrdered((configuration) -> {
              configurationRegistrationService.registerConfiguration(configuration);
            });
  }

}
