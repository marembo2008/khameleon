package khameleon.core.internal.service.request.postprocessing;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 24, 2019, 7:45:18 PM
 */
public interface ValuePostProcessor {

  @Nonnull
  Object process(@Nonnull final ValueContext valueContext);

  default int getPriority() {
    return Integer.MIN_VALUE;
  }

}
