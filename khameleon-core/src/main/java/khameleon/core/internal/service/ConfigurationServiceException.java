package khameleon.core.internal.service;

import jakarta.ejb.ApplicationException;

/**
 *
 * @author marembo
 */
@ApplicationException(rollback = true)
public class ConfigurationServiceException extends RuntimeException {

  public ConfigurationServiceException() {
  }

  public ConfigurationServiceException(String message) {
    super(message);
  }

  public ConfigurationServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public ConfigurationServiceException(Throwable cause) {
    super(cause);
  }

}
