package khameleon.core.internal.service.request;

import java.io.Serializable;
import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 20, 2015, 12:32:22 AM
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ConfigCacheKey implements Serializable {

  @Nonnull
  private final Class<?> configInterface;

  @Nonnull
  private final Method configMethod;

}
