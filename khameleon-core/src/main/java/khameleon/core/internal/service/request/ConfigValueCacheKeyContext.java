package khameleon.core.internal.service.request;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.transform;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.base.Function;

import anosym.common.StringTransient;
import khameleon.core.ApplicationContext;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.values.AdditionalParameterKeyValue;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 20, 2015, 12:32:22 AM
 */
@Data
public class ConfigValueCacheKeyContext implements Serializable {

	private static final long serialVersionUID = 9024454574714612683L;

	private static final Function<AdditionalParameterKeyValue, String> ADDITIONAL_PARAMETER_KEY_VALUE_FUNCTION = (
			parameterKey) -> checkNotNull(parameterKey, "the additional parameter key must not be null").keyValue();

	private final ApplicationContext applicationContext;

	private final String namespaceCanonicalName;

	private final String configName;

	private final Class<?> configInterface;

	private final Method configMethod;

	private final List<String> additionalParameterKeys;

	private final Object[] configMethodArgs;

	@StringTransient
	private final transient List<AdditionalParameterKeyValue> additionalParameterKeyValues;

	@StringTransient
	private final transient ConfigurationService configurationService;

	public ConfigValueCacheKeyContext(@Nonnull final ApplicationContext applicationContext,
			@Nonnull final Class<?> configInterface, @Nonnull final Method configMethod,
			@Nonnull final String namespaceCanonicalName,
			@Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
			@Nonnull final Object[] configMethodArgs, @Nonnull final ConfigurationService configurationService) {
		this.applicationContext = checkNotNull(applicationContext, "The ApplicationContext must not be null");
		this.configMethod = checkNotNull(configMethod, "the configMethod must be specified");
		this.configInterface = checkNotNull(configInterface, "The configInterface must not be null");
		this.namespaceCanonicalName = checkNotNull(namespaceCanonicalName,
				"The namespaceCanonicalName must not be null");
		this.configName = DefaultConfigurationInitializor.getConfigName(configMethod);
		this.additionalParameterKeys = checkNotNull(
				transform(additionalParameterKeyValues, ADDITIONAL_PARAMETER_KEY_VALUE_FUNCTION),
				"the additionalParameterKeyValues should not be null");
		this.configMethodArgs = configMethodArgs;
		this.additionalParameterKeyValues = additionalParameterKeyValues;
		this.configurationService = checkNotNull(configurationService, "The configurationService must not be null");
	}

	public ConfigValueCacheKeyContext(@Nonnull final ApplicationContext applicationContext,
			@Nonnull final String namespaceCanonicalName,
			@Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
			@Nonnull final String configName) {
		this.applicationContext = checkNotNull(applicationContext, "The applicationContext must not be null");
		this.namespaceCanonicalName = checkNotNull(namespaceCanonicalName,
				"The namespaceCanonicalName must not be null");
		this.configName = checkNotNull(configName, "The configName must not be null");
		this.additionalParameterKeys = checkNotNull(
				transform(additionalParameterKeyValues, ADDITIONAL_PARAMETER_KEY_VALUE_FUNCTION),
				"the additionalParameterKeyValues should not be null");
		this.additionalParameterKeyValues = additionalParameterKeyValues;
		this.configMethodArgs = null;
		this.configInterface = null;
		this.configMethod = null;
		this.configurationService = null;
	}

}
