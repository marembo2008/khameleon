package khameleon.core.internal.service.changelistener;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import khameleon.core.Configuration;
import khameleon.core.internal.authentication.Secured;
import khameleon.core.internal.localcache.LocalCacheService;
import khameleon.core.internal.service.request.ConfigurationServiceCacheService;
import khameleon.core.values.ConfigurationValue;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Secured
@Path("/")
@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ConfigurationChangeListenerEndPoint {

  @Inject
  private LocalCacheService localCacheService;

  @Inject
  private ConfigurationServiceCacheService configurationServiceCacheService;

  @POST
  @Path("/configuration-changes")
  public void onConfigurationChange(@NonNull final ConfigurationChangeEvent configurationChangeEvent) {
    switch (configurationChangeEvent.getConfigurationChangeType()) {
      case DELETE:
        onConfigurationDeleted(configurationChangeEvent.getConfigurationValue());
      case UPDATE:
        onConfigurationUpdated(configurationChangeEvent.getConfigurationValue());
    }
  }

  private void onConfigurationUpdated(@NonNull final ConfigurationValue configurationValue) {
    log.info("Configuration updates have been received. Updating local cache with: {}", configurationValue);
    try {
      final int contextId = configurationValue.getContext().getContextId();
      final Configuration configuration = configurationValue.getConfiguration();
      final String namespaceCanonicalName = configuration.getNamespace().getCanonicalName();

      //simply save this on the local cache.
      localCacheService.saveConfiguration(contextId, namespaceCanonicalName, configurationValue);

      configurationServiceCacheService.onConfigurationUpdated(configurationValue);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private void onConfigurationDeleted(@NonNull final ConfigurationValue deletedConfiguration) {
    log.info("Configuration updates have been received. Updating local cache with: {}", deletedConfiguration);

    try {
      final int contextId = deletedConfiguration.getContext().getContextId();
      final Configuration configuration = deletedConfiguration.getConfiguration();
      final String namespaceCanonicalName = configuration.getNamespace().getCanonicalName();

      localCacheService.removeConfiguration(contextId, namespaceCanonicalName, deletedConfiguration);
      configurationServiceCacheService.onConfigurationDeleted(deletedConfiguration);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

}
