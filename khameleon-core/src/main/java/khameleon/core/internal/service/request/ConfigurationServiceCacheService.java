package khameleon.core.internal.service.request;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.UncheckedExecutionException;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.annotations.ConfigUpdateListener;
import khameleon.core.updatelistener.ConfigurationUpdateListener;
import khameleon.core.updatelistener.ConfigurationUpdaterContext;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import lombok.extern.slf4j.Slf4j;

import static java.util.Objects.requireNonNull;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.initializer.DefaultConfigurationInitializor.getConfigName;
import static khameleon.core.internal.service.request.ConfigurationValueConverter.convert;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.util.concurrent.Futures.immediateFuture;
import static com.google.common.util.concurrent.Futures.submitAsync;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 20, 2015, 12:31:24 AM
 */
@Slf4j
@Startup
@Singleton
@ApplicationScoped
@Lock(LockType.READ)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ConfigurationServiceCacheService {

  private static final String TEMPORARY_IN_MEMORY_VALUE_CACHE_DISABLED = "anosym.khameleon.value-cache.disabled";

  @Any
  @Inject
  private Instance<ConfigurationUpdateListener> configurationUpdateListeners;

  @Resource
  private ManagedExecutorService managedExecutorService;

  private LoadingCache<ConfigValueCacheKeyContext, Optional<Object>> configurationValuesCache;

  @VisibleForTesting
  void initialize(@Nonnull final ManagedExecutorService managedExecutorService) {
    this.managedExecutorService = managedExecutorService;
    this.init();
  }

  @PostConstruct
  public void init() {
    configurationValuesCache = CacheBuilder.
            newBuilder()
            .expireAfterAccess(24, TimeUnit.HOURS)
            .maximumSize(10000)
            .build(new CacheLoader<ConfigValueCacheKeyContext, Optional<Object>>() {

              @Override
              public ListenableFuture<Optional<Object>> reload(
                      @Nonnull final ConfigValueCacheKeyContext cacheKeyContext,
                      @Nonnull final Optional<Object> oldValue) throws Exception {
                requireNonNull(cacheKeyContext, "The cacheKeyContext must not be null");

                return submitAsync(() -> immediateFuture(doReload(cacheKeyContext, oldValue)), managedExecutorService);
              }

              @Nonnull
              @Override
              public Optional<Object> load(@Nonnull final ConfigValueCacheKeyContext configValueCacheKey) throws Exception {
                checkNotNull(configValueCacheKey, "The configValueCacheKey must not be null");

                return Optional.ofNullable(getValue(configValueCacheKey));
              }

              @Nonnull
              private Optional<Object> doReload(@Nonnull final ConfigValueCacheKeyContext cacheKeyContext,
                                                @Nonnull final Optional<Object> oldValue) throws Exception {
                final Optional<Object> newValue = load(cacheKeyContext);
                final Method configMethod = cacheKeyContext.getConfigMethod();
                final Class<?> configInterface = cacheKeyContext.getConfigInterface();
                if (Objects.equals(newValue, oldValue) || configMethod == null || configInterface == null) {
                  return newValue;
                }

                final String namespaceCanonicalName = cacheKeyContext.getNamespaceCanonicalName();
                final ConfigurationUpdaterContext configurationUpdaterContext = ConfigurationUpdaterContext
                        .builder()
                        .namespace(namespaceCanonicalName)
                        .configName(getConfigName(configMethod))
                        .updatedValue(oldValue.orElse(null))
                        .build();

                log.info("The following configurations have been updated: {0}", configurationUpdaterContext);

                //We could also be listening on the parent canonical namespace.
                final Class<?> superConfigInterface = configInterface.getSuperclass();
                final String parentCanonicalNamesapce = superConfigInterface != null
                        ? superConfigInterface.getCanonicalName() : null;
                getConfigurationUpdateListeners(namespaceCanonicalName, parentCanonicalNamesapce)
                        .stream()
                        .forEach((configurationUpdaterListener) -> {
                          configurationUpdaterListener.onConfigurationUpdated(configurationUpdaterContext);
                        });

                return newValue;
              }

            });
  }

  @Schedule(hour = "*", minute = "*/5", persistent = false)
  public void refresh() {
    log.info("Starting configuration reload");

    final Stopwatch timer = Stopwatch.createStarted();
    final Set<ConfigValueCacheKeyContext> configKeys = configurationValuesCache.asMap().keySet();
    ImmutableList
            .copyOf(configKeys)
            .forEach(configurationValuesCache::refresh);

    log.info("Reloaded: <{}> Configuration keys after <{}> ms",
             configKeys.size(), timer.elapsed(TimeUnit.MILLISECONDS));
  }

  public void onConfigurationUpdated(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    final Object updatedValue = convert(configurationValue);
    if (updatedValue != null) {
      final ApplicationContext applicationContext = configurationValue.getContext();
      final Configuration configuration = configurationValue.getConfiguration();
      final String namespaceCanonicalName = configuration.getNamespace().getCanonicalName();
      final String configName = configuration.getConfigName();
      final List<AdditionalParameterKeyValue> additionalParameterKeyValues
              = configurationValue.getAdditionalParameterKeyValues();
      final ConfigValueCacheKeyContext configValueCacheKeyContext
              = new ConfigValueCacheKeyContext(applicationContext,
                                               namespaceCanonicalName,
                                               additionalParameterKeyValues,
                                               configName);
      configurationValuesCache.put(configValueCacheKeyContext, Optional.of(updatedValue));

      final ConfigurationUpdaterContext configurationUpdaterContext = ConfigurationUpdaterContext
              .builder()
              .namespace(namespaceCanonicalName)
              .configName(configName)
              .updatedValue(updatedValue)
              .build();

      getConfigurationUpdateListeners(namespaceCanonicalName)
              .stream()
              .forEach((configurationUpdaterListener) -> {
                configurationUpdaterListener.onConfigurationUpdated(configurationUpdaterContext);
              });
    }
  }

  public void onConfigurationDeleted(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    final ApplicationContext applicationContext = configurationValue.getContext();
    final Configuration configuration = configurationValue.getConfiguration();
    final String namespaceCanonicalName = configuration.getNamespace().getCanonicalName();
    final String configName = configuration.getConfigName();
    final List<AdditionalParameterKeyValue> additionalParameterKeyValues
            = configurationValue.getAdditionalParameterKeyValues();
    final ConfigValueCacheKeyContext configValueCacheKeyContext
            = new ConfigValueCacheKeyContext(applicationContext,
                                             namespaceCanonicalName,
                                             additionalParameterKeyValues,
                                             configName);
    configurationValuesCache.invalidate(configValueCacheKeyContext);

  }

  @Nonnull
  private Collection<ConfigurationUpdateListener> getConfigurationUpdateListeners(@Nonnull final String... namespaces) {
    return Stream
            .concat(ImmutableList.of("*").stream(), Arrays.asList(namespaces).stream())
            .filter(Objects::nonNull)
            .map(ConfigUpdateListenerImpl::new)
            .map(configurationUpdateListeners::select)
            .flatMap((listeners) -> ImmutableList.copyOf(listeners).stream())
            .collect(Collectors.toList());
  }

  @Nullable
  public Object getCachedValue(@Nonnull final ConfigValueCacheKeyContext configValueCacheKeyContext) {
    checkNotNull(configValueCacheKeyContext, "The configValueCacheKeyContext must not be null");

    if (Boolean.valueOf(System.getProperty(TEMPORARY_IN_MEMORY_VALUE_CACHE_DISABLED))) {
      return getValue(configValueCacheKeyContext);
    }

    Object cachedValue = null;
    ConfigValueCacheKeyContext tempConfigValueCacheKeyContext = configValueCacheKeyContext;
    ApplicationContext applicationContext;

    //It can be the case that the current application is not defined, and yet the parent is defined nd cached.
    Throwable cause = null;
    do {
      try {
        cachedValue = whicheverNotNull(configurationValuesCache.get(tempConfigValueCacheKeyContext).orElse(
                null), cachedValue);
      } catch (final ExecutionException | UncheckedExecutionException | InvalidCacheLoadException ex) {

        //some configurations define some requirements, which may cause the cache get failure
        cause = ex.getCause();
      }

      if (isValid(cachedValue)) {
        final ApplicationContext requestApplicationContext
                = configValueCacheKeyContext.getApplicationContext();
        final ApplicationContext foundApplicationContext
                = tempConfigValueCacheKeyContext.getApplicationContext();
        if (requestApplicationContext != foundApplicationContext) {
          configurationValuesCache.put(configValueCacheKeyContext, Optional.of(cachedValue));
        }

        return cachedValue;
      }

      applicationContext = tempConfigValueCacheKeyContext.getApplicationContext();

      log.info("No value found, resolving from parent context: ");

      tempConfigValueCacheKeyContext = copyWithParentApplicationContext(tempConfigValueCacheKeyContext);
    } while (!Objects.equals(applicationContext, DEFAULT_APPLICATION_CONTEXT));

    if (cause != null) {
      Throwables.propagateIfPossible(cause);
    }

    return cachedValue;
  }

  @Nullable
  private Object whicheverNotNull(@Nullable final Object first, @Nullable Object second) {
    return first != null ? first : second;
  }

  private boolean isValid(@Nullable final Object cachedValue) {
    if (cachedValue != null) {
      if (cachedValue instanceof Object[]) {
        return ((Object[]) cachedValue).length > 0;
      }

      return true;
    }
    return false;
  }

  @Nonnull
  private ConfigValueCacheKeyContext copyWithParentApplicationContext(
          @Nonnull final ConfigValueCacheKeyContext configValueCacheKeyContext) {
    final ApplicationContext applicationContext = configValueCacheKeyContext.getApplicationContext();
    final ApplicationContext parentApplicationContext = applicationContext.getParentApplicationContext();
    final Method configMethod = configValueCacheKeyContext.getConfigMethod();
    final List<AdditionalParameterKeyValue> additionalParameterKeyValues
            = configValueCacheKeyContext.getAdditionalParameterKeyValues();
    final Object[] configArgs = configValueCacheKeyContext.getConfigMethodArgs();
    final ConfigurationService configurationService
            = configValueCacheKeyContext.getConfigurationService();

    return new ConfigValueCacheKeyContext(parentApplicationContext,
                                          configValueCacheKeyContext.getConfigInterface(),
                                          configMethod,
                                          configValueCacheKeyContext.getNamespaceCanonicalName(),
                                          additionalParameterKeyValues,
                                          configArgs,
                                          configurationService);
  }

  @Nullable
  @VisibleForTesting
  public Object getValue(@Nonnull final ConfigValueCacheKeyContext configValueCacheKeyContext) {
    checkNotNull(configValueCacheKeyContext, "The configValueCacheKeyContext must not be null");

    final ConfigurationService configurationService
            = checkNotNull(configValueCacheKeyContext.getConfigurationService(),
                           "The configValueCacheKeyContext.getConfigurationService() must not be null");

    return configurationService.getValue(configValueCacheKeyContext);
  }

  @SuppressWarnings("AnnotationAsSuperInterface") //CDI
  private static final class ConfigUpdateListenerImpl extends AnnotationLiteral<ConfigUpdateListener> implements ConfigUpdateListener {

    private static final long serialVersionUID = -8734342973585564194L;

    private final String namespace;

    public ConfigUpdateListenerImpl(String namespace) {
      this.namespace = namespace;
    }

    @Override
    public String namespace() {
      return namespace;
    }

  }

}
