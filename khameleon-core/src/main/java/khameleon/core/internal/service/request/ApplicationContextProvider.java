package khameleon.core.internal.service.request;

import java.util.List;

import javax.annotation.Nonnull;

import khameleon.core.ApplicationContext;
import khameleon.core.annotations.AppContext;
import khameleon.core.applicationcontext.ApplicationContextService;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.processor.configuration.NamespaceApplicationContextHelper.getApplicationContexts;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 21, 2017, 10:24:51 AM
 */
@Slf4j
public class ApplicationContextProvider {

    @Nonnull
    private final ApplicationContextService applicationContextService;

    public ApplicationContextProvider(@Nonnull final ApplicationContextService applicationContextService) {
        this.applicationContextService
                = checkNotNull(applicationContextService, "The applicationContextService must not be null");
    }

    @Nonnull
    public ApplicationContext getApplicationContext(@Nonnull final Class<?> configInterface) {
        final ApplicationContext currentContext = this.applicationContextService.getApplicationContext();
        
        log.debug("currentContext: {}", currentContext);
        
        final List<ApplicationContext> configInterfaceRangeContext = getApplicationContexts(configInterface);
        
        log.debug("Config-Interface <{}> contexts: {}", configInterface, configInterfaceRangeContext);
        
        final boolean isAppContext = configInterface.isAnnotationPresent(AppContext.class);
        if (configInterfaceRangeContext.isEmpty() && isAppContext) {
            //Defined in AppContext
            //Return immediately, as the current appcontext may not be defined in the config-service, and hence we will not be able to
            //do a request with fallback.
            return DEFAULT_APPLICATION_CONTEXT;
        }

        if (configInterfaceRangeContext.isEmpty() || configInterfaceRangeContext.contains(currentContext)) {
            return currentContext;
        }

        /*
         * Return the application context, which is the parent of the currentcontext, if none is available, throw an
         * exception, rather than proceeding, as we do not want DEVELOPMENT/TEST context in live server!
         */
        for (final ApplicationContext applicationContext : configInterfaceRangeContext) {
            if (isParentContext(applicationContext, currentContext)) {
                return applicationContext;
            }
        }
        //Impossible to proceed!
        throw new IllegalStateException(format(
                "Cannot determine current configInterface applicable ApplicationContext: %s", configInterface));
    }

    private boolean isParentContext(@Nonnull final ApplicationContext possiblyParentContext,
                                    @Nonnull final ApplicationContext context) {
        if (context.getContextId() == possiblyParentContext.getContextId()) {
            return true;
        }
        if (context.isDefaultContext()) {
            return false;
        }
        return isParentContext(possiblyParentContext, context.getParentApplicationContext());
    }

}
