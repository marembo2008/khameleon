package khameleon.core.internal.service;

import javax.annotation.Nonnull;

import jakarta.enterprise.inject.spi.CDI;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 20, 2015, 1:08:54 AM
 */
public final class ConfigurationDependencyServiceHelper {

  public static <T> T lookup(@Nonnull final Class<T> clazz) {
    requireNonNull(clazz, "The clazz must not be null");

    return CDI.current().select(clazz).get();
  }

}
