package khameleon.core.internal.service.changelistener;

import khameleon.core.values.ConfigurationValue;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 20, 2018, 3:55:02 AM
 */
@Data
public class ConfigurationChangeEvent {

    @NonNull
    private ConfigurationChangeType configurationChangeType;

    @NonNull
    private ConfigurationValue configurationValue;

}
