package khameleon.core.internal.service.request.postprocessing;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.util.AnnotationLiteral;
import khameleon.core.values.AdditionalParameterKeyValue;
import lombok.extern.slf4j.Slf4j;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 24, 2019, 7:39:32 PM
 */
@Slf4j
public class ValuePostProcessorFactory {

  private static final String LOADER_KEY = "value-post-processors".intern();

  private static final Map<String, List<ValuePostProcessor>> VALUE_POST_PROCESSORS = new ConcurrentHashMap<>();

  @Nonnull
  public static Object postProcess(@Nonnull final Class<?> configInterface,
                                   @Nonnull final Method configMethod,
                                   @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues,
                                   @Nonnull final Object[] args,
                                   @Nonnull final Object value) {
    final List<ValuePostProcessor> valuePostProcessors
            = VALUE_POST_PROCESSORS.computeIfAbsent(LOADER_KEY, (k) -> loadValuePostProcessors());

    log.debug("Registered valuePostProcessors: {}", valuePostProcessors);

    final Iterator<ValuePostProcessor> it = valuePostProcessors.iterator();
    final ValueContext.ValueContextBuilder valueContextBuilder = ValueContext.builder()
            .configInterface(configInterface)
            .configMethod(configMethod)
            .parameterKeyValues(parameterKeyValues)
            .args(args)
            .value(value);
    Object valueToProcess = value;
    while (it.hasNext()) {
      final ValuePostProcessor processor = it.next();
      valueToProcess = processor.process(valueContextBuilder.build());
      valueContextBuilder.value(valueToProcess);
    }

    return valueToProcess;
  }

  private static List<ValuePostProcessor> loadValuePostProcessors() {
    try {
      return ImmutableList.copyOf(CDI.current().select(ValuePostProcessor.class, new AnyAnnotation()))
              .stream()
              .sorted(comparing(ValuePostProcessor::getPriority))
              .collect(toList());
    } catch (final IllegalStateException ex) {
      log.warn("Error getting post processors:", ex);
      return ImmutableList.of();
    }
  }

  @SuppressWarnings(value = "AnnotationAsSuperInterface")
  static final class AnyAnnotation extends AnnotationLiteral<Any> implements Any {

    private static final long serialVersionUID = -1485969418517333998L;

  }

}
