package khameleon.core.internal.service.request;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.text.DateFormatter;

import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.UncheckedExecutionException;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.DefaultConfigurator;
import khameleon.core.Namespace;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigOption;
import khameleon.core.annotations.ConfigOptions;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigPattern;
import khameleon.core.annotations.Context;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.DynamicDefault;
import khameleon.core.applicationcontext.AdditionalParameterProviderApplicationContextService;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.internal.service.request.postprocessing.ValuePostProcessorFactory;
import khameleon.core.manager.ConfigurationManager;
import khameleon.core.util.ConfigOptionValue;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import lombok.SneakyThrows;

import static java.lang.String.format;
import static java.text.DateFormat.getDateInstance;
import static java.util.Arrays.asList;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_NONNULL;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRED_THROWS;
import static khameleon.core.initializer.DefaultConfigurationInitializor.getConfigName;
import static khameleon.core.internal.ConfigurationUtil.isPrimitiveOrPrimitveWrapper;
import static khameleon.core.manager.ConfigurationManager.getNamespace;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.transform;

/**
 *
 * @author marembo
 */
public class ConfigurationService implements InvocationHandler, Serializable {

  private static final String DYNAMIC_ADDITIONAL_PARAMETER_PROPERTY_KEY_PREFIX = "anosym.khameleon.additional-parameters.dynamic";

  private static final List<AdditionalParameterKeyValue> DYNAMIC_ADDITIONAL_PARAMETER_KEY_VALUES;

  static {
    final Properties properties = System.getProperties();
    DYNAMIC_ADDITIONAL_PARAMETER_KEY_VALUES = properties
            .entrySet()
            .stream()
            .map((entry) -> new SimpleEntry<>(Objects.toString(entry.getKey()), entry.getValue()))
            .filter((entry) -> entry.getKey().startsWith(DYNAMIC_ADDITIONAL_PARAMETER_PROPERTY_KEY_PREFIX))
            .map((entry) -> new AdditionalParameterKeyValue(Optional.of(entry.getKey()).map((key) -> {
      return key.substring(DYNAMIC_ADDITIONAL_PARAMETER_PROPERTY_KEY_PREFIX.length() + 1);
    }).get(), entry.getValue(), true))
            .collect(Collectors.toList());
  }

  private static final long serialVersionUID = 14294284922092L;

  private static final Logger LOG = Logger.getLogger(ConfigurationService.class.getName());

  private final ConfigurationInitializer configurationInitializer = new DefaultConfigurationInitializor();

  private final String configCanonicalNamespace;

  private final ConfigurationServiceHelper configurationServiceHelper;

  private final List<AdditionalParameterKeyValue> additionalParameterKeyValues;

  private final ApplicationContextService applicationContextService;

  private final ApplicationContextProvider applicationContextProvider;

  //A cache of default values
  private final LoadingCache<ConfigValueCacheKeyContext, ConfigurationValue> defaultConfigurationValues = CacheBuilder
          .newBuilder()
          .expireAfterAccess(100, TimeUnit.DAYS)
          .maximumSize(1000)
          .build(new CacheLoader<ConfigValueCacheKeyContext, ConfigurationValue>() {

            @Override
            @Nullable
            public ConfigurationValue load(@Nonnull final ConfigValueCacheKeyContext configValueCacheKey) throws Exception {
              checkNotNull(configValueCacheKey, "The configValueCacheKey must not be null");

              //If we dont find the default value, we allow the null to be cached anyway, since there could never be any value.
              return getDefaultConfigurationValue0(configValueCacheKey);
            }

          });

  private final LoadingCache<ConfigCacheKey, Configuration> configurationCache = CacheBuilder
          .newBuilder()
          .expireAfterAccess(100, TimeUnit.DAYS)
          .maximumSize(1000)
          .build(new CacheLoader<ConfigCacheKey, Configuration>() {

            @Override
            @Nullable
            public Configuration load(@Nonnull final ConfigCacheKey configCacheKey) throws Exception {
              checkNotNull(configCacheKey, "The configValueCacheKey must not be null");

              //If we dont find the default value, we allow the null to be cached anyway, since there could never be any value.
              return configurationInitializer.getConfiguration(configCacheKey.getConfigInterface(),
                                                               configCacheKey.getConfigMethod());
            }

          });

  public ConfigurationService(@Nonnull final ApplicationContextService applicationContextService,
                              @Nonnull final String configCanonicalNamespace,
                              @Nonnull final ConfigurationServiceHelper configurationServiceHelper,
                              @Nullable final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    this.applicationContextService = checkNotNull(applicationContextService,
                                                  "the applicationContextService must be provided");
    this.configCanonicalNamespace = checkNotNull(configCanonicalNamespace, "The confignamespace must not be null");
    this.configurationServiceHelper = checkNotNull(configurationServiceHelper, "The confighelper must not be null");
    this.additionalParameterKeyValues
            = firstNonNull(additionalParameterKeyValues, ImmutableList.<AdditionalParameterKeyValue>of());
    this.applicationContextProvider = new ApplicationContextProvider(applicationContextService);
  }

  @Override
  public Object invoke(@Nonnull final Object proxy, @Nonnull final Method method, @Nullable final Object[] args) throws Throwable {
    final Class<?> configInterface = configInterface(proxy);
    checkState(configInterface.isAnnotationPresent(Config.class),
               "The following interface: {0} does not define a configuration service", configInterface);

    final String canonicalNamespace = method.getReturnType().isAnnotationPresent(Config.class)
            ? getCanonicalNamespace(method) : this.configCanonicalNamespace;
    return invoke(proxy, method, firstNonNull(args, new Object[0]), canonicalNamespace);
  }

  private Object invoke(Object proxy, Method configMethod, Object[] args, String canonicalNamespace) throws Throwable {
    //is this the applicationcontext request?
    final Class<?> configInterface = configInterface(proxy);
    if (configMethod.isAnnotationPresent(Context.class)) {
      return applicationContextProvider.getApplicationContext(configInterface);
    }

    final List<AdditionalParameterKeyValue> parameterKeyValues = getAdditionalParameterKeyValues(configInterface,
                                                                                                 configMethod, args);
    //are we working with config type?
    Object value;
    if (configMethod.getReturnType().isAnnotationPresent(Config.class)) {
      final Class<?> childConfigInterface = configMethod.getReturnType();
      value = getConfigInstance(childConfigInterface, canonicalNamespace, parameterKeyValues);
    } else {
      value = getValue(configInterface, configMethod, parameterKeyValues, args);
    }
    //check config option values and then format. if this is a string.
    if (value != null) {
      if (String.class.isAssignableFrom(value.getClass())) {
        value = formatWithConfigOptions(proxy, configMethod, args, value);
      }
    }

    // Add post processors to the returned value.
    if (value == null) {
      return null;
    }
    return ValuePostProcessorFactory.postProcess(configInterface, configMethod, parameterKeyValues, args, value);
  }

  private Class<?> configInterface(@Nonnull final Object proxy) {
    return proxy.getClass().getInterfaces()[0];
  }

  private Object getConfigInstance(Class<?> configInterface,
                                   String canonicalNamespace,
                                   List<AdditionalParameterKeyValue> parameterKeyValues) {
    return ConfigurationManager.getInstance(applicationContextService,
                                            configInterface,
                                            canonicalNamespace,
                                            configurationServiceHelper,
                                            parameterKeyValues);
  }

  @Nullable
  private Object getValue(@Nonnull final Class<?> configInterface,
                          @Nonnull final Method configMethod,
                          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
                          @Nonnull final Object[] args) {
    final String namespaceCanonicalName = getNamespace(configInterface);
    final ConfigValueCacheKeyContext configValueCacheKey
            = getConfigurationValueCacheKey(namespaceCanonicalName, configInterface, configMethod,
                                            additionalParameterKeyValues, args);
    final ConfigurationServiceCacheService configurationServiceCacheService
            = ConfigurationServiceCacheServiceLookup.getConfigurationServiceCacheService();
    return configurationServiceCacheService.getCachedValue(configValueCacheKey);
  }

  @Nullable
  Object getValue(@Nonnull final ConfigValueCacheKeyContext configValueCacheKeyContext) {
    final Method configMethod = checkNotNull(configValueCacheKeyContext.getConfigMethod(),
                                             "The configValueCacheKeyContext.getConfigMethod() must not be null");

    @SuppressWarnings("LocalVariableHidesMemberVariable")
    final List<AdditionalParameterKeyValue> additionalParameterKeyValues
            = configValueCacheKeyContext.getAdditionalParameterKeyValues();

    final String configName = getConfigName(configMethod);

    final ApplicationContext applicationContext = configValueCacheKeyContext.getApplicationContext();

    LOG.log(Level.FINE, "ConfigService Call: " + ""
            + "\n\tApplicationContext:{0}" + ", "
            + "\n\tConfigNamespace:{1}" + ", " + ""
            + "\n\tConfigName:{2}, " + ""
            + "\n\tConfigProperties:{3}",
            new Object[]{applicationContext, this.configCanonicalNamespace, configName, additionalParameterKeyValues});

    final ConfigurationValue defaultConfigurationValue = getDefaultConfigurationValue(configValueCacheKeyContext);

    final Object value = configurationServiceHelper.callService(applicationContext,
                                                                this.configCanonicalNamespace,
                                                                configName,
                                                                additionalParameterKeyValues,
                                                                defaultConfigurationValue);

    final Class<?> configInterface = configValueCacheKeyContext.getConfigInterface();
    if (configInterface == null) {
      return value;
    }

    try {
      final ConfigCacheKey configCacheKey = new ConfigCacheKey(configInterface, configMethod);
      final Configuration configuration = configurationCache.get(configCacheKey);
      return checkNull(configuration, value);
    } catch (ExecutionException ex) {
      throw Throwables.propagate(ex);
    }
  }

  @Nullable
  @SneakyThrows
  private <T> T checkNull(@Nonnull final Configuration configuration, @Nullable T value) {
    if (value != null) {
      return value;
    }

    final com.google.common.base.Optional<ConfigurationData> nonnullConfigurationData
            = configuration.getConfigurationData(CONFIGURATION_NONNULL);
    if (!nonnullConfigurationData.isPresent() || !nonnullConfigurationData.get().getBooleanValue()) {
      return value;
    }

    final com.google.common.base.Optional<ConfigurationData> requiredThrowsConfigurationData
            = configuration.getConfigurationData(CONFIGURATION_REQUIRED_THROWS);
    final Namespace namespace = configuration.getNamespace();
    final String errorMessage = format("%s.%s must not be null",
                                       namespace.getCanonicalName(),
                                       configuration.getConfigName());
    if (!requiredThrowsConfigurationData.isPresent()) {
      throw new NullPointerException(errorMessage);
    }

    final String exceptionClassName = requiredThrowsConfigurationData.get().getDataValue();
    final Class exceptionClass = Class.forName(exceptionClassName);
    final Constructor messageConstrutor = exceptionClass.getDeclaredConstructor(String.class);
    final RuntimeException exception = (RuntimeException) messageConstrutor.newInstance(errorMessage);
    throw exception;
  }

  @Nullable
  private ConfigurationValue getDefaultConfigurationValue(
          @Nonnull final ConfigValueCacheKeyContext configValueCacheKeyContext) {
    checkNotNull(configValueCacheKeyContext, "The configValueCacheKeyContext must not be null");

    try {
      return defaultConfigurationValues.get(configValueCacheKeyContext);
    } catch (final ExecutionException | UncheckedExecutionException | InvalidCacheLoadException ex) {
      LOG.log(Level.FINE,
              "Error retrieving default configuration value: {0}-{1}",
              new Object[]{ex.getClass(), ex.getLocalizedMessage()});

      Throwables.propagateIfPossible(ex.getCause());

      return null;
    }
  }

  @Nullable
  private ConfigurationValue getDefaultConfigurationValue0(
          @Nonnull final ConfigValueCacheKeyContext configValueCacheKey) throws InstantiationException, IllegalAccessException {
    final Method configMethod = checkNotNull(configValueCacheKey.getConfigMethod(),
                                             "The configValueCacheKey.getConfigMethod() must not be null");
    final Object[] configMethodArgs = checkNotNull(configValueCacheKey.getConfigMethodArgs(),
                                                   "The configValueCacheKey.getConfigMethodArgs() must not be null");
    final boolean hasDefault = configMethod.isAnnotationPresent(Default.class);
    final boolean hasDynamicDefault = configMethod.isAnnotationPresent(DynamicDefault.class);
    if (!hasDefault && !hasDynamicDefault) {
      return null;
    }

    final Class<?> configInterface = checkNotNull(configValueCacheKey.getConfigInterface(),
                                                  "The configValueCacheKey.getConfigInterface() must not be null");
    final ConfigCacheKey configCacheKey = new ConfigCacheKey(configInterface, configMethod);
    final Configuration configuration;
    try {
      configuration = configurationCache.get(configCacheKey);
    } catch (ExecutionException ex) {
      throw Throwables.propagate(ex);
    }
    //Have we defined dynamic default!
    if (hasDynamicDefault) {
      final Class<? extends DefaultConfigurator> defaultConfiguratorClass
              = configMethod.getAnnotation(DynamicDefault.class).value();
      final DefaultConfigurator defaultConfigurator = defaultConfiguratorClass.newInstance();
      final String defaultValue = defaultConfigurator.getDefault(configMethod, configMethodArgs);

      if (defaultValue == null) {
        throw new NullPointerException(format("%s must not be null", configMethod));
      }

      configuration.setDefaultValue(defaultValue);
    }

    final ApplicationContext context = DEFAULT_APPLICATION_CONTEXT;
    final ConfigurationValue defaultConfigurationValue
            = new ConfigurationValue(context, configuration, configuration.getDefaultValue());
    defaultConfigurationValues.put(configValueCacheKey, defaultConfigurationValue);
    return defaultConfigurationValue;
  }

  @Nonnull
  private ConfigValueCacheKeyContext getConfigurationValueCacheKey(@Nonnull final String namespaceCanonicalName,
                                                                   @Nonnull final Class<?> configInterface,
                                                                   @Nonnull final Method configMethod,
                                                                   @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
                                                                   @Nonnull final Object[] configMethodArgs) {
    final ApplicationContext applicationContext
            = applicationContextProvider.getApplicationContext(configMethod.getDeclaringClass());
    return new ConfigValueCacheKeyContext(applicationContext,
                                          configInterface,
                                          configMethod,
                                          namespaceCanonicalName,
                                          additionalParameterKeyValues,
                                          configMethodArgs,
                                          this);
  }

  @Nonnull
  private String getCanonicalNamespace(final Method m) {
    final String configNamespace_ = getNamespace(m.getReturnType());
    if (this.configCanonicalNamespace != null) {
      return this.configCanonicalNamespace + ":" + configNamespace_;
    }
    return configNamespace_;
  }

  private String formatWithConfigOptions(@Nonnull final Object proxy,
                                         @Nonnull final Method method,
                                         @Nonnull final Object[] args,
                                         @Nonnull final Object value) throws Throwable {
    final String valueToFormat = String.valueOf(value);
    final List<ConfigOptionState> configOptionsState = getConfigOptions(method);
    final List<ConfigOptionValue> optionValues = new ArrayList<>();
    if (!configOptionsState.isEmpty()) {
      final Class<?> configInterface = configInterface(proxy);
      final ApplicationContext applicationContext
              = applicationContextProvider.getApplicationContext(configInterface);
      for (final ConfigOptionState configOptionState : configOptionsState) {
        final ConfigOption configOption = configOptionState.configOption;
        if (configOptionState.parameterOption) {
          final int[] index = configOption.index();
          for (final int ix : index) {
            Object parameter = args[configOptionState.parameterIndex];
            //is this option a Date? It needs to be formatted based on the application context.
            if (parameter instanceof Calendar) {
              final Locale appContextLocale = applicationContext.getLocale();
              final Calendar date = (Calendar) parameter;
              final String formatParameter = configOptionState.configOption.parameter();
              final DateFormat format;
              if (!isNullOrEmpty(formatParameter)) {
                format = new SimpleDateFormat(formatParameter, appContextLocale);
              } else {
                format = getDateInstance(DateFormat.LONG, appContextLocale);
              }
              final DateFormatter formatter = new DateFormatter(format);
              parameter = formatter.valueToString(date.getTime());
            }
            optionValues.add(new ConfigOptionValue(ix, parameter, configOption, null,
                                                   applicationContextService));
          }
        } else {
          final int[] index = configOption.index();
          for (final int configOptionIndex : index) {
            final ConfigOptionValue configOptionValue = new ConfigOptionValue(configOptionIndex,
                                                                              configOption.value(),
                                                                              configOption,
                                                                              this.configurationServiceHelper,
                                                                              applicationContextService);
            optionValues.add(configOptionValue);
          }
        }

      }

      Collections.sort(optionValues);
      final Object optionalValuesParameter[] = new Object[optionValues.size()];
      int index = 0;
      for (final ConfigOptionValue configOptionValue : optionValues) {
        Object templateValue = configOptionValue.getOptionValue(proxy);
        if (configOptionValue.getConfigOption().templateReplace()) {
          //We avoid locale formatting and do an in replace.
          //This simply entails converting the value intos string by calling its to String method.
          templateValue = String.valueOf(templateValue);
        }
        optionalValuesParameter[index++] = templateValue;
      }
      return MessageFormat.format(valueToFormat, optionalValuesParameter);
    }

    return valueToFormat;
  }

  private static List<ConfigOptionState> getConfigOptions(@Nonnull final Method configMethod) {
    final List<ConfigOptionState> configOptions = new ArrayList<>();
    if (configMethod.isAnnotationPresent(ConfigOptions.class)) {
      final ConfigOptions cos = configMethod.getAnnotation(ConfigOptions.class);
      configOptions.addAll(transform(asList(cos.value()), (cOption) -> new ConfigOptionState(cOption, false)));
    }
    if (configMethod.isAnnotationPresent(ConfigOption.class)) {
      configOptions.add(new ConfigOptionState(configMethod.getAnnotation(ConfigOption.class), false));
    }
    //get any parameter annotation
    final Annotation[][] configParameterAnnotations = configMethod.getParameterAnnotations();
    for (int i = 0; i < configParameterAnnotations.length; i++) {
      final Annotation[] parameterAnnotations = configParameterAnnotations[i];
      for (final Annotation parameterAnnotation : parameterAnnotations) {
        if (parameterAnnotation.annotationType() == ConfigOption.class) {
          final ConfigOption co = (ConfigOption) parameterAnnotation;
          configOptions.add(new ConfigOptionState(co, true, i));
          break; //There can only be one per parameter.
        }
      }
    }
    return configOptions;
  }

  private List<AdditionalParameterKeyValue> getAdditionalParameterKeyValues(@Nonnull final Class<?> configInterface,
                                                                            @Nonnull final Method configMethod,
                                                                            @Nonnull final Object[] args) {
    //No need no checks here. When the configurations were generated, validation was done!
    final Annotation[][] annotations = configMethod.getParameterAnnotations();
    final Set<AdditionalParameterKeyValue> parameterKeyValues = Sets.newHashSet();
    for (int i = 0; i < annotations.length; i++) {
      final Annotation[] annots = annotations[i];
      final Object value = args[i];
      for (final Annotation annot : annots) {
        if (annot.annotationType() == ConfigParam.class) {
          final ConfigParam cParam = (ConfigParam) annot;
          parameterKeyValues.add(new AdditionalParameterKeyValue(cParam.name(), value));
          break; //we can only have param ot pattern but not both
        } else if (annot.annotationType() == ConfigPattern.class) {
          final ConfigPattern cPattern = (ConfigPattern) annot;
          parameterKeyValues.add(new AdditionalParameterKeyValue(cPattern.name(), value));
          break;
        }
      }
    }

    if (applicationContextService instanceof AdditionalParameterProviderApplicationContextService) {
      //Any applicationcontext dynamically defined additional parameters.
      checkState(configInterface.isAnnotationPresent(Config.class),
                 "ConfigInterface {%s} must be annotated with @Config", configInterface);

      final AdditionalParameterProviderApplicationContextService additionalParameterProviderApplicationContextService
              = (AdditionalParameterProviderApplicationContextService) applicationContextService;
      final List<Namespace> configNamespaces = configurationInitializer.getNamespaces(configInterface);
      configNamespaces
              .stream()
              .map((configNamespace) -> {
                final String configName = getConfigName(configMethod);
                final List<AdditionalParameterKeyValue> dynamicAdditionalParameterKeyValues
                        = additionalParameterProviderApplicationContextService.getAdditionalParameterKeyValues(
                                configNamespace, configName);
                return dynamicAdditionalParameterKeyValues;
              })
              .forEach((dynamicAdditionalParameterKeyValues) -> {
                parameterKeyValues.addAll(dynamicAdditionalParameterKeyValues);
              });
    }

    if (this.additionalParameterKeyValues != null) {
      parameterKeyValues.addAll(additionalParameterKeyValues);
    }

    parameterKeyValues.addAll(DYNAMIC_ADDITIONAL_PARAMETER_KEY_VALUES);

    return ImmutableList.copyOf(parameterKeyValues);
  }

  /**
   * Primitive or primitive wrappers, Strings, Calendar, BigDecimal or enum.
   */
  public static boolean checkType(Class type) {
    return isPrimitiveOrPrimitveWrapper(type)
            || String.class.isAssignableFrom(type)
            || BigDecimal.class.isAssignableFrom(type)
            || Calendar.class.isAssignableFrom(type)
            || type.isEnum();
  }

  private static class ConfigOptionState implements Serializable {

    private ConfigOption configOption;

    private boolean parameterOption;

    private int parameterIndex;

    public ConfigOptionState(ConfigOption configOption, boolean parameterOption, int parameterIndex) {
      this.configOption = configOption;
      this.parameterOption = parameterOption;
      this.parameterIndex = parameterIndex;
    }

    public ConfigOptionState(ConfigOption configOption, boolean parameterOption) {
      this.configOption = configOption;
      this.parameterOption = parameterOption;
    }

  }

}
