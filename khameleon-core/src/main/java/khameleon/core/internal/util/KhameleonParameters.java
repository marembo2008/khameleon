package khameleon.core.internal.util;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 2:15:10 PM
 */
public final class KhameleonParameters {

    public static final String KHAMELEON_FEIGN_CLIENT = "khameleon";

    public static final String KHAMELEON_FEIGN_CLIENT_REGISTRATION = "khameleon#registration";

    public static final String KHAMELEON_FEIGN_CLIENT_REQUEST = "khameleon#request";

    public static final String KHAMELEON_FEIGN_CLIENT_CHANGE_LISTENER = "khameleon-changelistener";

    public static final String TOKEN_KEY_PARAM = "khameleon.config-service.tokenKey";

    public static final String SECRET_KEY_PARAM = "khameleon.config-service.tokenSecret";

    public static final String TOKEN_KEY_HEADER = "key";

    public static final String SECRET_KEY_HEADER = "secret";

    public static final String KEY_VARIANT_HEADER = "variant";

    public static final String CONFIG_SOURCE_KEY = "khameleon.configSource";

    public static final String SKIP_CONFIG_INITIALIZATION = "maven.khameleon.skipInitialization";

    public static boolean isSkipConfigurationInitialization() {
        return Boolean.valueOf(System.getProperty(SKIP_CONFIG_INITIALIZATION, "false"));
    }

}
