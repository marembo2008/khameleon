package khameleon.core.internal.authentication;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static khameleon.core.internal.util.KhameleonParameters.KEY_VARIANT_HEADER;
import static khameleon.core.internal.util.KhameleonParameters.SECRET_KEY_HEADER;
import static khameleon.core.internal.util.KhameleonParameters.TOKEN_KEY_HEADER;

import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.core.Response;
import khameleon.core.config.RequestCredentialConfig;
import khameleon.core.config.RequestCredentialConfig.Variant;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Basic authentication verifier. Needs a much better check.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 19, 2017, 5:16:05 PM
 */
@Slf4j
@Secured
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class RequestCredentialVerifier {

	@Inject
	private HttpServletRequest servletRequest;

	@Inject
	private RequestCredentialConfig requestCredentialConfig;

	@AroundInvoke
	public Object checkSecurity(@NonNull final InvocationContext invocationContext) throws Exception {
		final String tokenKey = servletRequest.getHeader(TOKEN_KEY_HEADER);
		final String secretKey = servletRequest.getHeader(SECRET_KEY_HEADER);
		if (isNullOrEmpty(tokenKey) || isNullOrEmpty(secretKey)) {
			final Response _401 = Response.status(Response.Status.UNAUTHORIZED).build();
			throw new NotAuthorizedException("Invalid secret key authorization", _401);
		}
		
		final String keyVariant = firstNonNull(servletRequest.getHeader(KEY_VARIANT_HEADER), Variant.Default.name());

		log.info("Verifying credential for keyVariant <{}>", keyVariant);

		verifyCredential(tokenKey, secretKey, keyVariant);

		return invocationContext.proceed();
	}

	private void verifyCredential(@NonNull final String tokenKey, @NonNull final String secretKey,
			@NonNull final String keyVariant) {
		final Variant variant = Variant.valueOf(keyVariant);
		final String serverKey = requestCredentialConfig.getTokenKey(variant);
		final String serverSecret = requestCredentialConfig.getTokenSecret(variant);
		if (!serverKey.equals(tokenKey) || !serverSecret.equals(secretKey)) {
			final Response _401 = Response.status(Response.Status.UNAUTHORIZED).build();
			throw new NotAuthorizedException("Invalid secret key authorization", _401);
		}
	}

}
