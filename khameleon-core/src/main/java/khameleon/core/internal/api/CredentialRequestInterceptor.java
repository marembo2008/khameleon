package khameleon.core.internal.api;

import static khameleon.core.internal.util.KhameleonParameters.KHAMELEON_FEIGN_CLIENT;
import static khameleon.core.internal.util.KhameleonParameters.SECRET_KEY_HEADER;
import static khameleon.core.internal.util.KhameleonParameters.SECRET_KEY_PARAM;
import static khameleon.core.internal.util.KhameleonParameters.TOKEN_KEY_HEADER;
import static khameleon.core.internal.util.KhameleonParameters.TOKEN_KEY_PARAM;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import feign.RequestTemplate;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 2:10:15 PM
 */
@Slf4j
@ApplicationScoped
@FeignInterceptor(KHAMELEON_FEIGN_CLIENT)
public class CredentialRequestInterceptor implements ApiRequestInterceptor {

	@Override
	public void apply(@NonNull final FeignClientId feignClientId, @NonNull final RequestTemplate template) {
		final String tokenKey = System.getProperty(TOKEN_KEY_PARAM);
		final String secretKey = System.getProperty(SECRET_KEY_PARAM);

		log.trace("Tokey-Key: {}, Secret-Key: {}", tokenKey, secretKey);

		template.header(TOKEN_KEY_HEADER, tokenKey);
		template.header(SECRET_KEY_HEADER, secretKey);
	}

}
