package khameleon.core.internal.api;

import static khameleon.core.internal.util.KhameleonParameters.KHAMELEON_FEIGN_CLIENT_REQUEST;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;
import khameleon.core.values.ConfigurationValue;
import khameleon.core.values.ConfigurationValueRequest;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 11:19:51 AM
 */
@FeignClient(KHAMELEON_FEIGN_CLIENT_REQUEST)
public interface ConfigRequestApi {

    @RequestLine("POST /api/configuration-values")
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    ConfigurationValue getConfigurationValue(@NonNull final ConfigurationValueRequest configurationValueRequest);

}
