package khameleon.core.internal.api;

import static khameleon.core.internal.util.KhameleonParameters.KHAMELEON_FEIGN_CLIENT_REGISTRATION;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 11:19:51 AM
 */
@FeignClient(KHAMELEON_FEIGN_CLIENT_REGISTRATION)
public interface ConfigRegistrationApi {

	@RequestLine("POST /api/namespaces")
	@Headers({ "Content-Type: application/json", "Accept: application/json" })
	void registerNamespace(@NonNull final Namespace namespace);

	@RequestLine("POST /api/configurations")
	@Headers({ "Content-Type: application/json", "Accept: application/json" })
	void registerConfiguration(@NonNull final Configuration configuration);

}
