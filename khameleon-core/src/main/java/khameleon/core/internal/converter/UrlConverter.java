package khameleon.core.internal.converter;

import java.net.MalformedURLException;
import java.net.URL;

import khameleon.core.annotations.ValueConverter;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 4:13:12 PM
 */
public class UrlConverter implements ValueConverter.Converter<URL> {

    @NonNull
    @Override
    public URL apply(@NonNull final String value) {
        try {
            return new URL(value);
        } catch (final MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
