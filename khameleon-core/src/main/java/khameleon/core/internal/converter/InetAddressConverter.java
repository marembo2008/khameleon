package khameleon.core.internal.converter;

import java.net.InetAddress;
import java.net.UnknownHostException;

import khameleon.core.annotations.ValueConverter;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 4:13:27 PM
 */
public class InetAddressConverter implements ValueConverter.Converter<InetAddress> {

    @NonNull
    @Override
    public InetAddress apply(@NonNull final String value) {
        try {
            return InetAddress.getByName(value);
        } catch (final UnknownHostException ex) {
            throw new RuntimeException(ex);
        }
    }

}
