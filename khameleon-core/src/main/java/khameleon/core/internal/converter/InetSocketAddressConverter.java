package khameleon.core.internal.converter;

import java.net.InetSocketAddress;

import khameleon.core.annotations.ValueConverter;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 29, 2018, 4:13:27 PM
 */
public class InetSocketAddressConverter implements ValueConverter.Converter<InetSocketAddress> {

    @NonNull
    @Override
    public InetSocketAddress apply(@NonNull final String value) {
        final String[] hostAndPort = value.split(":");

        // Check if localhost and port.
        if (hostAndPort.length == 1) {
            final int port = Integer.parseInt(hostAndPort[0]);
            return new InetSocketAddress(port);
        }

        final int port = Integer.parseInt(hostAndPort[1]);
        return new InetSocketAddress(hostAndPort[0], port);
    }

}
