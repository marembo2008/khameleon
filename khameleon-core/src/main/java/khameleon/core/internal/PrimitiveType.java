/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package khameleon.core.internal;

/**
 *
 * @author marembo
 */
public enum PrimitiveType {

  INT,
  LONG,
  BYTE,
  SHORT,
  CHAR,
  FLOAT,
  DOUBLE,
  BOOLEAN,
  VOID;
}
