/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khameleon.core.internal;

import java.util.Objects;

/**
 *
 * @author marembo
 */
public class PrimitiveClass {

    private final Class primitiveClass;
    private final Class wrapperClass;

    public PrimitiveClass(Class primitiveClass, Class wrapperClass) {
        this.primitiveClass = primitiveClass;
        this.wrapperClass = wrapperClass;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.primitiveClass);
        hash = 79 * hash + Objects.hashCode(this.wrapperClass);
        return hash;
    }

    public boolean is(Class cls) {
        return cls == primitiveClass || cls == wrapperClass;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PrimitiveClass other = (PrimitiveClass) obj;
        return Objects.equals(this.primitiveClass, other.primitiveClass)
                || Objects.equals(this.wrapperClass, other.wrapperClass);
    }

}
