package khameleon.core.values;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Joiner;
import lombok.Data;

import static java.util.Objects.requireNonNull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * To be used to call config-service.
 *
 * @author marembo
 */
@Data
public class AdditionalParameterKeyValue implements Comparable<AdditionalParameterKeyValue> {

  private static final Joiner KEY_VALUE_JOINER = Joiner.on(":").skipNulls();

  private String key;

  private String value;

  private boolean dynamic;

  public AdditionalParameterKeyValue() {
  }

  public AdditionalParameterKeyValue(@Nonnull final String key, @Nullable final String value) {
    this.key = checkNotNull(key, "the key must not be null");
    this.value = value;
  }

  public AdditionalParameterKeyValue(@Nonnull final String key, @Nullable final Object value) {
    this.key = checkNotNull(key, "the key must not be null");

    if (value != null) {
      if (value.getClass().isEnum()) {
        this.value = ((Enum) value).name();
      } else {
        this.value = String.valueOf(value);
      }
    }
  }

  public AdditionalParameterKeyValue(@Nonnull final String key, @Nonnull final Object value, final boolean dynamic) {
    this.key = checkNotNull(key, "the key must not be null");
    requireNonNull(value, "The value must not be null");

    if (value.getClass().isEnum()) {
      this.value = ((Enum) value).name();
    } else {
      this.value = String.valueOf(value);
    }

    this.dynamic = dynamic;
  }

  @Nonnull
  public String keyValue() {
    return KEY_VALUE_JOINER.join(this.key, this.value);
  }

  @Override
  public int compareTo(@Nonnull final AdditionalParameterKeyValue o) {
    return key.compareToIgnoreCase(o.key);
  }

}
