package khameleon.core.values;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationUUID.uuid;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import anosym.common.StringTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.xml.bind.annotation.XmlTransient;
import khameleon.core.AdditionalParameter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * The actual values updated and sent to clients.
 *
 * @author marembo
 */
@Data
@Entity
@NamedQueries({
		@NamedQuery(name = "AdditionalParameterValue.searchAdditionalParameterValue", query = "SELECT apv FROM AdditionalParameterValue apv WHERE apv.parameterKeyValue LIKE :parameterKeyValue"),
		@NamedQuery(name = "AdditionalParameterValue.findByAdditionalParameter", query = "SELECT apv FROM AdditionalParameterValue apv WHERE apv.additionalParameter.paramId = :paramId") })
public class AdditionalParameterValue implements Serializable, Comparable<AdditionalParameterValue> {

	private static final long serialVersionUID = 134834389L;

	@Id
	@Column(length = 100)
	@SuppressWarnings("FieldMayBeFinal")
	private String parameterId = uuid();

	@ManyToOne
	@StringTransient
	private AdditionalParameter additionalParameter;

	@Column(columnDefinition = "TEXT")
	@StringTransient
	private String parameterValue;

	/**
	 * Combination of parameter key-value pair for faster lookup.
	 */
	@Column(columnDefinition = "TEXT")
	private String parameterKeyValue;

	/**
	 * The configuration value for which this parameter belongs to.
	 */
	@ManyToOne
	@JsonIgnore
	@XmlTransient
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private ConfigurationValue configurationValue;

	public AdditionalParameterValue() {
	}

	public AdditionalParameterValue(@Nonnull final AdditionalParameter additionalParameter,
			@Nullable final String parameterValue, @Nonnull final ConfigurationValue configurationValue) {
		this.additionalParameter = checkNotNull(additionalParameter, "AdditionalParameter must be specified");
		this.configurationValue = checkNotNull(configurationValue, "configurationValue must be specified");
		this.parameterValue = parameterValue;
	}

	@PrePersist
	@PreUpdate
	private void setParameterKeyValue() {
		this.parameterKeyValue = this.additionalParameter.getParameterKey() + ":" + parameterValue;
	}

	/**
	 * The key:value pair (separated by a colon)
	 *
	 * @return
	 */
	public String getParameterKeyValue() {
		if (Strings.isNullOrEmpty(parameterKeyValue)) {
			setParameterKeyValue();
		}
		return parameterKeyValue;
	}

	public boolean getBooleanValue() {
		return Boolean.valueOf(parameterValue);
	}

	public void setBooleanValue(final boolean value) {
		this.parameterValue = String.valueOf(value);
	}

	/**
	 * With the exception of enumerations, the String.valueOf(Object) is called for
	 * all other objects.
	 *
	 * Enums use Enum.name() to set the current value of the parameter.
	 *
	 * @param parameterValue
	 */
	public void setParameterValue(@Nullable final Object parameterValue) {
		if (parameterValue == null) {
			this.parameterValue = null;
			return;
		}
		// Enum use Enum.name()
		// Enum.toString() may simply contain displayable information.
		if (parameterValue.getClass().isEnum()) {
			this.parameterValue = ((Enum) parameterValue).name();
		} else {
			this.parameterValue = String.valueOf(parameterValue);
		}
	}

	/**
	 * Splits the value based on enumeration definition.
	 *
	 * @return
	 */
	public String[] getEnumerationList() {
		return parameterValue == null ? new String[0] : parameterValue.split(":");
	}

	@Override
	public int compareTo(AdditionalParameterValue o) {
		return additionalParameter.getParameterKey().compareTo(o.additionalParameter.getParameterKey());
	}

}
