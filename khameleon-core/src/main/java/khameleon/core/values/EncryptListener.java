package khameleon.core.values;

import static anosym.common.util.ObjectsUtil.firstNonNull;
import static anosym.common.util.ObjectsUtil.whicheverNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.lang.String.format;
import static java.lang.System.getenv;
import static java.lang.System.getProperty;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.annotations.VisibleForTesting;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Encrypt;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 28, 2016, 4:20:37 AM
 */
@ApplicationScoped
public class EncryptListener {

	@VisibleForTesting
	static final String ENCRYPT_INDICATOR_PREFIX = "akh:::";

	private static final Logger LOG = Logger.getLogger(EncryptListener.class.getName());

	@PrePersist
	@PreUpdate
	public void encrypt(@Nonnull final ConfigurationValue configurationValue) throws Exception {
		checkNotNull(configurationValue, "The configurationValue must not be null");

		final Configuration configuration = configurationValue.getConfiguration();
		final Boolean isEncryptConfigurationValue = configuration
				.getConfigurationData(ConfigurationDataOption.CONFIGURATION_ENCRYPT)
				.transform(ConfigurationData::getBooleanValue).or(false);
		if (!isEncryptConfigurationValue) {
			return;
		}

		final String configValue = configurationValue.getConfigValue();
		if (isNullOrEmpty(configValue)) {
			return;
		}

		if (configValue.startsWith(ENCRYPT_INDICATOR_PREFIX)) {
			return;
		}

		final Key key = getEncryptionKey();
		final Cipher cipher = Cipher.getInstance(getKeyAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, key);

		final byte[] encodedValue = cipher.doFinal(configValue.getBytes("UTF-8"));
		final String base64EncodedValue = Base64.getEncoder().encodeToString(encodedValue);
		final String prefixedEncodedValue = format("%s%s", ENCRYPT_INDICATOR_PREFIX, base64EncodedValue);
		configurationValue.setConfigValue(prefixedEncodedValue);
	}

	public void decrypt(@Nonnull final ConfigurationValue configurationValue) throws Exception {
		checkNotNull(configurationValue, "The configurationValue must not be null");

		if (!isDecryptable(configurationValue)) {
			return;
		}

		// Remove the prefix from the encrypted value.
		final Configuration configuration = configurationValue.getConfiguration();
		final String configValue = configurationValue.getConfigValue();
		final String base64EncodedValue = configValue.substring(ENCRYPT_INDICATOR_PREFIX.length());
		final byte[] encodedValue = Base64.getDecoder().decode(base64EncodedValue);
		final Key key = getEncryptionKey();
		final Cipher cipher = Cipher.getInstance(getKeyAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, key);

		final byte[] decodedValue = cipher.doFinal(encodedValue);
		final String decodedConfigValue = new String(decodedValue, "UTF-8");
		configurationValue.setConfigValue(decodedConfigValue);
	}

	public boolean isDecryptable(@Nonnull final ConfigurationValue configurationValue) {
		checkNotNull(configurationValue, "The configurationValue must not be null");

		final Configuration configuration = configurationValue.getConfiguration();
		final Boolean isEncryptConfigurationValue = configuration
				.getConfigurationData(ConfigurationDataOption.CONFIGURATION_ENCRYPT)
				.transform(ConfigurationData::getBooleanValue).or(false);

		if (!isEncryptConfigurationValue) {
			return false;
		}

		final String configValue = configurationValue.getConfigValue();
		if (isNullOrEmpty(configValue)) {
			return false;
		}

		return configValue.startsWith(ENCRYPT_INDICATOR_PREFIX);
	}

	@Nullable
	public static Key getEncryptionKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// Now encrypt.
		final String keyAlgorithm = getKeyAlgorithm();
		final String keyValue = getKey();
		if (keyValue == null) {
			LOG.warning(
					"No encryption key has been defined, yet @Encrypt configuration is present on a configuration service");
			return null;
		}

		final MessageDigest sha = MessageDigest.getInstance("SHA-1");
		final byte[] digestKey = sha.digest(keyValue.getBytes("UTF-8"));
		final byte[] _128BitKey = Arrays.copyOf(digestKey, 16);
		return new SecretKeySpec(_128BitKey, keyAlgorithm);
	}

	@Nonnull
	public static String getKeyAlgorithm() {
		return firstNonNull(getenv(EncyrptConstants.ENCRYPTION_KEY_ALGORITHM),
				() -> getProperty(EncyrptConstants.ENCRYPTION_KEY_ALGORITHM, "AES"));
	}

	@Nonnull
	public static String getKey() {
		return whicheverNonNull(() -> getenv(EncyrptConstants.ENCRYPTION_KEY),
				() -> getProperty(EncyrptConstants.ENCRYPTION_KEY));
	}

}
