package khameleon.core.values;

import java.util.List;

import khameleon.core.ApplicationContext;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Singular;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 2:23:31 PM
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConfigurationValueRequest {

    @NonNull
    private ApplicationContext applicationContext;

    @NonNull
    private String namespaceCanonicalName;

    @NonNull
    private String configName;

    @NonNull
    @Singular
    private List<AdditionalParameterKeyValue> parameterKeyValues;

}
