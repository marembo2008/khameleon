package khameleon.core.values.helper;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo
 */
public final class ConfigurationValueHelper {

  @Nonnull
  public static List<AdditionalParameterKeyValue> transform(
          @Nonnull final List<AdditionalParameterValue> additionalParameterValues) {
    checkNotNull(additionalParameterValues, "The additionalParameterValues must not be null");
    return additionalParameterValues
            .stream()
            .filter(Objects::nonNull)
            .map((apv) -> {
              final String key = apv.getAdditionalParameter().getParameterKey();
              final String value = apv.getParameterValue();
              return new AdditionalParameterKeyValue(key, value);
            })
            .collect(Collectors.toList());
  }

  /**
   * Returns a unique key representation of the additional parameter values. For the same list, it is guaranteed to
   * return the same key always.
   */
  public static String findUniqueKeyFromAdditionalParameterValue(
          @Nonnull final List<AdditionalParameterValue> additionalParameterValues) {
    requireNonNull(additionalParameterValues, "The additionalParameterValues must not be null");

    return findUniqueKeyFromAdditionalParameterKeyValue(transform(additionalParameterValues));
  }

  /**
   * Returns a unique key representation of the additional parameter values. For the same list, it is guaranteed to
   * return the same key always.
   */
  public static String findUniqueKeyFromAdditionalParameterKeyValue(
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterValues) {
    checkNotNull(additionalParameterValues, "The additionalParameterValues must not be null");

    final StringBuilder uniqueKey = new StringBuilder();
    additionalParameterValues
            .stream()
            .filter(Objects::nonNull)
            .map((apv) -> apv.keyValue())
            .sorted()
            .forEach((keyValue) -> uniqueKey.append(keyValue));
    return uniqueKey.toString();
  }

  @Nonnull
  public static ConfigurationValue swapApplicationContext(@Nonnull final ConfigurationValue configurationValue,
          @Nonnull final ApplicationContext applicationContext) {
    checkNotNull(configurationValue, "The configurationValue must not be null");
    checkNotNull(applicationContext, "The applicationContext must not be null");

    final Configuration configuration = configurationValue.getConfiguration();
    final ConfigurationValue newConfigurationValue
            = new ConfigurationValue(applicationContext, configuration, configurationValue.getConfigValue());
    newConfigurationValue.setAdditionalParameterValues(configurationValue.getAdditionalParameterValues());
    return newConfigurationValue;
  }

}
