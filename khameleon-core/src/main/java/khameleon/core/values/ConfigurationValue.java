package khameleon.core.values;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Predicates.notNull;
import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;
import static khameleon.core.ConfigurationUUID.uuid;
import static khameleon.core.values.helper.ConfigurationValueHelper.findUniqueKeyFromAdditionalParameterValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import anosym.common.StringTransient;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlTransient;
import khameleon.core.AdditionalParameter;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author marembo
 */
@Data
@Entity
@XmlAccessorType(FIELD)
@Table(indexes = { @Index(columnList = "additionalParameterValuesUniqueKey") })
@NamedQueries({
		@NamedQuery(name = "ConfigurationValue.findConfigurationValuesWithParameters", query = "SELECT cv FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId "
				+ "AND cv.configuration.namespace.canonicalName = :canonicalName "
				+ "AND cv.configuration.configName = :configName "
				+ "AND cv.additionalParameterValuesUniqueKey = :additionalParameterValuesUniqueKey"),
		@NamedQuery(name = "ConfigurationValue.findConfigurationValues", query = "SELECT cv FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId "
				+ "AND cv.configuration.namespace.canonicalName = :canonicalName "
				+ "AND cv.configuration.configName = :configName " + "ORDER BY cv.additionalParameterValuesUniqueKey"),
		@NamedQuery(name = "ConfigurationValue.findConfigurationValuesByApplicationContextAndByConfigId", query = "SELECT cv FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId AND cv.configuration.configId = :configId "),
		@NamedQuery(name = "ConfigurationValue.countConfigurationValuesByConfigId", query = "SELECT COUNT(cv) FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId AND cv.configuration.configId = :configId "),
		@NamedQuery(name = "ConfigurationValue.deleteConfigurationValues", query = "DELETE FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId AND cv.configuration.configId = :configId "),
		@NamedQuery(name = "ConfigurationValue.findConfigurationValueForApplicationContextAndConfigurationAndParameterKeyValues", query = "SELECT cv FROM ConfigurationValue cv "
				+ "WHERE cv.context.contextId = :contextId " + "AND cv.configuration.configId = :configId "
				+ "AND cv.additionalParameterValuesUniqueKey = :additionalParameterValuesUniqueKey"),
		@NamedQuery(name = "ConfigurationValue.findConfigurationValuesByConfigId", query = "SELECT cv FROM ConfigurationValue cv WHERE cv.configuration.configId = :configId") })
@EntityListeners(EncryptListener.class)
public class ConfigurationValue implements Serializable, Cloneable {

	private static final long serialVersionUID = 13483973909L;

	private static final Function<AdditionalParameterValue, AdditionalParameterKeyValue> VALUE_TO_KEYVALUE_FUNCTION = (
			i) -> new AdditionalParameterKeyValue(i.getAdditionalParameter().getParameterKey(), i.getParameterValue());

	@Id
	@XmlTransient
	@Column(length = 100)
	@SuppressWarnings("FieldMayBeFinal")
	private String valueId = uuid();

	/**
	 * The context for which this configuration has been defined.
	 *
	 * There will always be a default context associated with a configuration value,
	 * the general context.
	 */
	@ManyToOne
	private ApplicationContext context;

	@ManyToOne
	private Configuration configuration;

	@Column(columnDefinition = "TEXT")
	private String configValue;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "configurationValue", orphanRemoval = true)
	private List<AdditionalParameterValue> additionalParameterValues;

	// Used for a lookup if the configuration value for the specified combination is
	// already added.
	@JsonIgnore
	@XmlTransient
	@StringTransient
	@Column(length = 512)
	private String additionalParameterValuesUniqueKey;

	// For ordering.
	@JsonIgnore
	@XmlTransient
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar createdDate;

	// whether the configuration was updated by a call through the frontend. This
	// effectively disables updating the
	// config value through default configuration values.
	@Getter
	@Setter
	@JsonIgnore
	@XmlTransient
	private boolean frontendUpdated;

	@Transient
	@JsonIgnore
	@XmlTransient
	@StringTransient
	private final Map<String, AdditionalParameterValue> additionalParameterValuesCache = Maps.newConcurrentMap();

	public ConfigurationValue() {
	}

	public ConfigurationValue(@Nonnull final ApplicationContext context, @Nonnull final Configuration configuration) {
		this(context, configuration, configuration.getDefaultValue());
	}

	public ConfigurationValue(@Nonnull final ApplicationContext context, @Nonnull final Configuration configuration,
			@Nullable final String configValue) {
		this.context = checkNotNull(context, "The applicationcontext must be specified");
		this.configuration = checkNotNull(configuration, "Configuration must be specified");
		this.configValue = configValue;
		setDefaults();
	}

	private void setDefaults() {
		// create default parameter values if any.
		configuration.getAdditionalParameters().stream().forEach((ap) -> {
			addOrUpdateAdditionalParameterValue(ap, ap.getDefaultValue());
		});
	}

	@PrePersist
	@PreUpdate
	public void updateAdditionalParameterKeyValuesUniqueId() {
		// Dont be smart, dont call getAdditionalParameterValues() in this method.
		// Hibernate loads a the additionalParameterValues as an instanceof
		// PersistencCollection, but a call to our getAdditionalParameterValues()
		// will replace this with AdditionalParameterValueListCache, before the
		// transaction is complete, and further processing results into
		// ClassCastException.
		this.additionalParameterValuesUniqueKey = findUniqueKeyFromAdditionalParameterValue(
				firstNonNull(additionalParameterValues, ImmutableList.<AdditionalParameterValue>of()));
	}

	@PostLoad
	private void initAdditionalParameterValuesCache() {
		if (additionalParameterValues != null) {
			additionalParameterValues.stream().forEach((apv) -> {
				additionalParameterValuesCache.put(apv.getAdditionalParameter().getParameterKey(), apv);
			});
		}
	}

	public boolean getBooleanValue() {
		return Boolean.valueOf(configValue);
	}

	public void setBooleanValue(boolean value) {
		this.configValue = Boolean.toString(value);
	}

	public void setAdditionalParameterValues(@Nonnull final List<AdditionalParameterValue> additionalParameterValues) {
		checkNotNull(additionalParameterValues, "the additionalParameterValues list must not be null");
		if (this.additionalParameterValues == null) {
			this.additionalParameterValues = new ArrayList<>();
		}
		additionalParameterValues.stream().forEach((apv) -> {
			addOrUpdateAdditionalParameterValue(apv.getAdditionalParameter(), apv.getParameterValue());
		});
	}

	@Nonnull
	public List<AdditionalParameterValue> getAdditionalParameterValues() {
		if (additionalParameterValues == null) {
			return additionalParameterValues = new ArrayList<>();
		}
		return additionalParameterValues;
	}

	@Nonnull
	public Optional<AdditionalParameterValue> getAdditionalParameterValue(@Nonnull final String paramKey) {
		if (additionalParameterValuesCache.isEmpty()) {
			initAdditionalParameterValuesCache();
		}
		return Optional.fromNullable(additionalParameterValuesCache.get(paramKey));
	}

	@Nonnull
	public final List<AdditionalParameterKeyValue> getAdditionalParameterKeyValues() {
		return FluentIterable.from(getAdditionalParameterValues()).filter(notNull())
				.transform(VALUE_TO_KEYVALUE_FUNCTION).toList();
	}

	public final void addOrUpdateAdditionalParameterValue(@Nonnull final AdditionalParameter additionalParameter,
			@Nullable final String newValue) {
		if (!updateAdditionalParameterValue(additionalParameter.getParameterKey(), newValue)) {
			final AdditionalParameterValue newApValue = new AdditionalParameterValue(additionalParameter, newValue,
					this);
			addAdditionalParameterValue(newApValue);
		}
	}

	public final boolean updateAdditionalParameterValue(@Nonnull final String additionalParameterKey,
			@Nullable final String newValue) {
		final Optional<AdditionalParameterValue> parameterValue = getAdditionalParameterValue(additionalParameterKey);
		if (parameterValue.isPresent()) {
			parameterValue.get().setParameterValue(newValue);
		}

		return parameterValue.isPresent();
	}

	public final void updateAdditionalParameters(
			@Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
		checkNotNull(additionalParameterKeyValues, "The list of additional parameter key-values must not be null");

		additionalParameterKeyValues.stream().forEach((keyValue) -> {
			final String parameterKey = checkNotNull(keyValue.getKey(),
					"The AdditionalParameterKeyValue.getKey() must not be null");

			// Dynamic attributes may not exists
			checkState(keyValue.isDynamic() || updateAdditionalParameterValue(parameterKey, keyValue.getValue()),
					"AdditionalParameterKeyValue with key {%s} does not exists", parameterKey);
		});
	}

	public final void addAdditionalParameters(
			@Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
		checkNotNull(additionalParameterKeyValues, "The list of additional parameter key-values must not be null");

		additionalParameterKeyValues.stream().forEach((keyValue) -> {
			final String paramKey = checkNotNull(keyValue.getKey(),
					"The AdditionalParameterKeyValue.getKey() must not be null");
			checkState(!getAdditionalParameterValue(paramKey).isPresent(),
					"AdditionalParameterValue with Key {%s} already exists", paramKey);

			final Optional<AdditionalParameter> parameter = configuration.getAdditionalParameter(paramKey);
			checkState(parameter.isPresent(), "AdditionalParameter with key {%s} does not exist", paramKey);
			addAdditionalParameterValue(new AdditionalParameterValue(parameter.get(), keyValue.getValue(), this));
		});
	}

	@Override
	public ConfigurationValue clone() throws CloneNotSupportedException {
		return (ConfigurationValue) super.clone();
	}

	private void addAdditionalParameterValue(@Nonnull final AdditionalParameterValue newParameterValue) {
		checkNotNull(newParameterValue, "The new additional parameter value must not be null");

		final String parameterKey = newParameterValue.getAdditionalParameter().getParameterKey();
		checkArgument(!additionalParameterValuesCache.containsKey(parameterKey),
				"Parameter value with the same key already exists");

		getAdditionalParameterValues().add(newParameterValue);
		additionalParameterValuesCache.put(parameterKey, newParameterValue);
	}

}
