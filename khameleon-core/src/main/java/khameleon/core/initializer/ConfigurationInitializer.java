package khameleon.core.initializer;

import java.lang.reflect.Method;
import java.util.List;
import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.Namespace;

/**
 *
 * @author marembo
 */
public interface ConfigurationInitializer {

    /**
     * Returns a list of namespaces for current configInterface.
     *
     * @param configInterface
     *
     * @return
     */
    List<Namespace> getNamespaces(@Nonnull final Class<?> configInterface);

    /**
     * Generates configuration from the config interface.
     *
     * @param configInterface
     *
     * @return
     */
    List<Configuration> getConfigurations(@Nonnull final Class<?> configInterface);

    /**
     * Generates a single configuration from the configMethod.
     *
     * @param configMethod
     * @return
     */
    Configuration getConfiguration(@Nonnull final Method configMethod);

    /**
     * Generates a single configuration from the configMethod.
     *
     * @param configMethod
     * @return
     */
    Configuration getConfiguration(@Nonnull final Class<?> configInterface, @Nonnull final Method configMethod);
}
