package khameleon.core.initializer;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import khameleon.core.AdditionalParameter;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigPattern;
import khameleon.core.annotations.Context;
import khameleon.core.annotations.namespace.NamespaceProcessor;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.annotations.validator.ConfigValidator;
import khameleon.core.processor.configuration.ConfigParamConfigProcessor;
import khameleon.core.processor.configuration.ConfigurationProcessor;
import khameleon.core.processor.namespace.ConfigNamespaceProcessor;
import khameleon.core.validator.configuration.ConfigurationValidator;

import org.atteo.classindex.ClassIndex;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Sets.newHashSet;
import static khameleon.core.internal.ConfigConstants.METHOD_START_SYNTAX;
import static khameleon.core.internal.ConfigConstants.METHOD_SYNTAX;
import static khameleon.core.internal.ConfigurationUtil.OBJECT_MEMBERS;
import static khameleon.core.processor.configuration.ConfigurationProcessor.CONFIG_PROCESSOR_COMPARATOR;

/**
 * We cannot tell which environment we may be used, and considering that we are target servers, we better be
 * serializable.
 *
 * @author marembo
 */
public final class DefaultConfigurationInitializor implements ConfigurationInitializer, Serializable {

    private static final long serialVersionUID = 13490343984637437L;

    private static final Logger LOG = Logger.getLogger(DefaultConfigurationInitializor.class.getName());

    private static final List<ConfigurationProcessor> CONFIG_PROCESSORS = Lists.newArrayList();

    private static final List<ConfigurationValidator> CONFIG_VALIDATORS = Lists.newArrayList();

    private static final List<ConfigNamespaceProcessor> NAMESPACE_PROCESSORS = Lists.newArrayList();

    static {
        //initialize processors.
        init();
    }

    private static void init() {
        //get all annotation processors.
        ClassIndex
                .getAnnotated(ConfigProcessor.class)
                .forEach((cls) -> {
                    try {
                        //get the config annotation
                        ConfigurationProcessor configProcessor = (ConfigurationProcessor) cls.newInstance();
                        //Ensure that only one instance is added.
                        CONFIG_PROCESSORS.add(configProcessor);
                        Collections.sort(CONFIG_PROCESSORS, CONFIG_PROCESSOR_COMPARATOR);
                    } catch (InstantiationException | IllegalAccessException ex) {
                        throw Throwables.propagate(ex);
                    }
                });

        //namespace processors
        ClassIndex
                .getAnnotated(NamespaceProcessor.class)
                .forEach((cls) -> {
                    try {
                        ConfigNamespaceProcessor namespaceProcessor = (ConfigNamespaceProcessor) cls.newInstance();
                        NAMESPACE_PROCESSORS.add(namespaceProcessor);
                    } catch (InstantiationException | IllegalAccessException ex) {
                        throw Throwables.propagate(ex);
                    }
                });

        //config validators
        ClassIndex
                .getAnnotated(ConfigValidator.class)
                .forEach((cls) -> {
                    try {
                        ConfigurationValidator configurationValidator = (ConfigurationValidator) cls.newInstance();
                        CONFIG_VALIDATORS.add(configurationValidator);
                    } catch (final InstantiationException | IllegalAccessException e) {
                        throw Throwables.propagate(e);
                    }
                });
    }

    @Override
    public List<Namespace> getNamespaces(@Nonnull final Class<?> configInterface) {
        checkNotNull(configInterface, "the configInterface must not be null");

        final List<Namespace> namespaces = Lists.newArrayList();
        getNamespaces(configInterface, namespaces, null);

        return namespaces;
    }

    @Override
    public List<Configuration> getConfigurations(@Nonnull final Class<?> configInterface) {
        checkNotNull(configInterface, "The configinterface must be specified");

        final List<Configuration> configurations = Lists.newArrayList();
        getConfigurations(configInterface, null, configurations, Lists.<AdditionalParameter>newArrayList());

        return configurations;
    }

    @Override
    public Configuration getConfiguration(@Nonnull final Method configMethod) {
        checkNotNull(configMethod, "The configMethod must be specified");

        final List<Configuration> configurations = Lists.newArrayList();
        final Class<?> configInterface = configMethod.getDeclaringClass();
        final List<AdditionalParameter> parameters = Lists.newArrayList();
        getConfigurations(ImmutableSet.of(configMethod), configInterface, null, configurations, parameters);

        return configurations.get(0);
    }

    @Override
    public Configuration getConfiguration(@Nonnull final Class<?> configInterface, @Nonnull final Method configMethod) {
        checkNotNull(configInterface, "The configInterface must not be null");
        checkNotNull(configMethod, "The configMethod must be specified");

        final List<Configuration> configurations = Lists.newArrayList();
        final List<AdditionalParameter> parameters = Lists.newArrayList();
        getConfigurations(ImmutableSet.of(configMethod), configInterface, null, configurations, parameters);

        return configurations.get(0);
    }

    private void getConfigurations(@Nonnull final Class<?> configInterface,
                                   @Nullable final Namespace parentNamespace,
                                   @Nonnull final List<Configuration> configurations,
                                   @Nonnull final List<AdditionalParameter> parameters) {
        final Set<Method> configMethods = newHashSet();
        getConfigMethods(configInterface, configMethods);
        getConfigurations(configMethods, configInterface, parentNamespace, configurations, parameters);
    }

    private void getConfigurations(@Nonnull final Set<Method> configMethods,
                                   @Nonnull final Class<?> configInterface,
                                   @Nullable final Namespace parentNamespace,
                                   @Nonnull final List<Configuration> configurations,
                                   @Nonnull final List<AdditionalParameter> parameters) {
        final Namespace namespace = getNamespace(configInterface, parentNamespace);
        configMethods.stream()
                .forEach((configMethod) -> {
                    Class<?> configType = configMethod.getReturnType();
                    if (!configType.isAnnotationPresent(Config.class)) {
                        final String configName = getConfigName(configMethod);
                        final String configTypeName = configType.getName();
                        final Configuration configuration = new Configuration(namespace, configName, configTypeName);
                        final List<AdditionalParameter> additionalParameters = configuration.getAdditionalParameters();

                        //do we have config parameters?
                        additionalParameters.addAll(parameters);

                        //run validators
                        runValidators(configMethod, configuration);
                        //run the processors.
                        runProcessors(configMethod, configuration);

                        configurations.add(configuration);
                    } else {
                        /**
                         * This is a hack! We need a better way to do this! We try to get the AdditionalParameters for
                         *
                         * @Config type.
                         */
                        final List<AdditionalParameter> additionalParameters = getConfigInterfaceAdditionalParameters(
                                configMethod);
                        additionalParameters.addAll(parameters);
                        getConfigurations(configType, namespace, configurations, additionalParameters);
                    }
                });
    }

    /**
     * If the method declares a child namespace.
     *
     * @param configMethod
     * @return
     */
    public static List<AdditionalParameter> getConfigInterfaceAdditionalParameters(@Nonnull final Method configMethod) {
        checkNotNull(configMethod, "The configMethod must not be null");

        final Class<?> configInterface = configMethod.getDeclaringClass();
        final Namespace namespace = getNamespace(configInterface, null);
        final String configName = getConfigName(configMethod);
        final Class<?> configTypeClass = configMethod.getReturnType();
        final String configType = configTypeClass.getName();
        Configuration tmpConfiguration = new Configuration(namespace, configName, configType);

        new ConfigParamConfigProcessor().process(configMethod, tmpConfiguration);

        return tmpConfiguration.getAdditionalParameters();
    }

    private void runProcessors(final Method m, final Configuration configuration) {
        for (ConfigurationProcessor configProcessor : CONFIG_PROCESSORS) {
            configProcessor.process(m, configuration);
        }
    }

    private void runValidators(final Method m, final Configuration configuration) {
        for (ConfigurationValidator configurationValidator : CONFIG_VALIDATORS) {
            configurationValidator.validate(m, configuration);
        }
    }

    @Nonnull
    private static Namespace getNamespace(@Nonnull final Class<?> configInterface,
                                          @Nullable final Namespace parentNamespace) {
        final Config config = configInterface.getAnnotation(Config.class);
        final String displayName = config.name();
        final String simpleName = getNamespace(configInterface);
        final String description = config.description();
        final Namespace.Builder builder = Namespace.builder();
        builder.withSimpleName(simpleName);
        if (!isNullOrEmpty(displayName)) {
            builder.withDisplayName(displayName);
        }
        if (parentNamespace != null) {
            builder.withParent(parentNamespace);
        }
        if (!isNullOrEmpty(description)) {
            builder.withDescription(description);
        }
        return builder.build();
    }

    private void getNamespaces(@Nonnull final Class<?> configInterface,
                               @Nonnull final List<Namespace> namespaces,
                               @Nullable final Namespace parentNamespace) {
        final Namespace namespace = getNamespace(configInterface, parentNamespace);
        namespaces.add(namespace);

        final Set<Method> methods = newHashSet();
        getConfigMethods(configInterface, methods);

        for (final Method configMethod : methods) {
            final Class returnType = configMethod.getReturnType();
            if (returnType.isAnnotationPresent(Config.class)) {
                getNamespaces(returnType, namespaces, namespace);
            }
        }
        //process the namespace
        for (final ConfigNamespaceProcessor namespaceProcessor : NAMESPACE_PROCESSORS) {
            namespaceProcessor.process(namespace, configInterface);
        }
    }

    @Nonnull
    public static String getConfigName(@Nonnull final Method configMethod) {
        checkNotNull(configMethod, "The configMethod must not be null");

        final String fieldName = METHOD_START_SYNTAX.matcher(configMethod.getName()).replaceAll("");
        return fieldName.length() > 1 ? (fieldName.substring(0, 1).toLowerCase() + fieldName.
                substring(1)) : fieldName.toLowerCase();
    }

    @Nonnull
    public static String getAdditionalParameterName(@Nonnull final ConfigParam configParam,
                                                    @Nullable final ConfigPattern configPattern) {
        if (configPattern != null) {
            return configParam.name() + " expected pattern";
        }
        return configParam.name();
    }

    @Nullable
    public static Method getConfigMethods(@Nonnull final Class configInterface,
                                          @Nonnull final Set<Method> configMethodSets) {
        checkNotNull(configInterface, "The configInterface must not be null");
        checkNotNull(configMethodSets, "The configMethodSets must not be null");
        checkState(configInterface.isAnnotationPresent(Config.class),
                   "The following configInterface {%s} is not annotated with @Config",
                   configInterface);

        Method contextMethod = null;
        //weed out all non-interface methods.
        for (final Method configMethod : configInterface.getMethods()) {
            final Method possiblyConfigMethod = accept(configMethod);
            if (possiblyConfigMethod != null) {
                configMethodSets.add(possiblyConfigMethod);
                if (possiblyConfigMethod.isAnnotationPresent(Context.class)) {
                    contextMethod = possiblyConfigMethod;
                }
            }
        }
        return contextMethod;
    }

    @Nullable
    private static Method accept(Method configMethod) {
        return OBJECT_MEMBERS.contains(configMethod.getName()) || configMethod.isAnnotationPresent(Context.class)
                ? null : checkConforms(configMethod);
    }

    @Nonnull
    private static Method checkConforms(@Nonnull final Method configMethod) {
        final String configName = configMethod.getName();
        final Matcher configNameMatcher = METHOD_SYNTAX.matcher(configName);
        if (configNameMatcher.find()) {
            return configMethod;
        }
        throw new IllegalArgumentException("Interface method signature does not conform to: " + METHOD_SYNTAX);
    }

    @Nonnull
    private static String getNamespace(@Nonnull final Class configInterface) {
        checkState(configInterface.isAnnotationPresent(Config.class),
                   "The interface: {%s} is not declared as a configuration interface.",
                   configInterface);

        final Config config = (Config) configInterface.getAnnotation(Config.class);
        if (isNullOrEmpty(config.namespace())) {
            return configInterface.getCanonicalName();
        }
        return config.namespace();
    }

}
