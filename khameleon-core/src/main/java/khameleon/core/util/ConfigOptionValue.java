package khameleon.core.util;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import khameleon.core.annotations.ConfigOption;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.internal.ConfigurationUtil;
import khameleon.core.internal.service.request.ConfigurationServiceHelper;
import khameleon.core.manager.ConfigurationManager;
import khameleon.core.values.AdditionalParameterKeyValue;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
public class ConfigOptionValue implements Comparable<ConfigOptionValue>, Serializable {

    private static final long serialVersionUID = 134384893L;

    private static final Splitter CONFIG_NAMESPACE_SPLITTER = Splitter.on(":");

    private final int index;

    private final Object value;

    private final ConfigOption configOption;

    private final ConfigurationServiceHelper configHelper;

    private final ApplicationContextService applicationContextService;

    public ConfigOptionValue(final int index,
                             Object value,
                             ConfigOption configOption,
                             ConfigurationServiceHelper configHelper,
                             final ApplicationContextService applicationContextService) {
        this.index = index;
        this.value = value;
        this.configOption = configOption;
        this.configHelper = configHelper;
        this.applicationContextService = applicationContextService;
    }

    private Method getMethod(Object config, String configName) {
        Class<?> clazz = config.getClass();
        return ConfigurationUtil.getConfigMethod(clazz, configName);
    }

    public ConfigOption getConfigOption() {
        return configOption;
    }

    public final Object getOptionValue(Object config) throws Throwable {
        Preconditions.checkNotNull(config, "Config instance must not be null");
        if (value != null && (configOption == null || configOption.configName().length == 0)) {
            //if we have an empty string for value
            return value;
        } else if ((configOption != null || configOption.configName().length > 0)) {
            ConfigOption.ConfigName configName = configOption.configName()[0];
            if (Strings.isNullOrEmpty(configName.namespace())) {
                try {
                    Method m = getMethod(config, configName.value());
                    return m.invoke(config, new Object[]{});
                } catch (SecurityException | IllegalAccessException | IllegalArgumentException
                                 | InvocationTargetException ex) {
                    throw new IllegalArgumentException(
                            "configName is not part of the provided config interface namespace", ex);
                }
            } else {
                //get the raw value from config service.
                checkNotNull(configHelper,
                             "ConfigHelper instance is required for optional value with a specified namespace");
                return invoke(configName.namespace(), configName.value(), new Object[]{});
            }
        }
        throw new IllegalStateException("Config Option value must specify default value or configName");
    }

    private Object invoke(String canonicalNamespace, String configName, Object[] args) throws
            Throwable {
        List<String> namespaces = CONFIG_NAMESPACE_SPLITTER.splitToList(canonicalNamespace);
        String simpleNamespace = namespaces.get(namespaces.size() - 1);
        Class<?> configInterface = Class.forName(simpleNamespace, true, this.getClass().getClassLoader());
        Object proxy = getConfigInstance(configInterface, canonicalNamespace, ImmutableList
                                         .<AdditionalParameterKeyValue>of());
        Method method = ConfigurationUtil.getConfigMethod(configInterface, configName);
        return method.invoke(proxy, args);
    }

    private Object getConfigInstance(Class<?> configInterface, String canonicalNamespace,
                                     List<AdditionalParameterKeyValue> parameterKeyValues) {
        return ConfigurationManager.getInstance(this.applicationContextService,
                                                configInterface,
                                                canonicalNamespace,
                                                configHelper,
                                                parameterKeyValues);
    }

    @Override
    public int compareTo(ConfigOptionValue o) {
        return Integer.valueOf(index).compareTo(o.index);
    }

}
