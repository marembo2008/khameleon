package khameleon.core.util;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.xml.bind.annotation.XmlAccessorType;
import khameleon.core.Configuration;
import khameleon.core.values.AdditionalParameterKeyValue;

import static com.google.common.base.Preconditions.checkNotNull;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

/**
 *
 * @author mochieng
 */
@XmlAccessorType(FIELD)
public class ConfigurationDetail {

  private Configuration configuration;

  private List<AdditionalParameterKeyValue> defaultAdditionalParameterValues;

  public ConfigurationDetail() {
    this.configuration = null;
    this.defaultAdditionalParameterValues = ImmutableList.of();
  }

  public ConfigurationDetail(@Nonnull final Configuration configuration) {
    this.configuration = checkNotNull(configuration, "Configuration value must not be null");
    this.defaultAdditionalParameterValues = Lists.newArrayList();
  }

  public ConfigurationDetail(@Nonnull final Configuration configuration,
                             @Nullable final List<AdditionalParameterKeyValue> defaultAdditionalParameterValues) {
    this.configuration = checkNotNull(configuration, "Configuration value must not be null");
    this.defaultAdditionalParameterValues = Lists.newArrayList(
            MoreObjects.firstNonNull(defaultAdditionalParameterValues,
                                     ImmutableList.<AdditionalParameterKeyValue>of()));
  }

  public Configuration getConfiguration() {
    return configuration;
  }

  @Nonnull
  public List<AdditionalParameterKeyValue> getDefaultAdditionalParameterKeyValues() {
    return defaultAdditionalParameterValues;
  }

}
