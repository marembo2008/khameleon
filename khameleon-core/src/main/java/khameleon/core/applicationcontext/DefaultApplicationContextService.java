package khameleon.core.applicationcontext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Strings;

import anosym.cache.Cacheable;
import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.config.ApplicationContextEnabledConfig;

/**
 * Provided here so that the application can start.
 *
 * @author marembo
 */
@Default
@DefaultContext
@ApplicationScoped
public class DefaultApplicationContextService implements ApplicationContextService {

	private static final long serialVersionUID = 8717606841236808768L;

	private static final String DEFAULT_HOSTNAME;

	static {
		String hostname = null;
		try {
			final Process process = Runtime.getRuntime().exec("hostname");
			final InputStream inputStream = process.getInputStream();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			hostname = reader.readLine();
		} catch (final IOException ex) {
			Logger.getLogger(ApplicationContext.class.getName()).log(Level.SEVERE, null, ex);
		}

		DEFAULT_HOSTNAME = hostname;
	}

	private static final Logger LOG = Logger.getLogger(DefaultApplicationContextService.class.getName());

	protected static final String SYSTEM_PROPERTY_APPLICATION_MODE = "anosym.khameleon.applicationMode";

	protected static final String SYSTEM_PROPERTY_TLD = "anosym.khameleon.tld";

	protected static final String SYSTEM_PROPERTY_LANGUAGE = "anosym.khameleon.language";

	protected static final String SYSTEM_PROPERTY_COUNTRY = "anosym.khameleon.country";

	private static final ThreadLocal<ApplicationContext> CHECKED_APPLICATION_CONTEXT = new ThreadLocal<ApplicationContext>() {

		@Override
		protected ApplicationContext initialValue() {
			return new ApplicationContext().withApplicationMode(getDefaultApplicationMode())
					.withCountryCode(getDefaultCountry()).withLanguageCode(getDefaultLanguage())
					.withTopLevelDomain(getDefaultTLD());
		}

	};

	private static final ThreadLocal<AtomicBoolean> CHECKING_APPLICATION_CONTEXT = new ThreadLocal<AtomicBoolean>() {

		@Override
		protected AtomicBoolean initialValue() {
			return new AtomicBoolean(false);
		}

	};

	@Inject
	private Instance<ApplicationContextEnabledConfig> applicationContextConfigurationServices;

	@Nonnull
	@Override
	@Cacheable
	public ApplicationContext getApplicationContext() {
		ApplicationContext applicationContext = CHECKED_APPLICATION_CONTEXT.get();
		if (!CHECKING_APPLICATION_CONTEXT.get().compareAndSet(false, true)) {
			return applicationContext;
		}

		if (applicationContextConfigurationServices.isUnsatisfied()) {
			LOG.warning("Unable to get instance of ApplicationContextConfigurationService.....");

			return applicationContext;
		}

		int contextId = applicationContext.getContextId();
		while (!applicationContextConfigurationServices.get().isApplicationContextEnabled(contextId)
				&& contextId != ApplicationContext.DEFAULT_APPLICATION_CONTEXT.getContextId()) {
			applicationContext = applicationContext.getParentApplicationContext();

			contextId = applicationContext.getContextId();
		}

		// We set back so that if we call this in the same thread, we will still be
		// safe.
		CHECKING_APPLICATION_CONTEXT.get().set(false);
		return applicationContext;
	}

	@Nonnull
	private static ApplicationMode getDefaultApplicationMode() {
		final String mode = System.getProperty(SYSTEM_PROPERTY_APPLICATION_MODE, "DEVELOPMENT").toUpperCase();
		return ApplicationMode.valueOf(mode);
	}

	@Nullable
	private static TLD getDefaultTLD() {
		String tld = System.getProperty(SYSTEM_PROPERTY_TLD);
		if (!Strings.isNullOrEmpty(tld)) {
			return TLD.valueOf(tld.trim().toUpperCase());
		}
		return null;
	}

	@Nullable
	private static Language getDefaultLanguage() {
		String lang = System.getProperty(SYSTEM_PROPERTY_LANGUAGE);
		if (!Strings.isNullOrEmpty(lang)) {
			return Language.valueOf(lang.trim().toUpperCase());
		}
		return null;
	}

	@Nullable
	private static Country getDefaultCountry() {
		String country = System.getProperty(SYSTEM_PROPERTY_COUNTRY);
		if (!Strings.isNullOrEmpty(country)) {
			return Country.valueOf(country.trim().toUpperCase());
		}
		return null;
	}

	@Nullable
	public static String getDefaultHost() {
		return DEFAULT_HOSTNAME;
	}

}
