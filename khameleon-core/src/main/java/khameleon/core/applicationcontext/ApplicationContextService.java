package khameleon.core.applicationcontext;

import java.io.Serializable;
import javax.annotation.Nonnull;

import khameleon.core.ApplicationContext;

/**
 * ApplicationContext is the most sensitive point entry of the config-service.
 * We have absolutely no idea how an application determines its own context.
 * For session based and application-scope based systems, the determination of application context can be a daunting text.
 *
 * Hence we provide this service, which will be part of the generated source code.
 * @author marembo
 */
public interface ApplicationContextService extends Serializable {

    @Nonnull
    ApplicationContext getApplicationContext();

}
