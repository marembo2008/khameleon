package khameleon.core.applicationcontext;

import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;

import java.util.List;
import javax.annotation.Nonnull;

/**
 * Provides additional provider in the background for configuration which may have not explicitily defined this additional parameters.
 * @author marembo
 */
public interface AdditionalParameterProviderApplicationContextService extends ApplicationContextService {

    /**
     * Sometimes, we just need to pass additional parameters to the request, for which the configservice cannot define.
     * These is mainly here to provide a hook for this functionlity.
     * @param namespace
     * @param configName
     * @return
     */
    @Nonnull
    List<AdditionalParameterKeyValue> getAdditionalParameterKeyValues(@Nonnull final Namespace namespace, @Nonnull final String configName);
}
