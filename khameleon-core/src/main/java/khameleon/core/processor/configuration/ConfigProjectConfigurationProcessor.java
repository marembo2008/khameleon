package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.AdditionalParameter;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigProject;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:48:32 PM
 */
@ConfigProcessor
public class ConfigProjectConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "the configMethod should not be null");
        Preconditions.checkNotNull(configuration, "the configuration should not be null");
        if (configMethod.isAnnotationPresent(ConfigProject.class)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_PROJECT, true);
            //The best way to dynmically add the project data to the config!
            final AdditionalParameter additionalParameter = new AdditionalParameter(ConfigProject.PROJECT_KEY,
                                                                                    "The project for which the following value of the configuration applies to");
            configuration.addAdditionalParameter(additionalParameter);
        }
    }

}
