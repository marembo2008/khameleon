package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.MaxLength;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:10:25 PM
 */
@ConfigProcessor
public class MaxLengthConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(MaxLength.class)) {
            final MaxLength maxLength = configMethod.getAnnotation(MaxLength.class);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_MAX_LENGTH, maxLength.value());
        }
    }

}
