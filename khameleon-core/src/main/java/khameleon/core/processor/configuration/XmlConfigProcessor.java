package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Xml;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.processor.configuration.ConfigurationProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:16:23 PM
 */
@ConfigProcessor
public class XmlConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
                        @Nonnull final Method configMethod,
                        @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(Xml.class)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_XML, true);
        }
    }

}
