package khameleon.core.processor.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.NamespaceParam;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:11:20 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_NAMESPACE_PARAM)
public class NamespaceParamConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(NamespaceParam.class)) {
            final Class<?> type = configMethod.getReturnType();
            checkNamespaceParam(type);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_NAMESPACE_PARAM, true);
        }
        final Annotation[][] annotations = configMethod.getParameterAnnotations();
        final Class<?>[] parameterTypes = configMethod.getParameterTypes();
        for (int i = 0; i < annotations.length; i++) {
            final Annotation[] annots = annotations[i];
            ConfigParam configParam = null;
            NamespaceParam namespaceParam = null;
            for (final Annotation annot : annots) {
                if (annot.annotationType() == ConfigParam.class) {
                    configParam = (ConfigParam) annot;
                }
                if (annot.annotationType() == NamespaceParam.class) {
                    namespaceParam = (NamespaceParam) annot;
                }
            }
            if (namespaceParam != null) {
                if (configParam != null) {
                    checkNamespaceParam(parameterTypes[i]);
                    final String paramKey = configParam.name();
                    //guaranteed to exist, just being too overcautious here :)
                    final Optional<AdditionalParameter> apOptional = configuration.getAdditionalParameter(paramKey);
                    if (!apOptional.isPresent()) {
                        throw new ConfigurationInitializationException(
                                "Undefined additional parameter for @NamespaceParam: " + paramKey + ", Configuration: " + configuration);
                    }
                    final AdditionalParameter ap = apOptional.get();
                    ap.addConfigData(AdditionalParameterConfigDataType.PARAMETER_NAMESPACE_PARAM, true);
                } else {
                    throw new ConfigurationInitializationException(
                            "@NamespaceParam can only be defined for @ConfigParam");
                }
            }
        }
    }

    private void checkNamespaceParam(
            @Nonnull final Class<?> type) {
        final Class<?> expectedType = type.isArray() ? type.getComponentType() : type;
        if (!String.class.isAssignableFrom(expectedType)) {
            throw new ConfigurationInitializationException("@NamespaceParam must be of type: String.class");
        }
    }

}
