package khameleon.core.processor.configuration;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.RichText;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:13:52 PM
 */
@ConfigProcessor
public class RichTextConfigProcessor implements ConfigurationProcessor {

    private static final Logger LOG = Logger.getLogger(RichTextConfigProcessor.class.getName());

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(RichText.class)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_RICH_TEXT, true);
        }
    }

}
