package khameleon.core.processor.configuration;

import static jakarta.xml.bind.JAXBContext.newInstance;
import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;
import static java.util.Objects.requireNonNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.AppContext;
import khameleon.core.exception.InvalidApplicationContextException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 13, 2015, 11:23:35 PM
 */
public final class NamespaceApplicationContextHelper {

	private static final JAXBContext J_AXBCONTEXT;

	static {
		try {
			J_AXBCONTEXT = newInstance(ApplicationContextWrapper.class);
		} catch (final JAXBException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static final String INVALID_APP_CONTEXT_DEFINITION_ERROR = "ApplicationMode must be specified if any other property of the AppContext has been specified";

	private static final Logger LOG = Logger.getLogger(NamespaceApplicationContextHelper.class.getName());

	@Nonnull
	public static String marshall(@Nonnull final List<ApplicationContext> applicationContexts) throws JAXBException {
		requireNonNull(applicationContexts, "The applicationContexts must not be null");

		final Marshaller m = J_AXBCONTEXT.createMarshaller();
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		m.marshal(new ApplicationContextWrapper(applicationContexts), outputStream);

		final byte[] result = outputStream.toByteArray();
		return new String(result);
	}

	@Nonnull
	public static List<ApplicationContext> unmarshall(@Nonnull final String applicationContextXml)
			throws JAXBException {
		requireNonNull(applicationContextXml, "The applicationContextXml must not be null");

		final Unmarshaller m = J_AXBCONTEXT.createUnmarshaller();
		final ByteArrayInputStream inn = new ByteArrayInputStream(applicationContextXml.getBytes());

		return ((ApplicationContextWrapper) m.unmarshal(inn)).applicationContexts;
	}

	/**
	 * Returns the applicationcontexts for which the specified config service is
	 * defined.
	 *
	 * @param configInterface
	 * @return
	 */
	@Nonnull
	public static List<ApplicationContext> getApplicationContexts(@Nonnull final Class<?> configInterface) {
		requireNonNull(configInterface, "The configInterface must not be null");

		if (!configInterface.isAnnotationPresent(AppContext.class)) {
			return ImmutableList.of();
		}

		final AppContext appContext = configInterface.getAnnotation(AppContext.class);
		final List<ApplicationMode> modes = Lists.newArrayList(appContext.applicationModes());
		final List<TLD> tlds = Lists.newArrayList(appContext.topLevelDomains());
		final List<Language> languages = Lists.newArrayList(appContext.languages());
		final List<Country> countries = Lists.newArrayList(appContext.countries());

		// We initialize to null so that we can form at least a single appcontext for
		// the default app context.
		if (modes.isEmpty()) {
			modes.add(null);
		}
		if (tlds.isEmpty()) {
			tlds.add(null);
		}
		if (languages.isEmpty()) {
			languages.add(null);
		}
		if (countries.isEmpty()) {
			countries.add(null);
		}

		final List<ApplicationContext> applicationContexts = Lists.newArrayList();
		for (ApplicationMode applicationMode : modes) {
			for (TLD tld : tlds) {
				for (Language language : languages) {
					for (Country country : countries) {
						// This is a validation strategy, all must be null, or at least mode must be
						// defined.
						final boolean isAnyOtherPropertiesDefined = tld != null || language != null || country != null;
						final boolean isApplicationModeDefined = applicationMode != null;
						final boolean isValidAppContext = !isAnyOtherPropertiesDefined || isApplicationModeDefined;
						if (!isValidAppContext) {
							throw new InvalidApplicationContextException(INVALID_APP_CONTEXT_DEFINITION_ERROR);
						}

						applicationContexts.add(new ApplicationContext(applicationMode, tld, language, country));
					}
				}
			}
		}

		return ImmutableList.copyOf(applicationContexts);
	}

	/**
	 * We do not want to define {@link ApplicationContext} as root element.
	 * CXF/JBOSS webservice has an issue.
	 */
	@XmlRootElement
	@XmlAccessorType(FIELD)
	public static final class ApplicationContextWrapper {

		private final List<ApplicationContext> applicationContexts;

		public ApplicationContextWrapper() {
			this.applicationContexts = null;
		}

		public ApplicationContextWrapper(final List<ApplicationContext> applicationContexts) {
			this.applicationContexts = applicationContexts;
		}

	}

}
