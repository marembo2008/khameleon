package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ValueConverter;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:15:42 PM
 */
@Slf4j
@ConfigProcessor
public class ValueConverterConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (!configMethod.isAnnotationPresent(ValueConverter.class)) {
            return;
        }
        log.info("Registering value converter for config: {}:{}", configuration.getNamespace().getCanonicalName(),
                 configuration.getConfigName());
        final ValueConverter valueConverter = configMethod.getAnnotation(ValueConverter.class);
        configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_VALUE_CONVERTER,
                                           valueConverter.value().getName());
    }

}
