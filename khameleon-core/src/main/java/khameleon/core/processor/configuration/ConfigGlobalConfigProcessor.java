package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigGlobal;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:44:52 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_CONFIG_GLOBAL)
public final class ConfigGlobalConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "The configMethod must not be null");
        Preconditions.checkNotNull(configuration, "The configuration must not be null");
        if (configMethod.isAnnotationPresent(ConfigGlobal.class)) {
            Preconditions.checkArgument(Boolean.class.isAssignableFrom(configMethod.getReturnType()) || boolean.class
                    .isAssignableFrom(configMethod.getReturnType()),
                                        "@ConfigGlobal can only be used on a boolean configuration!");
            final ConfigGlobal configGlobal = configMethod.getAnnotation(ConfigGlobal.class);
            final String configGlobalId = getConfigGlobalId(configGlobal);
            //It will be insane to enforce uniqueness at this point. Either to much energy and memory will be spent
            //to ensure that we keep track of all configglobal uniqueness, or we can simply do this at runtime.
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_GLOBAL,
                                               configGlobal.enabledState());
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_GLOBAL_ID, configGlobalId);
        }
    }

    @Nonnull
    private String getConfigGlobalId(
            @Nonnull final ConfigGlobal configGlobal) {
        if (!Strings.isNullOrEmpty(configGlobal.value())) {
            return configGlobal.value();
        }
        //We generate a unique value, system wide. Guaranteed always.
        return ConfigGlobalUtil.generateConfigGlobalId();
    }

}
