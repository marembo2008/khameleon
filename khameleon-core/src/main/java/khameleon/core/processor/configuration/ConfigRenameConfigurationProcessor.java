package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigRename;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:50:23 PM
 */
@ConfigProcessor
public final class ConfigRenameConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "the configMethod must not be null");
        Preconditions.checkNotNull(configuration, "the configuration must not be null");
        if (configMethod.isAnnotationPresent(ConfigRename.class)) {
            final ConfigRename configRename = configMethod.getAnnotation(ConfigRename.class);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_RENAME,
                                               Preconditions.checkNotNull(Strings.emptyToNull(configRename.value()),
                                                                          "the old config name must be specified"));
        }
    }

}
