package khameleon.core.processor.configuration;

import javax.annotation.Nonnull;

import khameleon.core.annotations.ConfigGlobal;

/**
 *
 * @author marembo
 */
public final class ConfigGlobalUtil {

    @Nonnull
    public static String generateConfigGlobalId() {
        return ConfigGlobal.class.getCanonicalName();
    }
}
