package khameleon.core.processor.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigProvidedOption;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:49:10 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_CONFIG_PROVIDED_OPTION)
public final class ConfigProvidedOptionConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Objects.requireNonNull(configMethod, "The configMethod must not be null");
        Objects.requireNonNull(configuration, "The configuration must not be null");
        if (configMethod.isAnnotationPresent(ConfigProvidedOption.class)) {
            final ConfigProvidedOption configProvidedOption = configMethod.getAnnotation(ConfigProvidedOption.class);
            final ConfigProvidedOption.ConfigOptionType optionType = configProvidedOption.value();
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_PROVIDED_OPTION, optionType);
        }
        for (final Annotation[] annotations : configMethod.getParameterAnnotations()) {
            ConfigParam configParam = null;
            ConfigProvidedOption configProvidedOption = null;
            for (final Annotation annotation : annotations) {
                if (annotation.annotationType() == ConfigProvidedOption.class) {
                    configProvidedOption = (ConfigProvidedOption) annotation;
                }
                if (annotation.annotationType() == ConfigParam.class) {
                    configParam = (ConfigParam) annotation;
                }
            }
            if (configProvidedOption == null) {
                continue;
            }
            checkState(configParam != null, "Cannot declare @ConfigProvidedOption on a non @ConfigParam parameter");
            @SuppressWarnings(value = "null")
            final String paramKey = configParam.name();
            final Optional<AdditionalParameter> additionalParameterOptional = configuration.getAdditionalParameter(
                    paramKey);
            checkState(additionalParameterOptional.isPresent(),
                       "@ConfigParam is defined for config-value-parameters, but no additional-parameter evaluated: %s",
                       configMethod);
            final AdditionalParameter additionalParameter = additionalParameterOptional.get();
            final ConfigProvidedOption.ConfigOptionType parameterConfigOptionType = configProvidedOption.value();
            additionalParameter.addConfigData(AdditionalParameterConfigDataType.PARAMETER_CONFIG_PROVIDED_OPTION,
                                              parameterConfigOptionType);
        }
    }

    private static void checkState(final boolean state,
                                   @Nonnull final String parameter, final Object... args) {
        if (!state) {
            throw new ConfigurationInitializationException(String.format(parameter, args));
        }
    }

}
