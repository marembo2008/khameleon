package khameleon.core.processor.configuration.localcache;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.localcache.InMemoryCache;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.processor.configuration.ConfigurationProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:16:50 PM
 */
@ConfigProcessor
public class InMemoryCacheConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "the configmethod must be specified");
        Preconditions.checkNotNull(configuration, "the configuration must be specified");
        if (configMethod.isAnnotationPresent(InMemoryCache.class)) {
            final InMemoryCache inMemoryCache = configMethod.getAnnotation(InMemoryCache.class);
            final TimeUnit timeUnit = inMemoryCache.unit();
            Preconditions.checkArgument(timeUnit.compareTo(TimeUnit.SECONDS) >= 0,
                                        "The minimum time interval supported currently is TimeUnit.SECONDS");
            final String periodValue = String.format("%s%s", inMemoryCache.period(), timeUnit.name());
            configuration
                    .addConfigurationData(ConfigurationDataOption.CONFIGURATION_IN_MEMORY_CACHE_PERIOD, periodValue);
        }
    }

}
