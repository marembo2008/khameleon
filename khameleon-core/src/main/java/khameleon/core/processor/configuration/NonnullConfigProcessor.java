package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import jakarta.validation.constraints.NotNull;
import khameleon.core.Configuration;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

import static java.util.Objects.requireNonNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_NONNULL;
import static khameleon.core.annotations.processor.ConfigProcessor.ConfigProcessorOrder.ORDER_ANY_ORDER;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 20, 2015, 12:28:38 PM
 */
@ConfigProcessor(order = ORDER_ANY_ORDER)
public class NonnullConfigProcessor implements ConfigurationProcessor {

  @Override
  public void process(@Nonnull final Method configMethod, @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
    requireNonNull(configMethod, "The configMethod must not be null");
    requireNonNull(configuration, "The configuration must not be null");

    configuration.addConfigurationData(CONFIGURATION_NONNULL,
                                       configMethod.isAnnotationPresent(Nonnull.class)
                                       || configMethod.isAnnotationPresent(NotNull.class));
  }

}
