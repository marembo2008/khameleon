package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Constant;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:03:03 PM
 */
@ConfigProcessor
public class ConstantConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(Constant.class)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONSTANT, "true");
        }
    }

}
