package khameleon.core.processor.configuration;

import static khameleon.core.initializer.DefaultConfigurationInitializor.getConfigName;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigDeprecated;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:44:16 PM
 */
@ConfigProcessor
public final class ConfigDeprecatedConfigProcessor implements ConfigurationProcessor {

    private static final String DEPRECATION_MESSAGE_WARNING = "The following configuration has been deprecated, and is highly recommended to be removed from the config frontend";

    private static final String AUTOREMOVE_DATE_REQUIRED_MESSAGE = "the date for config removal removal must be specified if autoremove is selected";

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(ConfigDeprecated.class)) {
            final ConfigDeprecated configDeprecated = configMethod.getAnnotation(ConfigDeprecated.class);
            if (configDeprecated.autoRemove() && Strings.isNullOrEmpty(configDeprecated.when())) {
                throw new ConfigurationInitializationException(AUTOREMOVE_DATE_REQUIRED_MESSAGE);
            }
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEPRECATED, true);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEPRECATED_AUTOREMOVE,
                                               configDeprecated.autoRemove());
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEPRECATED_REMOVE_DATE,
                                               configDeprecated.when());
            final String configName = getConfigName(configMethod);
            final String aiName = configName + "-deprecation";
            configuration.addAdditionalInformation(aiName, configDeprecated.message());
            //Add a deprecated message warning too
            final String deprecatedWarn = configName + "-deprecation-warning";
            configuration.addAdditionalInformation(deprecatedWarn, DEPRECATION_MESSAGE_WARNING);
        }
    }

}
