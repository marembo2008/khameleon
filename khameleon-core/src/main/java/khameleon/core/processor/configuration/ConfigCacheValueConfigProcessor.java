package khameleon.core.processor.configuration;

import static khameleon.core.annotations.ConfigCacheValue.CacheValutOption.IGNORE_WHEN_NULL;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigCacheValue;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:42:26 PM
 */
@ConfigProcessor
public class ConfigCacheValueConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "The configMethod must not be null");
        Preconditions.checkNotNull(configuration, "The confconfigurationt not be null");
        if (configMethod.isAnnotationPresent(ConfigCacheValue.class)) {
            final ConfigCacheValue configCacheValue = configMethod.getAnnotation(ConfigCacheValue.class);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_LOCAL_CACHE_VALUE,
                                               configCacheValue.value());
        } else {
            //Default, ignore all null values for cache.
            configuration
                    .addConfigurationData(ConfigurationDataOption.CONFIGURATION_LOCAL_CACHE_VALUE, IGNORE_WHEN_NULL);
        }
    }

}
