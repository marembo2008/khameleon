package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.annotations.Info;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:09:14 PM
 */
@ConfigProcessor
public class InfoConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(Info.class)) {
            final Info info = configMethod.getAnnotation(Info.class);
            String name = info.name();
            if (Strings.isNullOrEmpty(name)) {
                name = configuration.getConfigName();
            }
            configuration.addAdditionalInformation(name, info.value());
        }
    }

}
