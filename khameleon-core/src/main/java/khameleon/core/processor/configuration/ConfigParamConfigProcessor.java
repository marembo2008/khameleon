package khameleon.core.processor.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Calendar;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.internal.service.request.ConfigurationService;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:46:36 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_CONFIG_PARAM)
public class ConfigParamConfigProcessor implements ConfigurationProcessor {

    @Override
    @SuppressWarnings(value = "null")
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        final Class<?>[] parameterTypes = configMethod.getParameterTypes();
        final Annotation[][] parameterAnnotations = configMethod.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            final Annotation[] annotations = parameterAnnotations[i];
            final Class<?> parameterType = parameterTypes[i];
            Preconditions.checkArgument(ConfigurationService.checkType(parameterType),
                                        "Config Parameter Type must be a primitive, string, calendar, bigdecimal or enum");
            ConfigParam cParam = null;
            for (final Annotation a : annotations) {
                if (a.annotationType() == ConfigParam.class) {
                    cParam = (ConfigParam) a;
                    break;
                }
            }
            if (cParam != null) {
                //the value of the additional parameter is to be set by users.
                final AdditionalParameter ap = new AdditionalParameter(cParam.name(), cParam.description(),
                                                                       cParam.value());
                if (parameterType.isEnum()) {
                    final AdditionalParameterConfigData enumConfigData = new AdditionalParameterConfigData(
                            AdditionalParameterConfigDataType.PARAMETER_ENUM,
                            Boolean.TRUE.toString());
                    enumConfigData.setEnumList((Class<? extends Enum>) parameterType);
                    ap.addConfigData(enumConfigData);
                } else if (Calendar.class.isAssignableFrom(parameterType)) {
                    final AdditionalParameterConfigData calendarConfigData = new AdditionalParameterConfigData(
                            AdditionalParameterConfigDataType.PARAMETER_CALENDAR,
                            Boolean.TRUE.toString());
                    ap.addConfigData(calendarConfigData);
                } else if (parameterType == boolean.class || parameterType == Boolean.class) {
                    final AdditionalParameterConfigData booleanConfigData = new AdditionalParameterConfigData(
                            AdditionalParameterConfigDataType.PARAMETER_BOOLEAN,
                            Boolean.TRUE.toString());
                    ap.addConfigData(booleanConfigData);
                }
                configuration.addAdditionalParameter(ap);
                //Is this parameter required
                if (cParam.required()) {
                    ap.addConfigData(AdditionalParameterConfigDataType.PARAMETER_REQUIRED, cParam.required());
                }
            }
        }
    }

}
