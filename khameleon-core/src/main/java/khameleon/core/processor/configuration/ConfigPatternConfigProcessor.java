package khameleon.core.processor.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.annotations.ConfigPattern;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import khameleon.core.internal.service.request.ConfigurationService;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:47:08 PM
 */
@ConfigProcessor
public class ConfigPatternConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        final Class<?>[] parameterTypes = configMethod.getParameterTypes();
        final Annotation[][] parameterAnnotations = configMethod.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            final Annotation[] annotations = parameterAnnotations[i];
            final Class<?> parameterType = parameterTypes[i];
            Preconditions.checkArgument(ConfigurationService.checkType(parameterType),
                                        "Config Parameter Type must be a primitive, string, calendar, bigdecimal or enum");
            ConfigPattern cPattern = null;
            for (Annotation a : annotations) {
                if (a.annotationType() == ConfigPattern.class) {
                    cPattern = (ConfigPattern) a;
                }
            }
            if (cPattern != null) {
                Preconditions.checkState(String.class.isAssignableFrom(parameterType),
                                         "@ConfigPattern can only be applied to String.class types");
                final AdditionalParameter ap = new AdditionalParameter(cPattern.name(), cPattern.description(),
                                                                       cPattern.value());
                ap.addConfigData(AdditionalParameterConfigDataType.PARAMETER_PATTERN, true);
                configuration.addAdditionalParameter(ap);
            }
        }
    }

}
