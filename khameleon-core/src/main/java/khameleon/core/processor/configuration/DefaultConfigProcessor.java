package khameleon.core.processor.configuration;

import static khameleon.core.internal.ConfigurationUtil.isPrimitiveOrPrimitveWrapper;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:04:29 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_DEFAULT_VALUE)
public class DefaultConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        final Class<?> configType = configMethod.getReturnType();
        if (configMethod.isAnnotationPresent(Default.class)) {
            final Default def = configMethod.getAnnotation(Default.class);
            configuration.setDefaultValue(def.value());
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE, true);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE_WHEN_NULL_OR_EMPTY,
                                               def.defaultWhenNullOrEmpty());
        } else if (isPrimitiveOrPrimitveWrapper(configType)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE, true);
            //if boolean
            if (isBoolean(configType)) {
                configuration.setDefaultValue("false");
            } else if (isFloatingPoint(configType)) {
                configuration.setDefaultValue("0.0");
            } else if (isCharacter(configType)) {
                configuration.setDefaultValue(" ");
            } else {
                //all primitives and wrappers set to 0.
                configuration.setDefaultValue("0");
            }
        }
    }

    private boolean isBoolean(
            @Nonnull final Class<?> configType) {
        return Boolean.class.isAssignableFrom(configType) || boolean.class.isAssignableFrom(configType);
    }

    private boolean isCharacter(
            @Nonnull final Class<?> configType) {
        return Character.class.isAssignableFrom(configType) || char.class.isAssignableFrom(configType);
    }

    private boolean isFloatingPoint(
            @Nonnull final Class<?> configType) {
        return Double.class.isAssignableFrom(configType) || double.class.isAssignableFrom(configType) || Float.class
                .isAssignableFrom(configType) || float.class.isAssignableFrom(configType);
    }

}
