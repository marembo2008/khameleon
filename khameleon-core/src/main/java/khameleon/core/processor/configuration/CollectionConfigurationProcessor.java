package khameleon.core.processor.configuration;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import javax.annotation.Nonnull;

import com.google.common.reflect.TypeToken;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkArgument;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_ARRAY;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_COLLECTION_COMPONENT_TYPE;

/**
 *
 * @author marembo
 */
@Slf4j
@ConfigProcessor
public class CollectionConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(@Nonnull final Method configMethod,
                        @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        checkCollectionType(configMethod, configuration);
    }

    private void checkCollectionType(final Method configMethod, final Configuration configuration) {
        final Class<?> configType = configMethod.getReturnType();
        if (!Collection.class.isAssignableFrom(configType)) {
            return;
        }

        final Type collectionParametrizedType = TypeToken.of(configMethod.getGenericReturnType()).getType();
        checkArgument(collectionParametrizedType instanceof ParameterizedType, "Collection must be parametrized");

        final ParameterizedType parameterizedType = (ParameterizedType) collectionParametrizedType;
        final Type[] typeArguments = parameterizedType.getActualTypeArguments();
        checkArgument(typeArguments.length == 1, "Collection must have one type argument");

        final Class componentType = (Class) typeArguments[0];
        configuration.addConfigurationData(CONFIGURATION_COLLECTION_COMPONENT_TYPE, componentType.getName());

        //To map collection to arrays in frontend
        configuration.addConfigurationData(CONFIGURATION_ARRAY, componentType.getName());
        if (componentType.isEnum()) {
            final ConfigurationDataOption enumOption = ConfigurationDataOption.CONFIGURATION_ENUM;
            final ConfigurationData enumData = configuration.addConfigurationData(enumOption, "true");
            enumData.setEnumList(componentType);
        }
    }

}
