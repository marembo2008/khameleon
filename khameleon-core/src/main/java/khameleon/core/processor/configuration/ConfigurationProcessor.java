package khameleon.core.processor.configuration;

import khameleon.core.Configuration;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

import java.lang.reflect.Method;
import java.util.Comparator;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author mochieng
 */
public interface ConfigurationProcessor {

    final Comparator<ConfigurationProcessor> CONFIG_PROCESSOR_COMPARATOR = new Comparator<ConfigurationProcessor>() {

        @Override
        public int compare(@Nonnull final ConfigurationProcessor configurationProcessor1,
                @Nonnull final ConfigurationProcessor configurationProcessor2) {
            checkNotNull(configurationProcessor1, "The configurationProcessor1 must not be null");
            checkNotNull(configurationProcessor2, "The configurationProcessor2 must not be null");

            final ConfigProcessor configProcessor1
                    = configurationProcessor1.getClass().getAnnotation(ConfigProcessor.class);
            final ConfigProcessor configProcessor2
                    = configurationProcessor2.getClass().getAnnotation(ConfigProcessor.class);
            return configProcessor1.order().compareTo(configProcessor2.order());
        }
    };

    void process(@Nonnull final Method configMethod, @Nonnull final Configuration configuration) throws
            ConfigurationInitializationException;

}
