package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigDelete;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:43:07 PM
 */
@ConfigProcessor
public final class ConfigDeleteConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "the configMethod must not be null");
        Preconditions.checkNotNull(configuration, "the configuration must not be null");
        if (configMethod.isAnnotationPresent(ConfigDelete.class) || configMethod.getDeclaringClass()
                .isAnnotationPresent(ConfigDelete.class)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_DELETE, true);
        }
    }

}
