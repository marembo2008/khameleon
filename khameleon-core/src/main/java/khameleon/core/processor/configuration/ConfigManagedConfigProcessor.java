package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigManaged;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:45:44 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_CONFIG_MANAGED)
public final class ConfigManagedConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "The configMethod must not be null");
        Preconditions.checkNotNull(configuration, "The configuration must not be null");
        if (configMethod.isAnnotationPresent(ConfigManaged.class)) {
            Preconditions.checkArgument(Boolean.class.isAssignableFrom(configMethod.getReturnType()) || boolean.class
                    .isAssignableFrom(configMethod.getReturnType()),
                                        "@ConfigGloballyManaged can only be used on a boolean configuration!");
            final ConfigManaged configManaged = configMethod.getAnnotation(ConfigManaged.class);
            final String configGlobalId = getConfigGlobalId(configManaged);
            //We cannot, currently enforce the existence of the @ConfigGlobal, as there is no defined order or
            //processing the configurations.
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_MANAGED, true);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_MANAGED_ID, configGlobalId);
        }
    }

    @Nonnull
    private String getConfigGlobalId(
            @Nonnull final ConfigManaged configManaged) {
        if (!Strings.isNullOrEmpty(configManaged.value())) {
            return configManaged.value();
        }
        //We generate a unique id, system wide. Guaranteed always.
        return ConfigGlobalUtil.generateConfigGlobalId();
    }

}
