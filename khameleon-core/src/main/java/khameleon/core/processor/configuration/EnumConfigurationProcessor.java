package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo
 */
@ConfigProcessor
public class EnumConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(@Nonnull final Method configMethod,
                        @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        checkEnumType(configMethod, configuration);
    }

    private void checkEnumType(final Method configMethod, final Configuration configuration) {
        final Class cls = configMethod.getReturnType();
        if (cls.isEnum()) {
            final ConfigurationDataOption dataOption = ConfigurationDataOption.CONFIGURATION_ENUM;
            final ConfigurationData confData = configuration.addConfigurationData(dataOption, cls.getName());
            confData.setEnumList(cls);
        }
    }

}
