package khameleon.core.processor.configuration;

import java.lang.reflect.Method;
import java.util.Calendar;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.CalendarFormat;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:41:35 PM
 */
@ConfigProcessor
public class CalendarFormatConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Class<?> configType = configMethod.getReturnType();
        if (configMethod.isAnnotationPresent(CalendarFormat.class)) {
            final CalendarFormat format = configMethod.getAnnotation(CalendarFormat.class);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CALLENDAR_FORMAT, format.value());
        } else if (Calendar.class.isAssignableFrom(configType)) {
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CALLENDAR_FORMAT,
                                               "yyyy-MM-dd HH:mm:ss");
        }
    }

}
