package khameleon.core.processor.configuration;

import static khameleon.core.initializer.DefaultConfigurationInitializor.getConfigName;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigDeprecated;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo
 */
@ConfigProcessor
public class DeprecatedCheckProcessor implements ConfigurationProcessor {

    private static final String deprecationMessage = "The following configuration has been marked deprecated "
            + "and should be removed from the configuration front-end";

    @Override
    public void process(@Nonnull final Method configMethod, @Nonnull final Configuration configuration) throws
            ConfigurationInitializationException {
        if (!configMethod.isAnnotationPresent(ConfigDeprecated.class) && configMethod.isAnnotationPresent(
                Deprecated.class)) {
            final String aiName = getConfigName(configMethod) + "-deprecation";
            configuration.addAdditionalInformation(aiName, deprecationMessage);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_DEPRECATED, true);
        }
    }

}
