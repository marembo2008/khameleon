package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo
 */
@ConfigProcessor
public class ArrayConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(@Nonnull final Method configMethod,
                        @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        checkArrayType(configMethod, configuration);
    }

    private void checkArrayType(final Method configMethod, final Configuration configuration) {
        final Class configType = configMethod.getReturnType();
        if (configType.isArray()) {
            final Class componentType = configType.getComponentType();
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_ARRAY, componentType.getName());
            if (componentType.isEnum()) {
                final ConfigurationDataOption enumOption = ConfigurationDataOption.CONFIGURATION_ENUM;
                final ConfigurationData enumData = configuration.addConfigurationData(enumOption, "true");
                enumData.setEnumList(componentType);
            }
        }
    }

}
