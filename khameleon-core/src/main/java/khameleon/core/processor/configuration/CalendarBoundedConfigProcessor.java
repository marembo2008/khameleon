package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.CalendarBounded;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:40:57 PM
 */
@ConfigProcessor
public class CalendarBoundedConfigProcessor implements ConfigurationProcessor {

  @Override
  public void process(
          @Nonnull final Method configMethod,
          @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
    if (!configMethod.isAnnotationPresent(CalendarBounded.class)) {
      return;
    }
    configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CALENDAR_BOUNDED, true);
    final AdditionalParameter boundedFrom = new AdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY,
                                                                    CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_DESC);
    final AdditionalParameterConfigData boundedFromCalendarFormat = new AdditionalParameterConfigData(
            AdditionalParameterConfigDataType.PARAMETER_CALENDAR,
            Boolean.TRUE);
    boundedFrom.addConfigData(boundedFromCalendarFormat);
    configuration.addAdditionalParameter(boundedFrom);
    final AdditionalParameter boundedTo = new AdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY,
                                                                  CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_DESC);
    final AdditionalParameterConfigData boundedToCalendarFormat = new AdditionalParameterConfigData(
            AdditionalParameterConfigDataType.PARAMETER_CALENDAR,
            Boolean.TRUE);
    boundedTo.addConfigData(boundedToCalendarFormat);
    configuration.addAdditionalParameter(boundedTo);
  }

}
