package khameleon.core.processor.configuration;

import java.lang.reflect.Method;
import java.util.Objects;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.RequiredThrows;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:12:37 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_ANY_ORDER)
public class RequiredThrowsConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Objects.requireNonNull(configMethod, "The configMethod must not be null");
        Objects.requireNonNull(configuration, "The configuration must not be null");
        if (configMethod.isAnnotationPresent(RequiredThrows.class)) {
            final RequiredThrows requiredThrows = configMethod.getAnnotation(RequiredThrows.class);
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_REQUIRED_THROWS,
                                               requiredThrows.value().getName());
        }
    }

}
