package khameleon.core.processor.configuration;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.Namespace;
import khameleon.core.annotations.Requires;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 9:13:16 PM
 */
@ConfigProcessor
public final class RequiresConfigurationProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        if (configMethod.isAnnotationPresent(Requires.class)) {
            final Requires requires = configMethod.getAnnotation(Requires.class);
            setRequiresState(requires, configuration);
        } else if (configMethod.getDeclaringClass().isAnnotationPresent(Requires.class)) {
            final Requires requires = configMethod.getDeclaringClass().getAnnotation(Requires.class);
            setRequiresState(requires, configuration);
        } else if (configMethod.getDeclaringClass().getPackage().isAnnotationPresent(Requires.class)) {
            final Requires requires = configMethod.getDeclaringClass().getPackage().getAnnotation(Requires.class);
            setRequiresState(requires, configuration);
        }
    }

    private void setRequiresState(
            @Nonnull final Requires requires,
            @Nonnull final Configuration configuration) {
        final Namespace thisNamespace = configuration.getNamespace();
        final String definedNamespace = Strings.emptyToNull(requires.namespace());
        final String requiresNamespace = MoreObjects.firstNonNull(definedNamespace, thisNamespace.getCanonicalName());
        configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_REQUIRES_VALUE, requires.value());
        configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_REQUIRES_CONFIGNAME,
                                           requires.configName());
        configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_REQUIRES_NAMESPACE, requiresNamespace);
    }

}
