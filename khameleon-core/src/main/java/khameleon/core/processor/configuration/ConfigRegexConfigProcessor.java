package khameleon.core.processor.configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRegex;
import khameleon.core.annotations.processor.ConfigProcessor;
import khameleon.core.exception.ConfigurationInitializationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:49:43 PM
 */
@ConfigProcessor(order = ConfigProcessor.ConfigProcessorOrder.ORDER_CONFIG_REGEX)
public class ConfigRegexConfigProcessor implements ConfigurationProcessor {

    @Override
    public void process(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) throws ConfigurationInitializationException {
        Preconditions.checkNotNull(configMethod, "The configMethod must not be null");
        Preconditions.checkNotNull(configuration, "The configuration must not be null");
        processConfigValue(configMethod, configuration);
        processConfigParameter(configMethod, configuration);
    }

    private void processConfigValue(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) {
        if (configMethod.isAnnotationPresent(ConfigRegex.class)) {
            Preconditions.checkArgument(configMethod.getReturnType().isAssignableFrom(String.class),
                                        "@ConfigRegex is valid only for String type config values");
            final String regex = configMethod.getAnnotation(ConfigRegex.class).value();
            //Check the default value first, if any.
            final String defaultValue = configuration.getDefaultValue();
            if (defaultValue != null) {
                Preconditions.checkArgument(Pattern.matches(regex, defaultValue),
                                            "The defaultValue for the config-regex is invalid. Value {%s}, does not match {%s}",
                                            defaultValue, regex);
            }
            configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_REGEX, regex);
        }
    }

    private void processConfigParameter(
            @Nonnull final Method configMethod,
            @Nonnull final Configuration configuration) {
        final Annotation[][] annotations = configMethod.getParameterAnnotations();
        final Class<?>[] parameterTypes = configMethod.getParameterTypes();
        for (int index = 0; index < parameterTypes.length; index++) {
            final Class<?> paramType = parameterTypes[index];
            ConfigParam configParam = null;
            ConfigRegex configRegex = null;
            final Annotation[] paramAnnotations = annotations[index];
            for (final Annotation paramAnnotation : paramAnnotations) {
                if (paramAnnotation.annotationType() == ConfigRegex.class) {
                    configRegex = (ConfigRegex) paramAnnotation;
                } else if (paramAnnotation.annotationType() == ConfigParam.class) {
                    configParam = (ConfigParam) paramAnnotation;
                }
            }
            if (configRegex != null) {
                Preconditions.checkArgument(paramType.isAssignableFrom(String.class),
                                            "The @ConfigRegex can only be applied on string type parameter values.");
                final String paramKey = Preconditions.checkNotNull(configParam,
                                                                   "A @ConfigRegex can only be declared on a @ConfigParam parameter")
                        .name();
                final Optional<AdditionalParameter> parameter = configuration.getAdditionalParameter(paramKey);
                Preconditions.checkState(parameter.isPresent(), "There is no additionalparameter defined with key {%s}",
                                         paramKey);
                final AdditionalParameter additionalParameter = parameter.get();
                additionalParameter
                        .addConfigData(AdditionalParameterConfigDataType.PARAMETER_REGEX, configRegex.value());
            }
        }
    }

}
