package khameleon.core.processor.namespace;

import khameleon.core.Namespace;

/**
 *
 * @author marembo
 */
public interface ConfigNamespaceProcessor {

    void process(final Namespace namespace, final Class<?> configInterface);

}
