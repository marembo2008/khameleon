package khameleon.core.processor.namespace;

import java.util.Arrays;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Preconditions;

import khameleon.core.Namespace;
import khameleon.core.NamespaceDataType;
import khameleon.core.annotations.ConfigProject;
import khameleon.core.annotations.namespace.NamespaceProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:47:52 PM
 */
@NamespaceProcessor
public class ConfigProjectNamespaceProcessor implements ConfigNamespaceProcessor {

    @Nullable
    private String[] getConfigProjects(final Package classPackage) {
        if (classPackage.isAnnotationPresent(ConfigProject.class)) {
            final ConfigProject configProject = classPackage.getAnnotation(ConfigProject.class);
            final String[] projects = configProject.value();
            Preconditions.checkState(projects.length > 0, "the @ConfigProject must define at least one project name");
            return projects;
        }
        return null;
    }

    @Nullable
    private String[] getConfigProjects(
            @Nonnull Class<?> configInterface) {
        final ConfigProject configProject = configInterface.getAnnotation(ConfigProject.class);
        if (configProject != null) {
            final String[] projects = configProject.value();
            Preconditions.checkState(projects.length > 0, "the @ConfigProject must define at least one project name");
            return projects;
        }
        //Iterate through the packages.
        return getConfigProjects(configInterface.getPackage());
    }

    @Override
    public void process(
            @Nonnull final Namespace namespace,
            @Nonnull Class<?> configInterface) {
        final String[] projects = getConfigProjects(configInterface);
        if (projects != null) {
            namespace.addOrUpdateNamespaceData(NamespaceDataType.NAMESPACE_CONFIG_PROJECT, Arrays.toString(projects));
        }
    }

}
