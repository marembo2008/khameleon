package khameleon.core.processor.namespace;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Throwables;
import jakarta.xml.bind.JAXBException;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.core.NamespaceDataType;
import khameleon.core.annotations.namespace.NamespaceProcessor;
import khameleon.core.processor.configuration.NamespaceApplicationContextHelper;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:40:23 PM
 */
@NamespaceProcessor
public final class AppContextNamespaceProcessor implements ConfigNamespaceProcessor {

  @Override
  public void process(
          @Nonnull final Namespace namespace,
          @Nonnull final Class<?> configInterface) {
    Objects.requireNonNull(namespace, "The namespace must not be null");
    Objects.requireNonNull(configInterface, "The configInterface must not be null");
    try {
      final List<ApplicationContext> applicationContexts = NamespaceApplicationContextHelper.getApplicationContexts(
              configInterface);
      if (applicationContexts.isEmpty()) {
        return;
      }
      final String xml = NamespaceApplicationContextHelper.marshall(applicationContexts);
      namespace.addOrUpdateNamespaceData(NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT, xml);
    } catch (JAXBException ex) {
      throw Throwables.propagate(ex);
    }
  }

}
