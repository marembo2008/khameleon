package khameleon.core.processor.namespace;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import khameleon.core.Namespace;
import khameleon.core.NamespaceDataType;
import khameleon.core.annotations.ConfigRename;
import khameleon.core.annotations.namespace.NamespaceProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:50:53 PM
 */
@NamespaceProcessor
public final class ConfigRenameNamespaceProcessor implements ConfigNamespaceProcessor {

    @Override
    public void process(
            @Nonnull final Namespace namespace,
            @Nonnull final Class<?> configInterface) {
        Preconditions.checkNotNull(namespace, "the namespace must not be null");
        Preconditions.checkNotNull(configInterface, "the configInterface must not be null");
        if (configInterface.isAnnotationPresent(ConfigRename.class)) {
            final ConfigRename configRename = configInterface.getAnnotation(ConfigRename.class);
            namespace.addOrUpdateNamespaceData(NamespaceDataType.NAMESPACE_CONFIG_RENAME,
                                               Preconditions.checkNotNull(Strings.emptyToNull(configRename.value()),
                                                                          "the old namespace name must be specified"));
        }
    }

}
