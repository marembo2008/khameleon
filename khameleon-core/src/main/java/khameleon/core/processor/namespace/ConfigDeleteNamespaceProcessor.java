package khameleon.core.processor.namespace;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;

import khameleon.core.Namespace;
import khameleon.core.NamespaceDataType;
import khameleon.core.annotations.ConfigDelete;
import khameleon.core.annotations.namespace.NamespaceProcessor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 1, 2018, 8:43:15 PM
 */
@NamespaceProcessor
public final class ConfigDeleteNamespaceProcessor implements ConfigNamespaceProcessor {

    @Override
    public void process(
            @Nonnull final Namespace namespace,
            @Nonnull final Class<?> configInterface) {
        Preconditions.checkNotNull(namespace, "the namespace must not be null");
        Preconditions.checkNotNull(configInterface, "the configInterface must not be null");
        if (configInterface.isAnnotationPresent(ConfigDelete.class)) {
            namespace.addOrUpdateNamespaceData(NamespaceDataType.NAMESPACE_CONFIG_DELETE, true);
        }
    }

}
