/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package khameleon.core.exception;

/**
 *
 * @author mochieng
 */
public class ConfigurationInitializationException extends IllegalArgumentException {

    public ConfigurationInitializationException() {
    }

    public ConfigurationInitializationException(String s) {
        super(s);
    }

    public ConfigurationInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigurationInitializationException(Throwable cause) {
        super(cause);
    }

}
