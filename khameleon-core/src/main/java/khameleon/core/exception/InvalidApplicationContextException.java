package khameleon.core.exception;

/**
 *
 * @author marembo
 */
public class InvalidApplicationContextException extends RuntimeException {

    public InvalidApplicationContextException() {
    }

    public InvalidApplicationContextException(String message) {
        super(message);
    }

    public InvalidApplicationContextException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidApplicationContextException(Throwable cause) {
        super(cause);
    }

    public InvalidApplicationContextException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
