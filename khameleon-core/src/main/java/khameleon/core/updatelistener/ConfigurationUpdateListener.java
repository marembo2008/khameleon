package khameleon.core.updatelistener;

import javax.annotation.Nonnull;

/**
 * Used in situations where it is impossible you need to be notified immediately when a configuration has changed. The
 * listener must be a CDI bean.
 *
 * The listener must be annotated with
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 10:39:52 PM
 */
public interface ConfigurationUpdateListener {

    void onConfigurationUpdated(@Nonnull final ConfigurationUpdaterContext configurationUpdaterContext);
}
