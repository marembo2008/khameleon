package khameleon.core.updatelistener;

import java.io.Serializable;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 19, 2015, 10:43:58 PM
 */
@Value
@Builder
@ToString
public class ConfigurationUpdaterContext implements Serializable {

    private static final long serialVersionUID = 347837434389l;

    @Nonnull
    private String namespace;

    @Nonnull
    private String configName;

    @Nullable
    private Object updatedValue;
}
