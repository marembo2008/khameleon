package khameleon.core.configsource;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 12, 2017, 10:06:17 PM
 */
public enum Source {

    LOCAL_SERVICE,
    CONFIG_SERVICE

}
