package khameleon.core.configsource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Qualifier;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 18, 2018, 2:43:53 PM
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface ConfigSource {

  Source value();

  @SuppressWarnings("AnnotationAsSuperInterface") //CDI
  class ConfigSourceImpl extends AnnotationLiteral<ConfigSource> implements ConfigSource {

    private final Source source;

    public ConfigSourceImpl(Source source) {
      this.source = source;
    }

    @Override
    public Source value() {
      return source;
    }

  }

}
