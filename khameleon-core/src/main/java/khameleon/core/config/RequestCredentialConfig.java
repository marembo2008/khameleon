package khameleon.core.config;

import java.io.Serializable;

import javax.annotation.Nonnull;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

@Config
@AppContext
@ConfigRoot
public interface RequestCredentialConfig extends Serializable {

    @Nonnull
    @Default("")
    @Info("The variant allows to select which variant of token key.")
    String getTokenKey(@ConfigParam(name = "variant") @Nonnull final Variant variant);

    @Nonnull
    @Default("")
    @Info("The variant allows to select which variant of token key.")
    String getTokenSecret(@ConfigParam(name = "variant") @Nonnull final Variant variant);

    enum Variant {

        Registration,
        Request,
        Default;

    }

}
