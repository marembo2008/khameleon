package khameleon.core.config;

import static khameleon.core.annotations.ConfigCacheValue.CacheValutOption.IGNORE_WHEN_EMPTY;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigCacheValue;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Constant;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.localcache.InMemoryCache;

@Config
@AppContext
@ConfigRoot
public interface ApplicationContextEnabledConfig extends Serializable {

    /**
     * There is no way to update the following configuration automatically from the config-service. Hence, it has an
     * inbuilt special handling, and is only cached in memory.
     *
     * An ApplicationContext is enabled if it is defined in the config-service.
     *
     * @param appContextId the application context id to check if is enabled/defined.
     */
    @Constant
    @Default("false")
    @ConfigCacheValue(IGNORE_WHEN_EMPTY)
    @InMemoryCache(period = 30, unit = TimeUnit.DAYS)
    boolean isApplicationContextEnabled(@ConfigParam(name = "appContextId") final int appContextId);

}
