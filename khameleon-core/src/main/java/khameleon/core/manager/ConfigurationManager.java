package khameleon.core.manager;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.Config;
import khameleon.core.applicationcontext.ApplicationContextService;
import khameleon.core.internal.service.request.ConfigurationService;
import khameleon.core.internal.service.request.ConfigurationServiceHelper;
import khameleon.core.internal.service.request.ConfigurationServiceHelperImpl;
import khameleon.core.values.AdditionalParameterKeyValue;

/**
 *
 * @author marembo
 */
public final class ConfigurationManager {

	public static final String SYSTEM_PROPERTY_APPLICATION_MODE = "anosym.khameleon.applicationMode";

	public static final String SYSTEM_PROPERTY_TLD = "anosym.khameleon.tld";

	public static final String SYSTEM_PROPERTY_LANGUAGE = "anosym.khameleon.language";

	public static final String SYSTEM_PROPERTY_COUNTRY = "anosym.khameleon.country";

	public static final String CONFIG_SERVICE_OVERRIDE_ENDPOINT_ADDRESS = "anosym.khameleon.overrideEndPointAddress";

	private static final ApplicationContext INITIAL_CONTEXT;

	static {
		// Create default from environmental variables.
		INITIAL_CONTEXT = new ApplicationContext().withApplicationMode(getDefaultApplicationMode())
				.withCountryCode(getDefaultCountry()).withLanguageCode(getDefaultLanguage())
				.withTopLevelDomain(getDefaultTLD());
	}
	private static final ThreadLocal<ApplicationContext> APPLICATION_CONTEXTS = new ThreadLocal<ApplicationContext>() {

		@Override
		protected ApplicationContext initialValue() {
			return INITIAL_CONTEXT;
		}

	};

	private static ApplicationMode getDefaultApplicationMode() {
		String mode = System.getProperty(SYSTEM_PROPERTY_APPLICATION_MODE, "DEVELOPMENT").toUpperCase();
		return ApplicationMode.valueOf(mode);
	}

	private static TLD getDefaultTLD() {
		String tld = System.getProperty(SYSTEM_PROPERTY_TLD);
		if (!Strings.isNullOrEmpty(tld)) {
			return TLD.valueOf(tld.trim().toUpperCase());
		}
		return null;
	}

	private static Language getDefaultLanguage() {
		String lang = System.getProperty(SYSTEM_PROPERTY_LANGUAGE);
		if (!Strings.isNullOrEmpty(lang)) {
			return Language.valueOf(lang.trim().toUpperCase());
		}
		return null;
	}

	private static Country getDefaultCountry() {
		String country = System.getProperty(SYSTEM_PROPERTY_COUNTRY);
		if (!Strings.isNullOrEmpty(country)) {
			return Country.valueOf(country.trim().toUpperCase());
		}
		return null;
	}

	/**
	 * Returns the default application context used by the current application.
	 *
	 * It is guaranteed to be the same for all configurations initialized without
	 * specifying an application context, if {@link #getInstance(java.lang.Class) }.
	 *
	 * @return
	 */
	public static ApplicationContext getDefaultApplicationContext() {
		return INITIAL_CONTEXT;
	}

	/**
	 * Returns the current application context used by the current application.
	 *
	 * The current application context for current thread execution.
	 *
	 * This can be set by
	 * {@link #setCurrentApplicationContext(khameleon.core.ApplicationContext)}
	 *
	 * If not set, this will always equals the default context.
	 *
	 * @return
	 */
	public static ApplicationContext getCurrentApplicationContext() {
		return APPLICATION_CONTEXTS.get();
	}

	/**
	 * Every thread uses the same context.
	 *
	 * Changing the current application context will reflect the changes in all
	 * Threads ApplicationContext. In order to ensure that each thread uses its own
	 * ApplicationContext, call this method to override the INITIAL_CONTEXT
	 * {@link #getDefaultApplicationContext() } which is used by default.
	 *
	 * @param applicationContext the new application context to set for this thread.
	 */
	public static void setCurrentApplicationContext(final ApplicationContext applicationContext) {
		APPLICATION_CONTEXTS.set(applicationContext);
	}

	public static <T> T getInstance(@Nonnull final ApplicationContextService applicationContextService,
			@Nonnull final Class<T> configInterface) {
		checkNotNull(configInterface, "ConfigInterface must not be null");

		checkState(configInterface.isAnnotationPresent(Config.class),
				"The following interface: {0} does not define a configuration service", configInterface);
		checkArgument(configInterface.isInterface(), "Configuration services must be declared as interfaces: {0}",
				configInterface);

		return getConfigInstance(applicationContextService, configInterface, new ConfigurationServiceHelperImpl());
	}

	public static <T> T getConfigInstance(@Nonnull final ApplicationContextService applicationContextService,
			@Nonnull final Class<T> configInterface,
			@Nonnull final ConfigurationServiceHelper configurationServiceHelper) {
		checkNotNull(configInterface, "ConfigInterface must not be null");
		checkNotNull(configurationServiceHelper, "ConfigHelper must not be null");

		checkState(configInterface.isAnnotationPresent(Config.class),
				"The following interface: {0} does not define a configuration service", configInterface);
		checkArgument(configInterface.isInterface(), "Configuration services must be declared as interfaces: {0}",
				configInterface);

		try {
			return getInstance(applicationContextService, configInterface, getNamespace(configInterface),
					configurationServiceHelper, ImmutableList.<AdditionalParameterKeyValue>of());
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static <T> T getInstance(@Nonnull final ApplicationContextService applicationContextService,
			@Nonnull final Class<T> configInterface, @Nullable final String canonicalNamespace,
			@Nonnull ConfigurationServiceHelper configurationServiceHelper,
			@Nonnull List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
		checkNotNull(configInterface, "ConfigInterface must not be null");
		checkNotNull(configurationServiceHelper, "ConfigHelper must not be null");
		checkNotNull(additionalParameterKeyValues, "Additional parameters key values must not be null");

		checkState(configInterface.isAnnotationPresent(Config.class),
				"The following interface: {0} does not define a configuration service", configInterface);
		checkArgument(configInterface.isInterface(), "Configuration services must be declared as interfaces: {0}",
				configInterface);

		final ConfigurationService configServiceHandler = new ConfigurationService(applicationContextService,
				canonicalNamespace, configurationServiceHelper, additionalParameterKeyValues);
		final List<Class<?>> interfaces = new ArrayList<>();
		interfaces.add(configInterface);

		Class<?> superInterface = configInterface.getSuperclass();
		while (superInterface != null && !superInterface.isAssignableFrom(Object.class)) {
			interfaces.add(superInterface);
			superInterface = superInterface.getSuperclass();
		}
		return (T) Proxy.newProxyInstance(configInterface.getClassLoader(), interfaces.toArray(new Class[0]),
				configServiceHandler);
	}

	/**
	 * Returns the simple namespace uri for the following config interface.
	 */
	@Nonnull
	public static String getNamespace(@Nonnull final Class<?> configInterface) {
		checkNotNull(configInterface, "The configInterface must not be null");
		checkArgument(configInterface.isAnnotationPresent(Config.class),
				"The following interface: {0} does not define a configuration service", configInterface);

		final Config cf = configInterface.getAnnotation(Config.class);
		if (Strings.isNullOrEmpty(cf.namespace())) {
			return configInterface.getCanonicalName();
		}
		return cf.namespace();
	}

	private ConfigurationManager() {
	}

}
