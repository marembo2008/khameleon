package khameleon.access;

import com.google.common.base.MoreObjects;

import java.io.Serializable;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Entity
@NamedQueries({
  @NamedQuery(name = "ConfigProjectData.findConfigProjectByName", query = "SELECT cp FROM ConfigProjectData cp WHERE cp.name = :name")
})
public class ConfigProjectData implements Serializable {

  private static final long serialVersionUID = 134738434389L;

  @Id
  @GeneratedValue
  private Long uuid;

  @Nonnull
  @Column(length = 20, unique = true)
  private String name;

  @Nullable
  @Column(length = 512)
  private String description;

  @Nonnull
  public Long getUuid() {
    return uuid == null ? -2000 : uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(@Nonnull final String name) {
    this.name = checkNotNull(name, "the project name is required");
  }

  @Nullable
  public String getDescription() {
    return description;
  }

  public void setDescription(@Nonnull final String description) {
    this.description = checkNotNull(description, "The description must not be null");
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid, name);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ConfigProjectData other = (ConfigProjectData) obj;
    return Objects.equals(this.uuid, other.uuid)
            && Objects.equals(this.name, other.name);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
            .add("uuid", uuid)
            .add("name", name)
            .toString();
  }

}
