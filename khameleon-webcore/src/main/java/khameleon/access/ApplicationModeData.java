package khameleon.access;

import java.io.Serializable;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import khameleon.core.ApplicationMode;

/**
 *
 * @author marembo
 */
@Entity
public class ApplicationModeData implements Serializable {

  @Id
  @SuppressWarnings("FieldMayBeFinal")
  @Column(length = 40)
  private String uuid = UUID.randomUUID().toString();

  private ApplicationMode applicationMode;

  public ApplicationModeData(ApplicationMode applicationMode) {
    this.applicationMode = applicationMode;
  }

  public ApplicationModeData() {
  }

  public ApplicationMode getApplicationMode() {
    return applicationMode;
  }

  public void setApplicationMode(ApplicationMode applicationMode) {
    this.applicationMode = applicationMode;
  }

  @Override
  public String toString() {
    return applicationMode.name();
  }

}
