package khameleon.access;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import khameleon.core.ApplicationMode;

import static jakarta.persistence.CascadeType.ALL;
import static khameleon.core.ApplicationMode.DEVELOPMENT;

/**
 *
 * @author marembo
 */
@Entity
@NamedQueries({
  @NamedQuery(name = "UserAccount.findAccount",
              query = "SELECT a FROM UserAccount a WHERE a.loginId = :loginId AND a.password = :password")
})
public class UserAccount implements Serializable {

  private static final long serialVersionUID = 124248924024L;

  @Id
  @Column(length = 50)
  private String loginId;

  private String password;
  //The application modes the current account is allowed to access.

  @OneToMany(cascade = ALL, fetch = FetchType.EAGER)
  private List<ApplicationModeData> allowedAccess;

  public UserAccount(String loginId, String password) {
    this();
    this.loginId = loginId;
    this.password = password;
  }

  public UserAccount() {
    allowedAccess = Lists.newArrayList(new ApplicationModeData(DEVELOPMENT));
  }

  public boolean hasAccess(ApplicationMode applicationMode) {
    for (ApplicationModeData amd : allowedAccess) {
      if (amd.getApplicationMode() == applicationMode) {
        return true;
      }
    }
    return false;
  }

  public void addAccess(ApplicationMode applicationMode) {
    if (!hasAccess(applicationMode)) {
      allowedAccess.add(new ApplicationModeData(applicationMode));
    }
  }

  public List<ApplicationMode> getAccessibleApplicationModes() {
    return Lists.newArrayList(Lists.transform(allowedAccess, new Function<ApplicationModeData, ApplicationMode>() {

                                        @Override
                                        public ApplicationMode apply(ApplicationModeData input) {
                                          return input.getApplicationMode();
                                        }

                                      }));
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
