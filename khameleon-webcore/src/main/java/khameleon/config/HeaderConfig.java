package khameleon.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo
 */
@Config
@AppContext
@ConfigRoot
public interface HeaderConfig {

    @Nonnull
    @Default("Khameleon:: Config Service")
    @Info(name = "headerText", value = "The Header title for the config service.")
    String getHeaderText();

}
