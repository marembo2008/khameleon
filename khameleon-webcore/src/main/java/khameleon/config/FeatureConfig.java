package khameleon.config;

import javax.annotation.Nonnull;

import khameleon.core.Feature;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo
 */
@Config
@AppContext
@ConfigRoot
public interface FeatureConfig {

    @Nonnull
    @Info(name = "featureEnabled",
          value = "By default, we only need up to the application mode."
          + " The logic on the web thus will not set the country nor tld nor language."
          + "<br/"
          + "<strong>Deleting the application_mode may have consequences that may affect the operation of your applications</string>")
    @Default("[APPLICATION_MODE]")
    Feature[] getFeatureEnabled();

}
