package khameleon.config;

import java.io.Serializable;

import javax.annotation.Nonnull;

import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Constant;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

@Config
@AppContext
@ConfigRoot
public interface ApplicationContextConfig extends Serializable {

	@Constant
	Language[] getSupportedLanguages();

	@Constant
	Country[] getSupportedCountries();

	@Constant
	TLD[] getSupportedTLDs();

	@Nonnull
	@Default("[DEVELOPMENT, TEST, LIVE]")
	@Info(name = "enabledApplicationModes", value = "The application modes enabled for the current config-service application. Changes only take effect on subsequent logins.")
	ApplicationMode[] getEnabledApplicationModes();

	@Info("The initial application mode selected when the config-frontend is started.")
	@Default("DEVELOPMENT")
	ApplicationMode getInitialApplicationMode();

	@Nonnull
	@Default("/")
	@Info(name = "applicationModeUrl", value = "Returns the application mode url for the specified mode. The default is a forward slash, implying current url")
	String getApplicationModeUrl(@ConfigParam(name = "applicationMode") final ApplicationMode applicationMode);

	@Info("Requires that the config service is accessed through ssl only. Note that ssl must be installed")
	@Default("false")
	boolean isRequireSSL();

	@Nonnull
	@Default("[]")
	@Info("Paths for which ssl should not be activated. <b>This could use regex</b>. This should contain only the relative paths")
	String[] getNonSSLPaths();

}
