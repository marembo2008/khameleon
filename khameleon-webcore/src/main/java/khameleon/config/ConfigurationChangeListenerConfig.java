package khameleon.config;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigDelete;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 20, 2018, 9:39:57 PM
 */
@Config
@AppContext
@ConfigRoot
public interface ConfigurationChangeListenerConfig {

	@Nonnull
	@Default("[]")
	@ConfigDelete
	@Info("A list of project ids which the feign client can listen to")
	String[] getProjectIds(@ConfigParam(name = "feignClientId") @NonNull final String feignClientId);

	@Nullable
	String getProjectEndPoint(@ConfigParam(name = "projectName") @Nonnull final String projectName);

	boolean isHttpsEnabled(@ConfigParam(name = "projectName") @Nonnull final String projectName);
}
