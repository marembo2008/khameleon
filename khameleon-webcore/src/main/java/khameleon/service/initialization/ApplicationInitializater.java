package khameleon.service.initialization;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;

import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.event.Observes;
import khameleon.access.UserAccount;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.spi.api.registration.OnRegistrationStarted;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.UserAccountFacade;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo
 */
@Slf4j
@Singleton
public class ApplicationInitializater {

	@EJB
	private ApplicationContextFacade applicationContextFacade;

	@EJB
	private UserAccountFacade userAccountFacade;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void beforeRegistrationStarts(@Observes @OnRegistrationStarted Object event) {
		log.info("Initializing default application contexts and default accounts if necessary...");

		// Switch off cache
		System.setProperty("khameleon.value-cache.disabled", "true");
		initializeDefaultApplicationContexts();
		initializeDefaultAccount();
	}

	private void initializeDefaultApplicationContexts() {
		// create context for all modes.
		for (ApplicationMode mode : ApplicationMode.values()) {
			ApplicationContext context = new ApplicationContext();
			context.setApplicationMode(mode);
			// check if it exists.
			int contextId = context.getContextId();
			if (applicationContextFacade.find(contextId) == null) {
				applicationContextFacade.create(context);
			}
		}
		// also create the default context for use in generating all other
		// configurations.
		ApplicationContext context = DEFAULT_APPLICATION_CONTEXT;
		int contextId = context.getContextId();
		if (applicationContextFacade.find(contextId) == null) {
			applicationContextFacade.create(context);
		}
	}

	private void initializeDefaultAccount() {
		if (userAccountFacade.count() == 0) {
			final UserAccount ua = new UserAccount("admin", "adminadmin");
			// add all the appliation modes.
			for (final ApplicationMode am : ApplicationMode.values()) {
				ua.addAccess(am);
			}
			userAccountFacade.create(ua);
		}
	}

}
