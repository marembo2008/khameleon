package khameleon.service.configproject;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.event.Observes;
import khameleon.access.ConfigProjectData;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.EnumValue;
import khameleon.core.annotations.ConfigProject;
import khameleon.domain.facade.AdditionalParameterConfigDataFacade;
import khameleon.domain.facade.AdditionalParameterFacade;
import khameleon.domain.facade.ConfigProjectDataFacade;

import static java.util.stream.Collectors.toList;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_CONFIG_PROVIDED_OPTION;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_ENUM;
import static khameleon.core.annotations.ConfigProvidedOption.ConfigOptionType.PROJECT_ID;
import static com.google.common.collect.Lists.transform;
import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 26, 2015, 7:21:33 PM
 */
@Startup
@Singleton
public class ConfigProjectUpdaterProcessor {

  @EJB
  private AdditionalParameterFacade additionalParameterFacade;

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @EJB
  private AdditionalParameterConfigDataFacade additionalParameterConfigDataFacade;

  @Asynchronous
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void onConfigProjectAdded(
          @Observes(during = AFTER_SUCCESS) @OnConfigProjectAdded @Nonnull final String newProjectName) {
    updateConfigurationValueConfigProject();
    updateAdditionalParameterConfigProject();
  }

  /**
   * Updates Additional Parameters, which have been annotated with @ConfigProvidedOption
   */
  private void updateAdditionalParameterConfigProject() {
    final List<EnumValue> configProjectDatas = configProjectDataFacade
            .findAll()
            .stream()
            .map((configData) -> new EnumValue(configData.getName(), configData.getName()))
            .collect(toList());
    additionalParameterConfigDataFacade
            .findAdditionalParameterConfigDataByDataType(PARAMETER_CONFIG_PROVIDED_OPTION)
            .stream()
            .filter((configData) -> Objects.equals(configData.getConfigData(), PROJECT_ID.name()))
            .forEach((configData) -> {
              configData.setEnumList(configProjectDatas);
              additionalParameterConfigDataFacade.edit(configData);
            });
  }

  /**
   * Updates configuration, which defines @ConfigProject on configuration return values.
   */
  private void updateConfigurationValueConfigProject() {
    final int limit = 1000;
    int offset = 0;
    while (true) {
      final List<AdditionalParameter> additionalParameters
              = additionalParameterFacade.findAdditionalParameterByKey(ConfigProject.PROJECT_KEY, offset, limit);
      if (additionalParameters.isEmpty()) {
        break;
      }

      additionalParameters.forEach((additionalParameter) -> updateAdditionalParameter(additionalParameter));

      offset += limit;
    }
  }

  private void updateAdditionalParameter(@Nonnull final AdditionalParameter additionalParameter) {
    //Add or replace the fake enum values for the projects
    final List<ConfigProjectData> configProjects = configProjectDataFacade.findAll();
    final Optional<AdditionalParameterConfigData> enumConfigDataOptional = additionalParameter.getConfigData(
            PARAMETER_ENUM);
    final boolean updateConfigData = enumConfigDataOptional.isPresent();
    final AdditionalParameterConfigData enumConfigData;
    if (updateConfigData) {
      enumConfigData = enumConfigDataOptional.get();
    } else {
      enumConfigData = new AdditionalParameterConfigData(PARAMETER_ENUM, Boolean.TRUE.toString());
    }

    enumConfigData
            .setEnumList(transform(configProjects, (input) -> new EnumValue(input.getName(), input.getName())));
    additionalParameter.addOrUpdateConfigData(enumConfigData);

    additionalParameterFacade.edit(additionalParameter);
  }

}
