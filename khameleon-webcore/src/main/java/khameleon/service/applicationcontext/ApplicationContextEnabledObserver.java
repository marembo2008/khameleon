package khameleon.service.applicationcontext;

import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.event.TransactionPhase;
import khameleon.core.AdditionalParameter;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.spi.api.registration.OnRegistrationCompleted;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;
import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;

/**
 *
 * @author marembo
 */
@Singleton
public class ApplicationContextEnabledObserver {

  private static final String APPLICATION_CONTEXT_ENABLED_CONFIG_NAMESAPCE
          = ApplicationContextEnabledConfig.class.getCanonicalName();

  private static final String IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME = "applicationContextEnabled";

  private static final String IS_APPLICATION_CONTEXT_ENABLED_CONFIG_PARAM_NAME = "appContextId";

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ConfigurationFacade configurationFacade;

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @TransactionAttribute(REQUIRES_NEW)
  public void addApplicationContextEnabledIfNotYetAdded(
          @Nonnull
          @OnRegistrationCompleted
          @Observes(during = TransactionPhase.AFTER_SUCCESS)
          final Object anyPayload) {
    //Some sanity check.

    final List<ApplicationContext> applicationContexts = applicationContextFacade.findAll();
    applicationContexts
            .stream()
            .forEach((applicationContext) -> {
              final AdditionalParameterKeyValue appContextIdParameterKeyValue
                      = new AdditionalParameterKeyValue(IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME,
                                                        applicationContext.getContextId());
              final List<ConfigurationValue> applicationContextEnabledConfigurationValue
                      = configurationValueFacade.findConfigurationValues(DEFAULT_APPLICATION_CONTEXT
                              .getContextId(),
                                                                         APPLICATION_CONTEXT_ENABLED_CONFIG_NAMESAPCE,
                                                                         IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME,
                                                                         ImmutableList.of(
                                                                                 appContextIdParameterKeyValue));
              if (!(!applicationContextEnabledConfigurationValue.isEmpty())) {
                addIsApplicationContextEnabledConfiguration(applicationContext);
              }
            });
  }

  @TransactionAttribute(REQUIRES_NEW)
  public void addIsApplicationContextEnabledConfiguration(
          @Nonnull
          @OnApplicationContextCreated
          @Observes(during = AFTER_SUCCESS)
          final ApplicationContext newApplicationContext) {
    checkNotNull(newApplicationContext, "The newApplicationContext must not be null");

    ADDApplicationContextEnabledConfig(newApplicationContext);
  }

  private void ADDApplicationContextEnabledConfig(@Nonnull final ApplicationContext applicationContext) {
    final Configuration applicationContextEnabledConfig
            = configurationFacade.findConfiguration(APPLICATION_CONTEXT_ENABLED_CONFIG_NAMESAPCE,
                                                    IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME);
    checkNotNull(applicationContextEnabledConfig, "The isApplicationEnabledConfiguration must not be null");

    final ApplicationContext registeredDefaultApplicationContext
            = applicationContextFacade.find(DEFAULT_APPLICATION_CONTEXT.getContextId());
    checkNotNull(registeredDefaultApplicationContext, "The DEFAULT_APPLICATION_CONTEXT must have been registered");

    final ConfigurationValue configurationValue
            = new ConfigurationValue(registeredDefaultApplicationContext, applicationContextEnabledConfig);
    configurationValue.setBooleanValue(true);

    final Optional<AdditionalParameter> appContextAdditionalParameter
            = applicationContextEnabledConfig.getAdditionalParameter(
                    IS_APPLICATION_CONTEXT_ENABLED_CONFIG_PARAM_NAME);
    checkState(appContextAdditionalParameter.isPresent(),
               "isApplicationContextEnabled must have \"%s\" as configParam",
               IS_APPLICATION_CONTEXT_ENABLED_CONFIG_PARAM_NAME);

    configurationValue.addOrUpdateAdditionalParameterValue(appContextAdditionalParameter.get(),
                                                           String.valueOf(applicationContext.getContextId()));
    configurationValueFacade.create(configurationValue);
  }

}
