package khameleon.service.applicationcontext;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.service.ConfigurationValueUpdateService;
import khameleon.service.manager.ConfigurationUpdate;
import lombok.extern.slf4j.Slf4j;

import static java.util.Objects.requireNonNull;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DEFAULT_VALUE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;
import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;

/**
 *
 * @author marembo
 */
@Slf4j
@Stateless
public class ApplicationContextManager {

  private static final Logger LOG = Logger.getLogger(ApplicationContextManager.class.getName());

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ConfigurationFacade configurationFacade;

  @EJB
  private ConfigurationValueUpdateService configurationValueUpdateService;

  @Inject
  @OnApplicationContextCreated
  private Event<ApplicationContext> onApplicationContextCreatedEvent;

  @TransactionAttribute(REQUIRES_NEW)
  public ConfigurationValue addConfigurationValues(@Nonnull final Configuration configuration,
                                                   @Nonnull final ApplicationContext applicationContext) {
    return addConfigurationValues(configuration, applicationContext, configuration.getDefaultValue());
  }

  @TransactionAttribute(REQUIRES_NEW)
  public ConfigurationValue addConfigurationValues(@Nonnull final Configuration configuration,
                                                   @Nonnull final ApplicationContext applicationContext,
                                                   @Nullable final String value) {
    return doAddConfigurationValues(configuration, applicationContext, value);
  }

  @Nonnull
  public ConfigurationValue createConfigurationValue(@Nonnull final Configuration configuration,
                                                     @Nonnull final ApplicationContext applicationContext,
                                                     @Nullable final String value) {
    requireNonNull(configuration, "The configuration must not be null");
    requireNonNull(applicationContext, "The applicationContext must not be null");

    //the configuration should exists by.
    //To avoid transient states!
    final Configuration managedConfiguration = configurationFacade.find(configuration.getConfigId());
    checkState(managedConfiguration != null,
               "ConfigurationValue can only be added after a configuration is registered");

    final ApplicationContext managedContext = applicationContextFacade.find(applicationContext.getContextId());
    final ConfigurationValue configurationValue = new ConfigurationValue(managedContext, managedConfiguration, value);
    managedConfiguration.getAdditionalParameters()
            .stream()
            .forEach((ap) -> {
              configurationValue.addOrUpdateAdditionalParameterValue(ap, ap.getDefaultValue());
            });

    return configurationValue;
  }

  private ConfigurationValue doAddConfigurationValues(@Nonnull final Configuration configuration,
                                                      @Nonnull final ApplicationContext applicationContext,
                                                      @Nullable final String value) {
    final ConfigurationValue configurationValue = createConfigurationValue(configuration, applicationContext, value);
    configurationValueUpdateService.create(configurationValue);
    return configurationValue;
  }

  private void registerDefaultConfigurationValues(@Nonnull final Configuration configuration,
                                                  @Nonnull final ApplicationContext applicationContext) {
    if (!configuration.getConfigurationData(CONFIGURATION_DEFAULT_VALUE).isPresent()) {
      log.debug("No default configuration value. Ignore: {}", configuration);
      return;
    }

    final Namespace namespace = configuration.getNamespace();
    final String configName = configuration.getConfigName();
    //default configuration can only be one!
    final List<ConfigurationValue> configurationValues = configurationValueFacade
            .findConfigurationValues(applicationContext, namespace, configName);
    final String defaultValue = configuration.getDefaultValue();
    if (configurationValues.isEmpty()) {
      doAddConfigurationValues(configuration, applicationContext, defaultValue);
    } else {
      configurationValues
              .stream()
              .filter((cValue) -> !cValue.isFrontendUpdated())
              .filter((cValue) -> cValue.getContext().equals(DEFAULT_APPLICATION_CONTEXT))
              .findFirst()
              .ifPresent((dCValue) -> {
                dCValue.setConfigValue(defaultValue);
                configuration.getAdditionalParameters()
                        .stream()
                        .forEach((ap) -> {
                          dCValue.addOrUpdateAdditionalParameterValue(ap, ap.getDefaultValue());
                        });
                configurationValueUpdateService.update(dCValue);
              });
    }
  }

  @Asynchronous
  @TransactionAttribute(REQUIRES_NEW)
  public void registerDefaultConfigurationValues(
          @Observes(during = AFTER_SUCCESS) @Nonnull final ConfigurationUpdate configurationUpdate) {
    final Configuration configuration = configurationUpdate.getConfiguration();
    final ApplicationContext applicationContext = configurationUpdate.getContext();
    registerDefaultConfigurationValues(configuration, applicationContext);
  }

  /**
   * Creates a new application context and registers the specified map of configurations as the only configurations.
   *
   * @param context
   * @param namespaceConfigurations
   */
  @Asynchronous
  @TransactionAttribute(REQUIRES_NEW)
  public void createNewApplicationContext(@Nonnull final ApplicationContext context,
                                          @Nonnull final List<Configuration> namespaceConfigurations) {
    checkNotNull(context, "The context must be specified");
    checkNotNull(namespaceConfigurations, "namespaceconfigurations must not be null");

    addNewApplicationContext(context, namespaceConfigurations);
  }

  @TransactionAttribute(REQUIRES_NEW)
  public void addNewApplicationContext(@Nonnull final ApplicationContext newApplicationContext,
                                       @Nonnull final List<Configuration> namespaceConfigurations) {
    checkNotNull(newApplicationContext, "The newApplicationContext must be specified");
    checkNotNull(namespaceConfigurations, "namespaceconfigurations must not be null");

    //We create the application context only if it does not exists yet.
    if (applicationContextFacade.find(newApplicationContext.getContextId()) != null) {
      LOG.warning("Attempting to create an exisiting application context!");
      return;
    }
    applicationContextFacade.create(newApplicationContext);

    namespaceConfigurations
            .stream()
            .forEach((configuration) -> {
              registerDefaultConfigurationValues(configuration, newApplicationContext);
            });

    onApplicationContextCreatedEvent.fire(newApplicationContext);
  }

}
