package khameleon.service.applicationcontext;

import java.util.List;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.event.Observes;
import khameleon.core.ApplicationContext;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;
import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.internal.ConfigurationUtil.appendValueToArrayStringIfNotMember;

/**
 *
 * @author marembo
 */
@Stateless
public class ApplicationContextSystemConstantUpdate {

  private static final String CONSTANTS_CANONICAL_NAMESPACE
          = ApplicationContextEnabledConfig.class.getCanonicalName();

  private static final String CONFIG_NAME_SUPPORTED_LANGUAGES = "supportedLanguages";

  private static final String CONFIG_NAME_SUPPORTED_COUNTRIES = "supportedCountries";

  private static final String CONFIG_NAME_SUPPORTED_TLDS = "supportedTLDs";

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @TransactionAttribute(REQUIRES_NEW)
  public void updateSystemConstants(
          @Observes(during = AFTER_SUCCESS)
          @OnApplicationContextCreated
          @Nonnull
          final ApplicationContext newApplicationContext) {
    checkNotNull(newApplicationContext, "The newApplicationContext must not be null");

    doUpdateSystemConstants(newApplicationContext);
  }

  private void doUpdateSystemConstants(@Nonnull final ApplicationContext newApplicationContext) {
    final int defaultContextId = DEFAULT_APPLICATION_CONTEXT.getContextId();
    if (newApplicationContext.getLanguageCode() != null) {
      final List<ConfigurationValue> supportedLanguages = configurationValueFacade
              .findConfigurationValues(defaultContextId, CONSTANTS_CANONICAL_NAMESPACE,
                                       CONFIG_NAME_SUPPORTED_LANGUAGES);
      final ConfigurationValue supportedLanguagesValues = supportedLanguages.get(0);
      updateArrayConfigurationValue(supportedLanguagesValues, newApplicationContext.getLanguageCode().name());
    }
    if (newApplicationContext.getCountryCode() != null) {
      final List<ConfigurationValue> supportedCountries = configurationValueFacade
              .findConfigurationValues(defaultContextId, CONSTANTS_CANONICAL_NAMESPACE,
                                       CONFIG_NAME_SUPPORTED_COUNTRIES);
      final ConfigurationValue supportedCountriesValues = supportedCountries.get(0);
      updateArrayConfigurationValue(supportedCountriesValues, newApplicationContext.getCountryCode().name());
    }
    if (newApplicationContext.getTopLevelDomain() != null) {
      final List<ConfigurationValue> supportedTlds = configurationValueFacade
              .findConfigurationValues(defaultContextId, CONSTANTS_CANONICAL_NAMESPACE, CONFIG_NAME_SUPPORTED_TLDS);
      final ConfigurationValue supportedTldsValues = supportedTlds.get(0);
      updateArrayConfigurationValue(supportedTldsValues, newApplicationContext.getTopLevelDomain().name());
    }
  }

  private void updateArrayConfigurationValue(@Nonnull final ConfigurationValue configurationValue,
                                             @Nonnull final String valueToAdd) {
    final String value = configurationValue.getConfigValue();
    final String newValue = appendValueToArrayStringIfNotMember(value, valueToAdd);
    configurationValue.setConfigValue(newValue);
    configurationValueFacade.edit(configurationValue);
  }

}
