package khameleon.service.request.filtering;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 11, 2016, 5:34:22 AM
 */
public interface ConfigurationValueFilter {

	@Nullable
	ConfigurationValue filter(@Nonnull final ConfigurationValue configurationValue);

	default int getOrder() {
		return 0;
	}
}
