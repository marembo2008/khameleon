package khameleon.service.request.filtering;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.Nonnull;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import anosym.common.converter.CalendarConverter;
import anosym.common.converter.Converter;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.CalendarBounded;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 11, 2016, 5:37:35 AM
 */
@ApplicationScoped
public class CalendarBoundedConfigurationValueFilter implements ConfigurationValueFilter {

  @VisibleForTesting
  static final String[] CALENDAR_FORMAT = {"yyyy-MM-dd HH:mm:ss"};

  private final Converter<Calendar, String> calendarConverter;

  public CalendarBoundedConfigurationValueFilter() {
    this(new CalendarConverter());
  }

  @VisibleForTesting
  protected CalendarBoundedConfigurationValueFilter(@Nonnull final Converter<Calendar, String> calendarConverter) {
    this.calendarConverter = checkNotNull(calendarConverter, "The calendarConverter must not be null");
  }

  @Override
  public ConfigurationValue filter(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    final Configuration configuration = configurationValue.getConfiguration();
    final Optional<ConfigurationData> calendarBounded = configuration.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_CALENDAR_BOUNDED);
    if (!calendarBounded.isPresent() || !calendarBounded.get().getBooleanValue()) {
      return configurationValue;
    }

    final Optional<AdditionalParameterValue> from = configurationValue.getAdditionalParameterValue(
            CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);

    final Optional<AdditionalParameterValue> to = configurationValue.getAdditionalParameterValue(
            CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY);

    final boolean isAfter = !from.isPresent() || isAfter(from.get());
    final boolean isBefore = !to.isPresent() || isBefore(to.get());

    if (isAfter && isBefore) {
      return configurationValue;
    }

    return null;
  }

  @VisibleForTesting
  Calendar getNow() {
    return Calendar.getInstance(TimeZone.getTimeZone("GMT"));
  }

  private boolean isAfter(@Nonnull final AdditionalParameterValue parameterValue) {
    final String value = parameterValue.getParameterValue();
    if (isNullOrEmpty(value)) {
      return true;
    }

    final Calendar now = getNow();
    final Calendar from = calendarConverter.from(value, CALENDAR_FORMAT);
    return now.after(from);
  }

  private boolean isBefore(@Nonnull final AdditionalParameterValue parameterValue) {
    final String value = parameterValue.getParameterValue();
    if (isNullOrEmpty(value)) {
      return true;
    }

    final Calendar now = getNow();
    final Calendar from = calendarConverter.from(value, CALENDAR_FORMAT);
    return now.before(from);
  }

}
