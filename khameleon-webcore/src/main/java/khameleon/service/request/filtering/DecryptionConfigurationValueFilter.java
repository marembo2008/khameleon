package khameleon.service.request.filtering;

import com.google.common.base.Throwables;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import khameleon.core.values.ConfigurationValue;
import khameleon.core.values.EncryptListener;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 11, 2016, 5:35:23 AM
 */
@ApplicationScoped
public class DecryptionConfigurationValueFilter implements ConfigurationValueFilter {

  @Inject
  private EncryptListener encryptListener;

  @Override
  public ConfigurationValue filter(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    if (!encryptListener.isDecryptable(configurationValue)) {
      return configurationValue;
    }

    try {
      final ConfigurationValue clonedConfigurationValue = configurationValue.clone();
      encryptListener.decrypt(clonedConfigurationValue);
      return clonedConfigurationValue;
    } catch (final Exception ex) {
      throw Throwables.propagate(ex);
    }
  }

}
