package khameleon.service.request;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Specializes;
import jakarta.inject.Inject;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.core.configsource.ConfigSource;
import khameleon.core.configsource.Source;
import khameleon.core.internal.service.ConfigurationServiceException;
import khameleon.core.spi.api.request.ConfigRequestApiDelegate;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.core.values.EncryptListener;
import khameleon.domain.facade.NamespaceFacade;
import lombok.extern.slf4j.Slf4j;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

/**
 *
 * @author marembo
 */
@Slf4j
@Specializes
@ApplicationScoped
@ConfigSource(Source.CONFIG_SERVICE)
public class InternalConfigRequestApiDelegate extends ConfigRequestApiDelegate {

  @EJB
  private NamespaceFacade namespaceFacade;

  @EJB
  private ConfigurationValueRetrievalService configurationValueRetrievalService;

  @Inject
  private EncryptListener encryptListener;

  @Nullable
  @Override
  public ConfigurationValue findConfigurationValue(@Nonnull final ApplicationContext applicationContext,
                                                   @Nonnull final String namespace,
                                                   @Nonnull final String configName,
                                                   @Nonnull final List<AdditionalParameterKeyValue> keyValue) {
    final List<AdditionalParameterKeyValue> parameterKeyValues = emptyIfNull(keyValue);
    final ConfigurationValue configurationValue
            = doFindConfigurationValue(applicationContext, namespace, configName, parameterKeyValues);
    if (configurationValue == null) {
      return null;
    }

    return decryptIfNecessary(configurationValue);

  }

  @Nullable
  private ConfigurationValue doFindConfigurationValue(@Nonnull final ApplicationContext applicationContext,
                                                      @Nonnull final String namespaceCanonicalName,
                                                      @Nonnull final String configName,
                                                      @Nonnull final List<AdditionalParameterKeyValue> keyValues)
          throws ConfigurationServiceException {
    final Namespace namespace = namespaceFacade.findByCanonicalName(namespaceCanonicalName);
    if (namespace == null) {
      log.info("The following namespace has not been registered with the config-service: {}",
               namespaceCanonicalName);
      return null;
    }

    final List<ConfigurationValue> configurationValues = configurationValueRetrievalService
            .findConfigurationValuesWithDefaultFallback(applicationContext, namespace, configName, keyValues);
    return !configurationValues.isEmpty() ? configurationValues.get(0) : null;
  }

  /**
   * Ideally, this should not happen here, should happen at the client side, but thats for another day.
   */
  @Nonnull
  private ConfigurationValue decryptIfNecessary(@Nonnull final ConfigurationValue configurationValue) {
    if (!encryptListener.isDecryptable(configurationValue)) {
      return configurationValue;
    }

    try {
      final ConfigurationValue clonedConfigurationValue = configurationValue.clone();
      encryptListener.decrypt(clonedConfigurationValue);
      return clonedConfigurationValue;
    } catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

}
