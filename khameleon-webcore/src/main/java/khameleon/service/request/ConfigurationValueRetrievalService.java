package khameleon.service.request;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.LocalBean;
import jakarta.ejb.Stateless;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 12, 2015, 9:23:16 PM
 */
@Stateless
@LocalBean
public class ConfigurationValueRetrievalService {

  private static final Logger LOG = Logger.getLogger(ConfigurationValueRetrievalService.class.getName());

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  /**
   * May potentially return several values if the configuration has additional parameters.
   */
  @Nonnull
  public List<ConfigurationValue> findConfigurationValuesWithDefaultFallback(
          @Nonnull final ApplicationContext applicationContext,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName) {
    checkNotNull(applicationContext, "The applicationContext must not be null");
    checkNotNull(namespace, "The namespace must not be null");
    checkNotNull(configName, "The configName must not be null");

    final int applicationContextId = applicationContext.getContextId();
    final List<ConfigurationValue> configurationValues = configurationValueFacade
            .findConfigurationValues(applicationContextId, namespace.getCanonicalName(), configName);
    if (!configurationValues.isEmpty()) {
      return configurationValues;
    }

    //check again, for parent context.
    final ApplicationContext parentContext = applicationContext.getParentApplicationContext();
    if (parentContext.getContextId() != applicationContextId) {
      return findConfigurationValuesWithDefaultFallback(parentContext, namespace, configName);
    }

    return configurationValues;
  }

  @Nonnull
  public List<ConfigurationValue> findConfigurationValuesWithoutFallback(
          @Nonnull final ApplicationContext context,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    LOG.log(Level.INFO,
            "Finding Configuration Values for: \n{0}\n{1}\n{2}\n{3}",
            new Object[]{context, namespace, configName, additionalParameterKeyValues});

    final int contextId = context.getContextId();
    final String canonicalNamespace = namespace.getCanonicalName();

    final List<AdditionalParameterKeyValue> dynamicKeyValues
            = additionalParameterKeyValues
                    .stream()
                    .filter((keyValue) -> keyValue.isDynamic())
                    .collect(Collectors.toList());
    if (dynamicKeyValues.isEmpty()) {
      return configurationValueFacade
              .findConfigurationValues(contextId, canonicalNamespace, configName, additionalParameterKeyValues);
    }

    final List<AdditionalParameterKeyValue> nonDynamicKeyValues
            = additionalParameterKeyValues
                    .stream()
                    .filter((keyValue) -> !keyValue.isDynamic())
                    .collect(Collectors.toList());

    final List<List<AdditionalParameterKeyValue>> combinedKeyValues
            = getCombinationsStream(dynamicKeyValues)
                    .map((list) -> ImmutableList.copyOf(concat(list, nonDynamicKeyValues)))
                    .sorted((list1, list2) -> Integer.valueOf(list2.size()).compareTo(list1.size()))
                    .collect(Collectors.toList());

    final List<ConfigurationValue> configurationValues
            = combinedKeyValues
                    .stream()
                    .map((keyValues) -> {
                      return configurationValueFacade
                              .findConfigurationValues(contextId, canonicalNamespace, configName, keyValues);
                    })
                    .filter((values) -> !values.isEmpty())
                    .findFirst()
                    .orElse(Collections.emptyList());

    if (!configurationValues.isEmpty()) {
      return configurationValues;
    }

    return configurationValueFacade
            .findConfigurationValues(contextId, canonicalNamespace, configName, nonDynamicKeyValues);
  }

  @Nonnull
  public List<ConfigurationValue> findConfigurationValuesWithDefaultFallback(
          @Nonnull final ApplicationContext context,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    requireNonNull(context, "The context must not be null");
    requireNonNull(namespace, "The namespace must not be null");
    requireNonNull(configName, "The configName must not be null");
    requireNonNull(additionalParameterKeyValues, "The additionalParameterKeyValues must not be null");

    LOG.log(Level.INFO,
            "Finding Configuration Values for: \n{0}\n{1}\n{2}\n{3}",
            new Object[]{context, namespace, configName, additionalParameterKeyValues});

    final List<AdditionalParameterKeyValue> dynamicKeyValues
            = additionalParameterKeyValues
                    .stream()
                    .filter((keyValue) -> keyValue.isDynamic())
                    .collect(Collectors.toList());
    if (dynamicKeyValues.isEmpty()) {
      return findConfigurationValuesWithDefaultFallbackWithNoDynamicAdditionalParameters(
              context, namespace, configName, additionalParameterKeyValues);
    }

    final List<AdditionalParameterKeyValue> nonDynamicKeyValues
            = additionalParameterKeyValues
                    .stream()
                    .filter((keyValue) -> !keyValue.isDynamic())
                    .collect(Collectors.toList());

    final List<List<AdditionalParameterKeyValue>> combinedKeyValues
            = getCombinedKeyValues(dynamicKeyValues, nonDynamicKeyValues);

    final List<ConfigurationValue> configurationValues
            = findConfigurationValuesWithDefaultFallbackWithDynamicAdditionalParameters(
                    context, namespace, configName, combinedKeyValues);
    if (!configurationValues.isEmpty()) {
      return configurationValues;
    }

    return findConfigurationValuesWithDefaultFallbackWithNoDynamicAdditionalParameters(
            context, namespace, configName, nonDynamicKeyValues);
  }

  @Nonnull
  private List<ConfigurationValue> findConfigurationValuesWithDefaultFallbackWithNoDynamicAdditionalParameters(
          @Nonnull final ApplicationContext context,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName,
          @Nonnull final List<AdditionalParameterKeyValue> parameterKeyValues) {
    final int contextId = context.getContextId();
    final String canonicalNamespace = namespace.getCanonicalName();

    //Dont be smart. Even if the parameterKeyValues is empty, it must match as configuration with no additional-parameters.
    final List<ConfigurationValue> configurationValues = configurationValueFacade
            .findConfigurationValues(contextId, canonicalNamespace, configName, parameterKeyValues);

    if (!configurationValues.isEmpty()) {
      return configurationValues;
    }

    final ApplicationContext parentContext = context.getParentApplicationContext();
    if (parentContext.getContextId() != context.getContextId()) {
      return findConfigurationValuesWithDefaultFallbackWithNoDynamicAdditionalParameters(
              parentContext, namespace, configName, parameterKeyValues);
    }

    return configurationValues;
  }

  @Nonnull
  private List<ConfigurationValue> findConfigurationValuesWithDefaultFallbackWithDynamicAdditionalParameters(
          @Nonnull final ApplicationContext context,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName,
          @Nonnull final List<List<AdditionalParameterKeyValue>> additionalParameterKeyValues) {
    final int contextId = context.getContextId();
    final String canonicalNamespace = namespace.getCanonicalName();
    final List<ConfigurationValue> configurationValues = additionalParameterKeyValues
            .stream()
            .map((parameterKeyValues) -> {
              return configurationValueFacade
                      .findConfigurationValues(contextId, canonicalNamespace, configName, parameterKeyValues);
            })
            .filter((values) -> !values.isEmpty())
            .findFirst()
            .orElse(Collections.emptyList());

    if (!configurationValues.isEmpty()) {
      return configurationValues;
    }

    final ApplicationContext parentContext = context.getParentApplicationContext();
    if (parentContext.getContextId() != context.getContextId()) {
      return findConfigurationValuesWithDefaultFallbackWithDynamicAdditionalParameters(
              parentContext, namespace, configName, additionalParameterKeyValues);
    }

    return configurationValues;
  }

  @Nonnull
  @VisibleForTesting
  List<List<AdditionalParameterKeyValue>> getCombinedKeyValues(
          @Nonnull final List<AdditionalParameterKeyValue> dynamicKeyValues,
          @Nonnull final List<AdditionalParameterKeyValue> nonDynamicKeyValues) {
    return getCombinationsStream(dynamicKeyValues)
            .map((list) -> ImmutableList.copyOf(concat(list, nonDynamicKeyValues)))
            .sorted((list1, list2) -> Integer.valueOf(list2.size()).compareTo(list1.size()))
            .collect(Collectors.toList());
  }

  @Nonnull
  @VisibleForTesting
  <T> Stream<List<T>> getCombinationsStream(@Nonnull final List<T> paramaters) {
    // there are 2 ^ list.size() possible combinations
    // stream through them and map the number of the combination to the combination
    return LongStream.range(1, 1 << paramaters.size())
            .mapToObj(l -> bitMapToList(l, paramaters));
  }

  @Nonnull
  private <T> List<T> bitMapToList(final long bitmap, @Nonnull final List<T> parameters) {
    // use the number of the combination (bitmap) as a bitmap to filter the input list
    return IntStream.range(0, parameters.size())
            .filter(i -> 0 != ((1 << i) & bitmap))
            .mapToObj(parameters::get)
            .collect(Collectors.toList());
  }

}
