package khameleon.service.manager;

import java.io.Serializable;
import javax.annotation.Nonnull;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
public final class ConfigurationUpdate implements Serializable {

    private final Configuration configuration;

    private final ApplicationContext context;

    public ConfigurationUpdate(@Nonnull final Configuration configuration, @Nonnull final ApplicationContext context) {
        this.configuration = checkNotNull(configuration, "Configuration must not be null");
        this.context = checkNotNull(context, "Context must not be null");
    }

    @Nonnull
    public Configuration getConfiguration() {
        return configuration;
    }

    @Nonnull
    public ApplicationContext getContext() {
        return context;
    }

}
