package khameleon.service.manager;

import com.google.common.collect.ImmutableList;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;

import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import khameleon.core.ApplicationContext;
import khameleon.core.config.ApplicationContextEnabledConfig;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.service.applicationcontext.ApplicationContextEnabledObserver;

/**
 * Things that can be added and removed at will after an upgrade.
 *
 * @author marembo
 */
@Startup
@Singleton
public class ConfigurationUpdaterService {

  private static final String RUN_UPGRADER = "com.khameleon.runUpgrader";

  private static final String APPLICATION_CONTEXT_CONFIGURATION_SERVICE_NAMESAPCE
          = ApplicationContextEnabledConfig.class.getCanonicalName();

  private static final String IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME = "applicationContextEnabled";

  private static final String IS_APPLICATION_CONTEXT_ENABLED_CONFIG_PARAM_NAME = "appContextId";

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @EJB
  private ApplicationContextEnabledObserver isApplicationContextEnabledConfiguration;

  @PostConstruct
  void upgrade() {
    if (Boolean.valueOf(System.getProperty(RUN_UPGRADER, "false"))) {
      upgradeApplicationContext();
      upgradeApplicationContextisEnabled();
    }
  }

  private void upgradeApplicationContext() {
    applicationContextFacade
            .findAll()
            .stream()
            .parallel()
            .map((context) -> {
              context.updateParentContextId();
              return context;
            })
            .forEach((context) -> {
              applicationContextFacade.edit(context);
            });
  }

  private void upgradeApplicationContextisEnabled() {
    final int defaultApplicationContextId = DEFAULT_APPLICATION_CONTEXT.getContextId();
    final List<ApplicationContext> allApplicationContexts = applicationContextFacade.findAll();
    allApplicationContexts
            .stream()
            .parallel()
            .forEach((applicationContext) -> {
              final AdditionalParameterKeyValue appContextKeyValue
                      = new AdditionalParameterKeyValue(IS_APPLICATION_CONTEXT_ENABLED_CONFIG_PARAM_NAME,
                                                        applicationContext.getContextId());
              final List<AdditionalParameterKeyValue> additionalParameterKeyValues = ImmutableList.of(
                      appContextKeyValue);
              final List<ConfigurationValue> configurationValues
                      = configurationValueFacade.findConfigurationValues(defaultApplicationContextId,
                                                                         APPLICATION_CONTEXT_CONFIGURATION_SERVICE_NAMESAPCE,
                                                                         IS_APPLICATION_CONTEXT_ENABLED_CONFIG_NAME,
                                                                         additionalParameterKeyValues);
              if (configurationValues.isEmpty()) {
                //then add it.
                isApplicationContextEnabledConfiguration.addIsApplicationContextEnabledConfiguration(
                        applicationContext);
              }
            });
  }

}
