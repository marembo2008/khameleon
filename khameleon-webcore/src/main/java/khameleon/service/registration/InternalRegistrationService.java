package khameleon.service.registration;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Specializes;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.spi.api.registration.DefaultConfigurationRegistrationService;
import khameleon.service.registration.configuration.ConfigurationRegistrationServiceConfigurationDelegate;
import khameleon.service.registration.namespace.ConfigurationRegistrationServiceNamespaceDelegate;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo
 */
@Slf4j
@Specializes
@ApplicationScoped
public class InternalRegistrationService extends DefaultConfigurationRegistrationService {

  @EJB
  private ConfigurationRegistrationServiceConfigurationDelegate configurationRegistrationServiceConfigurationDelegate;

  @EJB
  private ConfigurationRegistrationServiceNamespaceDelegate configurationRegistrationServiceNamespaceDelegate;

  @Override
  public void registerConfiguration(@NonNull final Configuration configuration) {
    log.info("Regstering configuration: {}#{}", configuration.getNamespace().getCanonicalName(), configuration
             .getConfigName());

    configurationRegistrationServiceConfigurationDelegate.registerConfiguration(configuration);
  }

  @Override
  public void registerNamespace(@Nonnull final Namespace namespace) {
    log.info("Registering namespace: {}", namespace.getCanonicalName());

    configurationRegistrationServiceNamespaceDelegate.registerNamespace(namespace);
  }

}
