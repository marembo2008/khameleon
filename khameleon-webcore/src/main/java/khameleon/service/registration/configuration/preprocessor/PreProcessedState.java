package khameleon.service.registration.configuration.preprocessor;

/**
 *
 * @author marembo
 */
public enum PreProcessedState {

    /**
     * That the preprocessor has finished all processing, and no more processing should be done.
     */
    COMPLETED,
    /**
     * That more preprocessors and normal processing can continue.
     */
    CONTINUE;
}
