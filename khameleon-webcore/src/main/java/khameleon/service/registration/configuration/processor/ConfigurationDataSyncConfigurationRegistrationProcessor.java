package khameleon.service.registration.configuration.processor;

import com.google.common.collect.Lists;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import khameleon.core.Configuration;
import khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder;
import khameleon.service.registration.configuration.preprocessor.ConfigurationRegistrationPreProcessor;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder.CONFIGURATION_ORDER_UNKNOWN;
import static khameleon.service.registration.configuration.preprocessor.PreProcessedState.CONTINUE;

/**
 *
 * @author marembo
 */
@Alternative
@ApplicationScoped
public class ConfigurationDataSyncConfigurationRegistrationProcessor implements ConfigurationRegistrationProcessor,
                                                                                ConfigurationRegistrationPreProcessor {

  private static final ThreadLocal<Configuration> NEW_CONFIGURATION_CACHE = new ThreadLocal<>();

  private static final long serialVersionUID = -5169256573833198162L;

  @Override
  @Nonnull
  public ConfigurationPreProcessorOrder getConfigurationPreProcessorOrder() {
    return CONFIGURATION_ORDER_UNKNOWN;
  }

  @Override
  @Nonnull
  public PreProcessedState preProcess(@Nonnull final Configuration newConfiguration) {
    checkNotNull(newConfiguration, "The newConfiguration must not be null");

    NEW_CONFIGURATION_CACHE.set(newConfiguration);

    return CONTINUE;
  }

  @Override
  public void process(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "The configuration must not be null");

    final Configuration newConfiguration = NEW_CONFIGURATION_CACHE.get();
    checkState(newConfiguration != null, "Expected the newConfiguration to be set");

    //Remove from the configuration, any configurationdata not specified in the new configuration
    //We force a copy to avoid comodifications here.
    Lists
            .newArrayList(configuration.getConfigurationDatas())
            .stream()
            .filter((confData) -> !newConfiguration.getConfigurationData(confData.getDataOption()).isPresent())
            .forEach((confData) -> configuration.removeConfigurationData(confData.getDataOption()));
  }

}
