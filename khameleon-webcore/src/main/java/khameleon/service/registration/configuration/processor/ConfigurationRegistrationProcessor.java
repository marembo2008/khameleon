package khameleon.service.registration.configuration.processor;

import java.io.Serializable;
import javax.annotation.Nonnull;

import khameleon.core.Configuration;

/**
 *
 * @author marembo
 */
public interface ConfigurationRegistrationProcessor extends Serializable {

    void process(@Nonnull final Configuration configuration);
}
