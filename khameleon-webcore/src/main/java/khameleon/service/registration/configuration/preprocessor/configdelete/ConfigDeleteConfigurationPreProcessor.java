package khameleon.service.registration.configuration.preprocessor.configdelete;

import com.google.common.base.Optional;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.Namespace;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder;
import khameleon.service.registration.configuration.preprocessor.ConfigurationRegistrationPreProcessor;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_DELETE;
import static khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder.CONFIGURATION_ORDER_CONFIG_DELETE;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ConfigDeleteConfigurationPreProcessor implements ConfigurationRegistrationPreProcessor {

  private static final Logger LOG = Logger.getLogger(ConfigDeleteConfigurationPreProcessor.class.getName());

  @EJB
  private ConfigDeleteHelperService configDeleteHelperService;

  @EJB
  private ConfigurationFacade configurationFacade;

  @Nonnull
  @Override
  public PreProcessedState preProcess(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    final Optional<ConfigurationData> configurationDataOptional = configuration.getConfigurationData(
            CONFIGURATION_CONFIG_DELETE);
    if (!configurationDataOptional.isPresent() || !configurationDataOptional.get().getBooleanValue()) {
      return PreProcessedState.CONTINUE;
    }

    try {
      final Namespace namespace = checkNotNull(configuration.getNamespace(),
                                               "The configuration.getNamespace() must not be null");
      final String namespaceCanonicalName = checkNotNull(namespace.getCanonicalName(),
                                                         "The namespace.getCanonicalName() must not be null");
      final String configName = checkNotNull(configuration.getConfigName(),
                                             "The configuration.getConfigName() must not be null");
      LOG.log(Level.INFO,
              "Found @ConfigDelete configuration... Preparing to remove: '{'{0},{1}'}'",
              new Object[]{namespaceCanonicalName, configName});

      final Configuration registeredConfiguration = configurationFacade.findConfiguration(
              namespaceCanonicalName, configName);
      if (registeredConfiguration != null) {
        //then remove the configuration
        configDeleteHelperService.deleteConfiguration(registeredConfiguration);
      } else {
        LOG.log(Level.WARNING,
                "The following Configuration is annotated @ConfigDelete, and yet it no longer exists: {0}.{1}",
                new Object[]{namespaceCanonicalName, configName});
      }
    } catch (final Exception ex) {
      LOG.log(Level.WARNING, "Error deleting configuration", ex);
    }

    //Regardless, since this is a deleted configuration, we cannot continue to process it.
    return PreProcessedState.COMPLETED;
  }

  @Override
  @Nonnull
  public ConfigurationPreProcessorOrder getConfigurationPreProcessorOrder() {
    return CONFIGURATION_ORDER_CONFIG_DELETE;
  }

}
