package khameleon.service.registration.configuration.preprocessor.configdelete;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;

import static com.google.common.base.Preconditions.checkNotNull;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 * To create some transactional issues.
 *
 * @author marembo
 */
@Stateless
@TransactionAttribute(REQUIRES_NEW)
public class ConfigDeleteHelperService {

  private static final Logger LOG = Logger.getLogger(ConfigDeleteHelperService.class.getName());

  @EJB
  private ConfigDeleteTransactionAwareService configDeleteTransactionAwareService;

  @EJB
  private ConfigurationFacade configurationFacade;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  public void deleteAllConfigurations(@Nonnull final Namespace namespace) {
    checkNotNull(namespace, "the namespaceId must not be null");

    deleteConfigurations(configurationFacade.findConfigurations(namespace));
  }

  public void deleteConfiguration(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    deleteConfigurations(ImmutableList.of(configuration));
  }

  private void deleteAllConfigurationValues(final String configurationId) {
    checkNotNull(configurationId, "the configurationId must not be null");

    //There could potentially be a lot of configuration values. So we do this in steps!
    final int pageCount = 100;
    for (int offset = 0;; offset += pageCount) {
      final List<ConfigurationValue> configurationValues
              = configurationValueFacade.findConfigurationValues(configurationId, offset, pageCount);

      LOG.log(Level.INFO, "Deleting ConfigurationValues.....{0}", configurationValues.size());

      if (configurationValues.isEmpty()) {
        break;
      }

      for (final ConfigurationValue configurationValue : configurationValues) {
        if (configurationValueFacade.find(configurationValue.getValueId()) != null) {
          configDeleteTransactionAwareService.removeEntity(configurationValue);
        }
      }
    }
  }

  private void deleteAllConfigurationValues(final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    deleteAllConfigurationValues(configuration.getConfigId());
  }

  private void deleteConfigurations(@Nonnull final List<Configuration> configurations) {
    checkNotNull(configurations, "the configurations must not be null");

    for (final Configuration configuration : configurations) {
      //Delete all values associated with this configuration first!
      deleteAllConfigurationValues(configuration);

      configDeleteTransactionAwareService.removeEntity(configuration);
    }
  }

}
