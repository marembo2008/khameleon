package khameleon.service.registration.configuration;

import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.LocalBean;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.core.AdditionalInformation;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.EnumerationConfigData;
import khameleon.core.Namespace;
import khameleon.core.internal.service.ConfigurationServiceException;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.manager.ConfigurationUpdate;
import khameleon.service.manager.ConfigurationUpdated;
import khameleon.service.registration.configuration.preprocessor.ConfigurationRegistrationPreProcessor;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.configuration.processor.ConfigurationRegistrationProcessor;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

import static java.lang.String.format;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_RICH_TEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_TEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_XML;
import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 *
 * @author marembo
 */
@Stateless
@LocalBean
public class ConfigurationRegistrationServiceConfigurationDelegate {

  private static final Logger LOG = Logger.getLogger(ConfigurationRegistrationServiceConfigurationDelegate.class
          .getName());

  private static final Ordering<ConfigurationRegistrationPreProcessor> CONFIGURATIONREGISTRATIONPREPROCESSOR_ORDER
          = Ordering.from((o1, o2) -> o1.getConfigurationPreProcessorOrder().compareTo(o2
          .getConfigurationPreProcessorOrder()));

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @EJB
  private NamespaceFacade namespaceFacade;

  @EJB
  private ConfigurationFacade configurationFacade;

  @Any
  @Inject
  private Instance<ConfigurationRegistrationProcessor> configurationRegistrationProcessors;

  private List<ConfigurationRegistrationPreProcessor> configurationRegistrationPreProcessors;

  @Inject
  @ConfigurationUpdated
  private Event<ConfigurationUpdate> configurationUpdateEvent;

  @Inject
  void injectAndOrderConfigurationRegistrationPreProcessors(
          @Any Instance<ConfigurationRegistrationPreProcessor> registrationPreProcessors) {
    this.configurationRegistrationPreProcessors = CONFIGURATIONREGISTRATIONPREPROCESSOR_ORDER.immutableSortedCopy(
            registrationPreProcessors);
  }

  /**
   * Registers a configuration with the config front-end.
   *
   * @param newConfiguration
   */
  @Asynchronous //Fail silently
  @TransactionAttribute(REQUIRES_NEW)
  public void registerConfiguration(@Nonnull final Configuration newConfiguration) throws ConfigurationServiceException {
    checkNotNull(newConfiguration, "Configuration must be specified");

    final boolean noMoreProcessing = configurationRegistrationPreProcessors
            .stream()
            .map((preProcessor) -> preProcessor.preProcess(newConfiguration))
            .filter((state) -> Objects.equals(state, PreProcessedState.COMPLETED))
            .findFirst()
            .isPresent();

    if (noMoreProcessing) {
      LOG.log(Level.INFO, "Ignoring all further processing of for this configuration: {0}", newConfiguration);
      return;
    }

    final Namespace newNamespace = checkNotNull(newConfiguration.getNamespace(),
                                                "The newConfiguration.getNamespace() must not be null");
    final Namespace configNamespace = namespaceFacade.findByCanonicalName(newNamespace.getCanonicalName());
    if (configNamespace == null) {
      throw new ConfigurationServiceException(
              format("Configuration: {%s} specifies non-existent namespace: {%s}", newConfiguration
                     .getConfigName(), newNamespace));
    }

    newConfiguration.setNamespace(configNamespace); //set namespace to existing value to avoid transient states!
    final String canonicalName = configNamespace.getCanonicalName();
    final String configName = checkNotNull(newConfiguration.getConfigName(),
                                           "The newConfiguration.getConfigName() must not be null");
    final Configuration registeredConfiguration = configurationFacade.findConfiguration(canonicalName, configName);
    final ApplicationContext context = applicationContextFacade.find(DEFAULT_APPLICATION_CONTEXT.getContextId());
    if (registeredConfiguration != null) {
      updateRegisteredConfiguration(registeredConfiguration, newConfiguration);
    }

    final Configuration configurationToUpdate = firstNonNull(registeredConfiguration, newConfiguration);
    runConfigurationRegistrationProcessors(configurationToUpdate);

    configurationFacade.edit(configurationToUpdate);

    configurationUpdateEvent.fire(new ConfigurationUpdate(configurationToUpdate, context));
  }

  private void runConfigurationRegistrationProcessors(@Nonnull final Configuration configuration) {
    ImmutableList
            .copyOf(configurationRegistrationProcessors)
            .stream()
            .forEach((processor) -> processor.process(configuration));
  }

  private void updateRegisteredConfiguration(@Nonnull final Configuration registeredConfiguration,
                                             @Nonnull final Configuration newConfiguration) {
    //config-name, namespace and typeclass do not change.
    final String defaultValue = newConfiguration.getDefaultValue();
    if (defaultValue != null) {
      registeredConfiguration.setDefaultValue(defaultValue);
    }

    registeredConfiguration.setTypeClass(checkNotNull(newConfiguration.getTypeClass(),
                                                      "The newConfiguration.getTypeClass() must not be null"));
    //update or add new additional parameters
    for (final AdditionalParameter newAdditionalParameter : newConfiguration.getAdditionalParameters()) {
      final Optional<AdditionalParameter> registeredAp = registeredConfiguration.getAdditionalParameter(
              newAdditionalParameter.getParameterKey());
      if (registeredAp.isPresent()) {
        updateRegisteredAdditionalParameter(registeredAp.get(), newAdditionalParameter);
      } else {
        registeredConfiguration.addAdditionalParameter(newAdditionalParameter);
      }
    }

    //update or add additional information
    for (final AdditionalInformation info : newConfiguration.getAdditionalInformations()) {
      final Optional<AdditionalInformation> registereedInfo = registeredConfiguration.getAdditionalInformation(
              info.getName());
      if (registereedInfo.isPresent()) {
        final AdditionalInformation additionalInformation = registereedInfo.get();
        additionalInformation.setInfoValue(info.getInfoValue());
      } else {
        registeredConfiguration.addAdditionalInformation(info.getName(), info.getInfoValue());
      }
    }

    //update or add configuration data.
    for (final ConfigurationData newConfData : newConfiguration.getConfigurationDatas()) {
      //This is manual handling. There is just no way to do this.
      //XML, RichText, and Text are mutually exclusive.
      //If we have a change in configuration, then we must remove the old one.
      final ConfigurationDataOption newDataOption = newConfData.getDataOption();
      switch (newDataOption) {
        case CONFIGURATION_RICH_TEXT:
        case CONFIGURATION_TEXT:
        case CONFIGURATION_XML:
          //remove any of the following if present in the old configuration data.
          FluentIterable
                  .from(Arrays.asList(CONFIGURATION_RICH_TEXT, CONFIGURATION_TEXT, CONFIGURATION_XML))
                  .filter(not(equalTo(newDataOption)))
                  .toList()
                  .stream()
                  .forEach((dataOption) -> registeredConfiguration.removeConfigurationData(dataOption));
      }

      final Optional<ConfigurationData> registeredConfData
              = registeredConfiguration.getConfigurationData(newDataOption);
      if (registeredConfData.isPresent()) {
        updateRegisteredConfigurationData(registeredConfData.get(), newConfData);
      } else {
        registeredConfiguration.addConfigurationData(newConfData);
      }
    }
  }

  private void updateRegisteredAdditionalParameter(@Nonnull final AdditionalParameter registeredAdditionalParameter,
                                                   @Nonnull final AdditionalParameter newAdditionalParameter) {
    final String newDefaultValue = newAdditionalParameter.getDefaultValue();
    if (newDefaultValue != null) {
      registeredAdditionalParameter.setDefaultValue(newDefaultValue);
    }

    final String newDescription = newAdditionalParameter.getParameterDescription();
    if (newDescription != null) {
      registeredAdditionalParameter.setParameterDescription(newDescription);
    }

    for (final AdditionalParameterConfigData configData : newAdditionalParameter.getConfigData()) {
      final Optional<AdditionalParameterConfigData> registeredConfigData
              = registeredAdditionalParameter.getConfigData(configData.getConfigDataType());
      if (registeredConfigData.isPresent()) {
        updateRegisteredAdditionalParameterConfigData(registeredConfigData.get(), configData);
      } else {
        registeredAdditionalParameter.addConfigData(configData);
      }
    }
  }

  private void updateRegisteredAdditionalParameterConfigData(
          @Nonnull final AdditionalParameterConfigData registeredConfigData,
          @Nonnull final AdditionalParameterConfigData newConfigData) {
    //config data type does not change
    registeredConfigData.updateAdditionalParameterConfigData(newConfigData);
    updateRegisteredEnumerationConfigData(registeredConfigData, newConfigData);
  }

  private void updateRegisteredConfigurationData(@Nonnull final ConfigurationData registeredConfigData,
                                                 @Nonnull final ConfigurationData newConfigData) {
    //config data type does not change
    registeredConfigData.updateConfigurationData(newConfigData);
    updateRegisteredEnumerationConfigData(registeredConfigData, newConfigData);
  }

  private void updateRegisteredEnumerationConfigData(@Nonnull final EnumerationConfigData registeredConfigData,
                                                     @Nonnull final EnumerationConfigData newConfigData) {
    registeredConfigData.getEnumList().clear();
    registeredConfigData.getEnumList().addAll(newConfigData.getEnumList());
  }

}
