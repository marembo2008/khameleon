package khameleon.service.registration.configuration.defaultvalueupdate;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.event.Observes;
import jakarta.xml.bind.JAXBException;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.service.manager.ConfigurationUpdate;
import khameleon.service.manager.ConfigurationUpdated;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT;
import static khameleon.core.processor.configuration.NamespaceApplicationContextHelper.unmarshall;

/**
 *
 * @author marembo
 */
@Stateless
@Dependent
public class DefaultConfigurationValueUpdateService {

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ConfigurationFacade configurationFacade;

  public void process(@Observes @ConfigurationUpdated @Nonnull final ConfigurationUpdate configurationUpdate) throws Exception {
    checkNotNull(configurationUpdate, "The configurationUpdate must not be null");

    final Configuration configuration = configurationUpdate.getConfiguration();
    final String configId = configuration.getConfigId();
    if (configurationFacade.find(configId) == null) {
      return;
    }

    //We only take care of the update if the configuration had been registered.
    final ApplicationContext applicationContext = DEFAULT_APPLICATION_CONTEXT;
    final Namespace namespace = checkNotNull(configuration.getNamespace(),
                                             "The configuration.getNamespace() must not be null");
    if (!updateDefaultValue(namespace)) {
      return;
    }

    //That means it is not defined for default context, we can safely update it, as it will never appear on the default context.
    final String configName = checkNotNull(configuration.getConfigName(),
                                           "The configuration.getConfigName() must not be null");
    final List<ConfigurationValue> defaultConfigurationValues
            = configurationValueFacade.findConfigurationValues(applicationContext, namespace, configName);
    if (defaultConfigurationValues.size() == 1) {
      final ConfigurationValue defaultConfigurationValue = defaultConfigurationValues.get(0);
      final String defaultValue = configuration.getDefaultValue();
      defaultConfigurationValue.setConfigValue(defaultValue);
      configurationValueFacade.edit(defaultConfigurationValue);
    }
  }

  private boolean updateDefaultValue(@Nonnull final Namespace namespace) {
    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT);
    return namespaceData
            .transform(this::containsDefaultApplicationContext)
            .or(true);
  }

  private boolean containsDefaultApplicationContext(@Nonnull final NamespaceData namespaceData) {
    try {
      final String xmlData = namespaceData.getDataValue();
      final List<ApplicationContext> applicationContexts = unmarshall(xmlData);
      return !applicationContexts.contains(DEFAULT_APPLICATION_CONTEXT);
    } catch (final JAXBException ex) {
      throw Throwables.propagate(ex);
    }
  }

}
