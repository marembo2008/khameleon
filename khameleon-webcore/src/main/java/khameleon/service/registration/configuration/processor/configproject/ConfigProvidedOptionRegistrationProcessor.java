package khameleon.service.registration.configuration.processor.configproject;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.access.ConfigProjectData;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.EnumValue;
import khameleon.domain.facade.ConfigProjectDataFacade;
import khameleon.service.registration.configuration.processor.ConfigurationRegistrationProcessor;

import static com.google.common.base.Preconditions.checkNotNull;

import static java.util.Objects.requireNonNull;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_CONFIG_PROVIDED_OPTION;
import static khameleon.core.annotations.ConfigProvidedOption.ConfigOptionType.PROJECT_ID;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ConfigProvidedOptionRegistrationProcessor implements ConfigurationRegistrationProcessor {

  private static final long serialVersionUID = 34834893434931L;

  private static final Function<ConfigProjectData, EnumValue> FUNCTION_MAP_ENUM_VALUE
          = (ConfigProjectData input) -> new EnumValue(input.getName(), input.getName());

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Override
  public void process(@Nonnull final Configuration configuration) {
    requireNonNull(configuration, "The configuration must not be null");

    processConfigValue(configuration);
    processConfigParameter(configuration);
  }

  private void processConfigParameter(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    //We add all the project names as if they are enum values!
    configuration.getAdditionalParameters()
            .stream()
            .map(ConfigProvidedOptionRegistrationProcessor::transform)
            .filter(Objects::nonNull)
            .forEach((configData) -> {
              final List<ConfigProjectData> configProjectDatas = configProjectDataFacade.findAll();
              configData.setEnumList(Lists.transform(configProjectDatas, FUNCTION_MAP_ENUM_VALUE));
            });
  }

  private void processConfigValue(@Nonnull final Configuration configuration) {
    //We add all the project names as if they are enum values!
    final Optional<ConfigurationData> configurationDataOptional
            = configuration.getConfigurationData(ConfigurationDataOption.CONFIGURATION_CONFIG_PROVIDED_OPTION);
    if (!configurationDataOptional.isPresent()) {
      return;
    }

    final ConfigurationData configurationData = configurationDataOptional.get();
    if (!Objects.equals(configurationData.getDataValue(), PROJECT_ID.name())) {
      return;
    }

    //Add or replace the fake enum values for the projects
    final List<ConfigProjectData> configProjectDatas = configProjectDataFacade.findAll();
    configurationData.setEnumList(Lists.transform(configProjectDatas, FUNCTION_MAP_ENUM_VALUE));
  }

  @Nullable
  private static AdditionalParameterConfigData transform(@Nonnull final AdditionalParameter additionalParameter) {
    final AdditionalParameterConfigData parameterConfigData = additionalParameter
            .getConfigData(PARAMETER_CONFIG_PROVIDED_OPTION)
            .orNull();
    if (parameterConfigData != null && Objects.equals(parameterConfigData.getConfigData(), PROJECT_ID.name())) {
      return parameterConfigData;
    }

    return null;
  }

}
