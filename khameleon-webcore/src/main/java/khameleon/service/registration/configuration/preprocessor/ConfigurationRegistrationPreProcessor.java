package khameleon.service.registration.configuration.preprocessor;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;

/**
 *
 * @author marembo
 */
public interface ConfigurationRegistrationPreProcessor {

    @Nonnull
    PreProcessedState preProcess(@Nonnull final Configuration configuration);

    @Nonnull
    ConfigurationPreProcessorOrder getConfigurationPreProcessorOrder();
}
