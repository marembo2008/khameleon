package khameleon.service.registration.configuration.preprocessor.configrename;

import com.google.common.base.Optional;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.Namespace;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder;
import khameleon.service.registration.configuration.preprocessor.ConfigurationRegistrationPreProcessor;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_RENAME;
import static khameleon.service.registration.configuration.preprocessor.ConfigurationPreProcessorOrder.CONFIGURATION_ORDER_CONFIG_RENAME;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigRenameConfigurationPreProcessor implements ConfigurationRegistrationPreProcessor {

  private static final Logger LOG = Logger.getLogger(ConfigRenameConfigurationPreProcessor.class.getName());

  @EJB
  private ConfigurationFacade configurationFacade;

  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public PreProcessedState preProcess(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    final String configName = configuration.getConfigName();
    final Namespace namespace = checkNotNull(configuration.getNamespace(),
                                             "the configuration namespace must be specified");
    final String canonicalName = namespace.getCanonicalName();
    final Configuration registeredConfiguration = configurationFacade.findConfiguration(canonicalName, configName);
    if (registeredConfiguration != null) {
      return PreProcessedState.CONTINUE;
    }

    final Optional<ConfigurationData> configDataOptional = configuration.getConfigurationData(
            CONFIGURATION_CONFIG_RENAME);
    if (!configDataOptional.isPresent()) {
      return PreProcessedState.CONTINUE;
    }

    final ConfigurationData configData = configDataOptional.get();
    final String oldConfigName = configData.getDataValue();
    final Configuration registereConfigurationInOldConfigName = configurationFacade.findConfiguration(
            canonicalName, oldConfigName);
    if (registereConfigurationInOldConfigName == null) {
      LOG.log(Level.WARNING,
              "@ConfigRename has been requested with old configName={0}:{1} which no longer exists",
              new Object[]{canonicalName, configName});
      return PreProcessedState.CONTINUE;
    }

    registereConfigurationInOldConfigName.setConfigName(configName);
    configurationFacade.edit(registereConfigurationInOldConfigName);

    return PreProcessedState.CONTINUE;
  }

  @Override
  public ConfigurationPreProcessorOrder getConfigurationPreProcessorOrder() {
    return CONFIGURATION_ORDER_CONFIG_RENAME;
  }

}
