package khameleon.service.registration.configuration.preprocessor;

/**
 *
 * @author marembo
 */
public enum ConfigurationPreProcessorOrder {

    CONFIGURATION_ORDER_CONFIG_RENAME,
    CONFIGURATION_ORDER_CONFIG_DELETE,
    CONFIGURATION_ORDER_UNKNOWN; //For those who dont care about their order!
}
