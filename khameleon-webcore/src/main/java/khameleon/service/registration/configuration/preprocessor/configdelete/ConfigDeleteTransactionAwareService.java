package khameleon.service.registration.configuration.preprocessor.configdelete;

import com.google.common.collect.ImmutableMap;

import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.persistence.EntityNotFoundException;
import khameleon.domain.facade.AbstractFacade;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 *
 * @author marembo
 */
@Stateless
@TransactionAttribute(REQUIRES_NEW)
public class ConfigDeleteTransactionAwareService {

  private static final Logger LOG = Logger.getLogger(ConfigDeleteTransactionAwareService.class.getName());

  private Map<Class<?>, AbstractFacade<?>> entityTypeAbstractFacadeMapper;

  @Inject
  void setEntityTypeAbstractFacadeMapper(@Any @Nonnull final Instance<AbstractFacade<?>> abstractFacades) {
    final ImmutableMap.Builder<Class<?>, AbstractFacade<?>> builder = ImmutableMap.builder();
    for (final AbstractFacade<?> abstractFacade : abstractFacades) {
      builder.put(abstractFacade.getEntityClass(), abstractFacade);
    }

    entityTypeAbstractFacadeMapper = builder.build();
  }

  private <T> AbstractFacade<T> facade(@Nonnull final Class<?> entityClass) {
    checkState(entityTypeAbstractFacadeMapper.containsKey(entityClass),
               "there is no AbstractFacade Registered for the following entity: %s",
               entityClass);
    return (AbstractFacade<T>) entityTypeAbstractFacadeMapper.get(entityClass);
  }

  public <T> void removeEntity(@Nonnull final T entity) {
    checkNotNull(entity, "the entity must be provided");

    try {
      final AbstractFacade<T> abstractFacade = facade(entity.getClass());
      abstractFacade.remove(entity);
    } catch (final EntityNotFoundException enfe) {
      LOG.warning("Entity already removed: ");
    }
  }

  public <T> void updateEntity(@Nonnull final T entity) {
    checkNotNull(entity, "the entity must be provided");

    final AbstractFacade<T> abstractFacade = facade(entity.getClass());
    abstractFacade.edit(entity);
  }

}
