package khameleon.service.registration.configuration.processor.configproject;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.access.ConfigProjectData;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.Configuration;
import khameleon.core.EnumValue;
import khameleon.core.annotations.ConfigProject;
import khameleon.domain.facade.ConfigProjectDataFacade;
import khameleon.service.registration.configuration.processor.ConfigurationRegistrationProcessor;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_ENUM;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ConfigProjectConfigurationRegistrationProcessor implements ConfigurationRegistrationProcessor {

  private static final long serialVersionUID = 34834893434931L;

  private static final Function<ConfigProjectData, EnumValue> FUNCTION_MAP_ENUM_VALUE
          = (ConfigProjectData input) -> new EnumValue(input.getName(), input.getName());

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Override
  public void process(@Nonnull final Configuration configuration) {
    checkNotNull(configuration, "the configuration must not be null");

    //We add all the project names as if they are enum values!
    final Optional<AdditionalParameter> additionalParameterOptional = configuration.getAdditionalParameter(
            ConfigProject.PROJECT_KEY);
    if (!additionalParameterOptional.isPresent()) {
      return;
    }

    final AdditionalParameter additionalParameter = additionalParameterOptional.get();
    //Add or replace the fake enum values for the projects
    final List<ConfigProjectData> configProjectDatas = configProjectDataFacade.findAll();
    final Optional<AdditionalParameterConfigData> enumConfigDataOptional = additionalParameter.getConfigData(
            PARAMETER_ENUM);
    final boolean updateConfigData = enumConfigDataOptional.isPresent();
    final AdditionalParameterConfigData enumConfigData;
    if (updateConfigData) {
      enumConfigData = enumConfigDataOptional.get();
    } else {
      enumConfigData = new AdditionalParameterConfigData(PARAMETER_ENUM, Boolean.TRUE.toString());
    }

    enumConfigData.setEnumList(Lists.transform(configProjectDatas, FUNCTION_MAP_ENUM_VALUE));
    additionalParameter.addOrUpdateConfigData(enumConfigData);
  }

}
