package khameleon.service.registration.namespace.preprocessor;

import khameleon.core.Namespace;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.namespace.NamespacePreProcessorOrder;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo
 */
public interface NamespaceRegistrationPreProcessor {

    @Nonnull
    PreProcessedState preProcess(@Nonnull final Namespace namespace);

    @Nonnull
    NamespacePreProcessorOrder getNamespacePreProcessorOrder();
}
