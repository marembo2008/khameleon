package khameleon.service.registration.namespace;

import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.NamespaceDataUnique;
import khameleon.domain.facade.NamespaceFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 14, 2015, 1:13:21 AM
 */
@Stateless
public class NamespaceDataDuplicateCleanerService {

  @EJB
  private NamespaceFacade namespaceFacade;

  @Asynchronous
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void removeDuplicateNamespaceData(@Nonnull final Namespace namespace) {
    final Set<NamespaceData> namespaceDatas = namespace
            .getNamespaceData()
            .stream()
            .map(NamespaceDataUnique::new)
            .distinct()
            .map(NamespaceDataUnique::getNamespaceData)
            .collect(Collectors.toSet());

    namespace.setNamespaceData(namespaceDatas);

    namespaceFacade.edit(namespace);
  }

}
