package khameleon.service.registration.namespace.preprocessor;

import com.google.common.base.Optional;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.configuration.preprocessor.configdelete.ConfigDeleteHelperService;
import khameleon.service.registration.namespace.NamespacePreProcessorOrder;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_DELETE;
import static khameleon.service.registration.namespace.NamespacePreProcessorOrder.NAMESPACE_ORDER_CONFIG_DELETE;

/**
 *
 * @author marembo
 */
@Singleton
@ApplicationScoped
public class ConfigDeleteNamespacePreProcessor implements NamespaceRegistrationPreProcessor {

  private static final Logger LOG = Logger.getLogger(ConfigDeleteNamespacePreProcessor.class.getName());

  @EJB
  private ConfigDeleteHelperService configDeleteHelperService;

  @EJB
  private NamespaceFacade namespaceFacade;

  @Nonnull
  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public PreProcessedState preProcess(@Nonnull final Namespace newNamespace) {
    checkNotNull(newNamespace, "the namespace must not be null");

    final Optional<NamespaceData> namespaceDataOptional = newNamespace.getNamespaceData(NAMESPACE_CONFIG_DELETE);
    if (!namespaceDataOptional.isPresent() || !namespaceDataOptional.get().getBooleanValue()) {
      return PreProcessedState.CONTINUE;
    }

    //Ok delete every configuration value, configuration and Namespace!
    final String namespaceCanonicalName = checkNotNull(newNamespace.getCanonicalName(),
                                                       "The namespace.getCanonicalName() must not be null");
    final Namespace registeredNamespace = namespaceFacade.findByCanonicalName(namespaceCanonicalName);
    if (registeredNamespace != null) {
      //Load the configurtions
      //Then delete all configurations
      configDeleteHelperService.deleteAllConfigurations(registeredNamespace);
      //Then delete the namespace
      namespaceFacade.remove(registeredNamespace);
    } else {
      LOG.log(Level.WARNING,
              "The following Namespace is annotated @ConfigDelete, and it no longer exists: {0}",
              namespaceCanonicalName);
    }

    //Regardless, since this is a deleted namespace, we cannot continue to process it.
    return PreProcessedState.COMPLETED;
  }

  @Override
  @Nonnull
  @Lock(LockType.READ)
  public NamespacePreProcessorOrder getNamespacePreProcessorOrder() {
    return NAMESPACE_ORDER_CONFIG_DELETE;
  }

}
