package khameleon.service.registration.namespace;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import khameleon.domain.facade.NamespaceFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 14, 2015, 1:13:21 AM
 */
@Startup
@Singleton
public class NamespaceDataDuplicateCleaner {

  @EJB
  private NamespaceFacade namespaceFacade;

  @EJB
  private NamespaceDataDuplicateCleanerService namespaceDataDuplicateCleanerService;

  @PostConstruct
  void cleanUpDuplicates() {
    namespaceFacade
            .findAll()
            .stream()
            .forEach(namespaceDataDuplicateCleanerService::removeDuplicateNamespaceData);
  }

}
