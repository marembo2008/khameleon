package khameleon.service.registration.namespace.preprocessor;

import com.google.common.base.Optional;

import javax.annotation.Nonnull;

import jakarta.ejb.AccessTimeout;
import jakarta.ejb.EJB;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.namespace.NamespacePreProcessorOrder;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT;
import static khameleon.service.registration.configuration.preprocessor.PreProcessedState.CONTINUE;
import static khameleon.service.registration.namespace.NamespacePreProcessorOrder.NAMESPACE_ORDER_UNKNOWN;

/**
 *
 * @author marembo
 */
@Singleton
@ApplicationScoped
public class AppContextUpdateNamespaceRegistrationProcessor implements NamespaceRegistrationPreProcessor {

  @EJB
  private NamespaceFacade namespaceFacade;

  @Nonnull
  @Override
  @Lock(LockType.READ)
  public NamespacePreProcessorOrder getNamespacePreProcessorOrder() {
    return NAMESPACE_ORDER_UNKNOWN;
  }

  @Nonnull
  @Override
  @AccessTimeout(Long.MAX_VALUE)
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public PreProcessedState preProcess(@Nonnull final Namespace newNamespace) {
    checkNotNull(newNamespace, "The namespace must not be null");

    final Namespace registeredNamespace = namespaceFacade.findByCanonicalName(newNamespace.getCanonicalName());
    if (registeredNamespace == null) {
      return CONTINUE;
    }

    final Optional<NamespaceData> newNamespaceData = newNamespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT);
    final Optional<NamespaceData> oldNamespaceData = registeredNamespace.getNamespaceData(
            NAMESPACE_APPLICATION_CONTEXT);
    if (!newNamespaceData.isPresent() && oldNamespaceData.isPresent()) {
      registeredNamespace.removeNamespaceData(NAMESPACE_APPLICATION_CONTEXT);
      namespaceFacade.edit(registeredNamespace);
    }

    return CONTINUE;
  }

}
