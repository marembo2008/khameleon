package khameleon.service.registration.namespace.processor;

import javax.annotation.Nonnull;

import khameleon.core.Namespace;

/**
 *
 * @author marembo
 */
public interface NamespaceRegistrationProcessor {

    void process(@Nonnull final Namespace namespace);

}
