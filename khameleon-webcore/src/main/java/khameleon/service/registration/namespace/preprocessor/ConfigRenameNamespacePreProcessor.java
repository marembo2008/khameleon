package khameleon.service.registration.namespace.preprocessor;

import com.google.common.base.Optional;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.namespace.NamespacePreProcessorOrder;

import static com.google.common.base.Preconditions.checkNotNull;

import static java.lang.String.format;
import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_RENAME;
import static khameleon.service.registration.namespace.NamespacePreProcessorOrder.NAMESPACE_ORDER_CONFIG_RENAME;

/**
 *
 * @author marembo
 */
@Singleton
@ApplicationScoped
public class ConfigRenameNamespacePreProcessor implements NamespaceRegistrationPreProcessor {

  private static final Logger LOG = Logger.getLogger(ConfigRenameNamespacePreProcessor.class.getName());

  @EJB
  private NamespaceFacade namespaceFacade;

  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public PreProcessedState preProcess(@Nonnull final Namespace newNamespace) {
    checkNotNull(newNamespace, "the namespace must not be null");

    final Optional<NamespaceData> configRenameData = newNamespace.getNamespaceData(NAMESPACE_CONFIG_RENAME);
    if (!configRenameData.isPresent()) {
      return PreProcessedState.CONTINUE;
    }

    final String oldSimpleName = configRenameData.get().getDataValue();
    final Namespace parentNamespace = newNamespace.getParent();
    final String parentCanonicalNamespace;
    if (parentNamespace != null) {
      preProcess(parentNamespace);
      parentCanonicalNamespace = parentNamespace.getCanonicalName();
    } else {
      parentCanonicalNamespace = null;
    }

    final String oldCanonicalName;
    if (parentCanonicalNamespace != null) {
      oldCanonicalName = format("%s:%s", parentCanonicalNamespace, oldSimpleName);
    } else {
      oldCanonicalName = oldSimpleName;
    }

    final Namespace registeredNamespaceWithOldCanonicalName
            = namespaceFacade.findByCanonicalName(oldCanonicalName);
    if (registeredNamespaceWithOldCanonicalName == null) {
      LOG.log(Level.WARNING,
              "@ConfigRename has been requested with old namespaceCanonicalName: {0}, which no longer exists",
              oldCanonicalName);
      return PreProcessedState.CONTINUE;
    }

    registeredNamespaceWithOldCanonicalName.setSimpleName(newNamespace.getSimpleName());
    namespaceFacade.edit(registeredNamespaceWithOldCanonicalName);

    return PreProcessedState.CONTINUE;
  }

  @Override
  @Lock(LockType.READ)
  public NamespacePreProcessorOrder getNamespacePreProcessorOrder() {
    return NAMESPACE_ORDER_CONFIG_RENAME;
  }

}
