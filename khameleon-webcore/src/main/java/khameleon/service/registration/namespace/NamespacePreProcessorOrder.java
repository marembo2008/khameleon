package khameleon.service.registration.namespace;

/**
 *
 * @author marembo
 */
public enum NamespacePreProcessorOrder {

    NAMESPACE_ORDER_CONFIG_RENAME,
    NAMESPACE_ORDER_CONFIG_DELETE,
    NAMESPACE_ORDER_UNKNOWN; //For those who dont care about their order!
}
