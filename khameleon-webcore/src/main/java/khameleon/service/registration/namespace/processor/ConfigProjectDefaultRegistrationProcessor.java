package khameleon.service.registration.namespace.processor;

import static khameleon.core.internal.ConfigurationUtil.fromArrayString;

import com.google.common.collect.ImmutableList;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.context.Dependent;
import khameleon.access.ConfigProjectData;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.NamespaceDataType;
import khameleon.domain.facade.ConfigProjectDataFacade;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 25, 2018, 9:57:48 PM
 */
@Dependent
@Stateless
public class ConfigProjectDefaultRegistrationProcessor implements NamespaceRegistrationProcessor {

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Override
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void process(@NonNull final Namespace namespace) {
    namespace
            .getNamespaceData(NamespaceDataType.NAMESPACE_CONFIG_PROJECT)
            .toJavaUtil()
            .map(NamespaceData::getDataValue)
            .map((projectList) -> ImmutableList.copyOf(fromArrayString(projectList)))
            .orElse(ImmutableList.of())
            .stream()
            .filter((project) -> configProjectDataFacade.findConfigProjectByName(project) == null)
            .forEach((project) -> {
              final ConfigProjectData projectData = new ConfigProjectData();
              projectData.setName(project);
              projectData.setDescription(project);
              configProjectDataFacade.edit(projectData);
            });
  }

}
