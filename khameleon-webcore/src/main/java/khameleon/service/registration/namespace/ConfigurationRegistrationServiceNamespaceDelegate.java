package khameleon.service.registration.namespace;

import com.google.common.collect.Ordering;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.core.Namespace;
import khameleon.core.internal.service.ConfigurationServiceException;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.configuration.preprocessor.PreProcessedState;
import khameleon.service.registration.namespace.preprocessor.NamespaceRegistrationPreProcessor;
import khameleon.service.registration.namespace.processor.NamespaceRegistrationProcessor;

import static com.google.common.base.Preconditions.checkNotNull;

import static java.lang.String.format;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigurationRegistrationServiceNamespaceDelegate {

  private static final Logger LOG = Logger.getLogger(ConfigurationRegistrationServiceNamespaceDelegate.class
          .getName());

  private static final Ordering<NamespaceRegistrationPreProcessor> NAMESPACEREGISTRATIONPREPROCESSOR_ORDER
          = Ordering
                  .from((o1, o2) -> o1.getNamespacePreProcessorOrder().compareTo(o2.getNamespacePreProcessorOrder()));

  @EJB
  private NamespaceFacade namespaceFacade;

  @Any
  @Inject
  private Instance<NamespaceRegistrationProcessor> namespaceRegistrationProcessors;

  private List<NamespaceRegistrationPreProcessor> namespaceRegistrationPreProcessors;

  @Inject
  void injectAndOrderNamespaceRegistrationPreProcessors(
          @Any Instance<NamespaceRegistrationPreProcessor> registrationPreProcessors) {
    this.namespaceRegistrationPreProcessors = NAMESPACEREGISTRATIONPREPROCESSOR_ORDER.immutableSortedCopy(
            registrationPreProcessors);
  }

  /**
   * Creates a namespace and returns its unique identifier.
   *
   * @param namespace the namespace to create. It is an error to specify non-existent parent.
   */
  @Asynchronous
  @TransactionAttribute(REQUIRES_NEW)
  public void registerNamespace(@Nonnull final Namespace namespace) throws
          ConfigurationServiceException {
    checkNotNull(namespace, "Namespace must be specified");

    for (final NamespaceRegistrationPreProcessor preProcessor : namespaceRegistrationPreProcessors) {
      if (preProcessor.preProcess(namespace) == PreProcessedState.COMPLETED) {
        LOG.log(Level.INFO, "Ignoring all further processing of this namespace: {0}", namespace);
        return;
      }
    }

    //if namespace exists, just return
    final Namespace registeredNamespace = namespaceFacade.findByCanonicalName(namespace.getCanonicalName());
    if (registeredNamespace != null) {
      updateNamespace(namespace, registeredNamespace);
      return;
    }

    //try to find its parent and create.
    final Namespace parentNamespace = namespace.getParent();
    if (parentNamespace == null) {

      runNamespaceProcessors(namespace);

      namespaceFacade.create(namespace);
    } else {
      final Namespace registeredParentNamespace = namespaceFacade.findByCanonicalName(parentNamespace
              .getCanonicalName());
      if (registeredParentNamespace == null) {
        throw new ConfigurationServiceException(
                format("Namespace: {%s} specifies unregistered parent: {%s}", namespace.getSimpleName(),
                       parentNamespace.getCanonicalName()));
      }
      namespace.setParent(registeredParentNamespace);

      runNamespaceProcessors(namespace);

      namespaceFacade.create(namespace);
    }
  }

  private void updateNamespace(@Nonnull final Namespace newNamespace, @Nonnull final Namespace registeredNamespace) throws
          ConfigurationServiceException {

    //The simpleName and the canonical name cannot change.
    final String displayname = newNamespace.getDisplayName();
    final String description = newNamespace.getDescription();
    if (displayname != null) {
      registeredNamespace.setDisplayName(displayname);
    }

    if (description != null) {
      registeredNamespace.setDescription(description);
    }

    newNamespace
            .getNamespaceData()
            .stream()
            .forEach(registeredNamespace::addOrUpdateNamespaceData);

    runNamespaceProcessors(registeredNamespace);

    namespaceFacade.edit(registeredNamespace);
  }

  private void runNamespaceProcessors(@Nonnull final Namespace namespace) {
    for (final NamespaceRegistrationProcessor processor : namespaceRegistrationProcessors) {
      processor.process(namespace);
    }
  }

}
