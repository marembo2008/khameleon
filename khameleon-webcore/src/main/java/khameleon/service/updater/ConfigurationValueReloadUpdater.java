package khameleon.service.updater;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.enterprise.context.ApplicationScoped;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 25, 2015, 8:42:51 PM
 */
@Singleton
@ApplicationScoped
public class ConfigurationValueReloadUpdater implements Updater {

  private static final Logger LOG = Logger.getLogger(ConfigurationValueReloadUpdater.class.getName());

  private static final String RUN_CONFIGURATION_VALUE_RELOAD_UPDATER
          = "khameleon.configurationValueReloadUpdater";

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ConfigurationValueTransactionalUpdater configurationValueTransactionalUpdater;

  @Override
  @Asynchronous
  public void update() {
    LOG.info("Starting ConfigurationValueReload updater...........");

    final boolean runUpdater = Boolean.valueOf(System.getProperty(RUN_CONFIGURATION_VALUE_RELOAD_UPDATER, "true"));
    if (!runUpdater) {
      LOG.info("ConfigurationValueReload not enabled...............");

      return;
    }

    final int maxCount = 500;
    final int count = configurationValueFacade.count();
    for (int offset = 0; offset < count; offset += maxCount) {
      final List<ConfigurationValue> configurationValues
              = configurationValueFacade.findRange(new int[]{offset, offset + maxCount});

      LOG.log(Level.INFO, "Updating ConfigurationValues '{'{0}'}'", configurationValues.size());

      for (final ConfigurationValue configurationValue : configurationValues) {
        configurationValueTransactionalUpdater.updateConfigurationValue(configurationValue);
      }
    }

    LOG.info("Completed ConfigurationValueReload update...............");
  }

}
