package khameleon.service.updater;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 25, 2015, 8:39:05 PM
 */
@Singleton
@Startup
public class UpdaterService {

  @Inject
  private Instance<Updater> updaters;

  @PostConstruct
  void startUpdaterService() {
    for (final Updater updater : updaters) {
      updater.update();
    }
  }

}
