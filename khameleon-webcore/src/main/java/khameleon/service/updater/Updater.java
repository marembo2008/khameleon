package khameleon.service.updater;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 25, 2015, 8:41:32 PM
 */
public interface Updater {

    void update();
}
