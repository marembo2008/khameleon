package khameleon.service.updater;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 30, 2015, 2:24:41 AM
 */
@Stateless
public class ConfigurationValueTransactionalUpdater {

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @Asynchronous
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void updateConfigurationValue(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    configurationValue.updateAdditionalParameterKeyValuesUniqueId();
    configurationValueFacade.edit(configurationValue);
  }

}
