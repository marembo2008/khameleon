package khameleon.service;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.eventlistener.ConfigurationEventListener;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_ADD;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_DELETE;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_UPDATE;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 13, 2015, 10:35:10 PM
 */
@Stateless
public class ConfigurationValueUpdateService {

  @Inject
  @ConfigurationEventListener(event = CONFIGURATION_UPDATE)
  private Event<ConfigurationValue> configurationUpdateEvent;

  @Inject
  @ConfigurationEventListener(event = CONFIGURATION_ADD)
  private Event<ConfigurationValue> configurationAddEvent;

  @Inject
  @ConfigurationEventListener(event = CONFIGURATION_DELETE)
  private Event<ConfigurationValue> configurationDeleteEvent;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  public void update(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "the configurationvalue must be specified");

    configurationValue.updateAdditionalParameterKeyValuesUniqueId();
    configurationValueFacade.edit(configurationValue);

    configurationUpdateEvent.fire(configurationValue);
  }

  public void create(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "the configurationvalue must be specified");

    configurationValueFacade.create(configurationValue);
    configurationAddEvent.fire(configurationValue);
  }

  public void remove(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "the configurationvalue must be specified");

    configurationValueFacade.remove(configurationValue);
    configurationDeleteEvent.fire(configurationValue);
  }

}
