package khameleon.service.configurationchange;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.event.TransactionPhase;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import khameleon.config.ApplicationContextConfig;
import khameleon.config.ConfigurationChangeListenerConfig;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.eventlistener.ConfigurationEventContext;
import khameleon.eventlistener.ConfigurationEventListener;
import khameleon.service.configurationchange.api.ConfigurationChangeListener;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.values.helper.ConfigurationValueHelper.findUniqueKeyFromAdditionalParameterValue;
import static khameleon.core.values.helper.ConfigurationValueHelper.swapApplicationContext;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_ADD;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_DELETE;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_UPDATE;

/**
 *
 * @author marembo
 */
@Slf4j
@SessionScoped
public class ConfigurationChangeListenerServiceProcessorObserver implements Serializable {

  private static final long serialVersionUID = 4394830303431L;

  @EJB
  private ConfigurationChangeListenerServiceProcessor configurationChangeListenerServiceProcessor;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @Inject
  private ApplicationContextConfig applicationContextConfig;

  @Inject
  private ConfigurationChangeListenerConfig configurationChangeListenerConfig;

  @Inject
  private Instance<ConfigurationChangeListener> configurationChangeListeners;

  @Asynchronous
  public void onConfigurationAdded(
          @Observes(during = TransactionPhase.AFTER_SUCCESS)
          @ConfigurationEventListener(event = CONFIGURATION_ADD)
          @Nonnull
          final ConfigurationValue configurationValue,
          @Nonnull
          final Instance<ConfigurationEventContext> configurationEventContext) {
    if (configurationEventContext.get().proceed()) {
      processOnConfigurationChange(configurationValue);
    }
  }

  @Asynchronous
  public void onConfigurationUpdated(
          @Observes(during = TransactionPhase.AFTER_SUCCESS)
          @ConfigurationEventListener(event = CONFIGURATION_UPDATE)
          @Nonnull
          final ConfigurationValue configurationValue,
          @Nonnull
          final Instance<ConfigurationEventContext> configurationEventContext) {
    if (configurationEventContext.get().proceed()) {
      processOnConfigurationChange(configurationValue);
    }
  }

  @Asynchronous
  public void onConfigurationDeleted(
          @Observes(during = TransactionPhase.AFTER_SUCCESS)
          @ConfigurationEventListener(event = CONFIGURATION_DELETE)
          @Nonnull
          final ConfigurationValue configurationValue,
          @Nonnull
          final Instance<ConfigurationEventContext> configurationEventContext) {
    if (!configurationEventContext.get().proceed()) {
      return;
    }

    checkNotNull(configurationValue, "ConfigurationValue must not be null");

    final ApplicationContext context = configurationValue.getContext();
    checkNotNull(context, "Configuration Application Context must be specified ");

    final Configuration configuration = configurationValue.getConfiguration();
    checkNotNull(configuration, "Configuration value must have configuration details");

    final Namespace namespace = configuration.getNamespace();
    final List<ConfigurationChangeListener> changeListeners = ImmutableList.copyOf(configurationChangeListeners);

    log.info("Received Configuration Delete Event:{}, {}, {}", context, namespace, configuration.getConfigName());

    notifyListeners(changeListeners,
                    context,
                    namespace,
                    configurationValue,
                    configurationChangeListenerServiceProcessor::onConfigurationDeleted);
  }

  private void processOnConfigurationChange(@Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "ConfigurationValue must not be null");

    final ApplicationContext context = checkNotNull(configurationValue.getContext(),
                                                    "The configurationValue.getContext() must not be null");
    final Configuration configuration = checkNotNull(configurationValue.getConfiguration(),
                                                     "The configurationValue.getConfiguration() must not be null");
    final Namespace namespace = checkNotNull(configuration.getNamespace(),
                                             "The configuration.getNamespace() must not be null");

    log.info("Received Configuration Update Event:{}, {}, {}", context, namespace, configuration.getConfigName());

    final List<ConfigurationChangeListener> changeListeners = ImmutableList.copyOf(configurationChangeListeners);

    notifyListeners(changeListeners,
                    context,
                    namespace,
                    configurationValue,
                    configurationChangeListenerServiceProcessor::onConfigurationChange);
  }

  private void notifyListeners(@Nonnull final List<ConfigurationChangeListener> changeListeners,
                               @Nonnull final ApplicationContext applicationContext,
                               @Nonnull final Namespace namespace,
                               @Nonnull final ConfigurationValue configurationValue,
                               @Nonnull final Callback callback) {
    log.info("Found {} counts registered for change events, for application context: {}",
             changeListeners.size(), applicationContext);

    callback.notify(configurationValue, changeListeners);

    final List<ApplicationContext> childrenApplicationContexts
            = applicationContextFacade.findChildrenApplicationContext(applicationContext.getContextId());
    final List<ApplicationMode> enabledApplicationModes
            = ImmutableList.copyOf(applicationContextConfig.getEnabledApplicationModes());
    childrenApplicationContexts
            .stream()
            .filter((context) -> enabledApplicationModes.contains(context.getApplicationMode()))
            .forEach((context) -> {

              //Notify the children only if the child has not defined a value for this configuration!
              //If this configurationvalue contains dynamic additional parameter, strip them off in determining
              //the child configuration values.
              final ConfigurationValue childConfigurationValue
                      = childConfigurationValue(context, configurationValue);
              if (childConfigurationValue == null) {

                final ConfigurationValue childConfigurationValue1WithoutDynamicAdditionalParameter
                        = childConfigurationValueWithoutDynamicAdditionalParameters(context, configurationValue);
                if (childConfigurationValue1WithoutDynamicAdditionalParameter == null) {
                  final ConfigurationValue changedConfigurationValue
                          = swapApplicationContext(configurationValue, context);
                  notifyListeners(changeListeners, context, namespace, changedConfigurationValue, callback);
                }
              }
            });
  }

  @Nullable
  private ConfigurationValue childConfigurationValue(@Nonnull final ApplicationContext context,
                                                     @Nonnull final ConfigurationValue configurationValue) {
    final List<AdditionalParameterValue> parameters = configurationValue.getAdditionalParameterValues();
    final String parameterUniqueKey = findUniqueKeyFromAdditionalParameterValue(parameters);
    return configurationValueFacade.findConfigurationValue(
            context, configurationValue.getConfiguration(), parameterUniqueKey);
  }

  @Nullable
  private ConfigurationValue childConfigurationValueWithoutDynamicAdditionalParameters(
          @Nonnull final ApplicationContext context,
          @Nonnull final ConfigurationValue configurationValue) {
    final List<AdditionalParameterValue> parameters
            = configurationValue
                    .getAdditionalParameterValues()
                    .stream()
                    .filter((apv) -> !apv.getAdditionalParameter().isDynamicParameter())
                    .collect(Collectors.toList());
    final String parameterUniqueKey = findUniqueKeyFromAdditionalParameterValue(parameters);
    return configurationValueFacade.findConfigurationValue(
            context, configurationValue.getConfiguration(), parameterUniqueKey);
  }

  private static interface Callback {

    void notify(@Nonnull final ConfigurationValue configurationValue,
                @Nonnull final List<ConfigurationChangeListener> changeListeners);

  }

}
