package khameleon.service.configurationchange.api;

import static khameleon.core.internal.util.KhameleonParameters.KHAMELEON_FEIGN_CLIENT_CHANGE_LISTENER;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;
import khameleon.core.internal.service.changelistener.ConfigurationChangeEvent;

/**
 *
 * @author marembo
 */
@FeignClient(KHAMELEON_FEIGN_CLIENT_CHANGE_LISTENER)
public interface ConfigurationChangeListener {

	@RequestLine("POST /api/configuration-changes")
	@Headers({ "Content-Type: application/json", "Accept: application/json" })
	void onConfigurationChange(final ConfigurationChangeEvent configurationChangeEvent);

}
