package khameleon.service.configurationchange.api;

import javax.annotation.Nonnull;

import anosym.feign.config.FeignConfigProvider;
import jakarta.enterprise.inject.spi.CDI;
import khameleon.access.ConfigProjectData;
import khameleon.config.ConfigurationChangeListenerConfig;
import khameleon.domain.facade.ConfigProjectDataFacade;

//@FeignConfig(KhameleonParameters.KHAMELEON_FEIGN_CLIENT_CHANGE_LISTENER)
public class ConfigurationChangeFeignClientProvider implements FeignConfigProvider {

    @Nonnull
    @Override
    public String[] getFeignClientInstanceIds(@Nonnull final String feignClientId) {
        final ConfigProjectDataFacade configProjectDataFacade = CDI.current().select(ConfigProjectDataFacade.class)
                .get();
        return configProjectDataFacade.findAll().stream().map(ConfigProjectData::getName).toArray(String[]::new);
    }

    @Nonnull
    @Override
    public String getEndPoint(@Nonnull final String feignClientInstanceId) {
        return CDI.current().select(ConfigurationChangeListenerConfig.class).get()
                .getProjectEndPoint(feignClientInstanceId);
    }

    @Override
    public boolean isHttpsEnabled(@Nonnull final String feignClientInstanceId) {
        return CDI.current().select(ConfigurationChangeListenerConfig.class).get()
                .isHttpsEnabled(feignClientInstanceId);
    }

}
