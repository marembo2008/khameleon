package khameleon.service.configurationchange;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.LocalBean;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.inject.Inject;
import khameleon.config.ApplicationContextConfig;
import khameleon.config.FeatureConfig;
import khameleon.config.HeaderConfig;
import khameleon.core.Configuration;
import khameleon.core.internal.localcache.LocalCacheService;
import khameleon.core.internal.service.changelistener.ConfigurationChangeEvent;
import khameleon.core.internal.service.changelistener.ConfigurationChangeType;
import khameleon.core.values.ConfigurationValue;
import khameleon.service.configurationchange.api.ConfigurationChangeListener;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkNotNull;
import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 * @author mochieng
 */
@Slf4j
@Stateless
@LocalBean
public class ConfigurationChangeListenerServiceProcessor implements Serializable {

  private static final long serialVersionUID = 124892902492L;

  @Inject
  private LocalCacheService localCacheService;

  @Asynchronous
  @TransactionAttribute(REQUIRES_NEW)
  public void onConfigurationChange(@Nonnull final ConfigurationValue configurationValue,
                                    @Nonnull final List<ConfigurationChangeListener> configurationChangeListeners) {
    checkNotNull(configurationValue, "The configurationValue must not be null");
    checkNotNull(configurationChangeListeners, "The configurationChangeListeners must not be null");

    if (isKhameleonInternalNamespace(configurationValue.getConfiguration().getNamespace().getSimpleName())) {
      updateKhameleonInternalConfiguration(configurationValue);
      //propagate changes to listeners who are interested in the khameleon configurtions.
    }

    final ConfigurationChangeEvent configurationChangeEvent
            = new ConfigurationChangeEvent(ConfigurationChangeType.UPDATE, configurationValue);
    configurationChangeListeners
            .stream()
            .peek((changeListener) -> log.debug("Configuration update notification to: {}", changeListener))
            .forEach((changeListener) -> {
              changeListener.onConfigurationChange(configurationChangeEvent);
            });
  }

  @Asynchronous
  @TransactionAttribute(REQUIRES_NEW)
  public void onConfigurationDeleted(@Nonnull final ConfigurationValue configurationValue,
                                     @Nonnull final List<ConfigurationChangeListener> configurationChangeListeners) {
    checkNotNull(configurationValue, "The configurationValue must not be null");
    checkNotNull(configurationChangeListeners, "The configurationChangeListeners must not be null");

    if (isKhameleonInternalNamespace(configurationValue.getConfiguration().getNamespace().getSimpleName())) {
      deleteKhameleonInternalConfiguration(configurationValue);
      //propagate changes to listeners who are interested in the khameleon configurtions.
    }

    final ConfigurationChangeEvent configurationChangeEvent
            = new ConfigurationChangeEvent(ConfigurationChangeType.DELETE, configurationValue);
    configurationChangeListeners
            .stream()
            .peek((changeListener) -> log.debug("Configuration delete notification to: {}", changeListener))
            .forEach((changeListener) -> {
              changeListener.onConfigurationChange(configurationChangeEvent);
            });
  }

  private boolean isKhameleonInternalNamespace(final String namespaceSimpleName) {
    return HeaderConfig.class.getCanonicalName().equalsIgnoreCase(namespaceSimpleName)
            || ApplicationContextConfig.class.getCanonicalName().equalsIgnoreCase(namespaceSimpleName)
            || FeatureConfig.class.getCanonicalName().equalsIgnoreCase(namespaceSimpleName);
  }

  private void updateKhameleonInternalConfiguration(@Nonnull final ConfigurationValue configurationValue) {
    try {
      final Configuration configuration = configurationValue.getConfiguration();
      localCacheService.saveConfiguration(configurationValue.getContext().getContextId(),
                                          configuration.getNamespace().getCanonicalName(),
                                          configurationValue);
    } catch (final Exception ex) {
      log.error("Error updating internal config.", ex);
    }
  }

  private void deleteKhameleonInternalConfiguration(@Nonnull final ConfigurationValue configurationValue) {
    try {
      final Configuration configuration = configurationValue.getConfiguration();
      localCacheService.removeConfiguration(configurationValue.getContext().getContextId(),
                                            configuration.getNamespace().getCanonicalName(),
                                            configurationValue);
    } catch (final Exception ex) {
      log.error("Error deleting internal config", ex);
    }
  }

}
