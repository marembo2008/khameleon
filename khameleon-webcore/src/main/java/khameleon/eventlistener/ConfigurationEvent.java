package khameleon.eventlistener;

/**
 *
 * @author marembo
 */
public enum ConfigurationEvent {

    CONFIGURATION_DELETE,
    CONFIGURATION_UPDATE,
    CONFIGURATION_ADD,;
}
