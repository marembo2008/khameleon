package khameleon.eventlistener;

import jakarta.enterprise.context.RequestScoped;

/**
 *
 * @author marembo
 */
@RequestScoped
public class ConfigurationEventContext {

  private boolean processingComplete;

  public boolean proceed() {
    return !processingComplete;
  }

  public void processingComplete() {
    processingComplete = true;
  }

}
