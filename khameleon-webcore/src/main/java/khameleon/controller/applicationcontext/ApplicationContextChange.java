package khameleon.controller.applicationcontext;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * @author marembo
 */
@Qualifier
@Documented
@Retention(RUNTIME)
@Target({METHOD, PARAMETER, TYPE, FIELD})
public @interface ApplicationContextChange {
}
