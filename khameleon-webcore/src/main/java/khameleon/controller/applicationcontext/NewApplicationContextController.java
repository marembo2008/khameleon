package khameleon.controller.applicationcontext;

import static khameleon.controller.JsfUtil.addErrorMessage;
import static khameleon.controller.JsfUtil.addSuccessMessage;
import static khameleon.controller.JsfUtil.addWarningMessage;
import static khameleon.controller.JsfUtil.getSelectItems;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;

import anosym.common.Country;
import anosym.common.Language;
import anosym.common.TLD;
import jakarta.ejb.EJB;
import jakarta.faces.model.SelectItem;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.config.FeatureConfig;
import khameleon.controller.navigation.NavigationController;
import khameleon.controller.navigation.NavigationTemplate;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Feature;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.service.applicationcontext.ApplicationContextManager;

/**
 *
 * @author marembo
 */
@Named
@ViewScoped
public class NewApplicationContextController implements Serializable {

	private static final Logger LOG = Logger.getLogger(NewApplicationContextController.class.getName());

	@EJB
	private ApplicationContextFacade applicationContextFacade;

	@EJB
	private ApplicationContextManager applicationContextManager;

	@Inject
	private ApplicationContextController applicationContextController;

	@Inject
	private NavigationController navigationController;

	@Inject
	private FeatureConfig featureConfigurationService;

	private ApplicationContext newApplicationContext;

	@Nonnull
	public ApplicationContext getNewApplicationContext() {
		if (newApplicationContext == null) {
			newApplicationContext = new ApplicationContext();
		}
		return newApplicationContext;
	}

	public String prepareNewApplicationContext() {
		// is it possible to create new application context!
		final List<Feature> enabledFeatures = ImmutableList.copyOf(featureConfigurationService.getFeatureEnabled());
		if (enabledFeatures.size() < 2) {
			addWarningMessage(
					"Cannot add new application context. You must enable more features to be able to add more contexts");
			return null;
		}
		// We only add the context for the current application mode.
		getNewApplicationContext().setApplicationMode(applicationContextController.getCurrentApplicationMode());
		this.navigationController.setNavigationTemplate(NavigationTemplate.NEW_APPLICATION_CONTEXT);
		return null;
	}

	public String endNewApplicationContext() {
		newApplicationContext = null;
		this.navigationController.resetNavigationTemplate();
		return null;
	}

	@Nonnull
	public SelectItem[] getLanguages() {
		return getSelectItems(Language.values(), true);
	}

	@Nonnull
	public SelectItem[] getTlds() {
		return getSelectItems(TLD.values(), true);
	}

	@Nonnull
	public SelectItem[] getCountries() {
		return getSelectItems(Country.values(), true);
	}

	@Nullable
	public String addApplicationContext() {
		try {
			if (applicationContextFacade.find(newApplicationContext.getContextId()) != null) {
				addErrorMessage("Application context with the same parameters already exists!");
				return null;
			}

			applicationContextManager.addNewApplicationContext(newApplicationContext,
					ImmutableList.<Configuration>of());
			addSuccessMessage("Successfully created new application context");
			applicationContextController.reloadApplicationContext();
		} catch (Exception ex) {
			LOG.log(Level.SEVERE, null, ex);
			addErrorMessage("Technical error adding new application context");
		} finally {
			endNewApplicationContext();
		}

		return null;
	}

}
