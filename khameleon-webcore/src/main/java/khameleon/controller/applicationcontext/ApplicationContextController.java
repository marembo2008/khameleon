package khameleon.controller.applicationcontext;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Event;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.config.ApplicationContextConfig;
import khameleon.config.HeaderConfig;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.domain.facade.ApplicationContextFacade;

import org.primefaces.component.tabview.Tab;
import org.primefaces.event.TabChangeEvent;

import static java.util.Arrays.binarySearch;
import static khameleon.controller.JsfUtil.getRequestParameter;
import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static com.google.common.base.MoreObjects.firstNonNull;
import static jakarta.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
public class ApplicationContextController implements Serializable {

  private static final Logger LOG = Logger.getLogger(ApplicationContextController.class.getName());

  private static final String APPLICATION_MODE_PARAM = "am";

  private static final Comparator<ApplicationMode> APPLICATION_MODE_COMPARATOR
          = (o1, o2) -> Integer.valueOf(o2.getOrder()).compareTo(o1.getOrder());

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @Inject
  private ApplicationContextConfig applicationContextConfig;

  @Inject
  private HeaderConfig headerConfigurationService;

  @Inject
  @ApplicationContextChange
  private Event<ApplicationContext> applicationContextChangeEvent;

  @Inject
  @ApplicationModeChange
  private Event<ApplicationMode> applicationModeEvent;

  private List<ApplicationContext> currentApplicationModeContexts;

  private Map<Integer, Integer> applicationContextTabIndeces;

  private ApplicationContext currentContext;

  private ApplicationMode[] applicationModes;

  private ApplicationMode currentApplicationMode;

  private int applicationContextActiveTab;

  public void onApplicationContextSelected(final TabChangeEvent tce) {
    final Tab tab = tce.getTab();
    if (tab == null) {
      return;
    }

    LOG.log(Level.INFO, "tab-attributes: {0}", tab.getAttributes());

    Optional.ofNullable(tab.getAttributes().get("data-context-id"))
            .map(String::valueOf)
            .map(Integer::parseInt)
            .map(applicationContextFacade::find)
            .ifPresent(this::onApplicationContextSelected);
  }

  public void onApplicationContextSelected0() {
    final String contextId = getRequestParameter("application_context_id");
    if (!Strings.isNullOrEmpty(contextId)) {
      final int contextId_ = Integer.parseInt(contextId.substring(4));
      final ApplicationContext applicationContext = applicationContextFacade.find(contextId_);
      onApplicationContextSelected(applicationContext);
    }
  }

  public void onApplicationContextSelected(@Nullable final ApplicationContext applicationContext) {
    if (applicationContext != null) {
      this.currentContext = applicationContext;
      onApplicationContextChanged();
    }
  }

  public void onApplicationContextChanged() {
    final Integer tabIndex = applicationContextTabIndeces.get(currentContext.getContextId());
    this.applicationContextActiveTab = firstNonNull(tabIndex, 0);
    this.applicationContextChangeEvent.fire(currentContext);
  }

  public void setApplicationContextActiveTab(int applicationContextActiveTab) {
    // ignore....
  }

  public int getApplicationContextActiveTab() {
    return applicationContextActiveTab;
  }

  public String getHeaderText() {
    return headerConfigurationService.getHeaderText();
  }

  public ApplicationContext getCurrentContext() {
    return currentContext;
  }

  public ApplicationMode getCurrentApplicationMode() {
    return getApplicationMode();
  }

  public boolean isCurrentApplicationMode(@Nullable final ApplicationMode applicationMode) {
    return getCurrentApplicationMode() == applicationMode;
  }

  public void reloadApplicationContext() {
    this.currentApplicationModeContexts = null;
  }

  public List<ApplicationContext> getCurrentApplicationModeContexts() {
    if (currentApplicationMode == null) {
      final ApplicationMode[] enabledModes = getEnabledApplicationModes();
      if (enabledModes.length > 0) {
        currentApplicationMode = enabledModes[0];
      } else {
        currentApplicationMode = DEVELOPMENT;
      }
    }
    if (currentApplicationModeContexts == null || currentApplicationModeContexts.isEmpty()) {
      currentApplicationModeContexts = applicationContextFacade.findApplicationContextByMode(
              currentApplicationMode);
      // Add the default Application Context at the beginning.
      currentApplicationModeContexts.add(0, DEFAULT_APPLICATION_CONTEXT);
      if (currentContext == null) {
        currentContext = currentApplicationModeContexts.get(0);
      }
      int index = 0;
      this.applicationContextTabIndeces = Maps.newHashMap();
      for (final ApplicationContext ac : currentApplicationModeContexts) {
        applicationContextTabIndeces.put(ac.getContextId(), index++);
      }
    }
    return currentApplicationModeContexts;
  }

  @Nonnull
  public ApplicationMode[] getEnabledApplicationModes() {
    if (applicationModes == null) {
      applicationModes = applicationContextConfig.getEnabledApplicationModes();
      Arrays.sort(applicationModes, APPLICATION_MODE_COMPARATOR);
    }
    return applicationModes;
  }

  public int getEnabledApplicationModesCount() {
    return getEnabledApplicationModes().length;
  }

  public String showApplicationMode(@Nonnull final ApplicationMode newApplicationMode) {
    if (newApplicationMode != currentApplicationMode) {
      setCurrentApplicationMode(newApplicationMode);
      clearFlashMessages();
      //Are we to redirect?
      final String appModeUlr = applicationContextConfig.getApplicationModeUrl(
              currentApplicationMode);
      if (!Strings.isNullOrEmpty(appModeUlr) && appModeUlr.startsWith("http")) {
        try {
          final String redirectUrl = appModeUlr + "?" + APPLICATION_MODE_PARAM + "=" + currentApplicationMode
                  .name();
          FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
        } catch (IOException ex) {
          LOG.log(Level.SEVERE, null, ex);
        }
      }
    }
    return null;
  }

  @PostConstruct
  void init() {
    // Set default mode from config service
    // If it is null, we should be guaranteed to be set by the initialization of the mode context to DEVELOPMENT.
    final ApplicationMode applicationMode = getApplicationMode();
    initialozeCurrentApplicationMode(applicationMode);
  }

  private ApplicationMode getApplicationMode() {
    final ApplicationMode currentapplicationMode;
    final String applicationMode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap()
            .get(APPLICATION_MODE_PARAM);
    if (!Strings.isNullOrEmpty(applicationMode)) {
      currentapplicationMode = ApplicationMode.valueOf(applicationMode);
    } else if (this.currentApplicationMode != null) {
      currentapplicationMode = this.currentApplicationMode;
    } else {
      currentapplicationMode = applicationContextConfig.getInitialApplicationMode();

    }
    final ApplicationMode[] enabledApplicationModes = getEnabledApplicationModes();
    final int isInitialApplicationModeEnabled = binarySearch(enabledApplicationModes, currentapplicationMode,
                                                             APPLICATION_MODE_COMPARATOR);
    if (isInitialApplicationModeEnabled > -1) {
      return currentapplicationMode;
    } else {
      final ApplicationMode defaultMode = enabledApplicationModes.length > 0 ? enabledApplicationModes[0] : ApplicationMode.DEVELOPMENT;
      LOG.log(Level.WARNING,
              "The khameleon configuration requests the following initial mode {0}, but it has not been enabled. {1}. "
              + "Defaulting to {2}",
              new Object[]{currentapplicationMode, enabledApplicationModes, defaultMode});
      return defaultMode;
    }
  }

  private void setCurrentApplicationMode(@Nonnull final ApplicationMode newApplicationMode) {
    this.currentApplicationModeContexts = null; //reset
    this.currentContext = null;
    initialozeCurrentApplicationMode(newApplicationMode);
    //notify everyone that we have changed.
    onApplicationContextChanged();
  }

  private void initialozeCurrentApplicationMode(@Nonnull final ApplicationMode newApplicationMode) {
    this.currentApplicationMode = newApplicationMode;
    getCurrentApplicationModeContexts();
    applicationModeEvent.fire(firstNonNull(this.currentApplicationMode, DEVELOPMENT));
  }

  private void clearFlashMessages() {

    final FacesContext cxt = getCurrentInstance();
    //TODO(marembo)
    //when we change application mode during login.
    //this is a hack needs to change.
    final Iterator<FacesMessage> it = cxt.getMessages();
    while (it.hasNext()) {
      it.next();
      it.remove();
    }
  }

}
