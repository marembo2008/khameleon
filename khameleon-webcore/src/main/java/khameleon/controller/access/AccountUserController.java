package khameleon.controller.access;

import static khameleon.controller.JsfUtil.addErrorMessage;
import static khameleon.controller.JsfUtil.addSuccessMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpSession;
import khameleon.access.UserAccount;
import khameleon.controller.JsfUtil;
import khameleon.controller.applicationcontext.ApplicationContextController;
import khameleon.controller.navigation.NavigationController;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;
import khameleon.domain.facade.UserAccountFacade;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
public class AccountUserController implements Serializable {

  private static final Logger LOG = Logger.getLogger(AccountUserController.class.getName());

  private static final long serialVersionUID = 1334843434903L;

  @EJB
  private UserAccountFacade userAccountFacade;

  @Inject
  private ApplicationContextController applicationContextController;

  @Inject
  private NavigationController navigationController;

  private UserAccount currentAccount;

  private String password;

  private String loginId;

  //new user details.
  private UserAccount newUserAccount;

  private List<ApplicationMode> selectedApplicationModes;

  private void checkApplicationModes() {
    if (selectedApplicationModes != null) {
      //something wrong with the list selection
      //it is returning a list of Strings instead of a list of ApplicationModes?????
      for (Object am : selectedApplicationModes) {
        ApplicationMode appMode;
        if (am instanceof ApplicationMode) {
          appMode = (ApplicationMode) am;
        } else {
          appMode = ApplicationMode.valueOf(am.toString());
        }
        newUserAccount.addAccess(appMode);
      }
    }
  }

  public List<ApplicationMode> getSelectedApplicationModes() {
    if (selectedApplicationModes == null) {
      selectedApplicationModes = new ArrayList<>();
    }
    return selectedApplicationModes;
  }

  public void setSelectedApplicationModes(List<ApplicationMode> applicationModes) {
    this.selectedApplicationModes = applicationModes;
  }

  public List<ApplicationMode> getLoggedInAccountApplicationModes() {
    return currentAccount.getAccessibleApplicationModes();
  }

  public void prepareAddAccountUser() {
    this.newUserAccount = new UserAccount();
    final ApplicationContext applicationContext = applicationContextController.getCurrentContext();
    this.newUserAccount.addAccess(applicationContext.getApplicationMode());
  }

  public UserAccount getNewUserAccount() {
    return newUserAccount;
  }

  public UserAccount getCurrentAccount() {
    return currentAccount;
  }

  public void addAccountUser() {
    try {
      checkApplicationModes();
      userAccountFacade.create(newUserAccount);
      newUserAccount = null;
      addSuccessMessage("Successfully created new user account");
    } catch (Exception e) {
      LOG.log(Level.SEVERE, "Error adding new user account", e);
      JsfUtil.addErrorMessage("Error adding new user account: " + e.getLocalizedMessage());
    }
  }

  public String logout() {
    try {
      final FacesContext context = FacesContext.getCurrentInstance();
      final HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
      if (session != null) {
        session.invalidate();
      }
      currentAccount = null;
      navigationController.resetNavigationTemplate();
    } catch (Exception e) {
      //ignore
    }
    return null;
  }

  public void login() {
    currentAccount = userAccountFacade.findAccount(loginId, password);
    if (currentAccount == null) {
      addErrorMessage("Error login you in. Invalid login ID or password");
    } else {
      //go to config-view
      this.loginId = null;
      this.password = null;
    }
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getLoginId() {
    return loginId;
  }

}
