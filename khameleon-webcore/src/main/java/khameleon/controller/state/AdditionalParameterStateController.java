package khameleon.controller.state;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.enterprise.context.Dependent;
import jakarta.inject.Named;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.AdditionalParameterConfigDataType;
import khameleon.core.EnumValue;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_BOOLEAN;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_CALENDAR;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_CONFIG_PROVIDED_OPTION;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_ENUM;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_NAMESPACE_PARAM;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_PATTERN;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_REQUIRED;

/**
 *
 * @author marembo
 */
@Named
@Dependent
public class AdditionalParameterStateController implements Serializable {

  private static final long serialVersionUID = 1343943943837L;

  private boolean isAdditionalParameterDefined(@Nullable final AdditionalParameter additionalParameter,
                                               @Nonnull final AdditionalParameterConfigDataType configDataType) {
    checkNotNull(configDataType, "The AdditionalParameterConfigDataType (configDataType) must be specified");

    if (additionalParameter == null) {
      return false;
    }

    final Optional<AdditionalParameterConfigData> configDataOptional
            = additionalParameter.getConfigData(configDataType);
    if (!configDataOptional.isPresent()) {
      return false;
    }

    final AdditionalParameterConfigData configData = configDataOptional.get();
    return configData.isBooleanData() ? configData.getBooleanValue() : true;
  }

  public boolean isRequired(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_REQUIRED);
  }

  public boolean isRegex(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_PATTERN);
  }

  public boolean isCalendar(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_CALENDAR);
  }

  public boolean isEnum(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_ENUM)
            || isAdditionalParameterDefined(additionalParameter, PARAMETER_CONFIG_PROVIDED_OPTION);
  }

  /**
   * Must be called after a check on the {@link #isEnum(khameleon.core.AdditionalParameter) }
   *
   * @param additionalParameter
   * @return
   */
  public List<EnumValue> getEnums(@Nonnull final AdditionalParameter additionalParameter) {
    checkNotNull(additionalParameter, "Additional parameter must be specified");

    final Optional<AdditionalParameterConfigData> apConfigData = additionalParameter.getConfigData(PARAMETER_ENUM);
    if (apConfigData.isPresent()) {
      return apConfigData.get().getEnumList();
    }

    final Optional<AdditionalParameterConfigData> providedConfigData
            = additionalParameter.getConfigData(PARAMETER_CONFIG_PROVIDED_OPTION);
    if (providedConfigData.isPresent()) {
      return providedConfigData.get().getEnumList();
    }

    return ImmutableList.<EnumValue>of();
  }

  public boolean isBoolean(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_BOOLEAN);
  }

  public boolean isNamespaceParam(@Nullable final AdditionalParameter additionalParameter) {
    return isAdditionalParameterDefined(additionalParameter, PARAMETER_NAMESPACE_PARAM);
  }

}
