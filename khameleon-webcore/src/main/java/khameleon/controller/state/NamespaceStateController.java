package khameleon.controller.state;

import com.google.common.base.Optional;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.core.processor.configuration.NamespaceApplicationContextHelper.unmarshall;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.xml.bind.JAXBException;
import khameleon.controller.applicationcontext.ApplicationContextController;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.core.NamespaceDataType;

/**
 *
 * @author marembo
 */
@Named
@Dependent
public class NamespaceStateController implements Serializable {

  private static final Logger LOG = Logger.getLogger(NamespaceStateController.class.getName());

  private static final int DEFAULT_CONTEXT_ID = DEFAULT_APPLICATION_CONTEXT.getContextId();

  private static final long serialVersionUID = 103733666L;

  @Inject
  private ApplicationContextController applicationContextController;

  public boolean isEnabledInApplicationContext(@Nullable final Namespace namespace) {
    if (namespace == null) {
      return false;
    }

    final ApplicationContext currentContext = applicationContextController.getCurrentContext();
    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(
            NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
    //if absent, then by default, it is defined for all context.
    if (namespaceData.isPresent()) {
      try {
        final NamespaceData appContextData = namespaceData.get();
        final List<ApplicationContext> appContexts = unmarshall(appContextData.getDataValue());
        return currentContext != null && appContexts.contains(currentContext);
      } catch (JAXBException ex) {
        LOG.log(Level.SEVERE, null, ex);
      }
    }
    return currentContext != null ? (currentContext.getContextId() != DEFAULT_CONTEXT_ID) : true;
  }

  //Strictly, the namespace declares that it is only to be defined in the current context..
  public boolean isDefinedInApplicationContext(@Nullable final Namespace namespace) {
    if (namespace == null) {
      return false;
    }
    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(
            NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
    //if absent, then by default, it is defined for all context.
    if (namespaceData.isPresent()) {
      try {
        final NamespaceData appContextData = namespaceData.get();
        final List<ApplicationContext> appContexts = unmarshall(appContextData.getDataValue());
        final ApplicationContext applicationContext = applicationContextController.getCurrentContext();
        return applicationContext != null && appContexts.contains(applicationContext);
      } catch (JAXBException ex) {
        LOG.log(Level.SEVERE, null, ex);
      }
    }
    return false;
  }

  //the namespace declares that it is defined in all context but the DEFAULT_CONTEXT.
  public boolean isDefinedInNoApplicationContext(@Nullable final Namespace namespace) {
    if (namespace == null) {
      return false;
    }

    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(
            NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT);
    return !namespaceData.isPresent();
  }

}
