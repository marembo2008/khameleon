package khameleon.controller.state;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.enterprise.context.Dependent;
import jakarta.faces.model.SelectItem;
import jakarta.inject.Named;
import khameleon.controller.JsfUtil;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.EnumValue;
import khameleon.core.values.ConfigurationValue;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_ARRAY;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CALLENDAR_FORMAT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_PROVIDED_OPTION;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONSTANT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_DEPRECATED;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_ENUM;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_MAX_LENGTH;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_NAMESPACE_PARAM;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_PASSWORD;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_RICH_TEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_TEXT;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_XML;

/**
 *
 * @author mochieng
 */
@Named
@Dependent
public class ConfigurationStateController implements Serializable {

  private boolean isConfigurationDataDefined(@Nullable final Configuration configuration,
                                             @Nonnull final ConfigurationDataOption configurationDataOption) {
    checkNotNull(configurationDataOption, "The ConfigurationDataOption (configurationDataOption) must be specfied");

    if (configuration == null) {
      return false;
    }
    final Optional<ConfigurationData> configDataOption = configuration.getConfigurationData(configurationDataOption);
    if (!configDataOption.isPresent()) {
      return false;
    }
    final ConfigurationData configData = configDataOption.get();
    return configData.isBooleanData() ? configData.getBooleanValue() : true;
  }

  public boolean isPassword(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_PASSWORD);
  }

  public boolean isMaxLengthSpecified(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_MAX_LENGTH);
  }

  public int getMaxLength(@Nonnull final Configuration configuration) {
    return configuration.getConfigurationData(CONFIGURATION_MAX_LENGTH).get().getIntValue();
  }

  public boolean isText(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_TEXT);
  }

  public boolean isRichText(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_RICH_TEXT);
  }

  public boolean isXml(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_XML);
  }

  public boolean isConstant(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_CONSTANT);
  }

  public boolean isEnum(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_ENUM)
            || isConfigurationDataDefined(configuration, CONFIGURATION_CONFIG_PROVIDED_OPTION);
  }

  /**
   * Must be called after a check to {@link #isEnum(khameleon.core.Configuration) }
   *
   * @param configuration
   * @return
   */
  public List<EnumValue> getEnums(@Nonnull final Configuration configuration) {
    final Optional<ConfigurationData> enumConfData = configuration.getConfigurationData(CONFIGURATION_ENUM);
    if (enumConfData.isPresent()) {
      return enumConfData.get().getEnumList();
    }

    final Optional<ConfigurationData> providedOptionConfData
            = configuration.getConfigurationData(CONFIGURATION_CONFIG_PROVIDED_OPTION);
    if (providedOptionConfData.isPresent()) {
      return providedOptionConfData.get().getEnumList();
    }

    return ImmutableList.of();
  }

  /**
   * Must be called after a check to {@link #isEnum(khameleon.core.Configuration) }
   *
   * @param configuration
   * @return
   */
  public SelectItem[] getEnumSelections(@Nonnull final Configuration configuration) {
    return JsfUtil.getSelectItems(getEnums(configuration), true, "---");
  }

  public boolean isArray(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_ARRAY);
  }

  public boolean isDeprecated(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_DEPRECATED);
  }

  public boolean isCalendar(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_CALLENDAR_FORMAT);
  }

  /**
   * Special handling of boolean check. The configuration type class is of primitive or wrapped boolean.
   *
   * @param configuration
   * @return
   */
  public boolean isBoolean(@Nullable final Configuration configuration) {
    return configuration != null
            && (configuration.getTypeClass().equals(boolean.class.getName())
            || configuration.getTypeClass().equals(Boolean.class.getName()));
  }

  public boolean isMultiple(@Nullable final Configuration configuration) {
    return configuration != null && !configuration.getAdditionalParameters().isEmpty();
  }

  public boolean isMultiple(@Nullable final ConfigurationValue configurationValue) {
    return configurationValue != null && !configurationValue.getAdditionalParameterKeyValues().isEmpty();
  }

  public boolean isNamespaceParam(@Nullable final Configuration configuration) {
    return isConfigurationDataDefined(configuration, CONFIGURATION_NAMESPACE_PARAM);
  }

}
