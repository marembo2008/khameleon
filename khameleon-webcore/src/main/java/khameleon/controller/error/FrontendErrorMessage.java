package khameleon.controller.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 *
 * @author marembo
 */
public final class FrontendErrorMessage {

    @Nonnull
    public static <T> T assertNotNull(@Nullable final T instance, @Nonnull final String messageParameter, @Nonnull final Object... args) {
        checkNotNull(messageParameter, "The messageParameter must not be null");
        if (instance == null) {
            throw new FrontendErroException(format(messageParameter, args));
        }
        return instance;
    }

    public static void assertState(final boolean state, @Nonnull final String messageParameter, @Nonnull final Object... args) {
        checkNotNull(messageParameter, "The messageParameter must not be null");
        if (!state) {
            throw new FrontendErroException(format(messageParameter, args));
        }
    }
}
