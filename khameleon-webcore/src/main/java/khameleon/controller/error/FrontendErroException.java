package khameleon.controller.error;

import jakarta.ejb.ApplicationException;

/**
 *
 * @author marembo
 */
@ApplicationException(inherited = true, rollback = true)
public class FrontendErroException extends RuntimeException {

  public FrontendErroException() {
  }

  public FrontendErroException(String message) {
    super(message);
  }

  public FrontendErroException(String message, Throwable cause) {
    super(message, cause);
  }

  public FrontendErroException(Throwable cause) {
    super(cause);
  }

  public FrontendErroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
