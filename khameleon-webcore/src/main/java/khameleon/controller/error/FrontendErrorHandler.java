package khameleon.controller.error;

import java.io.Serializable;

import javax.annotation.Nonnull;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Instance;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import khameleon.eventlistener.ConfigurationEventContext;

import static jakarta.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author marembo
 */
@Dependent
@Interceptor
@FrontendError
@Priority(Interceptor.Priority.APPLICATION)
public class FrontendErrorHandler implements Serializable {

  private static final long serialVersionUID = 134784939434L;

  @Inject
  private Instance<ConfigurationEventContext> configurationEventContext;

  @AroundInvoke
  public Object interceptErrorMessage(@Nonnull final InvocationContext context) throws Exception {
    try {
      return context.proceed();
    } catch (final FrontendErroException e) {
      final String message = e.getLocalizedMessage();
      final FacesContext facesContext = getCurrentInstance();
      final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
      facesContext.addMessage(null, facesMessage);
      facesContext.validationFailed();
      configurationEventContext.get().processingComplete();
    }
    return null;
  }

}
