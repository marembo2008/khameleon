package khameleon.controller.upload;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.LocalBean;
import jakarta.ejb.Stateless;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.spi.interactive.Config;
import khameleon.core.spi.interactive.ConfigNamespace;
import khameleon.core.spi.interactive.ConfigParameter;
import khameleon.core.spi.interactive.ConfigValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ApplicationContextFacade;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.domain.facade.NamespaceFacade;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo
 */
@Stateless
@LocalBean
public class ConfigurationUploadService {

  private static final Logger LOG = Logger.getLogger(ConfigurationUploadService.class.getName());

  @EJB
  private NamespaceFacade namespaceFacade;

  @EJB
  private ConfigurationFacade configurationFacade;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ApplicationContextFacade applicationContextFacade;

  @Asynchronous
  public void uploadConfigurationValues(@Nonnull final ConfigNamespace configNamespace,
                                        @Nonnull final ApplicationContext currentContext) {
    checkNotNull(configNamespace, "The confignamespace must not be null");
    checkNotNull(currentContext, "The currentContext must not be null");

    LOG.log(Level.INFO, "Uploading configurations for namespace: {0}", configNamespace.getName());

    final Namespace namespace = namespaceFacade.findByCanonicalName(configNamespace.getName());
    if (namespace == null) {
      LOG.log(Level.WARNING, "ConfigNamespace does not specify exisitng namespace: {0}", configNamespace);
      return;
    }

    final ApplicationContext registeredApplicationContext
            = applicationContextFacade.find(currentContext.getContextId());

    checkState(registeredApplicationContext != null,
               "There is no applicationcontext registered for: {%s}",
               currentContext);

    for (final Config config : configNamespace.getConfigs()) {
      final Configuration configuration
              = configurationFacade.findConfiguration(namespace.getCanonicalName(), config.getName());
      if (configuration == null) {
        LOG.log(Level.WARNING, "Did not find registered configuration with name: {0}, in namespace: {1}",
                new Object[]{config.getName(), namespace.getCanonicalName()});
        continue;
      }
      for (final ConfigValue value : config.getValues()) {
        final ConfigurationValue newValue = new ConfigurationValue(registeredApplicationContext, configuration);
        newValue.setConfigValue(value.getValue());
        //parameters
        for (final ConfigParameter param : value.getConfigParameters()) {
          if (!newValue.updateAdditionalParameterValue(param.getKey(), param.getValue())) {
            LOG.log(Level.WARNING, "Failed to update parameter: {0}", param);
          }
        }
        configurationValueFacade.create(newValue);
      }

    }
  }

}
