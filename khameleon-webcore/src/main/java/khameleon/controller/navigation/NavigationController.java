package khameleon.controller.navigation;

import java.io.Serializable;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.access.UserAccount;
import khameleon.controller.access.AccountUserController;
import khameleon.controller.applicationcontext.ApplicationContextController;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.controller.JsfUtil.addErrorMessage;
import static khameleon.controller.navigation.NavigationTemplate.CONFIGURATION_VIEW;
import static khameleon.controller.navigation.NavigationTemplate.LOGIN;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
public class NavigationController implements Serializable {

  @Inject
  private ApplicationContextController applicationContextController;

  @Inject
  private AccountUserController accountUserController;

  private NavigationTemplate navigationTemplate;

  public String getNavigationTemplate() {
    final UserAccount loggedInAccount = accountUserController.getCurrentAccount();
    if (loggedInAccount == null) {
      return LOGIN.getTemplate();
    }

    if (applicationContextController.getCurrentApplicationMode() == null) {
      addErrorMessage("Technical error, application mode not initialized.");
      return LOGIN.getTemplate();
    }

    if (!loggedInAccount.hasAccess(applicationContextController.getCurrentApplicationMode())) {
      addErrorMessage("You do not have access to the current application mode!");
      return LOGIN.getTemplate();
    }

    return firstNonNull(navigationTemplate, CONFIGURATION_VIEW).getTemplate();
  }

  public void setNavigationTemplate(@Nonnull final NavigationTemplate navigationTemplate) {
    this.navigationTemplate = checkNotNull(navigationTemplate, "The navigation template must not be null");
  }

  public void resetNavigationTemplate() {
    this.navigationTemplate = null;
  }

}
