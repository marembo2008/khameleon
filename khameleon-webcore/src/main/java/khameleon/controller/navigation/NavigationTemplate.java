package khameleon.controller.navigation;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo
 */
public enum NavigationTemplate {

    LOGIN("/templates/access/login-template.xhtml"),
    NEW_APPLICATION_CONTEXT("/templates/applicationcontext/new-application-context.xhtml"),
    CONFIGURATION_VIEW("/templates/config-template.xhtml"),
    CONFIG_PROJECTS("/templates/configprojects/configprojects.xhtml"),
    DYNAMIC_ADDITIONAL_PARAMETER("/templates/dynamicadditionalparameter/dynamicadditionalparameter.xhtml");

    private final String template;

    private NavigationTemplate(@Nonnull final String template) {
        this.template = template;
    }

    @Nonnull
    public String getTemplate() {
        return template;
    }

}
