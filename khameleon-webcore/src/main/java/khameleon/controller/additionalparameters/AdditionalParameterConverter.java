package khameleon.controller.additionalparameters;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import khameleon.core.AdditionalParameter;
import khameleon.domain.facade.AdditionalParameterFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 19, 2015, 1:55:31 AM
 */
@ApplicationScoped
@FacesConverter(forClass = AdditionalParameter.class)
public class AdditionalParameterConverter implements Converter {

  private static final Logger LOG = Logger.getLogger(AdditionalParameterConverter.class.getName());

  @EJB
  private AdditionalParameterFacade additionalParameterFacade;

  private final LoadingCache<String, AdditionalParameter> loadingCache = CacheBuilder
          .newBuilder()
          .expireAfterAccess(1, TimeUnit.DAYS)
          .concurrencyLevel(14)
          .maximumSize(1000)
          .build(new CacheLoader<String, AdditionalParameter>() {

            @Override
            public AdditionalParameter load(@Nonnull final String key) throws Exception {
              return additionalParameterFacade.find(key);
            }

          });

  @Override
  @Nullable
  public Object getAsObject(final FacesContext context, final UIComponent component, @Nullable final String value) {
    if (value == null) {
      return null;
    }

    try {
      return loadingCache.get(value);
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Did not retrieve any AdditionalParameter with paramId: {0}", value);
    }

    return null;
  }

  @Nullable
  @Override
  public String getAsString(final FacesContext context, final UIComponent component, @Nullable final Object value) {
    if (!(value instanceof AdditionalParameter)) {
      return null;
    }

    return ((AdditionalParameter) value).getParamId();
  }

}
