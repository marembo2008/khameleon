package khameleon.controller.additionalparameters;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.controller.navigation.NavigationController;
import khameleon.controller.navigation.NavigationTemplate;
import khameleon.core.AdditionalParameter;
import khameleon.domain.facade.AdditionalParameterFacade;
import lombok.Getter;
import lombok.Setter;

import static com.google.common.base.Strings.isNullOrEmpty;
import static khameleon.controller.JsfUtil.addErrorMessage;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 19, 2015, 1:19:27 AM
 */
@Named
@SessionScoped
public class DynamicAdditionalParameterController implements Serializable {

  @EJB
  private AdditionalParameterFacade additionalParameterFacade;

  @Inject
  private NavigationController navigationController;

  private List<AdditionalParameter> dynamicAdditionalParameters;

  @Setter
  @Getter
  private String paramaterKey;

  @Setter
  @Getter
  private String defaultParameterValue;

  @Setter
  @Getter
  private String parameterDescription;

  @Nullable
  public String prepareAddNewAdditionalParameter() {
    paramaterKey = null;
    defaultParameterValue = null;
    dynamicAdditionalParameters = null;
    navigationController.setNavigationTemplate(NavigationTemplate.DYNAMIC_ADDITIONAL_PARAMETER);
    return null;
  }

  @Nonnull
  public List<AdditionalParameter> getDynamicAdditionalParameters() {
    if (dynamicAdditionalParameters == null) {
      return (dynamicAdditionalParameters = additionalParameterFacade.findDynamicAdditionalParameters());
    }

    return dynamicAdditionalParameters;
  }

  public void addNewAdditionalParameter() {
    if (isNullOrEmpty(paramaterKey)) {
      addErrorMessage("Please specify the parameter key");
      return;
    }

    final AdditionalParameter newAdditionalParameter = new AdditionalParameter(paramaterKey);
    if (defaultParameterValue != null) {
      newAdditionalParameter.setDefaultValue(defaultParameterValue);
    }

    if (parameterDescription != null) {
      newAdditionalParameter.setParameterDescription(parameterDescription);
    }

    newAdditionalParameter.setDynamicParameter(true);
    additionalParameterFacade.create(newAdditionalParameter);

    //Reload parameters
    prepareAddNewAdditionalParameter();
  }

  @Nullable
  public String cancelAddNewDynamicAdditionalParameter() {
    prepareAddNewAdditionalParameter();
    navigationController.resetNavigationTemplate();
    return null;
  }

}
