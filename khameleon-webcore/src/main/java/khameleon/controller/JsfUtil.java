package khameleon.controller;

import java.util.List;
import java.util.function.Function;

import javax.annotation.Nonnull;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.model.SelectItem;

import static java.util.Objects.requireNonNull;

public class JsfUtil {

  public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
    int size = selectOne ? entities.size() + 1 : entities.size();
    SelectItem[] items = new SelectItem[size];
    int i = 0;
    if (selectOne) {
      items[0] = new SelectItem("", "select");
      i++;
    }
    for (Object x : entities) {
      items[i++] = new SelectItem(x, x.toString());
    }
    return items;
  }

  public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne,
                                            String noSelectionMessage) {
    int size = selectOne ? entities.size() + 1 : entities.size();
    SelectItem[] items = new SelectItem[size];
    int i = 0;
    if (selectOne) {
      items[0] = new SelectItem("", noSelectionMessage);
      i++;
    }
    for (Object x : entities) {
      items[i++] = new SelectItem(x, x.toString());
    }
    return items;
  }

  @Nonnull
  public static <T> SelectItem[] getSelectItems(@Nonnull final List<T> entities,
                                                final boolean selectOne,
                                                @Nonnull final String noSelectionMessage,
                                                @Nonnull final Function<T, String> transformer) {
    requireNonNull(entities, "The entities must not be null");
    requireNonNull(noSelectionMessage, "The noSelectionMessage must not be null");
    requireNonNull(transformer, "The transformer must not be null");

    final int size = selectOne ? entities.size() + 1 : entities.size();
    final SelectItem[] items = new SelectItem[size];
    int i = 0;
    if (selectOne) {
      items[0] = new SelectItem("", noSelectionMessage);
      i++;
    }

    for (final T entity : entities) {
      items[i++] = new SelectItem(entity, transformer.apply(entity));
    }

    return items;
  }

  public static <T> SelectItem[] getSelectItems(T[] entities, boolean selectOne) {
    int size = selectOne ? entities.length + 1 : entities.length;
    SelectItem[] items = new SelectItem[size];
    int i = 0;
    if (selectOne) {
      items[0] = new SelectItem("", "select");
      i++;
    }
    for (Object x : entities) {
      items[i++] = new SelectItem(x, x.toString());
    }
    return items;
  }

  public static <T> SelectItem[] getSelectItems(T[] entities, boolean selectOne,
                                                String noSelectionMessage) {
    int size = selectOne ? entities.length + 1 : entities.length;
    SelectItem[] items = new SelectItem[size];
    int i = 0;
    if (selectOne) {
      items[0] = new SelectItem("", noSelectionMessage);
      i++;
    }
    for (Object x : entities) {
      items[i++] = new SelectItem(x, x.toString());
    }
    return items;
  }

  public static void addErrorMessage(Exception ex, String defaultMsg) {
    String msg = ex.getLocalizedMessage();
    if (msg != null && msg.length() > 0) {
      addErrorMessage(msg);
    } else {
      addErrorMessage(defaultMsg);
    }
  }

  public static void addErrorMessages(List<String> messages) {
    for (String message : messages) {
      addErrorMessage(message);
    }
  }

  public static void addErrorMessage(String msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
  }

  public static void addWarningMessage(String msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg);
    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
  }

  public static void addErrorMessage(String description, Throwable error) {
    do {
      description += ", " + error.getLocalizedMessage();
      error = error.getCause();
    } while (error != null);
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, description, description);
    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
  }

  public static <T> void addErrorMessage(T msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg.toString(), msg.
                                             toString());
    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
  }

  public static void addSuccessMessage(String msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
    FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
  }

  public static <T> void addSuccessMessage(T msg) {
    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg.toString(), msg.toString());
    FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
  }

  public static String getRequestParameter(String key) {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
  }

  public static Object getObjectFromRequestParameter(String requestParameterName,
                                                     Converter converter, UIComponent component) {
    String theId = JsfUtil.getRequestParameter(requestParameterName);
    return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
  }

}
