package khameleon.controller.configproject;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Event;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.access.ConfigProjectData;
import khameleon.controller.navigation.NavigationController;
import khameleon.controller.navigation.NavigationTemplate;
import khameleon.domain.facade.ConfigProjectDataFacade;
import khameleon.service.configproject.OnConfigProjectAdded;

import org.primefaces.event.RowEditEvent;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
public class ConfigProjectDataController implements Serializable {

  private static final long serialVersionUID = 13439449330L;

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Inject
  private NavigationController navigationController;

  private ConfigProjectData configProjectData;

  @Inject
  @OnConfigProjectAdded
  private Event<String> onConfigprojectAddedEvent;

  public List<ConfigProjectData> getConfigProjectDatas() {
    return configProjectDataFacade.findAll();
  }

  @Nonnull
  public ConfigProjectData getConfigProjectData() {
    return configProjectData == null ? (configProjectData = new ConfigProjectData()) : configProjectData;
  }

  public void addOrConfigProjectData() {
    if (configProjectDataFacade.find(getConfigProjectData().getUuid()) != null) {
      configProjectDataFacade.edit(configProjectData);
    } else {
      configProjectDataFacade.create(configProjectData);
      onConfigprojectAddedEvent.fire(configProjectData.getName());
    }

    prepareCreateConfigProjectData();
  }

  public void updateConfigProjectData(@Nonnull final RowEditEvent rowEditEvent) {
    checkNotNull(rowEditEvent, "The rowEditEvent must not be null");

    final ConfigProjectData editedConfigProjectData = (ConfigProjectData) rowEditEvent.getObject();
    configProjectDataFacade.edit(editedConfigProjectData);
    prepareCreateConfigProjectData();
  }

  public String prepareCreateConfigProjectData() {
    this.configProjectData = null;
    getConfigProjectData();
    navigationController.setNavigationTemplate(NavigationTemplate.CONFIG_PROJECTS);
    return null;
  }

  public String cancelAddOrUpdateConfigData() {
    this.configProjectData = null;
    this.navigationController.resetNavigationTemplate();
    return null;
  }

}
