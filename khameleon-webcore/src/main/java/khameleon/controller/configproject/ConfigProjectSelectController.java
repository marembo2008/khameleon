package khameleon.controller.configproject;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import java.io.Serializable;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Event;
import jakarta.faces.model.SelectItem;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.access.ConfigProjectData;
import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.domain.facade.ConfigProjectDataFacade;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static khameleon.controller.JsfUtil.getSelectItems;
import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_PROJECT;
import static khameleon.core.internal.ConfigurationUtil.fromArrayString;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 17, 2016, 12:55:21 PM
 */
@Named
@SessionScoped
public class ConfigProjectSelectController implements Serializable {

  private static final long serialVersionUID = 434938493483l;

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Nullable
  private ConfigProjectData currentConfigProjectData;

  @Inject
  @ConfigProjectChange
  private Event<Boolean> onConfigProjectChange;

  @Nullable
  public ConfigProjectData getCurrentConfigProjectData() {
    return currentConfigProjectData;
  }

  public void setCurrentConfigProjectData(@Nullable final ConfigProjectData currentConfigProjectData) {
    this.currentConfigProjectData = currentConfigProjectData;
  }

  @Nonnull
  public SelectItem[] getConfigProjectDatas() {
    return getSelectItems(configProjectDataFacade.findAll(), true, "(All Config Projects)", (cf) -> toString(cf));
  }

  @Nonnull
  private String toString(@Nonnull final ConfigProjectData configProjectData) {
    return format("%s (%s)", configProjectData.getDescription(), configProjectData.getName());
  }

  public void onConfigProjectChanged() {
    onConfigProjectChange.fire(true);
  }

  public boolean isDefinedInProject(@Nonnull final Namespace namespace) {
    requireNonNull(namespace, "The namespace must not be null");

    if (currentConfigProjectData == null) {
      return true; //No selection yet
    }

    final Optional<NamespaceData> namespaceData = namespace.getNamespaceData(NAMESPACE_CONFIG_PROJECT);
    if (!namespaceData.isPresent()) {
      return true; //Defined in all config
    }

    final NamespaceData nd = namespaceData.get();

    //the service provides this as an array
    final String configProjectValues = nd.getDataValue();
    return ImmutableList.copyOf(fromArrayString(configProjectValues))
            .stream()
            .anyMatch((value) -> Objects.equals(value, currentConfigProjectData.getName()));
  }

}
