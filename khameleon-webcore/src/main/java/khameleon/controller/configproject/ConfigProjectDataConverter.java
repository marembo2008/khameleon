package khameleon.controller.configproject;

import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import khameleon.access.ConfigProjectData;
import khameleon.domain.facade.ConfigProjectDataFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 17, 2016, 1:01:15 PM
 */
@ApplicationScoped
@FacesConverter(forClass = ConfigProjectData.class)
public class ConfigProjectDataConverter implements Converter {

  @EJB
  private ConfigProjectDataFacade configProjectDataFacade;

  @Nullable
  @Override
  public Object getAsObject(final FacesContext context, final UIComponent component, @Nullable final String value) {
    if (value == null) {
      return null;
    }

    return configProjectDataFacade.find(Long.parseLong(value));
  }

  @Nullable
  @Override
  public String getAsString(final FacesContext context, final UIComponent component, @Nullable final Object value) {
    if (!(value instanceof ConfigProjectData)) {
      return null;
    }

    return ((ConfigProjectData) value).getUuid().toString();
  }

}
