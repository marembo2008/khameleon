package khameleon.controller.configuration.eventlistener.configregex;

import com.google.common.base.Optional;

import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.values.ConfigurationValue;
import khameleon.eventlistener.ConfigurationEventListener;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.controller.error.FrontendErrorMessage.assertState;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_REGEX;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_UPDATE;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ConfigRegexCheckConfigValue {

  public void checkConfigRegex(
          @Observes @ConfigurationEventListener(event = CONFIGURATION_UPDATE) @Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    final Configuration configuration = configurationValue.getConfiguration();
    final Optional<ConfigurationData> configurationDataOptional
            = configuration.getConfigurationData(CONFIGURATION_CONFIG_REGEX);
    if (configurationDataOptional.isPresent()) {
      final ConfigurationData configurationData = configurationDataOptional.get();
      final String configRegex = configurationData.getDataValue();
      final String configValue = configurationValue.getConfigValue();
      assertState(Pattern.matches(configRegex, configValue),
                  "The following configuration values {%s} do not match the required config regex {%s}",
                  configValue,
                  configRegex);
    }
  }

}
