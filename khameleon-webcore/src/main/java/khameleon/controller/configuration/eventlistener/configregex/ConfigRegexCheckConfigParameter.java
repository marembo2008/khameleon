package khameleon.controller.configuration.eventlistener.configregex;

import com.google.common.base.Optional;

import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import khameleon.core.AdditionalParameter;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.values.ConfigurationValue;
import khameleon.eventlistener.ConfigurationEventListener;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.controller.error.FrontendErrorMessage.assertState;
import static khameleon.core.AdditionalParameterConfigDataType.PARAMETER_REGEX;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_UPDATE;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ConfigRegexCheckConfigParameter {

  public void checkConfigRegex(
          @Observes @ConfigurationEventListener(event = CONFIGURATION_UPDATE) @Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    configurationValue
            .getAdditionalParameterValues()
            .stream()
            .parallel()
            .forEach((parameterValue) -> {
              final AdditionalParameter parameter = parameterValue.getAdditionalParameter();
              final Optional<AdditionalParameterConfigData> configDataOptional
                      = parameter.getConfigData(PARAMETER_REGEX);
              if (configDataOptional.isPresent()) {
                final String parameterRegex = configDataOptional.get().getConfigData();
                final String paramValue = parameterValue.getParameterValue();
                assertState(Pattern.matches(parameterRegex, paramValue),
                            "The follwoing parameter values {%s} does not match the required parameter regex {%s}",
                            paramValue,
                            parameterRegex);
              }
            });
  }

}
