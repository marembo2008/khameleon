package khameleon.controller.configuration.eventlistener.requires;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import khameleon.controller.error.FrontendErroException;
import khameleon.core.Configuration;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.eventlistener.ConfigurationEventListener;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_CONFIGNAME;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_NAMESPACE;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_REQUIRES_VALUE;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_ADD;
import static khameleon.eventlistener.ConfigurationEvent.CONFIGURATION_UPDATE;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class RequiresCheckConfigValue {

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  public void checkRequiresOnUpdate(
          @Observes @ConfigurationEventListener(event = CONFIGURATION_UPDATE) @Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    checkRequires(configurationValue);
  }

  public void checkRequiresOnAdd(
          @Observes @ConfigurationEventListener(event = CONFIGURATION_ADD) @Nonnull final ConfigurationValue configurationValue) {
    checkNotNull(configurationValue, "The configurationValue must not be null");

    checkRequires(configurationValue);
  }

  private void checkRequires(@Nonnull final ConfigurationValue configurationValue) {
    final Configuration conf = configurationValue.getConfiguration();
    final int contextId = configurationValue.getContext().getContextId();
    if (conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).isPresent()) {
      final String value = conf.getConfigurationData(CONFIGURATION_REQUIRES_VALUE).get().getDataValue();
      final String configName = conf.getConfigurationData(CONFIGURATION_REQUIRES_CONFIGNAME).get().getDataValue();
      final String namespace = conf.getConfigurationData(CONFIGURATION_REQUIRES_NAMESPACE).get().getDataValue();
      final List<ConfigurationValue> requiredValues
              = configurationValueFacade.findConfigurationValues(contextId, namespace, configName);

      if (requiredValues.isEmpty()) {
        final StringBuilder error = new StringBuilder()
                .append("Configuration: ")
                .append(conf.getConfigName())
                .append(", requires the following configuration values: ConfigName=")
                .append(configName)
                .append(", Namespace=")
                .append(namespace)
                .append(", ConfigValue=")
                .append(value);
        throw new FrontendErroException(error.toString());
      }

      requiredValues
              .parallelStream()
              .filter((cv) -> !Objects.equals(value, cv.getConfigValue()))
              .forEach((cv) -> {
                final StringBuilder error = new StringBuilder()
                        .append("Configuration: ")
                        .append(conf.getConfigName())
                        .append(", requires the following configuration values: ConfigName=")
                        .append(configName)
                        .append(", Namespace=")
                        .append(namespace)
                        .append(", ConfigValue=")
                        .append(value)
                        .append(", But found ConfigValue=")
                        .append(cv.getConfigValue());
                throw new FrontendErroException(error.toString());
              });
    }
  }

}
