package khameleon.controller.configuration;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Observes;
import jakarta.faces.model.DataModel;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.controller.JsfUtil;
import khameleon.controller.applicationcontext.ApplicationContextChange;
import khameleon.controller.applicationcontext.ApplicationContextController;
import khameleon.controller.error.FrontendError;
import khameleon.controller.namespace.NamespaceChange;
import khameleon.controller.namespace.NamespaceController;
import khameleon.controller.state.AdditionalParameterStateController;
import khameleon.controller.state.ConfigurationStateController;
import khameleon.controller.state.NamespaceStateController;
import khameleon.core.AdditionalParameter;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.EnumValue;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.AdditionalParameterValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import khameleon.service.ConfigurationValueUpdateService;
import khameleon.service.applicationcontext.ApplicationContextManager;

import org.primefaces.event.ReorderEvent;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;
import static khameleon.controller.JsfUtil.addErrorMessage;
import static khameleon.controller.JsfUtil.addSuccessMessage;
import static khameleon.controller.JsfUtil.getRequestParameter;
import static khameleon.controller.error.FrontendErrorMessage.assertNotNull;
import static khameleon.core.internal.ConfigurationUtil.appendValueToArrayStringIfNotMember;
import static khameleon.core.internal.ConfigurationUtil.fromArrayString;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
@FrontendError
public class ConfigurationController implements Serializable {

  private static final Logger LOG = Logger.getLogger(ConfigurationController.class.getName());

  private static final long serialVersionUID = 13438434384309L;

  @EJB
  private ConfigurationFacade configurationFacade;

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private ApplicationContextManager applicationContextManager;

  @EJB
  private ConfigurationValueUpdateService configurationValueUpdateService;

  @Inject
  private ConfigurationStateController configurationStateController;

  @Inject
  private AdditionalParameterStateController additionalParameterStateController;

  @Inject
  private ApplicationContextController applicationContextController;

  @Inject
  private NamespaceStateController namespaceStateController;

  @Inject
  private NamespaceController namespaceController;

  private final Predicate<Configuration> nonConstantConfiguration
          = (configuration) -> !configurationStateController.isConstant(configuration);

  private final Map<ConfigurationValueKey, ConfigurationValueModel> configurationValuesCache = Maps.newHashMap();

  private int configActiveTab;

  private List<Configuration> currentConfigurations;

  private List<String> currentConfigIds;

  private ConfigurationValue newConfigurationValue;

  private String configNameSearchQuery;

  public void onNamespaceChanged(@Observes @NamespaceChange @Nonnull final Namespace selectedNamespace) {
    resetConfigurationDetails();
  }

  public void onApplicationContextChanged(
          @Observes(during = AFTER_SUCCESS) @ApplicationContextChange @Nonnull final ApplicationContext currentContext) {
    resetConfigurationDetails();
  }

  private void resetConfigurationDetails() {
    this.configurationValuesCache.clear();
    this.currentConfigurations = null;
    this.configNameSearchQuery = null;
    this.configActiveTab = 0;
    this.newConfigurationValue = null;
    this.currentConfigIds = null;

  }

  public int getConfigActiveTab() {
    return configActiveTab;
  }

  public void setConfigActiveTab(int configActiveTab) {
    //ignore
  }

  public List<Configuration> getCurrentConfigurations() {
    final Namespace namespace = currentNamespace();
    if (currentConfigurations == null && namespace != null) {
      this.currentConfigurations = Lists.newArrayList(
              Collections2.filter(configurationFacade.findConfigurations(namespace), nonConstantConfiguration));
      initializeCurrentConfigurations();
    }
    return currentConfigurations;
  }

  private ApplicationContext applicationContext() {
    return applicationContextController.getCurrentContext();
  }

  @Nullable
  private Namespace currentNamespace() {
    return namespaceController.getCurrentNamespace();
  }

  public List<Configuration> getUndefinedConfigurationsForCurrentApplicationContext() {
    final Namespace namespace = currentNamespace();
    if (currentNamespace() == null || applicationContext() == null || namespace == null) {
      return ImmutableList.of();
    }
    return configurationFacade.findConfigurationsNotRegisteredForContext(namespace, applicationContext());
  }

  public void addConfigurationsToCurrentContext() {
  }

  public void onConfigurationSelected() {
    final String configId = getRequestParameter("config_id");
    if (!Strings.isNullOrEmpty(configId)) {
      final Integer tabIndex = currentConfigIds.indexOf(configId.substring(5));
      //if no tab found, show the first one.
      this.configActiveTab = Math.max(0, tabIndex);
    }
  }

  public String getComponentName(@Nullable final ConfigurationValue value) {
    return value == null ? "" : "CONFIG_VALUE_" + value.getValueId().replaceAll("-", ":");
  }

  public String getComponentName(@Nullable final AdditionalParameterValue parameterValue) {
    return parameterValue == null ? "" : "PARAM_VALUE_" + parameterValue.getParameterId().replaceAll("-", ":");
  }

  public String getComponentName(@Nullable final AdditionalParameter parameter) {
    return parameter == null ? "" : "PARAM_" + parameter.getParamId().replaceAll("-", ":");
  }

  public boolean isAddConfigValue(@Nullable final Configuration configuration) {
    if (configuration == null) {
      return false;
    }

    if (applicationContext().isDefaultContext()) {
      final boolean isDefinedInDefaultContext = namespaceStateController.isDefinedInApplicationContext(
              configuration.getNamespace());
      //We may have define the configuration only for default context
      if (!isDefinedInDefaultContext) {
        return false;
      }
    }

    return true;
  }

  public void onPageSelected(@Nullable final Configuration configuration, final int page) {
    final ApplicationContext applicationContext = applicationContext();
    if (configuration == null || applicationContext == null) {
      LOG.warning("Requesting configuration values when configuration or current applicationcontext is null");
      return;
    }

    final ConfigurationValueKey configurationValueKey = new ConfigurationValueKey(configuration, applicationContext);
    final ConfigurationValueModel cachedConfigurationValues = configurationValuesCache.get(configurationValueKey);
    cachedConfigurationValues.setCurrentPage(page);
  }

  public void onPreviousPage(@Nullable final Configuration configuration, final int currentPage) {
    onPageSelected(configuration, currentPage - 1); //Guaranteed to be more than 1.
  }

  public void onNextPage(@Nullable final Configuration configuration, final int currentPage) {
    onPageSelected(configuration, currentPage + 1); //Guaranteed to be less than page counts.
  }

  @Nullable
  public DataModel<ConfigurationValue> getConfigurationValues(@Nullable final Configuration configuration) {
    final ApplicationContext applicationContext = applicationContext();
    if (configuration == null || applicationContext == null) {
      LOG.warning("Requesting configuration values when configuration or current applicationcontext is null");
      return null;
    }

    final ConfigurationValueKey configurationValueKey = new ConfigurationValueKey(configuration, applicationContext);
    final DataModel<ConfigurationValue> cachedConfigurationValues = configurationValuesCache.get(
            configurationValueKey);
    if (cachedConfigurationValues != null) {
      return cachedConfigurationValues;
    }

    final ConfigurationValueModel newConfigurationValues
            = new ConfigurationValueModel(configuration, applicationContext, configurationValueFacade);
    configurationValuesCache.put(configurationValueKey, newConfigurationValues);
    return newConfigurationValues;
  }

  public void setNewConfigurationValue(ConfigurationValue newConfigurationValue) {
    this.newConfigurationValue = newConfigurationValue;
  }

  public ConfigurationValue getNewConfigurationValue() {
    return newConfigurationValue;
  }

  public boolean hasNewConfigurationValue(@Nullable final Configuration configuration) {
    if (configuration == null || newConfigurationValue == null) {
      return false;
    }

    return newConfigurationValue.getConfiguration().getConfigId().equals(configuration.getConfigId());
  }

  public void setConfigNameSearchQuery(String configNameSearchQuery) {
    this.configNameSearchQuery = configNameSearchQuery;
  }

  public String getConfigNameSearchQuery() {
    return configNameSearchQuery;
  }

  public void doSearchConfigurations() {
    final Namespace namespace = currentNamespace();
    if (namespace != null) {
      this.currentConfigurations = Lists.newArrayList(
              Collections2.filter(configurationFacade.searchConfigurations(namespace, configNameSearchQuery),
                                  nonConstantConfiguration));
      initializeCurrentConfigurations();
    }
  }

  public void deleteAllConfigurationValues(@Nullable final Configuration configuration) {
    if (configuration == null) {
      JsfUtil.addWarningMessage("Cannot delete configuration values for null configuration");
      return;
    }
    //delete all configuration values in the current namespace and context for the configuration.
    try {
      configurationValueFacade.deleteConfigurationValues(applicationContext(), configuration);
      JsfUtil.addSuccessMessage("successfully deleted all configuration values");
    } catch (Exception e) {
      LOG.log(Level.SEVERE, "Error deleting configuration values for: " + configuration, e);
      JsfUtil.addErrorMessage("Error deleting configuration values: " + configuration.getConfigName());
    }
  }

  private void resetConfigurationValues(@Nullable final Configuration configuration) {
    if (configuration == null) {
      return;
    }

    final ConfigurationValueKey valueKey = new ConfigurationValueKey(configuration, applicationContext());
    this.configurationValuesCache.remove(valueKey);
  }

  public void addConfigurationValue(@Nullable final Configuration configuration) {
    final Configuration configurationToSave = assertNotNull(configuration,
                                                            "Cannot add configuration values for null configuration");

    this.newConfigurationValue = applicationContextManager.addConfigurationValues(configurationToSave,
                                                                                  applicationContext());
    //force reload for the current configuration values!
    resetConfigurationValues(configuration);
    addSuccessMessage("Successfully created a new configuration...");
  }

  @Nonnull
  public List<String> getArrayValues(@Nullable final ConfigurationValue value) {
    if (value == null) {
      return ImmutableList.of();
    }

    //get the array list
    final String[] arrayValues = fromArrayString(value.getConfigValue());
    return Lists.newArrayList(arrayValues);
  }

  @Nonnull
  public List<EnumValue> getArrayEnumValues(@Nullable final ConfigurationValue value) {
    if (value == null) {
      return ImmutableList.of();
    }
    //get the array list
    final List<String> arrayValues = getArrayValues(value);
    final Map<String, EnumValue> enums = configurationStateController
            .getEnums(value.getConfiguration())
            .stream()
            .collect(Collectors.toMap(EnumValue::getEnumName, Function.identity()));

    return arrayValues
            .stream()
            .map((arrayValue) -> enums.get(arrayValue))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
  }

  private boolean isIncludedIn(@Nonnull final List<String> arrayValues, @Nonnull final EnumValue enumValue) {
    return arrayValues
            .stream()
            .anyMatch((value) -> Objects.equals(value.toLowerCase(), enumValue.getEnumName().toLowerCase()));
  }

  public void deleteArrayValue(@Nonnull final ConfigurationValue configurationValue, @Nonnull final String value) {
    checkNotNull(configurationValue, "The configurationValue must not be null");
    checkNotNull(value, "The value must not be null");

    final String[] arrayValues = FluentIterable
            .from(getArrayValues(configurationValue))
            .filter((input) -> !Objects.equals(input, value))
            .toArray(String.class);

    final String arrayValue = Arrays.toString(arrayValues);
    configurationValue.setConfigValue(arrayValue);
    configurationValueFacade.edit(configurationValue);

    addSuccessMessage("Successfully edited configuration value");
  }

  /**
   * Removes from the selection, already selected enum values.
   * <p>
   * @param value
   * @return
   */
  public List<EnumValue> getArrayEnumValuesForSelection(@Nullable final ConfigurationValue value) {
    if (value == null) {
      return Lists.newArrayList();
    }

    //get the array list
    final List<String> arrayValues = getArrayValues(value);
    final List<EnumValue> enums = configurationStateController.getEnums(value.getConfiguration());
    return enums
            .stream()
            .filter((enumValue) -> !isIncludedIn(arrayValues, enumValue))
            .collect(Collectors.toList());
  }

  public void onRowReorder(@Nonnull final ReorderEvent reorderEvent) {
    final String configValueId = getRequestParameter("configValueId");
    final ConfigurationValue configValueToUpdate;
    if (!isNullOrEmpty(configValueId)) {
      configValueToUpdate = configurationValueFacade.find(configValueId);
    } else {
      return;
    }

    if (configValueToUpdate == null) {
      return;
    }

    final String configValue = configValueToUpdate.getConfigValue();
    //Get the array values, we dont care about the actual type here, just reordering.
    final String[] arrayValues = fromArrayString(configValue);
    final int initialIndex = reorderEvent.getFromIndex();
    final int newIndex = reorderEvent.getToIndex();
    final String valueAtInitialIndex = arrayValues[initialIndex];
    final List<String> listValues = Lists.newArrayList(arrayValues);
    //Remove the value at initial index
    listValues.remove(initialIndex);

    //Set value at new index
    listValues.add(newIndex, valueAtInitialIndex);

    //Update config value
    final String updateConfigValued = Arrays.toString(listValues.toArray(new String[0]));
    configValueToUpdate.setConfigValue(updateConfigValued);
    configurationValueUpdateService.update(configValueToUpdate);

    addSuccessMessage("Successfully updated configuration value");
  }

  public void saveConfigurationValue() {
    final String configValueId = getRequestParameter("configValueId");
    final ConfigurationValue configValueToUpdate;
    if (!isNullOrEmpty(configValueId)) {
      configValueToUpdate = configurationValueFacade.find(configValueId);
    } else {
      return;
    }

    if (configValueToUpdate == null) {
      return;
    }

    //get the actual type of the value and set it up appropriateley.
    final String updatedValueComponentName = getComponentName(configValueToUpdate);
    final String updatedValue = getRequestParameter(updatedValueComponentName);
    updateConfigurationValue(configValueToUpdate, updatedValue);
    updateAdditionalParameterValues(configValueToUpdate);

    configValueToUpdate.setFrontendUpdated(true);
    configurationValueUpdateService.update(configValueToUpdate);

    addSuccessMessage("Successfully updated configuration value");

    if (newConfigurationValue != null) {
      newConfigurationValue = null;
    }
  }

  public void deleteConfigurationValue() {
    final String configValueId = getRequestParameter("configValueId");
    final ConfigurationValue configValueToUpdate;
    try {
      if (!Strings.isNullOrEmpty(configValueId)) {
        configValueToUpdate = configurationValueFacade.find(configValueId);
      } else {
        configValueToUpdate = null;
      }
      if (configValueToUpdate != null) {
        //reset the config values.
        configurationValueUpdateService.remove(configValueToUpdate);
        resetConfigurationValues(configValueToUpdate.getConfiguration());
        JsfUtil.addSuccessMessage("Successfully deleted configuration value");
      }
    } catch (Exception ex) {
      LOG.log(Level.SEVERE, "Technical error: " + configValueId, ex);
      JsfUtil.addErrorMessage("Technical error deleting configurations: " + configValueId);
    }
  }

  private void updateConfigurationValue(@Nonnull final ConfigurationValue configValue,
                                        @Nullable final String valueToUpdate) {
    final Configuration config = configValue.getConfiguration();
    if (configurationStateController.isBoolean(config)) {
      configValue.setConfigValue(firstNonNull(valueToUpdate, "false"));
    } else if (configurationStateController.isArray(config)) {
      if (!Strings.isNullOrEmpty(valueToUpdate)) {
        final String currentValue = configValue.getConfigValue();
        final String newUpdatedValue = appendValueToArrayStringIfNotMember(currentValue, valueToUpdate);
        configValue.setConfigValue(newUpdatedValue);
      }
    } else {
      //normal data, simply replace
      configValue.setConfigValue(valueToUpdate);
    }
  }

  public void doSearchConfigurationValues(@Nullable final Configuration config) {
    final String configId = getRequestParameter("configId");
    final Configuration configToSearch
            = config != null ? config : !isNullOrEmpty(configId) ? configurationFacade.find(configId) : null;
    if (configToSearch == null) {
      return;
    }

    //get the additional parameters with non-null values to search.
    final ConfigurationValueKey valueKey = new ConfigurationValueKey(configToSearch, applicationContext());
    final ConfigurationValueModel valueModel = (ConfigurationValueModel) configurationValuesCache.get(valueKey);
    if (valueModel == null) {
      LOG.log(Level.WARNING, "Retrieved null valuemodel for search configuration: {0}", valueKey);
      addErrorMessage("Technical error retrieving current value model");
      return;
    }

    final ImmutableList.Builder<AdditionalParameterKeyValue> builder = ImmutableList.builder();
    configToSearch
            .getAdditionalParameters()
            .stream()
            .forEach((ap) -> {
              final String apComponentName = getComponentName(ap);
              final String apSearchQuery = getRequestParameter(apComponentName);
              if (!Strings.isNullOrEmpty(apSearchQuery)) {
                builder.add(new AdditionalParameterKeyValue(ap.getParameterKey(), apSearchQuery));
              }
            });
    valueModel.withSearchParameters(builder.build().toArray(new AdditionalParameterKeyValue[0]));
  }

  private void updateAdditionalParameterValues(@Nonnull final ConfigurationValue valueToUpdate) {
    valueToUpdate.getAdditionalParameterValues()
            .stream()
            .forEach((apValue) -> {
              final String apValueComponentName = getComponentName(apValue);
              final String apValueToUpdate = getRequestParameter(apValueComponentName);
              final AdditionalParameter aParameter = apValue.getAdditionalParameter();
              if (additionalParameterStateController.isBoolean(aParameter)) {
                apValue.setParameterValue(firstNonNull(apValueToUpdate, "false"));
              } else {
                //just set the value as is!
                apValue.setParameterValue(apValueToUpdate);
              }
            });
  }

  private void initializeCurrentConfigurations() {
    currentConfigIds = currentConfigurations
            .stream()
            .map((config) -> config.getConfigId())
            .collect(Collectors.toList());
  }

}
