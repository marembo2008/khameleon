package khameleon.controller.configuration;

import com.google.common.collect.ImmutableList;

import static khameleon.controller.JsfUtil.getSelectItems;
import static khameleon.controller.error.FrontendErrorMessage.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.model.SelectItem;
import jakarta.inject.Named;
import khameleon.core.AdditionalParameter;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.AdditionalParameterFacade;
import khameleon.domain.facade.ConfigurationValueFacade;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 19, 2015, 2:38:34 AM
 */
@Named
@RequestScoped
public class DynamicAdditionalParamaterValueController {

  @EJB
  private ConfigurationValueFacade configurationValueFacade;

  @EJB
  private AdditionalParameterFacade additionalParameterFacade;

  @Getter
  @Setter
  private AdditionalParameter dynamicAdditionalParameter;

  private List<AdditionalParameter> dynamicAdditionalParameters;

  public boolean hasItems(@Nullable final ConfigurationValue configurationValue) {
    return !dynamicAdditionalParameters(configurationValue).isEmpty();
  }

  @Nonnull
  public SelectItem[] getDynamicAdditionalParameters(@Nullable final ConfigurationValue configurationValue) {
    if (configurationValue == null) {
      return new SelectItem[0];
    }

    final List<AdditionalParameter> additionalParameters
            = dynamicAdditionalParameters(configurationValue)
                    .stream()
                    .filter((dap) -> !configurationValue.getAdditionalParameterValue(dap.getParameterKey()).isPresent())
                    .collect(Collectors.toList());

    return getSelectItems(additionalParameters, true, "(Select Dynamic Parameter)", (ap) -> ap.getParameterKey());
  }

  public void addDynamicAdditionalParameter(@Nullable final ConfigurationValue configurationValue) {
    if (configurationValue == null) {
      return;
    }

    assertNotNull(dynamicAdditionalParameter, "Please select a dynamic attribute to add");

    configurationValue.addOrUpdateAdditionalParameterValue(dynamicAdditionalParameter,
                                                           dynamicAdditionalParameter.getDefaultValue());
    configurationValueFacade.edit(configurationValue);
  }

  @Nonnull
  private List<AdditionalParameter> dynamicAdditionalParameters(
          @Nullable final ConfigurationValue configurationValue) {
    if (configurationValue == null) {
      return ImmutableList.of();
    }

    if (dynamicAdditionalParameters == null) {
      dynamicAdditionalParameters = additionalParameterFacade.findDynamicAdditionalParameters();
    }

    return dynamicAdditionalParameters
            .stream()
            .filter((dap) -> !configurationValue.getAdditionalParameterValue(dap.getParameterKey()).isPresent())
            .collect(Collectors.toList());
  }

}
