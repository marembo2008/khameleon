package khameleon.controller.configuration;

import com.google.common.base.MoreObjects;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
class ConfigurationValueKey implements Serializable {

    private final Configuration configuration;
    private final ApplicationContext applicationContext;

    public ConfigurationValueKey(@Nonnull final Configuration configuration, @Nonnull final ApplicationContext applicationContext) {
        this.configuration = checkNotNull(configuration, "Configuration must be specified");
        this.applicationContext = checkNotNull(applicationContext, "the applicationcontext must be specified");
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.configuration, this.applicationContext);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConfigurationValueKey other = (ConfigurationValueKey) obj;
        return Objects.equals(this.configuration, other.configuration)
                && Objects.equals(this.applicationContext, other.applicationContext);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ApplicationContextId", applicationContext.getContextId())
                .add("configuration", configuration.getConfigName())
                .toString();
    }

}
