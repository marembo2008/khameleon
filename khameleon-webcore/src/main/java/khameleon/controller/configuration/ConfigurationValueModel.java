package khameleon.controller.configuration;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;

import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
public final class ConfigurationValueModel extends LazyDataModel<ConfigurationValue> implements Serializable {

    private static final Logger LOG = Logger.getLogger(ConfigurationValueModel.class.getName());

    private static final int PAGE_GROUP_SIZE = 10;

    private final Configuration configuration;

    private final ApplicationContext applicationContext;

    private final ConfigurationValueFacade configurationValueFacade;

    private List<AdditionalParameterKeyValue> searchParameterKeyValues;

    private final int pageSize;

    private int rowCounts;

    private int pageCounts;

    private int currentIndex;

    private List<Integer> pageIndeces;

    private List<Range<Integer>> pageGroups;

    public ConfigurationValueModel(@Nonnull final Configuration configuration,
            @Nonnull final ApplicationContext applicationContext,
            @Nonnull final ConfigurationValueFacade configurationValueFacade) {
        this.configuration = checkNotNull(configuration, "Configuration must be specified");
        this.applicationContext = checkNotNull(applicationContext, "Application context must be specified");
        this.configurationValueFacade = checkNotNull(configurationValueFacade,
                "configuration value facade must be specified");
        this.searchParameterKeyValues = ImmutableList.of();

        //Initialize paging and counts
        this.pageSize = 10;
        this.currentIndex = 0;
        withSearchParameters();
    }

    public final void withSearchParameters(@Nonnull final AdditionalParameterKeyValue... keyValues) {
        this.searchParameterKeyValues = ImmutableList.copyOf(keyValues);
        if (this.searchParameterKeyValues.isEmpty()) {
            //we may have deleted the search properties!
            rowCounts = this.configurationValueFacade.countConfigurationValues(applicationContext, configuration);
        } else {
            rowCounts = this.configurationValueFacade.countSearchConfigurationValues(applicationContext, configuration,
                    searchParameterKeyValues);
        }

        //Initialize page indeces;
        currentIndex = 0;
        pageCounts = (int) Math.ceil(rowCounts * 1.0f / pageSize);
        calculatePageGroups();
        calculatePageIndeces(1);
    }

    @Override
    public List<ConfigurationValue> load(int first,
            int pageSize,
            Map<String, SortMeta> sortBy,
            Map<String, FilterMeta> filterBy) {
        recalculateFirst(first, pageSize, rowCounts);
        setRowCount(rowCounts);
        if (searchParameterKeyValues.isEmpty()) {
            return configurationValueFacade.findConfigurationValues(applicationContext, configuration, first, pageSize);
        } else {
            return configurationValueFacade.searchConfigurationValues(
                    applicationContext, configuration, searchParameterKeyValues, first, pageSize);
        }
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        return 0;
    }

    private void calculatePageGroups() {
        pageGroups = Lists.newArrayList();
        for (int i = 1, j = PAGE_GROUP_SIZE; i < pageCounts; i += PAGE_GROUP_SIZE, j += PAGE_GROUP_SIZE) {
            final Range<Integer> pageGroup = Range.closed(i, Math.min(j, pageCounts));
            pageGroups.add(pageGroup);
        }

    }

    private void calculatePageIndeces(final int page) {
        for (final Range<Integer> pageGroup : pageGroups) {
            if (pageGroup.contains(page)) {
                pageIndeces = Lists.newArrayList(ContiguousSet.create(pageGroup, DiscreteDomain.integers()));
                break;
            }
        }
    }

    public boolean isShowPrevious() {
        return currentIndex > 0;
    }

    public boolean isShowNext() {
        return (currentIndex + 1) < pageCounts;
    }

    public int getCurrentPage() {
        return currentIndex + 1;
    }

    public int getPageCounts() {
        return pageCounts;
    }

    public boolean isSelectedPage(final int page) {
        return currentIndex == (page - 1);
    }

    public List<Integer> getPageIndeces() {
        return firstNonNull(pageIndeces, ImmutableList.<Integer>of());
    }

    public void setCurrentPage(final int page) {
        this.currentIndex = Math.max(page - 1, 0) % pageCounts;
        calculatePageIndeces(page);
    }

    public List<ConfigurationValue> getConfigurationValues() {
        final int offset = currentIndex * pageSize;

        if (searchParameterKeyValues.isEmpty()) {
            return configurationValueFacade.findConfigurationValues(applicationContext, configuration, offset, pageSize);
        } else {
            return configurationValueFacade.searchConfigurationValues(
                    applicationContext, configuration, searchParameterKeyValues, offset, pageSize);
        }
    }

}
