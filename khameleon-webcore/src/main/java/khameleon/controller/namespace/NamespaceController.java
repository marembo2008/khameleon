package khameleon.controller.namespace;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.controller.applicationcontext.ApplicationContextChange;
import khameleon.controller.configproject.ConfigProjectChange;
import khameleon.controller.configproject.ConfigProjectSelectController;
import khameleon.controller.state.NamespaceStateController;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.domain.facade.NamespaceFacade;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;

import static java.lang.String.format;

import static jakarta.enterprise.event.TransactionPhase.AFTER_SUCCESS;

/**
 *
 * @author marembo
 */
@Named
@SessionScoped
public class NamespaceController implements Serializable {

  private static final long serialVersionUID = 13483439304948L;

  private static final Function<Namespace, String> NAMESPACE_NAME = Namespace::getCanonicalName;

  @EJB
  private NamespaceFacade namespaceFacade;

  @Inject
  private NamespaceStateController namespaceStateController;

  @Inject
  private ConfigProjectSelectController configProjectSelectController;

  @Inject
  @NamespaceChange
  private Event<Namespace> namespaceChangeEvent;

  private List<String> allNamespaces;

  private List<Namespace> rootNamespaces;

  private Namespace currentNamespace;

  private TreeNode rootNamespaceTree;

  private TreeNode selectedNamespaceTree;

  private String searchQuery;

  @PostConstruct
  void loadRootNamespaces() {
    final List<Namespace> namespaces = namespaceFacade.findAll();
    this.allNamespaces = Lists.transform(namespaces, NAMESPACE_NAME);
    this.rootNamespaces = namespaceFacade.findRootNamespaces();
    initializeNamespaceTree();
  }

  public void onApplicationContextChanged(
          @Observes(during = AFTER_SUCCESS) @ApplicationContextChange @Nonnull final ApplicationContext currentContext) {
    this.rootNamespaceTree = null;
    this.selectedNamespaceTree = null;
    this.currentNamespace = null;
    initializeNamespaceTree();
  }

  public void onConfigProjectChanged(
          @Observes(during = AFTER_SUCCESS) @ConfigProjectChange @Nonnull final Boolean configProjectData) {
    this.rootNamespaceTree = null;
    this.selectedNamespaceTree = null;
    this.currentNamespace = null;
    initializeNamespaceTree();
  }

  public TreeNode getRootNamespaceTree() {
    return rootNamespaceTree;
  }

  @Nullable
  public String getSearchQuery() {
    return searchQuery;
  }

  public void setSearchQuery(@Nullable final String searchQuery) {
    this.searchQuery = searchQuery;
  }

  public void searchNamespaces() {
    this.currentNamespace = null;
    this.selectedNamespaceTree = null;

    if (isNullOrEmpty(searchQuery)) {
      this.rootNamespaces = namespaceFacade.findRootNamespaces();
    } else {
      this.rootNamespaces = namespaceFacade.searchNamespaces(searchQuery);
    }

    initializeNamespaceTree();

    //Because the selected namespace has changed due to search
    if (this.currentNamespace != null) {
      //We may not have found anything through search.
      this.namespaceChangeEvent.fire(currentNamespace);
    }
  }

  @Nullable
  public Namespace getCurrentNamespace() {
    return currentNamespace;
  }

  public String getCurrentNamespaceDisplayName() {
    return getNamespaceDisplayName(currentNamespace);
  }

  public String getCurrentNamespaceDescription() {
    return currentNamespace != null ? emptyToNull(currentNamespace.getDescription()) : null;
  }

  public String getNamespaceDisplayName(@Nullable final Namespace namespace) {
    //we may not have namespaces.
    if (namespace == null) {
      return null;
    }
    final String displayName = namespace.getDisplayName();
    final String simpleName = namespace.getSimpleName();
    return Objects.equals(displayName, simpleName) ? displayName : (format("%s (%s)", nullToEmpty(displayName),
                                                                           simpleName));
  }

  public void setSelectedNamespaceTree(TreeNode selectedNamespaceTree) {
    this.selectedNamespaceTree = selectedNamespaceTree;
  }

  public TreeNode getSelectedNamespaceTree() {
    return selectedNamespaceTree;
  }

  public void onNamespaceSelected() {
    if (this.selectedNamespaceTree != null) {
      this.selectedNamespaceTree.setExpanded(true);
      this.currentNamespace = (Namespace) this.selectedNamespaceTree.getData();
      this.namespaceChangeEvent.fire(currentNamespace);
    }
  }

  public List<String> getAllNamespaces() {
    return this.allNamespaces;
  }

  @Nonnull
  private Iterable<Namespace> currentNamespaces() {
    if (rootNamespaces == null) {
      return ImmutableList.of();
    }

    return rootNamespaces
            .stream()
            .filter(namespaceStateController::isEnabledInApplicationContext)
            .filter(configProjectSelectController::isDefinedInProject)
            .collect(Collectors.toList());
  }

  private void initializeNamespaceTree() {
    this.rootNamespaceTree = new DefaultTreeNode();
    for (final Namespace namespace : currentNamespaces()) {
      if (currentNamespace == null) {
        currentNamespace = namespace;
      }
      addNamespace(rootNamespaceTree, namespace, namespace == currentNamespace);
    }
  }

  private void addNamespace(@Nonnull final TreeNode parent, @Nonnull final Namespace namespace, boolean selected) {
    final DefaultTreeNode currentNode = new DefaultTreeNode(namespace, parent);
    currentNode.setSelected(selected);
    if (selected) {
      currentNode.setExpanded(selected);
    }
    //get children namespaces.
    final List<Namespace> childrenNamespaces = namespaceFacade.findChildNamespaces(namespace);
    if (!childrenNamespaces.isEmpty()) {
      for (final Namespace childNamespace : childrenNamespaces) {
        addNamespace(currentNode, childNamespace, childNamespace.equals(currentNamespace));
      }
    } else {
      currentNode.setType("child");
    }
  }

}
