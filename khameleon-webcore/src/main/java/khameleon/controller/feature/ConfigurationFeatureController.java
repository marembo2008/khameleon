package khameleon.controller.feature;

import com.google.common.collect.ImmutableList;

import static khameleon.core.Feature.APPLICATION_MODE;
import static khameleon.core.Feature.COUNTRY;
import static khameleon.core.Feature.LANGUAGE;
import static khameleon.core.Feature.TLD;

import java.io.Serializable;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import khameleon.config.FeatureConfig;
import khameleon.core.Feature;

/**
 *
 * @author marembo
 */
@SessionScoped
@Named
public class ConfigurationFeatureController implements Serializable {

  @Inject
  private FeatureConfig featureConfigurationService;

  public boolean isApplicationModeActive() {
    final List<Feature> enabledFeatures = ImmutableList.copyOf(featureConfigurationService.getFeatureEnabled());
    return enabledFeatures.contains(APPLICATION_MODE);
  }

  public boolean isTopLevelDomainActive() {
    final List<Feature> enabledFeatures = ImmutableList.copyOf(featureConfigurationService.getFeatureEnabled());
    return enabledFeatures.contains(TLD);
  }

  public boolean isLanguageActive() {
    final List<Feature> enabledFeatures = ImmutableList.copyOf(featureConfigurationService.getFeatureEnabled());
    return enabledFeatures.contains(LANGUAGE);
  }

  public boolean isCountryActive() {
    final List<Feature> enabledFeatures = ImmutableList.copyOf(featureConfigurationService.getFeatureEnabled());
    return enabledFeatures.contains(COUNTRY);
  }

}
