package khameleon.endpoint.registration;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.internal.authentication.Secured;
import khameleon.core.internal.service.ConfigurationRegistrationService;
import lombok.NonNull;

/**
 *
 * @author marembo
 */
@Secured
@Path("/")
@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ConfigurationRegistrationServiceEndPoint {

  @Inject
  private ConfigurationRegistrationService configurationRegistrationService;

  @POST
  @Path("/namespaces")
  public void registerNamespace(@NonNull final Namespace namespace) {
    configurationRegistrationService.registerNamespace(namespace);
  }

  @POST
  @Path("/configurations")
  public void registerConfiguration(@NonNull final Configuration configuration) {
    configurationRegistrationService.registerConfiguration(configuration);
  }

}
