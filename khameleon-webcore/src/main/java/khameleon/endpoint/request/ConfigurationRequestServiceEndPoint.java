package khameleon.endpoint.request;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import khameleon.core.configsource.ConfigSource;
import khameleon.core.configsource.Source;
import khameleon.core.internal.authentication.Secured;
import khameleon.core.spi.api.request.ConfigRequestApiDelegate;
import khameleon.core.values.ConfigurationValue;
import khameleon.core.values.ConfigurationValueRequest;
import lombok.NonNull;

/**
 *
 * @author marembo
 */
@Secured
@Path("/")
@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ConfigurationRequestServiceEndPoint {

  @Inject
  @ConfigSource(Source.CONFIG_SERVICE)
  private ConfigRequestApiDelegate configRequestApiDelegate;

  @POST
  @Path("/configuration-values")
  public ConfigurationValue findConfigurationValue(@NonNull final ConfigurationValueRequest configurationValueRequest) {
    return configRequestApiDelegate
            .findConfigurationValue(configurationValueRequest.getApplicationContext(),
                                    configurationValueRequest.getNamespaceCanonicalName(),
                                    configurationValueRequest.getConfigName(),
                                    configurationValueRequest.getParameterKeyValues());
  }

}
