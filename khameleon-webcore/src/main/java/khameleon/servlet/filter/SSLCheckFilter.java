package khameleon.servlet.filter;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import jakarta.inject.Inject;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import khameleon.config.ApplicationContextConfig;

/**
 *
 * @author marembo
 */
@WebFilter(urlPatterns = {"/*"})
public class SSLCheckFilter implements Filter {

  @Inject
  private ApplicationContextConfig applicationContextConfig;

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
                                                                                                   ServletException {
    final HttpServletRequest servletRequest = (HttpServletRequest) request;
    final HttpServletResponse servletResponse = (HttpServletResponse) response;
    final String servletPath = servletRequest.getServletPath();
    if (Objects.equals(servletPath, "/heartbeat.xhtml")) {
      servletResponse.setStatus(HttpServletResponse.SC_OK);
      return;
    }

    final boolean sslRequired = applicationContextConfig.isRequireSSL();
    final boolean isSecure = isSecure(servletRequest);
    if (!isSecure && sslRequired) {
      final String[] nonSSLPaths = applicationContextConfig.getNonSSLPaths();
      for (final String nonSSLPath : nonSSLPaths) {
        if (Pattern.matches(nonSSLPath, servletPath)) {
          chain.doFilter(request, response);
          return;
        }
      }
      final String host = servletRequest.getHeader("host");
      final String protocol = "https://";
      final String url = new StringBuilder(protocol).append(host).append(servletPath).toString();
      final String redirectUrl = servletResponse.encodeRedirectURL(url);
      servletResponse.sendRedirect(redirectUrl);
    } else {
      chain.doFilter(request, response);
    }
  }

  private boolean isSecure(@Nonnull final HttpServletRequest request) {
    if (request.isSecure()) {
      return true;
    }

    final String xproto = request.getHeader("X-FORWARDED-PROTO");
    return Objects.equals(xproto, "https");
  }

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
  }

}
