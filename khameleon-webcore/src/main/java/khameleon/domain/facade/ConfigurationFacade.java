package khameleon.domain.facade;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigurationFacade extends AbstractFacade<Configuration> {

  private static final Logger LOG = Logger.getLogger(ConfigurationFacade.class.getName());

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public ConfigurationFacade() {
    super(Configuration.class);
  }

  @Nullable
  public Configuration findConfiguration(@Nonnull final String namespace, @Nonnull final String configName) {
    checkNotNull(namespace, "CanonicalNamespace must not be null");
    checkNotNull(configName, "ConfigName must not be null");

    return em
            .createNamedQuery("Configuration.findConfigurationByCanonicalNamespaceAndConfigName",
                              Configuration.class)
            .setParameter("canonicalName", namespace)
            .setParameter("configName", configName)
            .getResultList()
            .stream()
            .findAny()
            .orElse(null);
  }

  public List<Configuration> findConfigurations(@Nonnull final Namespace namespace) {
    checkNotNull(namespace, "namespace must not be null");

    return listResult(em.createNamedQuery("Configuration.findConfigurationByNamespace", Configuration.class)
            .setParameter("namespaceId", namespace.getNamespaceId()), -1, -1);
  }

  public List<Configuration> searchConfigurations(@Nonnull final Namespace namespace,
                                                  @Nonnull final String configNameQuery) {
    checkNotNull(namespace, "The namespace must not be null");
    checkNotNull(configNameQuery, "the configname search query must not be null");

    return listResult(em.createNamedQuery("Configuration.searchConfigurationByNamespace")
            .setParameter("namespaceId", namespace.getNamespaceId())
            .setParameter("configName", "%" + configNameQuery + "%"), -1, -1);
  }

  public List<Configuration> findConfigurationsNotRegisteredForContext(@Nonnull final Namespace namespace,
                                                                       @Nonnull final ApplicationContext context) {
    checkNotNull(namespace, "The namespace must not be null");
    checkNotNull(context, "The context must not be null");

    return listResult(em.createNamedQuery("Configuration.findConfigurationsNotRegisteredForContext")
            .setParameter("namespaceId", namespace.getNamespaceId())
            .setParameter("contextId", context.getContextId()), -1, -1);
  }

}
