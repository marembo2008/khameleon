package khameleon.domain.facade;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.CharMatcher;
import com.google.common.base.Function;
import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import khameleon.core.ApplicationContext;
import khameleon.core.Configuration;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;

import static java.lang.String.format;
import static khameleon.core.values.helper.ConfigurationValueHelper.findUniqueKeyFromAdditionalParameterKeyValue;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;
import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigurationValueFacade extends AbstractFacade<ConfigurationValue> {

  private static final Logger LOG = Logger.getLogger(ConfigurationValueFacade.class.getName());

  private static final CharMatcher DOUBLE_QUOTE = CharMatcher.anyOf("\"").precomputed();

  private static final Function<AdditionalParameterKeyValue, Pair<String, Boolean>> SEARCH_FUNCTION_TRANSFORMER
          = (keyValue) -> {
            final String searchQuery = keyValue.getValue();
            final String searchQueryExact = DOUBLE_QUOTE.trimFrom(searchQuery);
            final boolean isExactSearch = (searchQuery.length() - searchQueryExact.length()) == 2;
            if (isExactSearch) {
              final String searchParam = format("%s:%s", keyValue.getKey(), searchQueryExact);
              return new Pair<>(searchParam, isExactSearch);
            } else {
              final String searchParam = format("%s:%%%s%%", keyValue.getKey(), searchQuery);
              return new Pair<>(searchParam, isExactSearch);
            }
          };

  private static final String SEARCH_QUERY_GROUP_BY = "GROUP BY cv.valueId";

  private static final String COUNT_SEARCH_QUERY
          = "SELECT DISTINCT COUNT(cv) FROM ConfigurationValue cv JOIN cv.additionalParameterValues apv "
          + "WHERE cv.context.contextId = :contextId AND cv.configuration.configId = :configId";

  private static final String FIND_SEARCH_QUERY
          = "SELECT DISTINCT cv FROM ConfigurationValue cv JOIN cv.additionalParameterValues apv "
          + "WHERE cv.context.contextId = :contextId AND cv.configuration.configId = :configId";

  private static final String ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY
          = "AND (SELECT COUNT(pv) FROM AdditionalParameterValue pv "
          + "WHERE pv.configurationValue.valueId = cv.valueId AND pv.parameterKeyValue LIKE :parameterKeyValue%s) > 0";

  private static final String ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY_EXACT
          = "AND (SELECT COUNT(pv) FROM AdditionalParameterValue pv "
          + "WHERE pv.configurationValue.valueId = cv.valueId  AND pv.parameterKeyValue = :parameterKeyValue%s) > 0";

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager entityManager;

  public ConfigurationValueFacade() {
    super(ConfigurationValue.class);
  }

  @Override
  protected EntityManager getEntityManager() {
    return entityManager;
  }

  /**
   * May potentially return several values if the configuration has additional parameters.
   */
  @Nonnull
  public List<ConfigurationValue> findConfigurationValues(
          @Nonnull final ApplicationContext applicationContext,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName) {
    checkNotNull(applicationContext, "The applicationContext must not be null");
    checkNotNull(namespace, "The namespace must not be null");
    checkNotNull(configName, "The configName must not be null");

    return findConfigurationValues(applicationContext.getContextId(), namespace.getCanonicalName(), configName);
  }

  /**
   * May potentially return several values if the configuration has additional parameters.
   */
  @Nonnull
  public List<ConfigurationValue> findConfigurationValues(
          final int contextId,
          @Nonnull final String namespaceCanonicalName,
          @Nonnull final String configName) {
    checkNotNull(namespaceCanonicalName, "The namespaceCanonicalName must not be null");
    checkNotNull(configName, "The configName must not be null");

    return entityManager.createNamedQuery("ConfigurationValue.findConfigurationValues")
            .setParameter("contextId", contextId)
            .setParameter("canonicalName", namespaceCanonicalName)
            .setParameter("configName", configName)
            .getResultList();
  }

  @Nonnull
  public List<ConfigurationValue> findConfigurationValues(
          @Nonnull final ApplicationContext applicationContext,
          @Nonnull final Namespace namespace,
          @Nonnull final String configName,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    checkNotNull(applicationContext, "The applicationContext must not be null");
    checkNotNull(namespace, "The namespace must not be null");
    checkNotNull(configName, "The configName must not be null");
    checkNotNull(additionalParameterKeyValues, "The additionalParameterKeyValues must not be null");

    final int contextId = applicationContext.getContextId();
    final String namespaceCanonicalName = namespace.getCanonicalName();
    return findConfigurationValues(contextId, namespaceCanonicalName, configName, additionalParameterKeyValues);
  }

  @Nonnull
  public List<ConfigurationValue> findConfigurationValues(
          final int contextId,
          @Nonnull final String namespaceCanonicalName,
          @Nonnull final String configName,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    checkNotNull(namespaceCanonicalName, "The namespaceCanonicalName must not be null");
    checkNotNull(configName, "The configName must not be null");
    checkNotNull(additionalParameterKeyValues, "The additionalParameterKeyValues must not be null");

    final String additionalParameterValuesUniqueKey
            = findUniqueKeyFromAdditionalParameterKeyValue(additionalParameterKeyValues);

    return entityManager.createNamedQuery("ConfigurationValue.findConfigurationValuesWithParameters")
            .setParameter("contextId", contextId)
            .setParameter("canonicalName", namespaceCanonicalName)
            .setParameter("configName", configName)
            .setParameter("additionalParameterValuesUniqueKey", additionalParameterValuesUniqueKey)
            .getResultList();
  }

  public List<ConfigurationValue> searchConfigurationValues(
          @Nonnull final ApplicationContext context,
          @Nonnull final Configuration configuration,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues,
          final int offset,
          final int pageCounts) {
    final StringBuilder searchQuery = new StringBuilder(FIND_SEARCH_QUERY);
    final StringBuilder searchQueryParam = new StringBuilder();
    final List<Pair<String, Boolean>> searchAdditionalParameterKeyValues
            = newArrayList(transform(additionalParameterKeyValues, SEARCH_FUNCTION_TRANSFORMER));
    final int parameterKeyValuesCount = searchAdditionalParameterKeyValues.size();
    for (int i = 0; i < parameterKeyValuesCount; i++) {
      final Pair<String, Boolean> searchParam = searchAdditionalParameterKeyValues.get(i);
      if (searchParam.value2) {
        searchQueryParam.append(" ").append(format(ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY_EXACT, i));
      } else {
        searchQueryParam.append(" ").append(format(ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY, i));
      }
    }
    searchQuery.append(searchQueryParam).append(" ").append(SEARCH_QUERY_GROUP_BY);
    LOG.log(Level.INFO, "searchQuery: \n{0}", searchQuery);

    final Query q = entityManager.createQuery(searchQuery.toString())
            .setParameter("contextId", context.getContextId())
            .setParameter("configId", configuration.getConfigId());
    for (int i = 0; i < parameterKeyValuesCount; i++) {
      final Pair<String, Boolean> parameterKeyValue = searchAdditionalParameterKeyValues.get(i);
      q.setParameter(format("parameterKeyValue%s", i), parameterKeyValue.value1);
    }
    return listResult(q, offset, pageCounts);
  }

  public int countSearchConfigurationValues(
          @Nonnull final ApplicationContext context,
          @Nonnull final Configuration configuration,
          @Nonnull final List<AdditionalParameterKeyValue> additionalParameterKeyValues) {
    final StringBuilder searchQuery = new StringBuilder(COUNT_SEARCH_QUERY);
    final StringBuilder searchQueryParam = new StringBuilder();
    final List<Pair<String, Boolean>> searchAdditionalParameterKeyValues
            = newArrayList(transform(additionalParameterKeyValues, SEARCH_FUNCTION_TRANSFORMER));
    final int parameterKeyValuesCount = searchAdditionalParameterKeyValues.size();
    for (int i = 0; i < parameterKeyValuesCount; i++) {
      final Pair<String, Boolean> searchParam = searchAdditionalParameterKeyValues.get(i);
      if (searchParam.value2) {
        searchQueryParam.append(" ").append(format(ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY_EXACT, i));
      } else {
        searchQueryParam.append(" ").append(format(ADDITIONAL_PARAMETER_VALUE_SEARCH_QUERY, i));
      }
    }
    searchQuery.append(searchQueryParam).append(" ").append(SEARCH_QUERY_GROUP_BY);
    LOG.log(Level.INFO, "searchQuery: \n{0}", searchQuery);

    final TypedQuery<Long> q = entityManager.createQuery(searchQuery.toString(), Long.class)
            .setParameter("contextId", context.getContextId())
            .setParameter("configId", configuration.getConfigId());
    for (int i = 0; i < parameterKeyValuesCount; i++) {
      final Pair<String, Boolean> parameterKeyValue = searchAdditionalParameterKeyValues.get(i);
      q.setParameter(format("parameterKeyValue%s", i), parameterKeyValue.value1);
    }
    return q.getSingleResult().intValue();
  }

  public List<ConfigurationValue> findConfigurationValues(@Nonnull final ApplicationContext context,
                                                          @Nonnull final Configuration configuration,
                                                          final int offset,
                                                          final int page) {
    checkNotNull(context, "ApplicationContext must be specified");
    checkNotNull(configuration, "Configuration must be specified");

    return listResult(entityManager.createNamedQuery(
            "ConfigurationValue.findConfigurationValuesByApplicationContextAndByConfigId")
            .setParameter("contextId", context.getContextId())
            .setParameter("configId", configuration.getConfigId()), offset, page);
  }

  public int countConfigurationValues(@Nonnull final ApplicationContext context,
                                      @Nonnull final Configuration configuration) {
    checkNotNull(context, "ApplicationContext must be specified");
    checkNotNull(configuration, "Configuration must be specified");

    return singleResult(entityManager.createNamedQuery("ConfigurationValue.countConfigurationValuesByConfigId",
                                                       Long.class)
            .setParameter("contextId", context.getContextId())
            .setParameter("configId", configuration.getConfigId())).intValue();
  }

  public void deleteConfigurationValues(@Nonnull final ApplicationContext applicationContext,
                                        @Nonnull final Configuration configuration) {
    checkNotNull(applicationContext, "ApplicationContext must be specified");
    checkNotNull(configuration, "Configuration must be specified");

    entityManager.createNamedQuery("ConfigurationValue.deleteConfigurationValues")
            .setParameter("contextId", applicationContext.getContextId())
            .setParameter("configId", configuration.getConfigId())
            .executeUpdate();
  }

  @Nullable
  @TransactionAttribute(REQUIRES_NEW)
  public ConfigurationValue findConfigurationValue(
          @Nonnull final ApplicationContext applicationContext,
          @Nonnull final Configuration configuration,
          @Nonnull final String additionalParameterValuesUniqueKey) {
    checkNotNull(applicationContext, "The applicationContext must not be null");
    checkNotNull(configuration, "The configuration must not be null");
    checkNotNull(additionalParameterValuesUniqueKey, "The additionalParameterValuesUniqueKey must not be null");

    //We should be guaranteed of a single result if any.
    final String namedQuery = "ConfigurationValue.findConfigurationValueForApplicationContextAndConfigurationAndParameterKeyValues";
    return singleResult(getEntityManager()
            .createNamedQuery(namedQuery, ConfigurationValue.class)
            .setParameter("contextId", applicationContext.getContextId())
            .setParameter("configId", configuration.getConfigId())
            .setParameter("additionalParameterValuesUniqueKey", additionalParameterValuesUniqueKey));
  }

  public List<ConfigurationValue> findConfigurationValues(@Nonnull final String configurationId, final int offset,
                                                          final int pageCount) {
    checkNotNull(configurationId, "The configurationId must not be null");

    return getEntityManager()
            .createNamedQuery("ConfigurationValue.findConfigurationValuesByConfigId")
            .setParameter("configId", configurationId)
            .setFirstResult(offset)
            .setMaxResults(pageCount)
            .getResultList();
  }

  private static final class Pair<V, P> {

    private V value1;

    private P value2;

    public Pair(V value1, P value2) {
      this.value1 = value1;
      this.value2 = value2;
    }

  }

}
