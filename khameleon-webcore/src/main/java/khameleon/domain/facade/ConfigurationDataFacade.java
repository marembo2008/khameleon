package khameleon.domain.facade;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.ConfigurationData;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigurationDataFacade extends AbstractFacade<ConfigurationData> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public ConfigurationDataFacade() {
    super(ConfigurationData.class);
  }

}
