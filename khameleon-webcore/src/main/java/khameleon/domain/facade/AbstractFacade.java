package khameleon.domain.facade;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 * @param <T>
 */
public abstract class AbstractFacade<T> implements Serializable {

  private static final long serialVersionUID = 134394839L;

  private final Class<T> entityClass;

  public AbstractFacade(Class<T> entityClass) {
    this.entityClass = entityClass;
  }

  @Nonnull
  public final Class<T> getEntityClass() {
    return entityClass;
  }

  protected abstract EntityManager getEntityManager();

  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void create(T entity) {
    getEntityManager().persist(entity);
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void edit(T entity) {
    getEntityManager().merge(entity);
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
  public void remove(T entity) {
    getEntityManager().remove(getEntityManager().merge(entity));
  }

  public T find(@Nonnull final Object id) {
    checkNotNull(id, "The entity id must not be null");
    return getEntityManager().find(entityClass, id);
  }

  public List<T> findAll() {
    jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().
            createQuery();
    cq.select(cq.from(entityClass));
    return getEntityManager().createQuery(cq).getResultList();
  }

  public List<T> findRange(int[] range) {
    jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().
            createQuery();
    cq.select(cq.from(entityClass));
    jakarta.persistence.Query q = getEntityManager().createQuery(cq);
    q.setMaxResults(range[1] - range[0] + 1);
    q.setFirstResult(range[0]);
    return q.getResultList();
  }

  public int count() {
    jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().
            createQuery();
    jakarta.persistence.criteria.Root<T> rt = cq.from(entityClass);
    cq.select(getEntityManager().getCriteriaBuilder().count(rt));
    jakarta.persistence.Query q = getEntityManager().createQuery(cq);
    return ((Long) q.getSingleResult()).intValue();
  }

  /**
   * Returns null if there is no result. Otherwise, returns the first result found.
   *
   * @param <T>
   * @param query
   * @return
   */
  protected static <T> T singleResult(TypedQuery<T> query) {
    List<T> result = query.getResultList();
    return result.isEmpty() ? null : result.get(0);
  }

  /**
   * Returns a list of results based on the offset and the maxResult count.
   *
   * if offset is less than zero, it is ignored.
   *
   * If maxresult is less than 1, it is ignored.
   *
   * @param <T>
   * @param query
   * @param offset
   * @param maxResult
   * @return
   */
  protected static <T> List<T> listResult(Query query, int offset, int maxResult) {
    if (offset >= 0) {
      query.setFirstResult(offset);
    }
    if (maxResult > 0) {
      query.setMaxResults(maxResult);
    }
    return query.getResultList();
  }

}
