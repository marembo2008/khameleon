package khameleon.domain.facade;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.AdditionalInformation;

/**
 *
 * @author marembo
 */
@Stateless
public class AdditionalInformationFacade extends AbstractFacade<AdditionalInformation> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public AdditionalInformationFacade() {
    super(AdditionalInformation.class);
  }

}
