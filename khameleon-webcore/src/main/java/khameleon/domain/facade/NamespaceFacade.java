package khameleon.domain.facade;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.Namespace;

import static com.google.common.base.Preconditions.checkNotNull;

import static java.lang.String.format;

/**
 *
 * @author marembo
 */
@Stateless
public class NamespaceFacade extends AbstractFacade<Namespace> {

  private static final Logger LOG = Logger.getLogger(NamespaceFacade.class.getName());

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public NamespaceFacade() {
    super(Namespace.class);
  }

  @Nullable
  public Namespace findByCanonicalName(@Nonnull final String canonicalName) {
    checkNotNull(canonicalName, "The canonicalName must be specified");

    return singleResult(getEntityManager()
            .createNamedQuery("Namespace.findNamespaceByCanonicalName", Namespace.class)
            .setParameter("canonicalName", canonicalName));
  }

  @Nonnull
  public List<Namespace> findRootNamespaces() {
    return listResult(getEntityManager()
            .createNamedQuery("Namespace.findRootNamespaces"), -1, -1);
  }

  @Nonnull
  public List<Namespace> findChildNamespaces(@Nonnull final Namespace namespace) {
    checkNotNull(namespace, "The namespace must be specified");

    return listResult(getEntityManager()
            .createNamedQuery("Namespace.findChildNamespaces")
            .setParameter("parentNamespaceId", namespace.getNamespaceId()), -1, -1);
  }

  public List<Namespace> searchNamespaces(@Nonnull final String searchQuery) {
    checkNotNull(searchQuery, "The namespace searchQuery must be specified");

    final String searchQueryParam = format("%%%s%%", searchQuery);
    final List<Namespace> namespaces = listResult(getEntityManager()
            .createNamedQuery("Namespace.searchNamespaces")
            .setParameter("searchQuery", searchQueryParam), -1, -1);
    return namespaces;
  }

}
