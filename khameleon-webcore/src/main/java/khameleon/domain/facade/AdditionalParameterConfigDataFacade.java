package khameleon.domain.facade;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.AdditionalParameterConfigData;
import khameleon.core.AdditionalParameterConfigDataType;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Stateless
public class AdditionalParameterConfigDataFacade extends AbstractFacade<AdditionalParameterConfigData> {

  private static final long serialVersionUID = -6350376491627125319L;

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public AdditionalParameterConfigDataFacade() {
    super(AdditionalParameterConfigData.class);
  }

  @Nonnull
  public List<AdditionalParameterConfigData> findAdditionalParameterConfigDataByDataType(
          @Nonnull final AdditionalParameterConfigDataType configDataType) {
    checkNotNull(configDataType, "The configDataType must not be null");

    return em
            .createNamedQuery("AdditionalParameterConfigData.findAdditionalParameterConfigDataByDataType")
            .setParameter("configDataType", configDataType)
            .getResultList();
  }

}
