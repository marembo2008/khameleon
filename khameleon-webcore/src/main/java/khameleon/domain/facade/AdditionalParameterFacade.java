package khameleon.domain.facade;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.AdditionalParameter;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo
 */
@Stateless
public class AdditionalParameterFacade extends AbstractFacade<AdditionalParameter> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public AdditionalParameterFacade() {
    super(AdditionalParameter.class);
  }

  @Nonnull
  public List<AdditionalParameter> findAdditionalParameterByKey(@Nonnull final String parameterKey,
                                                                final int offset,
                                                                final int limit) {
    requireNonNull(parameterKey, "The parameterKey must not be null");

    return em
            .createNamedQuery("AdditionalParameter.findAdditionalParameterByKey")
            .setParameter("parameterKey", parameterKey)
            .setFirstResult(offset)
            .setMaxResults(limit)
            .getResultList();
  }

  @Nonnull
  public List<AdditionalParameter> findDynamicAdditionalParameters() {
    return em
            .createNamedQuery("AdditionalParameter.findDynamicAdditionalParameters")
            .setParameter("dynamicParameter", true)
            .getResultList();
  }

}
