package khameleon.domain.facade;

import java.util.List;

import javax.annotation.Nonnull;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.AdditionalParameter;
import khameleon.core.values.AdditionalParameterValue;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Stateless
public class AdditionalParameterValueFacade extends AbstractFacade<AdditionalParameterValue> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public AdditionalParameterValueFacade() {
    super(AdditionalParameterValue.class);
  }

  public List<AdditionalParameterValue> findByAdditionalParameter(@Nonnull final AdditionalParameter additionalParameter) {
    checkNotNull(additionalParameter, "the additionalParameter must not be null");

    return em.createNamedQuery("AdditionalParameterValue.findByAdditionalParameter")
            .setParameter("paramId", additionalParameter.getParamId())
            .getResultList();
  }

}
