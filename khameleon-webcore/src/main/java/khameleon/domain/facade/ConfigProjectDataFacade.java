package khameleon.domain.facade;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.access.ConfigProjectData;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
@Stateless
public class ConfigProjectDataFacade extends AbstractFacade<ConfigProjectData> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public ConfigProjectDataFacade() {
    super(ConfigProjectData.class);
  }

  @Nullable
  public ConfigProjectData findConfigProjectByName(@Nonnull final String projectName) {
    checkNotNull(projectName, "the projectName should not be null");

    return singleResult(getEntityManager()
            .createNamedQuery("ConfigProjectData.findConfigProjectByName", ConfigProjectData.class)
            .setParameter("name", projectName));
  }

}
