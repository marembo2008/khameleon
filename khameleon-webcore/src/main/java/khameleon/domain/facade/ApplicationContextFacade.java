package khameleon.domain.facade;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.core.ApplicationContext;
import khameleon.core.ApplicationMode;

import static com.google.common.base.Preconditions.checkNotNull;

import static jakarta.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 *
 * @author marembo
 */
@Stateless
public class ApplicationContextFacade extends AbstractFacade<ApplicationContext> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  public ApplicationContextFacade() {
    super(ApplicationContext.class);
  }

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  @Nonnull
  public List<ApplicationContext> findApplicationContextByMode(@Nonnull final ApplicationMode applicationMode) {
    checkNotNull(applicationMode, "The applicationMode must be specified");

    return getEntityManager()
            .createNamedQuery("ApplicationContext.findApplicationContextByMode")
            .setParameter("applicationMode", applicationMode)
            .getResultList();
  }

  @Nullable
  public ApplicationContext findRootApplicationContextByMode(@Nonnull final ApplicationMode applicationMode) {
    checkNotNull(applicationMode, "The applicationMode must be specified");

    return getEntityManager()
            .createNamedQuery("ApplicationContext.findRootApplicationContextByMode", ApplicationContext.class)
            .setParameter("applicationMode", applicationMode)
            .getResultList()
            .stream()
            .findAny()
            .orElse(null);
  }

  @Nonnull
  @TransactionAttribute(REQUIRES_NEW)
  public List<ApplicationContext> findChildrenApplicationContext(final int contextId) {
    return em.createNamedQuery("ApplicationContext.findChildrenApplicationContext")
            .setParameter("contextId", contextId)
            .getResultList();
  }

}
