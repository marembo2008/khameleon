package khameleon.domain.facade;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import khameleon.access.UserAccount;

/**
 *
 * @author marembo
 */
@Stateless
public class UserAccountFacade extends AbstractFacade<UserAccount> {

  @PersistenceContext(unitName = "khameleon-web_war-SNAPSHOTPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public UserAccountFacade() {
    super(UserAccount.class);
  }

  public UserAccount findAccount(String loginId, String password) {
    return singleResult(getEntityManager()
            .createNamedQuery("UserAccount.findAccount", UserAccount.class)
            .setParameter("loginId", loginId)
            .setParameter("password", password));
  }

}
