package khameleon.service.registration.namespace.preprocessor;

import com.google.common.base.Optional;

import khameleon.core.Namespace;
import khameleon.core.NamespaceData;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.namespace.preprocessor.ConfigRenameNamespacePreProcessor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static khameleon.core.NamespaceDataType.NAMESPACE_CONFIG_RENAME;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class ConfigRenameNamespacePreProcessorTest {

  @Mock
  private NamespaceFacade namespaceFacade;

  @InjectMocks
  private ConfigRenameNamespacePreProcessor configRenameNamespacePreProcessor;

  @Test
  public void testNoConfigRenameWhenThereIsNoConfigRenameRequest() {
    final String canonicalName = "simple-namespace";
    final Namespace namespace = mock(Namespace.class);
    lenient().when(namespace.getCanonicalName()).thenReturn(canonicalName);
    lenient().when(namespace.getNamespaceData(NAMESPACE_CONFIG_RENAME)).thenReturn(Optional.<NamespaceData>absent());

    configRenameNamespacePreProcessor.preProcess(namespace);

    verify(namespace, times(1)).getNamespaceData(NAMESPACE_CONFIG_RENAME);
    verifyNoMoreInteractions(namespaceFacade);
  }

  public void testNoConfigRenameWhenTheOldNamespaceIsNotRegistered() {
    final String canonicalName = "simple-namespace";
    final Namespace namespace = mock(Namespace.class);
    lenient().when(namespace.getCanonicalName()).thenReturn(canonicalName);

    final String oldCanonicalName = "old-simple-namespace";
    final NamespaceData namespaceData = new NamespaceData(NAMESPACE_CONFIG_RENAME, oldCanonicalName);
    lenient().when(namespace.getNamespaceData(NAMESPACE_CONFIG_RENAME)).thenReturn(Optional.of(namespaceData));

    configRenameNamespacePreProcessor.preProcess(namespace);
    verify(namespaceFacade, times(1)).findByCanonicalName(canonicalName);
    verify(namespace, times(1)).getNamespaceData(NAMESPACE_CONFIG_RENAME);
    verify(namespaceFacade, times(0)).edit(namespace);
  }

  @Test
  public void testConfigRenameWhenTheOldNamespaceIsRegistered() {
    final String canonicalName = "simple-namespace";
    final Namespace namespace = mock(Namespace.class);
    lenient().when(namespace.getCanonicalName()).thenReturn(canonicalName);
    lenient().when(namespace.getSimpleName()).thenReturn(canonicalName);

    final String oldCanonicalName = "old-simple-namespace";
    final Namespace oldNamesapce = mock(Namespace.class);
    final NamespaceData namespaceData = new NamespaceData(NAMESPACE_CONFIG_RENAME, oldCanonicalName);
    lenient().when(namespace.getNamespaceData(NAMESPACE_CONFIG_RENAME)).thenReturn(Optional.of(namespaceData));
    lenient().when(oldNamesapce.getCanonicalName()).thenReturn(oldCanonicalName);
    lenient().when(namespaceFacade.findByCanonicalName(oldCanonicalName)).thenReturn(oldNamesapce);

    configRenameNamespacePreProcessor.preProcess(namespace);

    verify(namespaceFacade, times(1)).findByCanonicalName(oldCanonicalName);
    verify(namespace, times(1)).getNamespaceData(NAMESPACE_CONFIG_RENAME);
    verify(oldNamesapce, times(1)).setSimpleName(canonicalName);
    verify(namespaceFacade, times(1)).edit(oldNamesapce);
  }

}
