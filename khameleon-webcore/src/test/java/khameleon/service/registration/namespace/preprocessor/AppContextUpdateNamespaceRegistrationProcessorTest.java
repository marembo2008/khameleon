package khameleon.service.registration.namespace.preprocessor;

import khameleon.core.Namespace;
import khameleon.domain.facade.NamespaceFacade;
import khameleon.service.registration.namespace.preprocessor.AppContextUpdateNamespaceRegistrationProcessor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static khameleon.core.NamespaceDataType.NAMESPACE_APPLICATION_CONTEXT;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class AppContextUpdateNamespaceRegistrationProcessorTest {

  @Mock
  private NamespaceFacade namespaceFacade;

  @InjectMocks
  private AppContextUpdateNamespaceRegistrationProcessor appContextUpdateConfigurationRegistrationProcessor;

  @Test
  public void testRemoveAppContextNamespace() {
    final Namespace newNamespace = Namespace.builder().withSimpleName("config.namespace").build();
    final Namespace registeredNamespace = Namespace.builder().withSimpleName("config.namespace").build();
    registeredNamespace.addOrUpdateNamespaceData(NAMESPACE_APPLICATION_CONTEXT, "config-app-context");
    when(namespaceFacade.findByCanonicalName(registeredNamespace.getCanonicalName()))
            .thenReturn(registeredNamespace);

    //Check state before processing.
    assertThat(registeredNamespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT).isPresent(), is(true));

    //Process
    appContextUpdateConfigurationRegistrationProcessor.preProcess(newNamespace);

    //Check state after processing.
    assertThat(registeredNamespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT).isPresent(), is(false));
    verify(namespaceFacade, times(1)).edit(registeredNamespace);
  }

  @Test
  public void testNoAppContextIsRemovedFromNamespace() {
    final Namespace newNamespace = Namespace.builder().withSimpleName("config.namespace").build();
    newNamespace.addOrUpdateNamespaceData(NAMESPACE_APPLICATION_CONTEXT, "config-app-context");

    final Namespace registeredNamespace = Namespace.builder().withSimpleName("config.namespace").build();
    registeredNamespace.addOrUpdateNamespaceData(NAMESPACE_APPLICATION_CONTEXT, "config-app-context");

    //Check state before processing.
    assertThat(registeredNamespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT).isPresent(), is(true));

    //Process
    appContextUpdateConfigurationRegistrationProcessor.preProcess(newNamespace);

    //Check state after processing.
    assertThat(registeredNamespace.getNamespaceData(NAMESPACE_APPLICATION_CONTEXT).isPresent(), is(true));
    verify(namespaceFacade, times(0)).edit(registeredNamespace);
  }

}
