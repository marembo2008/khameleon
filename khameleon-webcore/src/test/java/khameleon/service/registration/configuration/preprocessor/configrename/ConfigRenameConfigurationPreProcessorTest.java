package khameleon.service.registration.configuration.preprocessor.configrename;

import javax.annotation.Nonnull;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.Namespace;
import khameleon.domain.facade.ConfigurationFacade;
import khameleon.service.registration.configuration.preprocessor.configrename.ConfigRenameConfigurationPreProcessor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.google.common.base.Preconditions.checkNotNull;
import static khameleon.core.ConfigurationDataOption.CONFIGURATION_CONFIG_RENAME;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class ConfigRenameConfigurationPreProcessorTest {

  private final Namespace namespace = Namespace.builder().withSimpleName("simple-namespace").build();

  @Mock
  private ConfigurationFacade configurationFacade;

  @InjectMocks
  private ConfigRenameConfigurationPreProcessor configRenameConfigurationPreProcessor;

  @Test
  public void testNoConfigRenameIfThereIsConfigurationWithCurrentConfigName() {
    final String configName = "registered-config-name";
    final Configuration configuration = createConfiguration(configName);
    lenient().when(configurationFacade.findConfiguration(checkNotNull(namespace.getCanonicalName()), configName))
            .thenReturn(
                    configuration);
    configRenameConfigurationPreProcessor.preProcess(configuration);

    verify(configurationFacade, times(1)).findConfiguration(checkNotNull(namespace.getCanonicalName()), configName);
  }

  @Test
  public void testNoConfigRenameIfThereNoConfigRenameRequest() {
    final String configName = "registered-config-name";
    final Configuration configuration = createConfiguration(configName);
    configRenameConfigurationPreProcessor.preProcess(configuration);

    verify(configurationFacade, times(1)).findConfiguration(checkNotNull(namespace.getCanonicalName()), configName);
  }

  public void testNoConfigRenameIfThereIsNoConfigurationWithTheSpecifiedOldConfigName() {
    final String configName = "registered-config-name";
    final String oldConfigName = "old-registered-config-name";
    final Configuration configuration = createConfiguration(configName, new ConfigurationData(
                                                            CONFIGURATION_CONFIG_RENAME, oldConfigName));
    configRenameConfigurationPreProcessor.preProcess(configuration);
    verify(configurationFacade, times(2)).findConfiguration(anyString(), anyString());
    verify(configurationFacade, times(0)).edit(configuration);
  }

  @Test
  public void testConfigRename() {
    final String configName = "registered-config-name";
    final String oldConfigName = "old-registered-config-name";
    final Configuration configuration = createConfiguration(configName, new ConfigurationData(
                                                            CONFIGURATION_CONFIG_RENAME, oldConfigName));
    final Configuration oldConfiguration = createConfiguration(oldConfigName);
    lenient().when(configurationFacade.findConfiguration(checkNotNull(namespace.getCanonicalName()), oldConfigName))
            .thenReturn(oldConfiguration);
    configRenameConfigurationPreProcessor.preProcess(configuration);

    verify(configurationFacade, times(1)).findConfiguration(checkNotNull(namespace.getCanonicalName()), configName);
    verify(configurationFacade, times(1)).findConfiguration(checkNotNull(namespace.getCanonicalName()),
                                                            oldConfigName);
    verify(configurationFacade, times(1)).edit(oldConfiguration);
  }

  private Configuration createConfiguration(@Nonnull final String configName,
                                            @Nonnull final ConfigurationData... configurationData) {
    final Configuration conf = new Configuration(namespace, configName, "typeclass");
    for (ConfigurationData cd : configurationData) {
      conf.addConfigurationData(cd);
    }
    return conf;
  }

}
