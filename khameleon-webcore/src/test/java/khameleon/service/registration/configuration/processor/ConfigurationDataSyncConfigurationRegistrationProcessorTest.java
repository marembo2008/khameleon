package khameleon.service.registration.configuration.processor;

import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.Namespace;
import khameleon.service.registration.configuration.processor.ConfigurationDataSyncConfigurationRegistrationProcessor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 *
 * @author marembo
 */
@ExtendWith(MockitoExtension.class)
public class ConfigurationDataSyncConfigurationRegistrationProcessorTest {

  @InjectMocks
  private ConfigurationDataSyncConfigurationRegistrationProcessor configurationDataSyncConfigurationRegistrationProcessor;

  @Test
  public void testNoConfigurationSyncDone() {
    final Configuration newConfiguration = createNewConfiguration();
    final Configuration oldConfiguration = createNewConfiguration();
    configurationDataSyncConfigurationRegistrationProcessor.preProcess(newConfiguration);
    configurationDataSyncConfigurationRegistrationProcessor.process(oldConfiguration);

    assertThat(newConfiguration.getConfigurationDatas(), hasSize(1));
    assertThat(oldConfiguration.getConfigurationDatas(), hasSize(1));

    final ConfigurationData configurationData = newConfiguration.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_ARRAY).get();
    final ConfigurationData configurationData1 = oldConfiguration.getConfigurationData(
            ConfigurationDataOption.CONFIGURATION_ARRAY).get();
    assertThat(configurationData.getDataValue(), is(configurationData1.getDataValue()));
  }

  @Test
  public void testConfigurationSyncDone() {
    final Configuration newConfiguration = createNewConfiguration();
    final Configuration oldConfiguration = createConfiguration();
    configurationDataSyncConfigurationRegistrationProcessor.preProcess(newConfiguration);

    //Sanity, before sync, we expect 2 data
    assertThat(oldConfiguration.getConfigurationDatas(), hasSize(2));
    assertThat(oldConfiguration.getConfigurationData(ConfigurationDataOption.CONFIGURATION_XML).isPresent(), is(true));

    //Then process
    configurationDataSyncConfigurationRegistrationProcessor.process(oldConfiguration);

    assertThat(newConfiguration.getConfigurationDatas(), hasSize(1));
    assertThat(oldConfiguration.getConfigurationDatas(), hasSize(1));
    assertThat(oldConfiguration.getConfigurationData(ConfigurationDataOption.CONFIGURATION_XML).isPresent(), is(false));
    assertThat(oldConfiguration.getConfigurationData(ConfigurationDataOption.CONFIGURATION_ARRAY).isPresent(), is(true));
  }

  private Configuration createNewConfiguration() {
    final Namespace namespace = Namespace.builder().withSimpleName("namespace-name").withDisplayName("displayName")
            .build();
    final Configuration configuration = new Configuration(namespace, "configName", "typeClass");
    configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_ARRAY, "an array");
    return configuration;
  }

  private Configuration createConfiguration() {
    final Configuration configuration = createNewConfiguration();

    //Add one configdata
    configuration.addConfigurationData(ConfigurationDataOption.CONFIGURATION_XML, "anyData");
    return configuration;
  }

}
