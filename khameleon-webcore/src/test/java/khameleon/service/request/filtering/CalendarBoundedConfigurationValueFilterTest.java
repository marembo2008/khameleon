package khameleon.service.request.filtering;

import static khameleon.core.ApplicationContext.DEFAULT_APPLICATION_CONTEXT;
import static khameleon.service.request.filtering.CalendarBoundedConfigurationValueFilter.CALENDAR_FORMAT;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.base.Optional;

import anosym.common.converter.CalendarConverter;
import anosym.common.converter.Converter;
import khameleon.core.AdditionalParameter;
import khameleon.core.Configuration;
import khameleon.core.ConfigurationData;
import khameleon.core.ConfigurationDataOption;
import khameleon.core.annotations.CalendarBounded;
import khameleon.core.annotations.Config;
import khameleon.core.initializer.ConfigurationInitializer;
import khameleon.core.initializer.DefaultConfigurationInitializor;
import khameleon.core.values.ConfigurationValue;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 11, 2016, 5:57:47 AM
 */
@ExtendWith(MockitoExtension.class)
public class CalendarBoundedConfigurationValueFilterTest {

	private static final Calendar NOW = Calendar.getInstance();

	static {
		// Aug
		NOW.set(2016, 7, 11, 06, 0, 0);
	}

	private ConfigurationInitializer configurationInitializer;

	private Converter<Calendar, String> calendarConverter;

	private CalendarBoundedConfigurationValueFilter configurationValueFilter;

	@BeforeEach
	public void setUp() {
		configurationInitializer = new DefaultConfigurationInitializor();
		calendarConverter = spy(new CalendarConverter());
		configurationValueFilter = spy(new CalendarBoundedConfigurationValueFilter(calendarConverter));
	}

	@Test
	public void verifyBoundedFromOnly() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		assertThat(configurations, hasSize(1));

		final Configuration configuration = configurations.get(0);
		final Optional<ConfigurationData> calendarBounded = configuration
				.getConfigurationData(ConfigurationDataOption.CONFIGURATION_CALENDAR_BOUNDED);
		assertThat(calendarBounded.isPresent(), is(true));

		final Optional<AdditionalParameter> boundedFromParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);
		assertThat(boundedFromParameter.isPresent(), is(true));
	}

	@Test
	public void allowedFromOnly() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedFromParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedFromParameter.get(), "2016-08-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(configurationValue));

		verify(calendarConverter, times(1)).from("2016-08-11 00:30:00", CALENDAR_FORMAT);
	}

	@Test
	public void notAllowedFromOnly() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedFromParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedFromParameter.get(), "2016-09-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(nullValue()));

		verify(calendarConverter, times(1)).from("2016-09-11 00:30:00", CALENDAR_FORMAT);
	}

	@Test
	public void allowedToOnly() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedToParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedToParameter.get(), "2016-09-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(configurationValue));

		verify(calendarConverter, times(1)).from("2016-09-11 00:30:00", CALENDAR_FORMAT);
	}

	@Test
	public void notAllowedToOnly() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedToParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedToParameter.get(), "2016-07-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(nullValue()));

		verify(calendarConverter, times(1)).from("2016-07-11 00:30:00", CALENDAR_FORMAT);
	}

	@Test
	public void allowed() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedToParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedToParameter.get(), "2016-09-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final Optional<AdditionalParameter> boundedFromParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedFromParameter.get(), "2016-07-01 00:30:00");

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(configurationValue));

		verify(calendarConverter, times(1)).from("2016-09-11 00:30:00", CALENDAR_FORMAT);
		verify(calendarConverter, times(1)).from("2016-07-01 00:30:00", CALENDAR_FORMAT);
	}

	@Test
	public void notAllowed() {
		final List<Configuration> configurations = configurationInitializer
				.getConfigurations(BoundedConfiguration.class);
		final Configuration configuration = configurations.get(0);

		final ConfigurationValue configurationValue = new ConfigurationValue(DEFAULT_APPLICATION_CONTEXT,
				configuration);
		final Optional<AdditionalParameter> boundedToParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_TO_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedToParameter.get(), "2016-09-11 00:30:00");
		doReturn(NOW).when(configurationValueFilter).getNow();

		final Optional<AdditionalParameter> boundedFromParameter = configuration
				.getAdditionalParameter(CalendarBounded.CALENDAR_BOUNDED_FROM_PARAM_KEY);
		configurationValue.addOrUpdateAdditionalParameterValue(boundedFromParameter.get(), "2016-09-01 00:30:00");

		final ConfigurationValue filteredValue = configurationValueFilter.filter(configurationValue);
		assertThat(filteredValue, is(nullValue()));

		verify(calendarConverter, times(1)).from("2016-09-11 00:30:00", CALENDAR_FORMAT);
		verify(calendarConverter, times(1)).from("2016-09-01 00:30:00", CALENDAR_FORMAT);
	}

	@Config
	static interface BoundedConfiguration {

		@CalendarBounded
		String getBoundedValue();

	}

}
