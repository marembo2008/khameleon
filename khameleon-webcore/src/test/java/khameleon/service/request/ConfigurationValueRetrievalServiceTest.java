package khameleon.service.request;

import static com.google.common.collect.ImmutableList.of;
import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.lenient;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.collect.ImmutableList;

import anosym.common.Country;
import khameleon.core.ApplicationContext;
import khameleon.core.Namespace;
import khameleon.core.values.AdditionalParameterKeyValue;
import khameleon.core.values.ConfigurationValue;
import khameleon.domain.facade.ConfigurationValueFacade;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 13, 2015, 6:53:50 PM
 */
@ExtendWith(MockitoExtension.class)
public class ConfigurationValueRetrievalServiceTest {

	private static final ApplicationContext APPLICATION_CONTEXT = new ApplicationContext(DEVELOPMENT, null, null,
			Country.CHAD);

	private static final ApplicationContext UNAVAILABLE_APPLICATION_CONTEXT = new ApplicationContext(DEVELOPMENT, null,
			null, Country.ALBANIA);

	private static final ApplicationContext PARENT_APPLICATION_CONTEXT = APPLICATION_CONTEXT
			.getParentApplicationContext();

	private static final String namespaceCanonicalName = "com.khameleon.config.test.RetrievaltestService";

	private static final Namespace NAMESPACE = Namespace.builder().withSimpleName(namespaceCanonicalName).build();

	private static final String configName = "testConfigName";

	private static final String configName2 = "testConfigName2";

	@Mock
	private ConfigurationValueFacade configurationValueFacade;

	@InjectMocks
	private ConfigurationValueRetrievalService configurationValueRetrievalService;

	@Mock
	private ConfigurationValue valueWithNoParameter;

	@Mock
	private ConfigurationValue valueWithOneParameter;

	@Mock
	private ConfigurationValue valueWithNoDynamicParameter;

	@Mock
	private ConfigurationValue valueWithOneDynamicParameterOnly;

	@Mock
	private ConfigurationValue valueWithTwoDynamicParametersOnly;

	@Mock
	private ConfigurationValue valueWithOneDynamicParameterAndANonDynamicParameter;

	@Mock
	private ConfigurationValue valueWithTwoDynamicParametersAndANonDynamicParameter;

	@Mock
	private ConfigurationValue valueWithOneParameterForParentApplicationContext;

	@Mock
	private ConfigurationValue valueWithNoDynamicParameterForParentApplicationContext;

	@Mock
	private ConfigurationValue valueWithOneDynamicParameterOnlyForParentApplicationContext;

	@Mock
	private ConfigurationValue valueWithTwoDynamicParametersOnlyForParentApplicationContext;

	@Mock
	private ConfigurationValue valueWithOneDynamicParameterAndANonDynamicParameterForParentApplicationContext;

	@Mock
	private ConfigurationValue valueWithTwoDynamicParametersAndANonDynamicParameterForParentApplicationContext;

	private AdditionalParameterKeyValue nonDynamicParameter = new AdditionalParameterKeyValue("non-dynamic", "true");

	private AdditionalParameterKeyValue nonExistingNonDynamicParameter = new AdditionalParameterKeyValue(
			"non-exisitng-non-dynamic", "true");

	private AdditionalParameterKeyValue dynamicParameter1 = new AdditionalParameterKeyValue("dynamic1", "true", true);

	private AdditionalParameterKeyValue dynamicParameter2 = new AdditionalParameterKeyValue("dynamic2", "true", true);

	private AdditionalParameterKeyValue nonExistingDynamicParameter1 = new AdditionalParameterKeyValue(
			"non-ex-dynamic1", "true", true);

	private AdditionalParameterKeyValue nonExistingDynamicParameter2 = new AdditionalParameterKeyValue(
			"non-ex-dynamic2", "true", true);

	private AdditionalParameterKeyValue nonDynamicParameterForParentApplicationContext = new AdditionalParameterKeyValue(
			"non-dynamic-pac", "true");

	private AdditionalParameterKeyValue dynamicParameter1ForParentApplicationContext = new AdditionalParameterKeyValue(
			"dynamic1-pac", "true", true);

	private AdditionalParameterKeyValue dynamicParameter2ForParentApplicationContext = new AdditionalParameterKeyValue(
			"dynamic2-pac", "true", true);

	@BeforeEach
	public void setUp() {
		lenient()
				.when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName, ImmutableList.of()))
				.thenReturn(ImmutableList.of(valueWithNoParameter));
		lenient().when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
				namespaceCanonicalName, configName)).thenReturn(ImmutableList.of(valueWithOneParameter));
		lenient()
				.when(configurationValueFacade.findConfigurationValues(PARENT_APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName))
				.thenReturn(ImmutableList.of(valueWithOneParameterForParentApplicationContext));
		lenient()
				.when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName, ImmutableList.of(nonDynamicParameter)))
				.thenReturn(of(valueWithNoDynamicParameter));
		lenient().when(configurationValueFacade.findConfigurationValues(PARENT_APPLICATION_CONTEXT.getContextId(),
				namespaceCanonicalName, configName, ImmutableList.of(nonDynamicParameterForParentApplicationContext)))
				.thenReturn(of(valueWithNoDynamicParameterForParentApplicationContext));
		lenient()
				.when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName, ImmutableList.of(dynamicParameter1)))
				.thenReturn(of(valueWithOneDynamicParameterOnly));
		lenient().when(configurationValueFacade.findConfigurationValues(PARENT_APPLICATION_CONTEXT.getContextId(),
				namespaceCanonicalName, configName, ImmutableList.of(dynamicParameter1ForParentApplicationContext)))
				.thenReturn(of(valueWithOneDynamicParameterOnlyForParentApplicationContext));
		lenient()
				.when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName, ImmutableList.of(dynamicParameter1, nonDynamicParameter)))
				.thenReturn(of(valueWithOneDynamicParameterAndANonDynamicParameter));
		lenient()
				.when(configurationValueFacade.findConfigurationValues(PARENT_APPLICATION_CONTEXT.getContextId(),
						namespaceCanonicalName, configName,
						ImmutableList.of(dynamicParameter1ForParentApplicationContext,
								nonDynamicParameterForParentApplicationContext)))
				.thenReturn(of(valueWithOneDynamicParameterAndANonDynamicParameterForParentApplicationContext));

		lenient().when(configurationValueFacade.findConfigurationValues(APPLICATION_CONTEXT.getContextId(),
				namespaceCanonicalName, configName2)).thenReturn(of(valueWithNoDynamicParameter));

	}

	@Test
	public void testDynamicAdditionalParameterCombinationsWithoutDefaultAdditionalParameters() {
		final List<AdditionalParameterKeyValue> keyValues = ImmutableList.of(dynamicParameter1, dynamicParameter2);
		final List<List<AdditionalParameterKeyValue>> combinedKeyValues = configurationValueRetrievalService
				.getCombinationsStream(keyValues).collect(Collectors.toList());
		assertThat(combinedKeyValues, hasSize(3));

		final List<List<AdditionalParameterKeyValue>> sortedCombinedKeyValues = combinedKeyValues.stream()
				.sorted((list1, list2) -> Integer.valueOf(list2.size()).compareTo(list1.size()))
				.collect(Collectors.toList());
		assertThat(sortedCombinedKeyValues.get(0), hasSize(2));
		assertThat(sortedCombinedKeyValues.get(1), hasSize(1));
		assertThat(sortedCombinedKeyValues.get(2), hasSize(1));
	}

	@Test
	public void testDynamicAdditionalParameterCombinationsWithDefaultAdditionalParameters() {
		final List<AdditionalParameterKeyValue> dynamicKeyValues = ImmutableList.of(dynamicParameter1,
				dynamicParameter2);
		final List<AdditionalParameterKeyValue> nonDynamicKeyValues = ImmutableList.of(nonDynamicParameter,
				nonDynamicParameterForParentApplicationContext);
		final List<List<AdditionalParameterKeyValue>> combinedKeyValues = configurationValueRetrievalService
				.getCombinedKeyValues(dynamicKeyValues, nonDynamicKeyValues);
		assertThat(combinedKeyValues, hasSize(3));

		final List<List<AdditionalParameterKeyValue>> sortedCombinedKeyValues = combinedKeyValues.stream()
				.sorted((list1, list2) -> Integer.valueOf(list2.size()).compareTo(list1.size()))
				.collect(Collectors.toList());
		assertThat(sortedCombinedKeyValues.get(0), hasSize(4));
		assertThat(sortedCombinedKeyValues.get(1), hasSize(3));
		assertThat(sortedCombinedKeyValues.get(2), hasSize(3));
	}

	@Test
	public void testConfigurationValueWithoutAdditionalParameters() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName);
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithOneParameter));
	}

	@Test
	public void testConfigurationValueWithoutAdditionalParametersWithFallback() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(UNAVAILABLE_APPLICATION_CONTEXT, NAMESPACE, configName);
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithOneParameterForParentApplicationContext));
	}

	@Test
	public void testConfigurationValueWithDefaultAdditionalParameters() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(nonDynamicParameter));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithNoDynamicParameter));
	}

	@Test
	public void testConfigurationValueWithDefaultAdditionalParametersWithFallback() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(UNAVAILABLE_APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(nonDynamicParameterForParentApplicationContext));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithNoDynamicParameterForParentApplicationContext));
	}

	@Test
	public void testConfigurationValueWithOneDynamicAdditionalParameterOnly() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(dynamicParameter1));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithOneDynamicParameterOnly));
	}

	@Test
	public void testConfigurationValueWithOneDynamicAdditionalParameterOnlyWithFallback() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(UNAVAILABLE_APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(dynamicParameter1ForParentApplicationContext));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithOneDynamicParameterOnlyForParentApplicationContext));
	}

	@Test
	public void testConfigurationValueWithOneDefaultAndOneDynamicAdditionalParameterOnly() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(dynamicParameter1, nonDynamicParameter, dynamicParameter2));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithOneDynamicParameterAndANonDynamicParameter));
	}

	@Test
	public void testConfigurationValueWithOneDefaukltAndOneDynamicAdditionalParameterOnlyWithFallback() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(UNAVAILABLE_APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(dynamicParameter1ForParentApplicationContext,
								nonDynamicParameterForParentApplicationContext,
								dynamicParameter2ForParentApplicationContext));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue,
				hasItems(valueWithOneDynamicParameterAndANonDynamicParameterForParentApplicationContext));
	}

	@Test
	public void testConfigurationValueWithDefaultAndDynamicAdditionalParametersOnly() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(nonExistingNonDynamicParameter, dynamicParameter2));
		assertThat(configurationValue, hasSize(0));
	}

	@Test
	public void testConfigurationValueWithDefaukltAndDynamicAdditionalParametersOnlyWithFallback() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(UNAVAILABLE_APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(nonExistingNonDynamicParameter, dynamicParameter2ForParentApplicationContext));
		assertThat(configurationValue, hasSize(0));
	}

	@Test
	public void testConfigurationValueWithAllDynamicAdditionalParametersOnly() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName,
						ImmutableList.of(nonExistingDynamicParameter1, nonExistingDynamicParameter2));
		assertThat(configurationValue, hasSize(1));
		assertThat(configurationValue, hasItems(valueWithNoParameter));
	}

	@Test
	public void testConfigurationValueWithAllDynamicAdditionalParametersOnlyFallbackToNoParameter() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName2,
						ImmutableList.of(nonExistingDynamicParameter1, nonExistingDynamicParameter2));
		assertThat(configurationValue, hasSize(0));
	}

	@Test
	public void testConfigurationValueWithNoAdditionalParameters() {
		final List<ConfigurationValue> configurationValue = configurationValueRetrievalService
				.findConfigurationValuesWithDefaultFallback(APPLICATION_CONTEXT, NAMESPACE, configName2,
						ImmutableList.<AdditionalParameterKeyValue>of());
		assertThat(configurationValue, hasSize(0));
	}

}
