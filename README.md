# khameleon
A web management tool for application configuration requirements

## What is khameleon?
Enterprise applications always want to separate the application configurations from the running applications, so that runtime
configuration can easily be performed.
Further more, static configurations using properties file would require application redeployment whenever a change is desired.

Khameleon provides an easy configuration for both java-ee developer and application configuration managers/administrators.

For a java ee developer, a type safe configuration system, and a context based system is a plus.

## How to use khameleon - Quick Start Guide
 - using any java application server (jboss, glassfish, weblogic) install khameleon-web
 - define database connection at jdbc-resource: *java:jboss/datasources/khameleonV2*
 - make sure you have a jpa compliant implementation in your classpath: hibernate or eclipselink.

## Java EE Developers
Khameleon requires in the minimum, a CDI environment.

## Configuration Definition. 
- In the minimum, a configuration is an interface, annotated with ```@Config``` and ```@ConfigRoot```

```
@Config
@ConfigRoot
public interface MyApplicationConfiguration {

   String getMyConfigValue();
}
```

- Every configuration-value follows a java-bean property pattern (of course without the setter - spouse)

## How to add configuration to your project.
- Define a separate library-module/project.
- Add all your configurations interfaces in this library.
- in the project you want to use your configuration, add dependency `khameleon-spi` and define the following maven-plugin:

```xml

    <properties>
        <khameleon.configuration.registration.wsdlLocation>
            http://myconfig.mycompany-khameleon-server.com/KhameleonConfigurationRegistrationService?wsdl
        </khameleon.configuration.registration.wsdlLocation>
    </properties>

    <build>
        <plugins>
            <plugin>
              <groupId>com.anosym.khameleon</groupId>
              <artifactId>khameleon-maven-plugin</artifactId>
              <executions>
                  <execution>
                      <phase>package</phase>
                      <goals>
                          <goal>config-registration</goal>
                      </goals>
                  </execution>
              </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>com.anosym.khameleon</groupId>
            <artifactId>khameleon-spi</artifactId>
            <version>3.0.1-SNAPSHOT</version>
        </dependency>
    </dependencies>
```

## Runtime Request Configuration
- There are several ways to define runtime request access.

1. Declare within the ```@Config``` annotation, the *requestServiceWsdlLocation* and *registrationServiceWsdlLocation*

2. Declare within the ```@Config``` annotation, a system property *registrationServiceWsdlLocationProperty* default="com.anosym.khameleon.registrationServiceWsdlLocation" with the registration wsdl location 
   and *requestServiceWsdlLocationProperty* default="com.anosym.khameleon.requestServiceWsdlLocation"

3. Declare within the ```@Config``` annotation, *propertyFile* property file:

```properties
   com.anosym.khameleon.registrationServiceWsdlLocation=https://khameleonweb-running-instance-host/KhameleonConfigurationRegistrationService?wsdl
   com.anosym.khameleon.requestServiceWsdlLocation=https://khameleonweb-running-instance-host/KhameleonConfigurationRequestService?wsdl
```

## How to use configuration in your project.

```
@RequestScoped
public class MyService {

  @Inject
  private MyApplicationConfiguration myApplicationConfiguration;
}
```

## How to point jta-datasource to different value for different application-server, e.g. glassfish

- mvn clean install -Dcom.anosym.khameleon.jta-data-source=jdbc/khameleon-jta-datasource

## Enable automatic change listener registration
- update ```WsdlLocationConfigurationService``` with required configuration
- define and annotate a configurationchange listener as follows:

```java
@Stateless
@ConfigProject("PROJECT_ID")
@WebService(portName = "ConfigurationChangeListenerProvider",
            serviceName = "ConfigurationChangeListenerService",
            targetNamespace = "http://khameleon.anosym.com/config/service/changelisteners",
            endpointInterface = "com.anosym.khameleon.core.internal.service.changelistener.ConfigurationChangeListener")
public class MyProjectConfigurationChangeListener extends AbstractConfigurationChangeListener implements ConfigurationChangeListener {
}
```
- Note that you must register the projectname: ```PROJECT_ID``` within the configuration service.

### Additional Parameters ###
- This can be defined explicitly through configuration-service interface method formal parameters
- Or can be defined as dynamic additional-parameters through environment variables in the form of
```com.khameleon.additional-parameters.dynamic.xxxx``` where ```xxxx``` is the unique key of the parameter registered in the config service
- Or through the implementation of; ```AdditionalParameterProviderApplicationContextService``` in which you have the option to make the additional-parameters as dynamic or as normal parameters.

- The difference between a dynamic additional-parameter and normal parameter is that the dynamic-parameter is an optional parameter. If no configuration value matches, it will be filtered out and a new search made without this parameter. for n dynamic-parameters and non-empty m normal parameters, a combination of series-sum(k-1 to n) of (2-k)! combination  trials will be made including the final m normal parameters, if no configuration value is returned for the dynamic-parameters.

- The fallback strategy is based on dynamic-parameters followed by application-context. That is for n-combination of additional-parameter list, for a search of configuration value P, in application-context C, the algorithm will first look for any value in C, which satisfy any of the additional-parameters(considered in size preference), and then falls back an application-context C1, the parent of C, and does a search with all the additional-parameter combinations again.