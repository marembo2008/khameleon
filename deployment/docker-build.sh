PROJECT_VERSION=$1

echo "Copying Project Version $PROJECT_VERSION"

ls -las ../khameleon-web/target/khameleon-web-${PROJECT_VERSION}.war

cp ../khameleon-web/target/khameleon-web-${PROJECT_VERSION}.war .

docker build --build-arg PROJECT_VERSION=$PROJECT_VERSION  -t registry.tuleni.co.ke:5000/khameleon:$PROJECT_VERSION .

rm khameleon-web-${PROJECT_VERSION}.war
