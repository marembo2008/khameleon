package khameleon.maven.plugin;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import khameleon.maven.plugin.Qualifier;
import khameleon.maven.plugin.QualifierFilter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 15, 2015, 9:32:44 PM
 */
public class QualifierFilterTest {

  private final QualifierFilter qualifierFilter = new QualifierFilter();

  @Test
  public void testNoQualifiers() {
    final List<Qualifier> qualifiers = ImmutableList.of();
    boolean selected = qualifierFilter.select(qualifiers, TestNonQualifiedConfigService.class);
    assertThat(selected, is(true));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterlessConfigService.class);
    assertThat(selected, is(true));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterConfigService.class);
    assertThat(selected, is(true));

    selected = qualifierFilter.select(qualifiers, TestQualifiedMultiParameterConfigService.class);
    assertThat(selected, is(true));
  }

  @Test
  public void testParameterlessQualifier() {
    final Map<String, String> properties = ImmutableMap.of();
    final Qualifier qualifier = new Qualifier(ParameterlessQualifier.class.getName(), properties);
    final List<Qualifier> qualifiers = ImmutableList.of(qualifier);
    boolean selected = qualifierFilter.select(qualifiers, TestNonQualifiedConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterlessConfigService.class);
    assertThat(selected, is(true));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedMultiParameterConfigService.class);
    assertThat(selected, is(false));
  }

  @Test
  public void testParameterQualifier() {
    final Map<String, String> properties = ImmutableMap.of("value", "ahoi");
    final Qualifier qualifier = new Qualifier(ParameterQualifier.class.getName(), properties);
    final List<Qualifier> qualifiers = ImmutableList.of(qualifier);
    boolean selected = qualifierFilter.select(qualifiers, TestNonQualifiedConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterlessConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterConfigService.class);
    assertThat(selected, is(true));

    selected = qualifierFilter.select(qualifiers, TestQualifiedMultiParameterConfigService.class);
    assertThat(selected, is(false));
  }

  @Test
  public void testMultiParameterQualifier() {
    final Map<String, String> properties = ImmutableMap.of("value1", "ahoi", "value2", "344");
    final Qualifier qualifier = new Qualifier(MultiParameterQualifier.class.getName(), properties);
    final List<Qualifier> qualifiers = ImmutableList.of(qualifier);
    boolean selected = qualifierFilter.select(qualifiers, TestNonQualifiedConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterlessConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedMultiParameterConfigService.class);
    assertThat(selected, is(true));
  }

  @Test
  public void testMultiParameterQualifierOneInvalidParameter() {
    final Map<String, String> properties = ImmutableMap.of("value1", "ahoi", "value2", "34466");
    final Qualifier qualifier = new Qualifier(MultiParameterQualifier.class.getName(), properties);
    final List<Qualifier> qualifiers = ImmutableList.of(qualifier);
    boolean selected = qualifierFilter.select(qualifiers, TestNonQualifiedConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterlessConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedParameterConfigService.class);
    assertThat(selected, is(false));

    selected = qualifierFilter.select(qualifiers, TestQualifiedMultiParameterConfigService.class);
    assertThat(selected, is(false));
  }

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface ParameterlessQualifier {
  }

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface ParameterQualifier {

    String value();

  }

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface MultiParameterQualifier {

    String value1();

    int value2();

  }

  @ParameterlessQualifier
  public static class TestQualifiedParameterlessConfigService {

  }

  @ParameterQualifier("ahoi")
  public static class TestQualifiedParameterConfigService {

  }

  @MultiParameterQualifier(value1 = "ahoi", value2 = 344)
  public static class TestQualifiedMultiParameterConfigService {

  }

  public static class TestNonQualifiedConfigService {

  }

}
