package khameleon.maven.plugin;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import javax.annotation.Nonnull;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 15, 2015, 2:05:29 PM
 */
public final class Qualifier {

    @Nonnull
    private String annotation;

    private Map<String, String> properties;

    public Qualifier() {
    }

    @VisibleForTesting
    Qualifier(@Nonnull final String annotation, @Nonnull final Map<String, String> properties) {
        this.annotation = requireNonNull(annotation, "The annotation must not be null");
        this.properties = requireNonNull(properties, "The properties must not be null");
    }

    @Nonnull
    public String getAnnotation() {
        return requireNonNull(annotation, "The annotation must not be null");
    }

    @Nonnull
    public Map<String, String> getProperties() {
        return firstNonNull(properties, ImmutableMap.<String, String>of());
    }

}
