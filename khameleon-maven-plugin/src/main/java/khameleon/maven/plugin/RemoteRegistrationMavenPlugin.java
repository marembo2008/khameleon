package khameleon.maven.plugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.apache.maven.plugins.annotations.LifecyclePhase.PACKAGE;
import static org.apache.maven.plugins.annotations.ResolutionScope.COMPILE_PLUS_RUNTIME;

/**
 *
 * @author marembo
 */
@Slf4j
@Mojo(name = "config-registration", defaultPhase = PACKAGE, requiresDependencyResolution = COMPILE_PLUS_RUNTIME)
public class RemoteRegistrationMavenPlugin extends AbstractMojo {

    @Parameter(property = "project.runtimeClasspathElements", required = true, readonly = true)
    private List<String> classpath;

    @Parameter(readonly = true)
    private String additionalClasspath;

    @Parameter(required = true, defaultValue = "${configRegistrationTokenKey}")
    private String configRegistrationTokenKey;

    @Parameter(required = true, defaultValue = "${configRegistrationTokenSecret}")
    private String configRegistrationTokenSecret;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

    }

    private void initClasspath() throws Exception {
        final Set<URL> urls = new HashSet<>();
        final List<String> classpaths = MoreObjects.firstNonNull(classpath, ImmutableList.<String>of());
        for (String element : classpaths) {
            urls.add(new File(element).toURI().toURL());
        }

        if (!Strings.isNullOrEmpty(additionalClasspath)) {
            urls.add(new File(additionalClasspath).toURI().toURL());
        }

        if (!urls.isEmpty()) {
            final ClassLoader contextClassLoader = URLClassLoader.newInstance(
                    urls.toArray(new URL[0]),
                    Thread.currentThread().getContextClassLoader());
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

}
