package khameleon.maven.plugin;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.CharMatcher;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Nov 15, 2015, 9:22:14 PM
 */
public class QualifierFilter {

  public boolean select(@Nullable final List<Qualifier> qualifiers, @Nonnull final Class<?> configInterface) {
    requireNonNull(configInterface, "The configInterface must not be null");

    if (qualifiers == null || qualifiers.isEmpty()) {
      return true;
    }

    return qualifiers
            .stream()
            .filter((qualifier) -> configInterface.isAnnotationPresent(toAnnotation(qualifier)))
            .anyMatch((qualifier) -> matchAny(qualifier, configInterface));
  }

  private boolean matchAny(@Nonnull final Qualifier qualifier, @Nonnull final Class<?> configInterface) {
    if (qualifier.getProperties().isEmpty()) {
      return true;
    }

    final Class<? extends Annotation> annotClass = toAnnotation(qualifier);
    final Annotation annot = configInterface.getAnnotation(annotClass);
    final Map<String, String> properties = qualifier.getProperties();
    return ImmutableList
            .copyOf(annotClass.getDeclaredMethods())
            .stream()
            .map((method) -> map(annot, method))
            .allMatch((entry) -> Objects.equals(entry.getValue(), properties.get(entry.getKey())));
  }

  private Entry<String, String> map(@Nonnull final Annotation annot, @Nonnull final Method method) {
    final Object value;
    method.setAccessible(true);

    try {
      value = method.invoke(annot);
    } catch (final IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
      throw Throwables.propagate(ex);
    }

    return new AbstractMap.SimpleEntry<>(method.getName(), toString(value));
  }

  @Nonnull
  private Class<? extends Annotation> toAnnotation(@Nonnull final Qualifier qualifier) {
    try {
      return (Class<? extends Annotation>) Class.forName(qualifier.getAnnotation());
    } catch (final ClassNotFoundException ex) {
      throw Throwables.propagate(ex);
    }
  }

  @Nonnull
  private String toString(@Nonnull final Object obj) {
    final String str;
    if (obj.getClass().isArray()) {
      str = Arrays.toString(((Object[]) obj));
    } else {
      str = String.valueOf(obj);
    }

    return CharMatcher.whitespace().removeFrom(str);
  }

}
