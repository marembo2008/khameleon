package khameleon.maven.plugin;

import com.google.common.collect.Sets;

import khameleon.core.annotations.ConfigUpload;
import khameleon.core.spi.ConfigUploadProcessor;
import khameleon.core.spi.Option;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.atteo.classindex.ClassIndex;

import static org.apache.maven.plugins.annotations.LifecyclePhase.PACKAGE;
import static org.apache.maven.plugins.annotations.ResolutionScope.COMPILE_PLUS_RUNTIME;

/**
 *
 * @author marembo
 */
@Mojo(name = "config-upload", defaultPhase = PACKAGE, requiresDependencyResolution = COMPILE_PLUS_RUNTIME)
public class UploadMavenPlugin extends AbstractMojo {

    private static final Logger LOG = Logger.getLogger(UploadMavenPlugin.class.getName());

    @Parameter(property = "project.runtimeClasspathElements", required = true, readonly = true)
    private List<String> classpath;
    @Parameter(readonly = true)
    private List<Option> options;
    private final Set<ConfigUploadProcessor> configUploadProcessors = Sets.newHashSet();

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        init();
        for (ConfigUploadProcessor processor : configUploadProcessors) {
            try {
                processor.doProcess(options);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed processor: " + processor.getClass().getName(), ex);
            }
        }
    }

    void init() {
        Set<URL> urls = new HashSet<>();
        for (String element : classpath) {
            try {
                urls.add(new File(element).toURI().toURL());
            } catch (MalformedURLException ex) {
                Logger.getLogger(UploadMavenPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ClassLoader contextClassLoader = URLClassLoader.newInstance(
                urls.toArray(new URL[0]),
                Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(contextClassLoader);
        //load all the classes marked with @Config
        for (Class<?> cls : ClassIndex.getAnnotated(ConfigUpload.class)) {
            try {
                configUploadProcessors.add((ConfigUploadProcessor) cls.newInstance());
            } catch (InstantiationException | IllegalAccessException ex) {
                throw new IllegalStateException("Expected ConfigUploadProcessor with a no arg constructor", ex);
            }
        }
    }
}
