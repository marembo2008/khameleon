package khameleon.maven.plugin;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.lang.String.format;
import static java.util.logging.Logger.getLogger;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static khameleon.core.internal.ConfigConstants.METHOD_START_SYNTAX;
import static org.apache.maven.plugins.annotations.ResolutionScope.COMPILE_PLUS_RUNTIME;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.atteo.classindex.ClassIndex;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import anosym.common.Language;
import anosym.common.Pair;
import khameleon.core.ApplicationMode;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Also processed all those configurations, which have not been defined in this
 * package.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 3, 2017, 11:13:50 AM
 */
@Mojo(name = "config-local-registration", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = COMPILE_PLUS_RUNTIME)
public class LocalRegistrationMavenPlugin extends AbstractMojo {

	private static final Logger LOG = getLogger(LocalRegistrationMavenPlugin.class.getName());

	private static final Joiner HYPHEN_JOINER = Joiner.on("-").skipNulls();

	private static final String DEFAULT_APPLICATION_CONTEXT_PATH = "application.conf";

	@org.apache.maven.plugins.annotations.Parameter(property = "project.runtimeClasspathElements", required = true, readonly = true)
	private List<String> classpath;

	@org.apache.maven.plugins.annotations.Parameter(readonly = true)
	private String additionalClasspath;

	@org.apache.maven.plugins.annotations.Parameter(readonly = true, defaultValue = "${basedir}/src/main/resources/configurations")
	private String configurationsBasePath;

	private final Map<String, List<String>> configuredKeys = new HashMap<>();

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			init();
			initConfiguredKeys();
			process().forEach((contextPath, configKeys) -> {

				final File path = new File(configurationsBasePath, contextPath);
				if (!path.getParentFile().exists()) {
					path.getParentFile().mkdirs();
				}
				try (final FileWriter writer = new FileWriter(path, true)) {
					writer.write(configKeys);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
		} catch (Exception e) {
			throw new MojoExecutionException(getClass().getName(), e);
		}
	}

	private void init() throws Exception {
		final Set<URL> urls = new HashSet<>();
		final List<String> classpaths = firstNonNull(this.classpath, ImmutableList.<String>of());
		for (String element : classpaths) {
			urls.add(new File(element).toURI().toURL());
		}

		if (!Strings.isNullOrEmpty(additionalClasspath)) {
			urls.add(new File(additionalClasspath).toURI().toURL());
		}

		if (!urls.isEmpty()) {
			ClassLoader contextClassLoader = URLClassLoader.newInstance(urls.toArray(new URL[0]),
					Thread.currentThread().getContextClassLoader());
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	private void initConfiguredKeys() {
		final File configurationPath = new File(configurationsBasePath);
		final File[] configuredKeysPath = configurationPath.listFiles();
		if (configuredKeysPath == null) {
			return;
		}

		Arrays.stream(configuredKeysPath).forEach((configFile) -> {
			try {
				initConfiguredKeys(configFile);
			} catch (final IOException ex) {
				throw new RuntimeException(ex);
			}
		});
	}

	private void initConfiguredKeys(final File configurationPath) throws IOException {
		if (!configurationPath.exists()) {
			return;
		}

		final String cxtKey = configurationPath.getName();
		final List<String> cxtConfiguredKeys = configuredKeys.getOrDefault(cxtKey, new ArrayList<>());
		if (cxtConfiguredKeys.isEmpty()) {
			configuredKeys.put(cxtKey, cxtConfiguredKeys);
		}

		try (final BufferedReader reader = new BufferedReader(new FileReader(configurationPath))) {
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.trim().isEmpty() || line.trim().startsWith("#")) {
					continue;
				}

				final String[] configParts = line.split("=");
				cxtConfiguredKeys.add(configParts[0]);
			}
		}
	}

	@Nonnull
	private Map<String, String> process() {
		final List<Class<?>> configInterfaces = ImmutableList.copyOf(ClassIndex.getAnnotated(ConfigRoot.class));
		final Map<AppContextKey, List<AppContextConfigInterface>> appContextConfigInterfaces = configInterfaces.stream()
				.map(this::buildAppContextConfigInterface)
				.collect(groupingBy(AppContextConfigInterface::getAppContextKey));
		return appContextConfigInterfaces.entrySet().stream().map((entry) -> {
			final List<String> applicationContextPaths = createApplicationContextPaths(entry.getKey());
			final List<Class<?>> applicationContextConfigInterfaces = entry.getValue().stream()
					.map(AppContextConfigInterface::getConfigInterface).collect(toList());
			return applicationContextPaths.stream().map((applicationContextPath) -> {
				final List<String> applicationContextConfigKeys = applicationContextConfigInterfaces.stream()
						.map((cInterface) -> createConfigKeys(applicationContextPath, cInterface))
						.flatMap(Function.identity()).collect(toList());
				return Pair.of(applicationContextPath, applicationContextConfigKeys);
			});
		}).flatMap(Function.identity()).collect(toMap(Pair::getFirst, Pair::getSecond, merger())).entrySet().stream()
				.map((cxtConfigs) -> Pair.of(cxtConfigs.getKey(),
						cxtConfigs.getValue().stream().collect(joining("\n", "\n", "\n"))))
				.filter((cxtConfig) -> !cxtConfig.getSecond().trim().isEmpty())
				.collect(toMap(Pair::getFirst, Pair::getSecond));
	}

	@Nonnull
	private BinaryOperator<List<String>> merger() {
		return (first, second) -> ImmutableList.<String>builder().addAll(first).addAll(second).build();
	}

	@Nonnull
	private Stream<String> createConfigKeys(@Nonnull final String appContextKeyPath,
			@Nonnull final Class<?> configInterface) {
		final List<String> cxtConfiguredKeys = this.configuredKeys.getOrDefault(appContextKeyPath, ImmutableList.of());
		final String configNamespace = configInterface.getCanonicalName();
		return getConfigMethods(configInterface).stream()
				.map((configMethod) -> new ConfigKey(createConfigKey(configMethod, configNamespace), configMethod))
				.filter((configKey) -> !cxtConfiguredKeys.contains(configKey.key)).distinct()
				.map((configKey) -> createConfigKeyWithDefaultValue(configKey.method, configKey.key));
	}

	@Nonnull
	private List<Method> getConfigMethods(@Nonnull final Class<?> configInterface) {
		final List<Method> configMethods = Arrays.asList(configInterface.getDeclaredMethods());
		final Class<?> superClass = Arrays.stream(configInterface.getInterfaces()).findAny().orElse(null);
		if (superClass == null) {
			return configMethods;
		}

		return ImmutableList.<Method>builder().addAll(configMethods).addAll(getConfigMethods(superClass)).build();
	}

	@Nonnull
	private String createConfigKeyWithDefaultValue(@Nonnull final Method configMethod,
			@Nonnull final String configKey) {
		final Info commentInfo = configMethod.getAnnotation(Info.class);
		final String[] comments = commentInfo != null ? commentInfo.value().split("\n") : new String[0];
		final String commentLines = Arrays.stream(comments).map((line) -> format("# %s", line)).collect(joining("\n"));
		final Default defaultValue = configMethod.getAnnotation(Default.class);
		final String defValue = defaultValue != null ? defaultValue.value() : "";
		return format("%s\n%s=%s\n", commentLines, configKey, defValue);
	}

	@Nonnull
	private String createConfigKey(@Nonnull final Method configMethod, @Nonnull final String configNamespace) {
		final String methodName = getConfigName(configMethod.getName());
		final String parameters = Arrays.stream(configMethod.getParameters()).map(this::createParameter)
				.filter(Objects::nonNull).sorted().collect(joining(".", "{", "}"));
		return format("%s.%s.%s", configNamespace, methodName, parameters);
	}

	@Nullable
	private String createParameter(@Nonnull final Parameter parameter) {
		final ConfigParam cp = parameter.getAnnotation(ConfigParam.class);
		if (cp == null) {
			return null;
		}

		return format("%s#", cp.name());
	}

	@Nonnull
	private String getConfigName(@Nonnull final String configMethod) {
		final String fieldName = METHOD_START_SYNTAX.matcher(configMethod).replaceAll("");
		return fieldName.length() > 1 ? (fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1))
				: fieldName.toLowerCase();
	}

	@Nonnull
	private AppContextConfigInterface buildAppContextConfigInterface(@Nonnull final Class<?> configInterface) {
		final AppContext appContext = configInterface.getAnnotation(AppContext.class);
		final AppContextKey appContextKey = new AppContextKey(appContext);
		return new AppContextConfigInterface(appContextKey, configInterface);
	}

	@Nonnull
	private List<String> createApplicationContextPaths(@Nonnull final AppContextKey appContextKey) {
		final AppContext appContext = appContextKey.getAppContext();
		if (appContext == null) {
			return ImmutableList.of(DEFAULT_APPLICATION_CONTEXT_PATH);
		}

		// For now, just consider application-mode and language.
		final List<ApplicationMode> modes = ImmutableList.copyOf(appContext.applicationModes());
		if (modes.isEmpty()) {
			return ImmutableList.of(DEFAULT_APPLICATION_CONTEXT_PATH);
		}

		final List<Language> languageCodes = ImmutableList.copyOf(appContext.languages()).stream()
				.filter(Objects::nonNull).collect(toList());
		if (languageCodes.isEmpty()) {
			languageCodes.add(null);
		}

		return modes.stream().map((mode) -> {
			return languageCodes.stream().map((lang) -> HYPHEN_JOINER.join(mode.name().toLowerCase(), lang));
		}).flatMap(Function.identity()).filter(Objects::nonNull).map((cxt) -> format("application-%s.conf", cxt))
				.distinct().collect(toList());
	}

	@Data
	@EqualsAndHashCode(of = "key")
	private static final class ConfigKey {

		@Nonnull
		private final String key;

		@Nonnull
		private final Method method;

	}

	@Data
	private static final class AppContextKey {

		@Nullable
		private final AppContext appContext;

	}

	@Data
	private static final class AppContextConfigInterface {

		@Nonnull
		private final AppContextKey appContextKey;

		@Nonnull
		private final Class<?> configInterface;

	}

}
